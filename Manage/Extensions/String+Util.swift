//
//  String+Util.swift
//  Manage
//
//  Created by Rong Li on 12/12/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation

extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
    
    func toFloat() -> Float? {
        return NumberFormatter().number(from: self)?.floatValue
    }
    
    func parse<D>(to type: D.Type) -> D? where D: Decodable {
        
        let data: Data = self.data(using: .utf8)!
        
        let decoder = JSONDecoder()
        
        do {
            let _object = try decoder.decode(type, from: data)
            return _object
            
        } catch {
            return nil
        }
    }
    
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func formattedToUTC(dateString: String, fromFormat: String, toFormat: String) -> String? {
//        let inputFormatter = DateFormatter()
//        inputFormatter.dateFormat = fromFormat
//
//        if let date = inputFormatter.date(from: dateString) {
//            let outputFormatter = DateFormatter()
//            outputFormatter.timeZone = TimeZone(abbreviation: "UTC")
//            outputFormatter.dateFormat = toFormat
//            return outputFormatter.string(from: date)
//        }
        
        return formatedDate(dateString: dateString, fromFormat: fromFormat, toFormat: toFormat, timeZone: "UTC")
    }
    
    func convertToDate(dateString: String?, fromFormatters: [String], timeZone: String?) -> Date?{
        var date: Date?
        for fromFormatter in fromFormatters {
            date = convertToDate(dateString: dateString, fromFormat: fromFormatter, timeZone: timeZone)
            if(date != nil){
                return date
            }
        }
        return date
    }
    
    func convertToDate(dateString: String?, fromFormat: String, timeZone: String?) -> Date?{
        if(dateString == nil){
            return nil
        }
        let inputFormatter = DateFormatter()
        inputFormatter.timeZone = timeZone == nil ? TimeZone.ReferenceType.default : TimeZone(abbreviation: timeZone!)
        inputFormatter.dateFormat = fromFormat
        return inputFormatter.date(from: dateString!)
    }
    
    func formatedDate(dateString: String?, fromFormat: String, toFormat: String, timeZone: String?) -> String?{
//        let inputFormatter = DateFormatter()
//        inputFormatter.dateFormat = fromFormat
//
//        if let date = inputFormatter.date(from: dateString) {
//            let outputFormatter = DateFormatter()
//            outputFormatter.timeZone = timeZone == nil ? TimeZone.ReferenceType.default : TimeZone(abbreviation: timeZone!)
//            outputFormatter.dateFormat = toFormat
//            return outputFormatter.string(from: date)
//        }
    
        if(dateString == nil){
            return nil
        }
        let date = convertToDate(dateString: dateString, fromFormat: fromFormat, timeZone: timeZone)
        let formatter = DateFormatter()
        formatter.dateFormat = toFormat
    
        return formatter.string(from: date!)
    }
}
