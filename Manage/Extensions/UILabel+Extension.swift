//
//  UILabel+Padding.swift
//  Manage
//
//  Created by Rong Li on 11/30/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    private struct LabelPadding {
        static var padding = UIEdgeInsets()
    }
    
    public var padding: UIEdgeInsets? {
        get {
            return objc_getAssociatedObject(self, &LabelPadding.padding) as? UIEdgeInsets
        }
        set {
            if let newValue = newValue {
//                objc_setAssociatedObject(self, &LabelPadding.padding, newValue as UIEdgeInsets!, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
                objc_setAssociatedObject(self, &LabelPadding.padding, newValue as UIEdgeInsets, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    override open func draw(_ rect: CGRect) {
        if let insets = padding {
//            let container = CGRect(x: 0, y: 0, width: 100, height: 100)
//            let margin = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            let margin = UIEdgeInsets(top: (padding?.top)!, left: (padding?.left)!, bottom: (padding?.bottom)!,  right: (padding?.right)!)
            let contentRect = UIEdgeInsetsInsetRect(rect, margin)
            self.drawText(in: contentRect)
            
//            //For swift 4.2
//            let margin = UIEdgeInsets(top: (padding?.top)!, left: (padding?.left)!, bottom: (padding?.bottom)!,  right: (padding?.right)!)
//            let contentRect = rect.inset(by: margin)
//            self.drawText(in: contentRect)
        } else {
            self.drawText(in: rect)
        }
    }
    
    
//    override open var intrinsicContentSize: CGSize {
//        get {
//            var contentSize = super.intrinsicContentSize
//            contentSize.height += (padding?.top)! + (padding?.bottom)!
//            contentSize.width += (padding?.left)! + (padding?.right)!
//            return contentSize
//        }
//    }
    
    func addImageWith(name: String, front: Bool) {
        
        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: name)
        let attachmentString = NSAttributedString(attachment: attachment)
        
        guard let txt = self.text else {
            return
        }
        
        if front {
            let strLabelText = NSAttributedString(string: txt)
            let mutableAttachmentString = NSMutableAttributedString(attributedString: attachmentString)
            mutableAttachmentString.append(strLabelText)
            self.attributedText = mutableAttachmentString
        }else {
            let strLabelText = NSMutableAttributedString(string: txt)
            strLabelText.append(attachmentString)
            self.attributedText = strLabelText
        }
    }
    
    func removeImage() {
        let text = self.text
        self.attributedText = nil
        self.text = text
    }
}
