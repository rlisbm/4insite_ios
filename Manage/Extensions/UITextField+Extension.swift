//
//  UITextField_Padding.swift
//  Manage
//
//  Created by Rong Li on 10/26/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import UIKit

public extension UITextField {
    
    class func connectTextField(textFields: [UITextField]){
        guard let last = textFields.last else {
            return
        }
        for i in 0 ..< textFields.count - 1 {
            textFields[i].returnKeyType = .next
            textFields[i].addTarget(textFields[i+1], action: #selector(UIResponder.becomeFirstResponder), for: .editingDidEndOnExit)
        }
        last.returnKeyType = .done
        last.addTarget(last, action: #selector(UIResponder.resignFirstResponder), for: .editingDidEndOnExit)
        
    }

    func setPadding(left: CGFloat? = nil, right: CGFloat? = nil){
        if let left = left {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: left, height: self.frame.size.height))
            self.leftView = paddingView
            self.leftViewMode = .always
        }

        if let right = right {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: right, height: self.frame.size.height))
            self.rightView = paddingView
            self.rightViewMode = .always
        }
    }
}

