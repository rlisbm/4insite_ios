//
//  Array+Extenstion.swift
//  Manage
//
//  Created by Rong Li on 1/21/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

extension Array where Array.Element: AnyObject {
    
    func index(ofElement element: Element) -> Int? {
        for (currentIndex, currentElement) in self.enumerated() {
            if currentElement === element {
                return currentIndex
            }
        }
        return nil
    }
}

