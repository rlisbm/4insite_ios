//
//  Date+Util.swift
//  Manage
//
//  Created by Rong Li on 12/12/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation

extension Date {
    
    var startOfDay: Date{
        let cal = Calendar(identifier: .gregorian)
        return cal.startOfDay(for: self)
    }
    
    var midNightOfNextDay: Date{
        let cal = Calendar(identifier: .gregorian)
        return cal.date(byAdding: .hour, value: 16 + 24, to: self.startOfDay)!
        
    }
    
    var back180DayDate: Date{
        let cal = Calendar(identifier: .gregorian)
        return cal.date(byAdding: .hour, value: -180 * 24, to: self.startOfDay)!
    }
    
    var formattedDateMedium: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter.string(from: self)
    }
    
    var formattedDateShort: String {
        get {
            let formatter = DateFormatter()
            formatter.dateStyle = .short
            return formatter.string(from: self)
        }
    }
    
    func addSeconds(seconds: Int64) -> Date {
        return Calendar.current.date(byAdding: .second, value: Int(seconds), to: self)!
    }
    
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
    
    func toSeconds() -> Int64! {
        return Int64(self.timeIntervalSince1970)
    }
    
    func formateDateUTC(date: Date, withFormat format: String) -> String{
//        let formatter = DateFormatter()
//        formatter.timeZone = TimeZone(abbreviation: "UTC")
//        formatter.dateFormat = format
//        return formatter.string(from: date)
        
        return formateDate(date: date, format: format, timeZone: "UTC")
    }
    
    func formateDate(date: Date, format: String, timeZone: String?) -> String{
        let formatter = DateFormatter()
        formatter.timeZone = timeZone == nil ? TimeZone.ReferenceType.default : TimeZone(abbreviation: timeZone!)
        formatter.dateFormat = format
        return formatter.string(from: date)
    }
    
    func localString(dateStyle: DateFormatter.Style = .medium,
                     timeStyle: DateFormatter.Style = .medium) -> String {
        return DateFormatter.localizedString(
            from: self,
            dateStyle: dateStyle,
            timeStyle: timeStyle)
    }
    
    

    
//    func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
//
//        let inputFormatter = NSDateFormatter()
//        inputFormatter.dateFormat = "dd/MM/yyyy"
//
//        if let date = inputFormatter.dateFromString(dateString) {
//
//            let outputFormatter = NSDateFormatter()
//            outputFormatter.dateFormat = format
//
//            return outputFormatter.stringFromDate(date)
//        }
//
//        return nil
//    }
}
