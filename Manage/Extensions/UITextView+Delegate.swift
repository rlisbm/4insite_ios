//
//  UITextView+Delegate.swift
//  Manage
//
//  Created by Rong Li on 12/14/18.
//  Copyright © 2018 Insite. All rights reserved.
//
//
//import Foundation
//import UIKit
//
//extension UIViewController: UITextViewDelegate {
//    
//    func textViewDidBeginEditing(_ textView: UITextView) {
//        if textView.textColor == UIColor.init(hexString: Constants.textDarkWhite) {
//            textView.text = nil
//            textView.textColor = UIColor.init(hexString: Constants.textWhite)
//        }
//    }
//    
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        if text == "\n" {
//            textView.resignFirstResponder()
//            return false
//        }
//        return true
//    }
//    
//}
