//
//  UIView+Extension.swift
//  Manage
//
//  Created by Rong Li on 4/5/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func twoRoundedCorner(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
