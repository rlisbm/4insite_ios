//
//  UIButton+UnderLine.swift
//  Manage
//
//  Created by Rong Li on 11/30/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import UIKit

public extension UIButton {
    func underLine() {
        guard let text = self.titleLabel?.text else { return }
        
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 0, length: text.count))
        
//        //for swift 4.2
//        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
        
        self.setAttributedTitle(attributedString, for: .normal)
        
    }
    
    func toggleMultiSelectionBtn(label: UILabel){
        if(self.tag == 0){
            self.tag = 1
            label.text = "UnSelect All"
            self.setImage(UIImage(named: "Check all - Remove.png"), for: .normal)
        }else{
            self.tag = 0
            label.text = "Select All"
            self.setImage(UIImage(named: "Check - Select All.png"), for: .normal)
        }
    }
}
