//
//  Observer.swift
//  Manage
//
//  Created by Rong Li on 10/25/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation

public struct Observer<T> {
    let id: UUID
    let handler: T
    
    public init(_ id: UUID = UUID(), handler: T) {
        self.id = id
        self.handler = handler
    }
}

extension Observer: Equatable {
    public static func ==(lhs: Observer, rhs: Observer) -> Bool {
        return lhs.id == rhs.id
    }
}
