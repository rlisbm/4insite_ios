//
//  LocationManager.swift
//  Manage
//
//  Created by Rong Li on 10/25/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import GoogleMaps
import CoreLocation

public class LocationManager: NSObject, CLLocationManagerDelegate {

    /// Internal Core Location manager
    fileprivate lazy var locationManager: CLLocationManager = {
        $0.delegate = self
        if let value = self.desiredAccuracy { $0.desiredAccuracy = value }
        if let value = self.distanceFilter { $0.distanceFilter = value }
        if let value = self.activityType { $0.activityType = value }
        return $0
    }(CLLocationManager())
    
    /// Default location manager options
    fileprivate let desiredAccuracy: CLLocationAccuracy?
    fileprivate let distanceFilter: Double?
    fileprivate let activityType: CLActivityType?
    
    public init(
        desiredAccuracy: CLLocationAccuracy? = nil,
        distanceFilter: Double? = nil,
        activityType: CLActivityType? = nil) {
        
        // Assign values to location manager options
        self.desiredAccuracy = desiredAccuracy
        self.distanceFilter = distanceFilter
        self.activityType = activityType
        
        super.init()
    }
    
    /// Subscribes to receive new location data when available.
    public var didUpdateLocations = SynchronizedArray<LocationObserver>()
    fileprivate var didUpdateLocationsSingle = SynchronizedArray<LocationHandler>()
    
    /// Subscribes to receive new authorization data when available.
    public var didChangeAuthorization = SynchronizedArray<AuthorizationObserver>()
    fileprivate var didChangeAuthorizationSingle = SynchronizedArray<AuthorizationHandler>()
    
    deinit {
        // Empty task queues of references
        didUpdateLocations.removeAll()
        didUpdateLocationsSingle.removeAll()
        
        didChangeAuthorization.removeAll()
        didChangeAuthorizationSingle.removeAll()
    }
    
}

// MARK: - Nested types
public extension LocationManager {
    
    /// Location handler queue type.
    typealias LocationObserver = Observer<LocationHandler>
    typealias LocationHandler = (CLLocation) -> Void
    
    // Authorization handler queue type.
    typealias AuthorizationObserver = Observer<AuthorizationHandler>
    typealias AuthorizationHandler = (Bool) -> Void
    
    /// - whenInUse: While the app is in the foreground.
    /// - always: Whenever the app is running.
    enum AuthorizationType {
        case whenInUse, always
    }
}

// CLLocationManagerDelegate functions
public extension LocationManager {
    func requestAuthorization() {
        locationManager.requestWhenInUseAuthorization()
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            if(status == .denied){
            }else if(status == .restricted){
            }else if(status == .notDetermined){
                locationManager.requestWhenInUseAuthorization()
            }
            return
        }
        self.startUpdating()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        didUpdateLocations.forEach { $0.handler(location) }
        didUpdateLocationsSingle.removeAll { $0.forEach { $0(location) } }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
       print(" *** Error \(error)")
    }
}

// MARK: - CLLocationManager wrappers
public extension LocationManager {
    /// Determines if location services is enabled and authorized for always or when in use.
    var isAuthorized: Bool {
        return CLLocationManager.locationServicesEnabled()
            && [.authorizedAlways, .authorizedWhenInUse].contains(
                CLLocationManager.authorizationStatus())
    }
    
    /// Determines if location services is enabled and authorized for the specified authorization type.
    func isAuthorized(for type: AuthorizationType) -> Bool {
        guard CLLocationManager.locationServicesEnabled() else { return false }
        return (type == .whenInUse && CLLocationManager.authorizationStatus() == .authorizedWhenInUse)
            || (type == .always && CLLocationManager.authorizationStatus() == .authorizedAlways)
    }
    
    /// Starts the generation of updates that report the user’s current location.
    func startUpdating(enableBackground: Bool = false) {
//        print("*** *** startUpdating")
        locationManager.allowsBackgroundLocationUpdates = enableBackground
        locationManager.startUpdatingLocation()
    }
    
    /// Stops the generation of location updates.
    func stopUpdating() {
//        print("*** *** stopUpdating")
        locationManager.allowsBackgroundLocationUpdates = false
        locationManager.stopUpdatingLocation()
    }
}
