//
//  RealmDBService.swift
//  Manage
//
//  Created by Rong Li on 11/7/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import RealmSwift

class RealmDB {
    
    private init(){
    }
    
    static let shared = RealmDB()
    
    var realm = try! Realm()
    
    // delete table
    func deleteDatabase() {
        do{
            try realm.write {
                realm.deleteAll()
            }
        }catch{
            post(error)
        }
    }
    
    //get one object according to primaryKey
    func getObject<T: Object>(type: T.Type, primaryKey: String) -> T? {
        return realm.object(ofType: type, forPrimaryKey: primaryKey)
    }
    
    //get objects for specific Type (className)
    func getObjects(type: Object.Type) -> Results<Object>? {
        return realm.objects(type)
    }
    
    //insert one object
    func insertObject<T: Object>(_ object: T){
        do{
            try realm.write {
                realm.add(object, update: false)
            }
        }catch{
            post(error)
        }
    }
    
    
    //insert objects
    func insertObjects<T: Object>(_ objects: T){
        do{
            try realm.write {
                realm.add(objects, update: false)
            }
        }catch{
            post(error)
        }
    }
    
    //update one object
    func updateObject<T: Object>(_ object: T){
        do{
            try realm.write {
                realm.add(object, update: true)
            }
        }catch{
            post(error)
        }
    }
    
    //update objects
    func updateObjects<T: Object>(_ objects: T){
        do{
            try realm.write {
                realm.add(objects, update: true)
            }
        }catch{
            post(error)
        }
    }
    
//    func updateObject<T: Object>(_ object: T, with dictionary: [String: Any?]) {
//        do{
//            try realm.write {
//                for (key, value) in dictionary {
//                    object.setValue(value, forKey: key)
//                }
//            }
//        }catch{
//            post(error)
//        }
//    }
    
    //delete one object
    func deleteObject<T: Object>(_ object: T) {
        do{
            try realm.write {
                realm.delete(object)
            }
        }catch{
            post(error)
        }
        
    }
    
    //delete sequence of objects
    func deleteObjectSequence<T: Object>(_ objects: T) {
        do{
            try realm.write {
                realm.delete(objects)
            }
        }catch{
            post(error)
        }
    }
    
    //delete list of objects
    func deleteObjectList<T: Object>(_ objects: T) {
        do{
            try realm.write {
                realm.delete(objects)
            }
        }catch{
            post(error)
        }
    }
    
    //delete Results of objects
    func deleteObjectResults<T: Object>(_ objects: T) {
        do{
            try realm.write {
                realm.delete(objects)
            }
        }catch{
            post(error)
        }
    }
    
    func post(_ error: Error){
        NotificationCenter.default.post(name: NSNotification.Name("RealmError"), object: error)
    }
    
    func observeRealmError(in vc: UIViewController, completion: @escaping (Error?) -> Void){
        NotificationCenter.default.addObserver(forName: NSNotification.Name("RealmError"),
                                               object: nil,
                                               queue: nil) {(notification) in
                                                completion(notification.object as? Error)
        }
    }
    
    func stopObservingError(in vc: UIViewController){
        NotificationCenter.default.removeObserver(vc, name: Notification.Name("RealmError"), object: nil)
    }
    
}
