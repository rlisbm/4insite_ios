//
//  RealmObjects.swift
//  Manage
//
//  Created by Rong Li on 11/8/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

@objcMembers class ClientsObject: Object {
    
    dynamic var clientsPrimaryKey: String?
    dynamic var clients: ClientMappable?
    
    override static func primaryKey() -> String? {
        return "clientsPrimaryKey"
    }
}

@objcMembers class SitesObject: Object {
    
    dynamic var sitesPrimaryKey: String?
    var sites = List<SiteMappable>()
    
    override static func primaryKey() -> String? {
        return "sitesPrimaryKey"
    }
}

@objcMembers class ProgramsObject: Object {
    
    dynamic var programsPrimaryKey: String?
    var programs = List<KeyValueMappable>()
    
    override static func primaryKey() -> String? {
        return "programsPrimaryKey"
    }
}

@objcMembers class FloorsObject: Object {
    
    dynamic var floorsPrimaryKey: String?
    var floors = List<Floor>()
    
    override static func primaryKey() -> String? {
        return "floorsPrimaryKey"
    }
}

@objcMembers class DepartmentsObject: Object {
    
    dynamic var departmentsPrimaryKey: String?
    var departments = List<Department>()
    
    override static func primaryKey() -> String? {
        return "departmentsPrimaryKey"
    }
}

@objcMembers class SiteReportSettingObject: Object {
    
    dynamic var siteReportSettingObjectPrimaryKey: String?
    dynamic var data: Data?
    
    override static func primaryKey() -> String? {
        return "siteReportSettingObjectPrimaryKey"
    }
}

@objcMembers class CustomerRepresentativeObject: Object {
    
    dynamic var customerRepresentativeObjectPrimaryKey: String?
    dynamic var data: Data?
    
    override static func primaryKey() -> String? {
        return "customerRepresentativeObjectPrimaryKey"
    }
}

@objcMembers class JSONData: Object {
    
    dynamic var jsonDataPrimaryKey: String?
    dynamic var date: Date?
    dynamic var data: Data?
    
    override static func primaryKey() -> String? {
        return "jsonDataPrimaryKey"
    }
}

@objcMembers class SubmitItemObject: Object {
    
    dynamic var submitItemObjectPrimaryKey: String?
    dynamic var urlString: String?
    dynamic var headers: String?
    dynamic var userName: String?
    dynamic var password: String?
    dynamic var params: String?
    var responseCode: Int?
    var resendingState: Int?
    
    
    override static func primaryKey() -> String? {
        return "submitItemObjectPrimaryKey"
    }
}

@objcMembers class RequestObject: Object {
    var requestObjectPrimaryKey: String?
    var data: Data?
//    var headers: [String: String]?
    var headers: Data?
    var url: String?
    var method: String?
    var userName: String?
    var password: String?
    var responseCode: Int?
    var resendingState: Int?

    override static func primaryKey() -> String? {
        return "requestObjectPrimaryKey"
    }
}








