//
//  ProfessionalismViewController.swift
//  Manage
//
//  Created by Rong Li on 11/29/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell{
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var successBtn: UIButton!
    @IBOutlet weak var failBtn: UIButton!

    var failBtnClickAction: ((UITableViewCell) -> Void)?
    var successBtnClickAction: ((UITableViewCell) -> Void)?
    
    @IBAction func successBtnClicked(_ sender: UIButton) {
        
        if(sender.tag == 1){
            sender.tag = 0
            sender.setBackgroundImage(UIImage(named: "prof-audit-Positive-ON.png"), for: .normal)
            failBtn.tag = 0
            failBtn.setBackgroundImage(UIImage(named: "prof-audit-Positive-OFF.png"), for: .normal)
        }
        
        successBtnClickAction?(self)
    }
    
    @IBAction func failBtnClicked(_ sender: UIButton) {
        if(sender.tag == 0){
            sender.tag = 1
            sender.setBackgroundImage(UIImage(named: "prof-audit-NEGATIVE-ON.png"), for: .normal)
            successBtn.tag = 1
            successBtn.setBackgroundImage(UIImage(named: "prof-audit-NEGATIVE-OFF.png"), for: .normal)
        }
        
        failBtnClickAction?(self)
    }
}


class HistoryTableViewCell: UITableViewCell{
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
}

class ProfessionalismViewController: KeyboardBaseViewController, UITableViewDataSource, UITableViewDelegate {
    var employeeAuditItem: PressionalismAudit = PressionalismAudit()
    
    var professionalismItems = [ProfessionalismItem]()
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var assignToView: AssignToOneView!
    @IBOutlet weak var itemTableView: UITableView!
    @IBOutlet weak var historyTableView: UITableView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var yesBtn: UIButton!
    @IBOutlet weak var noBtn: UIButton!
    
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomSpace: NSLayoutConstraint!
    @IBOutlet weak var scrollContentHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //prepare ProfessionalismItem which is hardcoded as required
        for i in 0...3{
            professionalismItems.append(ProfessionalismItem(kpiSubOptionId: kpiSubOptionIdAry[i], title: titleAry[i], description: descriptionAry[i], isPass: isPassAry[i]))
        }
      
        if(refreshTokenNotificationName != nil){
            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
        }else{
            if(AssociateHelper.get()?.employeeId == nil){
                fetchAssociateInfoNotificationName = Notification.Name(rawValue: fetchAssocateInfoNotificationKey)
                NotificationCenter.default.addObserver(self, selector: #selector(fetchAssociateInfoCompletion(notification:)), name: fetchAssociateInfoNotificationName, object: nil)
            }
        }
        
        initViews()
        
        //Listen for keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidHide, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidChangeFrame, object: nil)
    }
    
    fileprivate func initViews(){
        UIApplication.statusBarBackgroundColor = .black
        
        headerView.screenName.text = NSLocalizedString("key_professionalism", comment: "")
        headerView.onCloseBtnClicked = {
            self.discardWarningAlert()
        }
        
        assignToView.onEditBtnClicked = {
            self.goSelectEmployeeViewController(featureName: FeatureName.Todos)
        }
        let firstName = self.selectedEmployee!.FirstName ?? ""
        let lastName = self.selectedEmployee!.LastName ?? ""
        let fullName = firstName + " " + lastName
        assignToView.fullNameLable.text = fullName
        
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_professionalism", comment: ""))
        
        initAssignToOneView(assignToView: assignToView)
        assignToView.onEditBtnClicked = {
            self.popupSelectEmployeeViewController()
        }
    
        itemTableView.separatorInset.right = itemTableView.separatorInset.left
        itemTableView.allowsSelection = false
        itemTableView.isScrollEnabled = false
        
        historyTableView.separatorStyle = .none
        historyTableView.allowsSelection = false
        
        descriptionTextView.delegate = self
        descriptionTextView.text = NSLocalizedString("key_description", comment: "")
        descriptionTextView.textColor = UIColor.init(hexString: Constants.textDarkWhite)
        
        historyTableView.removeFromSuperview()
        bottomSpace.constant = 18
        scrollContentHeight.constant = 1020 - 120
    }
    
    fileprivate func onConfirmBtnClicked(_ selectedTeam: [TeamMemberV2]) {
        selectedEmployee = selectedTeam[0]
        initAssignToOneView(assignToView: assignToView)
    }
    
    fileprivate func popupSelectEmployeeViewController(){
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SelectEmployeeViewController") as! SelectEmployeeViewController
        viewController.remoteManager = remoteManager
        viewController.featureName = FeatureName.Professionalism
        viewController.isEdit = true
        viewController.selectedEmployee = selectedEmployee
        viewController.onConfirmBtnClicked = onConfirmBtnClicked
        
        viewController.modalTransitionStyle = .crossDissolve
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: true, completion: nil)
    }

    func submitProfessionalismEmployeeAudit(employeeAuditItem: PressionalismAudit){
        if(remoteManager == nil){
            fatalError("Missing dependencies on submitProfessionalismEmployeeAudit")
        }
        remoteManager!.submitItem(remoteManager: remoteManager!, item: employeeAuditItem, apiRouter: APIRouter.employeeAudit, completion: submitCompletion(_:_:_:))
    }
    
    fileprivate func updateYesNoBtn(sender: UIButton, btn: UIButton, isEventBasedAudit: Bool){
        employeeAuditItem.IsEventBasedAudit = isEventBasedAudit
        
        sender.setTitleColor(UIColor.init(hexString: Constants.textBlack), for: .normal)
        sender.backgroundColor = UIColor.init(hexString: Constants.btnLightGrayBG)
        
        btn.setTitleColor(UIColor.init(hexString: Constants.btnTextWhite), for: .normal)
        btn.backgroundColor = UIColor.init(hexString: Constants.btnDarkGrayBG)
    }
    
    @objc func refreshTokenCompletion(notification: NSNotification){
        //It's ready for http request, Do nothing.
    }
    
    @objc func fetchAssociateInfoCompletion(notification: NSNotification){
        //It's ready for http request, Do nothing.
    }
    
    @IBAction func yesBtnClicked(_ sender: UIButton) {
        updateYesNoBtn(sender: sender, btn: noBtn, isEventBasedAudit: true)
    }
    
    @IBAction func noBtnClicked(_ sender: UIButton) {
        updateYesNoBtn(sender: sender, btn: yesBtn, isEventBasedAudit: false)
    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        let description = descriptionTextView.text
        if(description == "" || description == NSLocalizedString("key_description", comment: "")){
            showInfoAlert(message: NSLocalizedString("key_provide_input", comment: ""))
            return
        }
        if(selectedEmployee == nil){
            showInfoAlert(message: NSLocalizedString("key_have_select_employee", comment: ""))
            return
        }
        employeeAuditItem.EmployeeId = (selectedEmployee?.EmployeeId)!
        employeeAuditItem.Comment = description
        employeeAuditItem.CheckBoxSelections.removeAll()
        for audit in professionalismItems {
            if(!audit.isPass){
                employeeAuditItem.CheckBoxSelections.append(audit.kpiSubOptionId)
                
            }
        }
        submitProfessionalismEmployeeAudit(employeeAuditItem: employeeAuditItem)
    }
    
    //MARK: UITableViewDelegate
//    func numberOfSections(in tableView: UITableView) -> Int {
//        if(tableView == historyTableView){
//            return 1
//        }
//        return 0
//    }
//
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let sectionHeader = UIView()
//        if(tableView == historyTableView){
//            sectionHeader.backgroundColor = UIColor.darkGray
//            return sectionHeader
//        }
//        sectionHeader.intrinsicContentSize.height
//        return sectionHeader
//    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(tableView == historyTableView){
            return "Failed Audit History"
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        (view as! UITableViewHeaderFooterView).backgroundView?.backgroundColor = UIColor.init(hexString: "#3D3C4D")
        (view as! UITableViewHeaderFooterView).textLabel?.textColor = .white
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == itemTableView){
            return professionalismItems.count
        }else{
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == itemTableView){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTableViewCell", for: indexPath) as! ItemTableViewCell
            cell.titleLabel.text = professionalismItems[indexPath.row].title
            cell.descriptionLabel.text = professionalismItems[indexPath.row].description
            
            cell.successBtnClickAction = {(cell) in
                self.professionalismItems[indexPath.row].isPass = true
            }
            
            cell.failBtnClickAction = {(cell) in
                self.professionalismItems[indexPath.row].isPass = false
            }
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell", for: indexPath) as! HistoryTableViewCell

            return cell
        }
    }
}
