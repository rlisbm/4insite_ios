//
//  SelectEmployeeViewController.swift
//  Manage
//
//  Created by Rong Li on 11/26/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import AlamofireImage

class EmployeeCell: UITableViewCell {    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var position: UILabel!
}

class SelectEmployeeViewController: BaseViewController {
    var isSingleSelection: Bool = true
    var featureName: FeatureName?
    var isEdit: Bool = false
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var searchEmployeeBtn: UIButton!
    @IBOutlet weak var multiSelectionView: MultiSelectionView!
    @IBOutlet weak var employeeTable: UITableView!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var employeeTableTopConstraint: NSLayoutConstraint!
    
    var isSearching = false
//    var team = List<TeamMemberV2>()
    var team = [TeamMemberV2]()
    var searchedTeam: [TeamMemberV2] = []
    var selectedTeam: [TeamMemberV2] = []
//    var isSelectAllClicked: Bool = false
    
    var firstName: String = ""
    var lastName: String = ""
    var position: String = ""
    var avatarUrl: String = ""
    
    var onConfirmBtnClicked: ((_ selectedTeam: [TeamMemberV2]) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if(isSingleSelection){
//            removeMultiSelectionView()
//        }
//        initViews()
        
        if(refreshTokenNotificationName != nil){
            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
        }else{
            initViews()
            if(AssociateHelper.get()?.employeeId == nil){
                fetchAssociateInfoNotificationName = Notification.Name(rawValue: fetchAssocateInfoNotificationKey)
                NotificationCenter.default.addObserver(self, selector: #selector(fetchAssociateInfoCompletion(notification:)), name: fetchAssociateInfoNotificationName, object: nil)
            }else{
                fetchTeamRoster()
            }
        }
    }
    
    fileprivate func removeMultiSelectionView(){
        multiSelectionView?.removeFromSuperview()
        employeeTableTopConstraint.constant = 0
    }
    
    fileprivate func isSingleEmployeeSelection() -> Bool{
        if(featureName == FeatureName.Conduct
            || featureName == FeatureName.Todos
            || featureName == FeatureName.Professionalism
            || featureName == FeatureName.ReportIt
            || featureName == FeatureName.Attendance
            || featureName == FeatureName.AuditAssignTodo){
            return true
        }
        return false
    }
    
    fileprivate func initViews(){
        
        
        isSingleSelection = isSingleEmployeeSelection()
        if(isSingleSelection){
            removeMultiSelectionView()
        }
        
        searchBar.delegate = self
        hideSearchBar()
 
        headerView.screenName.text = NSLocalizedString("key_select_employee", comment: "")
        
        confirmBtn.layer.cornerRadius = 4
        headerView.onCloseBtnClicked = {
            self.dismiss(animated: true)
            
//            if self.featureName == FeatureName.Complaint {
//                self.dismiss(animated: true)
//            }else{
//                self.goLandingViewController()
//            }
        }
//        employeeTable.separatorInset = UIEdgeInsets.zero
//        employeeTable.separatorColor = UIColor.darkGray
        
//        multiSelectionView?.onSelectBtnClicked = {
//            print("############# select all clicked")
//        }
        
        multiSelectionView?.onSelectBtnClicked = {
            self.selectedTeam.removeAll()
            if(self.multiSelectionView.button.tag == 0){
                for employee in self.team {
                    self.selectedTeam.append(employee)
                }
            }
            self.employeeTable.reloadData()
            self.multiSelectionView.button.toggleMultiSelectionBtn(label: self.multiSelectionView.label)

//            self.selectedTeam.removeAll()
//            if(self.multiSelectionView.button.tag == 0){
//                self.multiSelectionView.button.tag = 1
//                for employee in self.team {
//                    self.selectedTeam.append(employee)
//                }
//            }else{
//                self.multiSelectionView.button.tag = 0
//            }
//            self.employeeTable.reloadData()
            
        }
    }
    
//    fileprivate func updateTableSelection(){
//
//        if(selectedEmployees != nil && selectedEmployees.count > 0){
//            let totalRows = employeeTable.numberOfRows(inSection: 0)
//            for employee in selectedEmployees {
//                for row in 0..<totalRows {
//                    let indexPath = IndexPath(row: row, section: 0)
//                    let cell = employeeTable.cellForRow(at: indexPath) as! EmployeeCell
//                     print("############# indexPath: \(indexPath.row)   \(cell.fullName.text)")
//                    let fullName = employee.firstName ?? "" + " " + employee.lastName!
//                    if(cell.fullName.text == fullName){
//                        cell.accessoryType = .checkmark
//                        break
//                    }
//                }
//            }
//        }
//    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(refreshTokenNotificationName != nil || fetchAssociateInfoNotificationName != nil ){
            NotificationCenter.default.removeObserver(self)
        }
    }
    
    @objc func refreshTokenCompletion(notification: NSNotification){
        print("### ### SelectEmployeeViewController refreshTokenCompletion")
        initViews()
    }
    
    @objc func fetchAssociateInfoCompletion(notification: NSNotification){
        print("### ### SelectEmployeeViewController fetchAssociateInfoCompletion")
        fetchTeamRoster()
    }
    
    fileprivate func fetchTeamRoster(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetchTeamRoster")
        }
        remoteManager!.fetchDataWithDate(remoteManager: remoteManager!, apiRouter: APIRouter.teamRosterV2, completion: fetchTeamRosterCompletion)
    }
    
    fileprivate func fetchTeamRosterCompletion(_ data: Data?, _ error: Error?, _ responseCode: Int?, date: Date?){
        if(responseCode == Constants.ResponseFailed401){
            refreshTokenNotificationName = Notification.Name(rawValue: refreshTokenNotificationKey)
            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
            refreshToken()
            return
        }
        
        let jsonDecoder = JSONDecoder()
        guard let teamRoster = try? jsonDecoder.decode(TeamRosterV2.self, from: data!) else{
            //No data to be shown
            return
        }

        //if success, fetch from server, otherwise fetch from database
        //empty data could happen on the very first time failed fetch from server.
        if(teamRoster != nil && teamRoster.TeamMembers!.count > 0){
            if(selectedEmployees.count > 0){
                for employee in selectedEmployees{
                    selectedTeam.append(employee)
                }
                selectedEmployees.removeAll()
            }
            team = teamRoster.TeamMembers!
            employeeTable.reloadData()
        }
    }


//    fileprivate func fetchTeamMembersCompletion(_ teamRoster: TeamRoster?, _ error: Error?, _ responseCode: Int){
//        
//        //if success, fetch from server, otherwise fetch from database
//        //empty data could happen on the very first time failed fetch from server. 
//        if(teamRoster != nil && teamRoster!.teamMembers.count > 0){
//            if(selectedEmployees.count > 0){
//                for employee in selectedEmployees{
//                    selectedTeam.append(employee)
//                }
//                selectedEmployees.removeAll()
//            }
//            team = teamRoster!.teamMembers
//            employeeTable.reloadData()
//        }
//    }
    
    fileprivate func enableSearchCancelBtn() {
        if let cancelButton : UIButton = searchBar.value(forKey: "_cancelButton") as? UIButton{
            cancelButton.isEnabled = true
        }
    }
    fileprivate func showSearchBar(){
        headerView.isHidden = true
        searchEmployeeBtn.isHidden = true
        
        searchBar.isHidden = false
        enableSearchCancelBtn()
        
        searchBar.becomeFirstResponder()
    }
    
    fileprivate func hideSearchBar(){
        headerView.isHidden = false
        searchEmployeeBtn.isHidden = false
        
        searchBar.isHidden = true
    }
    
    func didSingleSelectTableView(tableView: UITableView, indexPath: IndexPath, list: [TeamMemberV2]){
        if let selectedCell  = tableView.cellForRow(at: indexPath as IndexPath) {
            if(selectedTeam.count == 0){
                selectedTeam.append(list[indexPath.row])
                selectedCell.accessoryType = .checkmark
            }else{
                //if pre-selection is visible, update it
                var index = list.index(ofElement: selectedTeam[0])
                
                //This block of code is for team list (var team = List<TeamMember>())
                //Because for some reason Array.index(of: ) does not work Array trasfromed from List
                let selected = selectedTeam[0]
                if(index == nil){
                    for member in list {
//                        if(member.isSameObject(as: selected)){
//                            index = list.index(of: member)
//                            break
//                        }
                        
                        if(member.EmployeeId == selected.EmployeeId) {
                            index = list.index(ofElement: member)
                            break
                        }
                    }
                }
                
                if(index != nil){
                    let preIndexPath = IndexPath(row: index!, section: 0)
                    let preSelectedCell = tableView.cellForRow(at: preIndexPath as IndexPath)
                    if(preSelectedCell != nil){
                        preSelectedCell!.accessoryType = .none
                    }
                }
                //update currently selected gui
                if(selectedCell.accessoryType == .checkmark){
                    selectedTeam.removeAll()
                    selectedCell.accessoryType = .none
                }else{
                    selectedCell.accessoryType = .checkmark
                    selectedTeam[0] = list[indexPath.row]
                }
            }
        }
    }
    func didMultiSelectTableView(tableView: UITableView, indexPath: IndexPath, list: [TeamMemberV2]){
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .checkmark{
                
                
                
                var index = selectedTeam.index(ofElement: list[indexPath.row])

                
                if(index == nil){
                    let selected = list[indexPath.row]
                    for member in selectedTeam {
                        if(member.EmployeeId == selected.EmployeeId){
                            index = selectedTeam.index(ofElement: member)
                            break
                        }
                    }
                }
                
                selectedTeam.remove(at: index!)
                
                cell.accessoryType = .none
            }
            else{
                selectedTeam.append(list[indexPath.row])
                cell.accessoryType = .checkmark
            }
        }else{
            print("############### not working: \(indexPath.row)")
        }
    }
    
    fileprivate func editSelectEmployeeDone(){
        onConfirmBtnClicked?(selectedTeam)
        dismiss(animated: true)
    }
    
    fileprivate func goAttendanceViewController(){
        let storyboard =  UIStoryboard(name: "Attendance", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AttendancePageViewController") as! AttendancePageViewController
        viewController.remoteManager = remoteManager
        viewController.selectedEmployee = selectedTeam[0]
        
        viewController.selectEmployeeViewController = self
        present(viewController, animated: true, completion: nil)
    }
    fileprivate func goAuditsViewController(){
        
    }
    
    fileprivate func goComplaintViewController(){
        onConfirmBtnClicked?(selectedTeam)
        dismiss(animated: true)
    }
    
    fileprivate func goComplimentViewController(){
//        let storyboard =  UIStoryboard(name: "Compliments", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "ComplimentsPageViewController") as! ComplimentsPageViewController
//        viewController.remoteManager = remoteManager
//        viewController.selectedEmployee = selectedTeam[0]
//        present(viewController, animated: true, completion: nil)
    }
    fileprivate func goConductViewController(){
        let storyboard =  UIStoryboard(name: "Conduct", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ConductPageViewController") as! ConductPageViewController
        viewController.remoteManager = remoteManager
        viewController.selectedEmployee = selectedTeam[0]
        present(viewController, animated: true, completion: nil)
        
    }
    
    fileprivate func goProfessionalismViewController(){
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ProfessionalismViewController") as! ProfessionalismViewController
        viewController.remoteManager = remoteManager
        viewController.selectedEmployee = selectedTeam[0]
        present(viewController, animated: true, completion: nil)
        
    }
    fileprivate func goReportItViewController(){
        let storyboard =  UIStoryboard(name: "ReportIt", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ReportItPageViewController") as! ReportItPageViewController
        viewController.remoteManager = remoteManager
        viewController.selectedEmployee = selectedTeam[0]
        present(viewController, animated: true, completion: nil)
    }
    fileprivate func goSafetyViewController(){
        
    }
    fileprivate func goSendMessageViewController(){
        
    }
    fileprivate func goTodosViewController(){
        let storyboard =  UIStoryboard(name: "Todos", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "TodosPageViewController") as! TodosPageViewController
        viewController.remoteManager = remoteManager
        viewController.selectedEmployee = selectedTeam[0]
        present(viewController, animated: true, completion: nil)
    }
    
    
    fileprivate func goTempViewController(){
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "TodosViewController") as! TodosViewController
        viewController.remoteManager = remoteManager
        viewController.selectedEmployee = selectedTeam[0]
        viewController.featureName = featureName
        present(viewController, animated: true, completion: nil)
    }
    
    @IBAction func searchEmployeeBtnClicked(_ sender: Any) {
        showSearchBar()
    }
    
    @IBAction func confirmBtnClicked(_ sender: Any) {
        if(featureName == nil){
            return
        }
        
//        goTempViewController()
        if(selectedTeam == nil || selectedTeam.count == 0){
            showAlert(NSLocalizedString("key_infomation", comment: ""), NSLocalizedString("key_select_an_employee", comment: ""), NSLocalizedString("key_dismiss", comment: ""), viewController: self)
            return
        }
        
        if(isEdit){
            editSelectEmployeeDone()
            return
        }
        
        switch featureName! {
        case FeatureName.Attendance:
            goAttendanceViewController()
            break
        case FeatureName.Audit:
            goTempViewController()
            break
        case FeatureName.Complaint:
            goComplaintViewController()
            break
        case FeatureName.Compliment:
            goComplimentViewController()
            break
        case FeatureName.Conduct:
            goConductViewController()
            break
        case FeatureName.Professionalism:
            goProfessionalismViewController()
            break
        case FeatureName.ReportIt:
            goReportItViewController()
            break
        case FeatureName.Safety:
            break
        case FeatureName.SendMessage:
            break
        case FeatureName.Todos:
            goTodosViewController()
            break
        default:
            break
        }
    }
    
}

extension SelectEmployeeViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(isSearching){
            return searchedTeam.count
        }else{
            return team.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmployeeCell") as! EmployeeCell
 
        

        if(isSearching){
            firstName = searchedTeam[indexPath.row].FirstName ?? ""
            lastName = searchedTeam[indexPath.row].LastName ?? ""
            position = searchedTeam[indexPath.row].Job?.Value ?? ""
            if(searchedTeam[indexPath.row].ProfileImage != nil && searchedTeam[indexPath.row].ProfileImage!.Value != nil){
                avatarUrl = searchedTeam[indexPath.row].ProfileImage!.Value!
            }
        }else{
            firstName = team[indexPath.row].FirstName ?? ""
            lastName = team[indexPath.row].LastName ?? ""
            position = team[indexPath.row].Job?.Value ?? ""

            if(team[indexPath.row].ProfileImage != nil && team[indexPath.row].ProfileImage!.Value != nil){
                avatarUrl = team[indexPath.row].ProfileImage!.Value!
            }
        }
        
        cell.fullName.text = firstName + " " + lastName
        cell.position.text = position
        avatarUrl = remoteManager!.baseCDNUrl + avatarUrl
        
        setImage(imageView: cell.avatar, imgUrl: avatarUrl, isCircle: true)
        
        var isSelected = false
        if(selectedTeam.count > 0){
            for employee in selectedTeam {
                if(firstName == employee.FirstName ?? "" && lastName == employee.LastName ?? "" && position == employee.Job?.Value) {
                    cell.accessoryType = .checkmark
                    isSelected = true
                    break
                }
            }
        }
        if(!isSelected){
            cell.accessoryType = .none
        }
        
        
//        if(isSelectAllClicked){
//            if(multiSelectionView.button.tag == 1){
//                cell.accessoryType = .checkmark
//            }else{
//                cell.accessoryType = .none
//            }
//        }

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        isSelectAllClicked = false
        tableView.deselectRow(at: indexPath, animated: true)
        if(isSingleSelection){
            if(isSearching){
                didSingleSelectTableView(tableView: tableView, indexPath: indexPath, list: searchedTeam)
            }else{
                didSingleSelectTableView(tableView: tableView, indexPath: indexPath, list: Array(team))
            }
        }else{
            if(isSearching){
                didMultiSelectTableView(tableView: tableView, indexPath: indexPath, list: searchedTeam)
                
            }else{
                didMultiSelectTableView(tableView: tableView, indexPath: indexPath, list: Array(team))
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if(isSingleSelection){
            tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .none
        }
    }

}

extension SelectEmployeeViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchedTeam = Array(team).filter({$0.FirstName!.lowercased().prefix(searchText.count) == searchText.lowercased()})
        isSearching = true
        employeeTable.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        isSearching = false
//        searchBar.text = ""
//        employeeTable.reloadData()

        searchBarSearchButtonClicked(searchBar)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
        searchBar.text = ""
        employeeTable.reloadData()
        
        searchBar.endEditing(true)
        hideSearchBar()
    }
}

