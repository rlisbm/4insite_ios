//
//  SelectPositionViewController.swift
//  Manage
//
//  Created by Rong Li on 1/15/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class SelectPositionViewController: BaseViewController {
    
    var onConfirmBtnClicked: ((_ selectedJobs: [JobForSite]) -> ())?

    var isSingleSelection: Bool = true
    var featureName: FeatureName?
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var multiSelectionView: MultiSelectionView!
    @IBOutlet weak var departmentTable: UITableView!
    @IBOutlet weak var departmentTableTopConstraint: NSLayoutConstraint!
    
    
    var isSearching = false
    
    var jobs: [JobForSite] = []
    var searchedJobs: [JobForSite] = []
    var selectedJobs: [JobForSite] = []
    
    var jobName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(refreshTokenNotificationName != nil){
            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
        }else{
            initViews()
            if(AssociateHelper.get()?.employeeId == nil){
                fetchAssociateInfoNotificationName = Notification.Name(rawValue: fetchAssocateInfoNotificationKey)
                NotificationCenter.default.addObserver(self, selector: #selector(fetchAssociateInfoCompletion(notification:)), name: fetchAssociateInfoNotificationName, object: nil)
            }else{
                fetchSiteJobs()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(refreshTokenNotificationName != nil || fetchAssociateInfoNotificationName != nil ){
            NotificationCenter.default.removeObserver(self)
        }
    }
    
    fileprivate func isSingleEmployeeSelection() -> Bool{
        if(featureName == FeatureName.SendMessage){
            return false
        }
        return true
    }
    
    fileprivate func removeMultiSelectionView(){
        multiSelectionView?.removeFromSuperview()
        departmentTableTopConstraint.constant = 0
    }
    
    func initViews(){
        headerView.screenName.text = NSLocalizedString("key_send_message_to", comment: "")
        
        isSingleSelection = isSingleEmployeeSelection()
        if(isSingleSelection){
            removeMultiSelectionView()
        }
        
        searchBar.delegate = self
        hideSearchBar()
        
        headerView.onCloseBtnClicked = {
            self.goLandingViewController()
        }
        
        multiSelectionView?.onSelectBtnClicked = {
            self.selectedJobs.removeAll()
//            if(self.multiSelectionView.button.tag == 0){
//                self.multiSelectionView.button.tag = 1
//                self.multiSelectionView.label.text = "UnSelect All"
//                self.multiSelectionView.button.setImage(UIImage(named: "Check all - Remove.png"), for: .normal)
//                for job in self.jobs {
//                    self.selectedJobs.append(job)
//                }
//            }else{
//                self.multiSelectionView.button.tag = 0
//                self.multiSelectionView.label.text = "Select All"
//                self.multiSelectionView.button.setImage(UIImage(named: "Check - Select All.png"), for: .normal)
//            }
            
            if(self.multiSelectionView.button.tag == 0){
                for job in self.jobs {
                    self.selectedJobs.append(job)
                }
            }
            self.multiSelectionView.button.toggleMultiSelectionBtn(label: self.multiSelectionView.label)
            
            self.departmentTable.reloadData()
            
        }
    }
    
    fileprivate func fetchSiteJobs(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetchTeamMembers")
        }
        remoteManager!.fetchSiteJobs(remoteManager: remoteManager!, apiRouter: APIRouter.jobsForSites, completion: fetchSiteJobsCompletion)
    }
    
    fileprivate func fetchSiteJobsCompletion(_ jobList: [JobForSite]?, _ error: Error?, _ responseCode: Int){
        if(error != nil){
            print("*** *** fetchSiteJobsCompletion: responseCode: \(responseCode) error: \(error)")
        }else{
            print("*** *** fetchSiteJobsCompletion: responseCode: \(responseCode) ")
        }
        //if success, fetch from server, otherwise fetch from database
        //empty data could happen on the very first time failed fetch from server.
        if(jobList != nil && jobList!.count > 0){
            if(selectedJobList.count > 0){
                for job in selectedJobList {
                    selectedJobs.append(job)
                }
            }
            jobs = jobList!
            departmentTable.reloadData()
        }
    }
    
    fileprivate func goSendMessageViewController(){
        
        let storyboard =  UIStoryboard(name: "SendMessage", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SendMsgViewController") as! SendMsgViewController
        
        if(viewController.modalPresentationStyle == .overCurrentContext
            && viewController.modalTransitionStyle == .crossDissolve){
            onConfirmBtnClicked?(selectedJobs)
            dismiss(animated: true)
            return
        }
        
        viewController.remoteManager = remoteManager
        viewController.selectedJobList = selectedJobs
        present(viewController, animated: true, completion: nil)
    }
    
    @objc func refreshTokenCompletion(notification: NSNotification){
        initViews()
    }
    
    @objc func fetchAssociateInfoCompletion(notification: NSNotification){
        fetchSiteJobs()
    }
    
    @IBAction func searchBtnClicked(_ sender: Any) {
        showSearchBar()
    }
    
    @IBAction func confirmBtnClicked(_ sender: Any) {
        if(featureName == nil){
            return
        }
        if(selectedJobs.count == 0){
            showAlert(NSLocalizedString("key_infomation", comment: ""), NSLocalizedString("key_select_a_department", comment: ""), NSLocalizedString("key_dismiss", comment: ""), viewController: self)
            return
        }
        switch featureName! {
        case FeatureName.SendMessage:
            goSendMessageViewController()
            break
        default:
            break
        }
    }
    
    
    fileprivate func enableSearchCancelBtn() {
        if let cancelButton : UIButton = searchBar.value(forKey: "_cancelButton") as? UIButton{
            cancelButton.isEnabled = true
        }
    }
    fileprivate func showSearchBar(){
        headerView.isHidden = true
        searchBtn.isHidden = true
        
        searchBar.isHidden = false
        enableSearchCancelBtn()
        
        searchBar.becomeFirstResponder()
    }
    fileprivate func hideSearchBar(){
        headerView.isHidden = false
        searchBtn.isHidden = false
        
        searchBar.isHidden = true
    }
    
    func didSingleSelectTableView(tableView: UITableView, indexPath: IndexPath, list: [JobForSite]){
        if let selectedCell  = tableView.cellForRow(at: indexPath as IndexPath) {
            if(selectedJobs.count == 0){
                selectedJobs.append(list[indexPath.row])
                selectedCell.accessoryType = .checkmark
            }else{
                //if pre-selection is visible, update it
//                var index = selectedJobs.index(of: list[indexPath.row])
                var index = selectedJobs.index(ofElement: list[indexPath.row])
                print("***************** index : \(index)")
                
                //                //This block of code is for team list (var team = List<TeamMember>())
                //                //Because for some reason Array.index(of: ) does not work Array trasfromed from List
                //                let selected = selectedDepartents[0]
                //                if(index == nil){
                //                    for member in list {
                //                        if(member.isSameObject(as: selected)){
                //                            index = list.index(of: member)
                //                            break
                //                        }
                //                    }
                //                }
                
                if(index != nil){
                    let preIndexPath = IndexPath(row: index!, section: 0)
                    let preSelectedCell = tableView.cellForRow(at: preIndexPath as IndexPath)
                    if(preSelectedCell != nil){
                        preSelectedCell!.accessoryType = .none
                    }
                }
                //update currently selected gui
                if(selectedCell.accessoryType == .checkmark){
                    selectedJobs.removeAll()
                    selectedCell.accessoryType = .none
                }else{
                    selectedCell.accessoryType = .checkmark
                    selectedJobs[0] = list[indexPath.row]
                }
            }
        }
    }
    func didMultiSelectTableView(tableView: UITableView, indexPath: IndexPath, list: [JobForSite]){
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .checkmark{
                
                var index = selectedJobs.index(ofElement: list[indexPath.row])
                print("***************** index : \(index)")
                
                if(index == nil){
                    let selected = list[indexPath.row]
                    for job in selectedJobs {
                        if(job.JobId == selected.JobId){
                            index = selectedJobs.index(ofElement: job)
                            break
                        }
                    }
                }
                
                selectedJobs.remove(at: index!)
                
                cell.accessoryType = .none
            }
            else{
                selectedJobs.append(list[indexPath.row])
                cell.accessoryType = .checkmark
            }
        }
    }
}

extension SelectPositionViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(isSearching){
            return searchedJobs.count
        }else{
            return jobs.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "JobCell")!
        if(isSearching){
            jobName = searchedJobs[indexPath.row].Name
        }else{
            jobName = jobs[indexPath.row].Name
        }
        
        cell.textLabel?.text = jobName
        
        var isSelected = false
        if(selectedJobs.count > 0){
            for job in selectedJobs {
                if(job.Name == jobName) {
                    cell.accessoryType = .checkmark
                    isSelected = true
                    break
                }
            }
        }
        if(!isSelected){
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(isSingleSelection){
            if(isSearching){
                didSingleSelectTableView(tableView: tableView, indexPath: indexPath, list: searchedJobs)
            }else{
                didSingleSelectTableView(tableView: tableView, indexPath: indexPath, list: jobs)
            }
        }else{
            if(isSearching){
                didMultiSelectTableView(tableView: tableView, indexPath: indexPath, list: searchedJobs)
                
            }else{
                didMultiSelectTableView(tableView: tableView, indexPath: indexPath, list: jobs)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if(isSingleSelection){
            tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .none
        }
    }
    
}

extension SelectPositionViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchedJobs = jobs.filter({$0.Name!.lowercased().prefix(searchText.count) == searchText.lowercased()})
        isSearching = true
        departmentTable.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
        searchBar.text = ""
        departmentTable.reloadData()
        
        searchBarSearchButtonClicked(searchBar)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
        searchBar.text = ""
        departmentTable.reloadData()
        
        searchBar.endEditing(true)
        hideSearchBar()
    }
}

//extension Array where Array.Element: AnyObject {
//    
//    func index(ofElement element: Element) -> Int? {
//        for (currentIndex, currentElement) in self.enumerated() {
//            if currentElement === element {
//                return currentIndex
//            }
//        }
//        return nil
//    }
//}
