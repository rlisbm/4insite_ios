//
//  SendMsgViewController.swift
//  Manage
//
//  Created by Rong Li on 1/15/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class SendMsgViewController: AddLabelKeyboardBaseViewController {
    let msgText = NSLocalizedString("key_message", comment: "")
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var assignToView: AssignToView!
    @IBOutlet weak var nameContainerView: UIView!
    @IBOutlet weak var switchView: SwitchView!
    @IBOutlet weak var pickDateTimeView: PickDateTimeView!
    @IBOutlet weak var postDateLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var mobileBtn: UIButton!
    @IBOutlet weak var highPriorityBtn: UIButton!
    
    @IBOutlet weak var scrollContentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nameLabelViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nameContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nameContainerViewTop: NSLayoutConstraint!
    @IBOutlet weak var postDateLabelTop: NSLayoutConstraint!
    @IBOutlet weak var pickDateTimeViewHeight: NSLayoutConstraint!
    
    var message = TrainingMessage()
    var isPortrait: Bool = true
    var isSubmitNow: Bool = false

    override func viewDidLoad() {
        self.assignTo = assignToView
        self.nameContainer = nameContainerView
        
        self.scrollContentHeight = scrollContentViewHeight
        self.nameLabelHeight = nameLabelViewHeight
        self.nameContainerHeight = nameContainerViewHeight
        self.nameContainerTop = nameContainerViewTop
        
        yOriginalPos = Int(nameContainerViewTop.constant + nameContainerViewHeight.constant)
        scrollContentViewOriginalHeight = scrollContentViewHeight.constant
        nameLabelViewOriginalHeight = nameLabelViewHeight.constant
        nameContainerViewOriginalHeight = nameContainerViewHeight.constant
        
        super.viewDidLoad()
        
        initViews()

        isPortrait = getIsPortrait()
        
        //Listen for keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)
        
//        fetchDepartmentMe()
        
    }
    
//    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        updateNameLabelViewAfterRotate()
//    }
//    func updateNameLabelViewAfterRotate(){
//        if(isPortrait != getIsPortrait()){
//            isPortrait = !isPortrait
//            perform(#selector(updateNameLabelView), with: nil, afterDelay: 1)
//        }
//    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        updateNameLabelView()
    }
    
//    func getIsPortrait() -> Bool{
//        return UIDevice.current.orientation.isLandscape == true ? false : true
//    }
    
    fileprivate func initViews(){
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_send_message_to", comment: ""))
        
        headerView.onCloseBtnClicked = {
            self.discardWarningAlert()
        }
        
        assignToView.onEditBtnClicked = {
            self.onEditBtnClicked()
        }
        assignToView.onToggleBtnClicked = {
            (toggleBtn) in
            self.onToggleBtnClicked(toggleBtn: toggleBtn)
        }
        
        if(!switchView.switchBtn.isOn){
            removePickDateTimeView()
        }
        
        switchView.onSwitchClicked = {
            (isOn) in
            if(isOn){
                self.addPickDateTimeView()
            }else{
                self.removePickDateTimeView()
            }
        }
        
        pickDateTimeView.onPopupDatePickerBtnClicked = {
            (dateTimeBtn) in
            self.popupDatePickerBtnClicked(sender: dateTimeBtn)
        }
        
        textView.text = msgText
        textView.textColor = UIColor.init(hexString: Constants.textDarkWhite)
        textView.layer.borderColor = UIColor.init(hexString: Constants.boarderColor).cgColor
        
//        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTouched))
//        contentView.addGestureRecognizer(tapRecognizer)
        
        updateNameContainerView(isShowAll: false)
    }
    
    fileprivate func addPickDateTimeView(){
        pickDateTimeView.isHidden = false
        
        postDateLabelTop.constant = pickDateTimeViewHeight.constant
        scrollContentViewHeight.constant = scrollContentViewHeight.constant + pickDateTimeViewHeight.constant
    }
    
    fileprivate func removePickDateTimeView(){
        pickDateTimeView.isHidden = true
        
        postDateLabelTop.constant = 0
        scrollContentViewHeight.constant = scrollContentViewHeight.constant - pickDateTimeViewHeight.constant
    }
    
    override func updateNameContainerView(isShowAll: Bool){
        updateNameContainerView(isShowAll: isShowAll, featureName: FeatureName.SendMessage)
//        for label in nameContainerView.subviews {
//            label.removeFromSuperview()
//        }
//        nameContainerViewHeight.constant = nameContainerViewOriginalHeight
//        addNames(nameContainerView: nameContainerView, isShowAll: isShowAll, featureName: FeatureName.SendMessage)
    }
    
    
    //web    DueDate: "2018-12-14T07:59:59.999Z"
    //mobile    DueDate: "2018-12-14T07:59:59"
    fileprivate func onSelectedDateText(_ formattedText: String, _ tag: Int){
        switch tag {
        case 2:
            pickDateTimeView.timeTextField.text = formattedText
            break
        default:
            pickDateTimeView.dateTextField.text = formattedText
            break
        }
    }
    
    fileprivate func onEditBtnClicked() {
        self.popupSelectPositionViewController(featureName: FeatureName.SendMessage)
    }
    
    fileprivate func onToggleBtnClicked(toggleBtn: UIButton) {
        updateNameLabelView()
    }
    
    fileprivate func onConfirmBtnClicked(_ selectedJobs: [JobForSite]) {
        selectedJobList = selectedJobs
        updateNameContainerView(isShowAll: false)
    }
    
    fileprivate func setCheckBtn(_ sender: UIButton){
        if(sender.tag == 0){
            sender.tag = 1
            sender.setImage(UIImage(named: "check-box-selected.png"), for: .normal)
        }else{
            sender.tag = 0
            sender.setImage(UIImage(named: "check-box-unselected.png"), for: .normal)
        }
    }
    
    @IBAction func mobileBtnClicked(_ sender: UIButton) {
        setCheckBtn(sender)
    }
    
    @IBAction func highPriorityBtnClicked(_ sender: UIButton) {
        setCheckBtn(sender)
    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        submit()
    }
    
    fileprivate func popupSelectPositionViewController(featureName: FeatureName){
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SelectPositionViewController") as! SelectPositionViewController
        viewController.remoteManager = remoteManager
        viewController.featureName = featureName
        viewController.selectedJobList = selectedJobList
        viewController.onConfirmBtnClicked = onConfirmBtnClicked
        
        viewController.modalTransitionStyle = .crossDissolve
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: true, completion: nil)
    }

//    @objc override func updateNameLabelView(){
//        var title: String = ""
//
//        if let attributedTitle = assignToView.toggleBtn.attributedTitle(for: .normal) {
//            let mutableAttributedTitle = NSMutableAttributedString(attributedString: attributedTitle)
//
//            if (assignToView.toggleBtn.tag == 1){
//                title = NSLocalizedString("key_see_less", comment: "")
//                updateNameContainerView(isShowAll: true)
//                nameLabelViewHeight.constant = nameLabelViewHeight.constant + CGFloat(yPos - yOriginalPos)
//                scrollContentViewHeight.constant = scrollContentViewHeight.constant + CGFloat(yPos - yOriginalPos)
//            }else{
//                title = NSLocalizedString("key_see_all", comment: "")
//                updateNameContainerView(isShowAll: false)
//                nameLabelViewHeight.constant = nameLabelViewOriginalHeight
//                scrollContentViewHeight.constant = scrollContentViewOriginalHeight
//            }
//
//            mutableAttributedTitle.replaceCharacters(in: NSMakeRange(0, mutableAttributedTitle.length), with: title)
//            assignToView.toggleBtn.setAttributedTitle(mutableAttributedTitle, for: .normal)
//        }
//    }
    func popupDatePickerBtnClicked(sender: UIButton) {
        let storyboard =  UIStoryboard(name: "Popup", bundle: nil)
        let viewControler = storyboard.instantiateViewController(withIdentifier: "DatePickerPopupViewController") as! DatePickerPopupViewController
        viewControler.tag = sender.tag
        viewControler.dateStyle = .short
        viewControler.onSelectedDateText = onSelectedDateText
        present(viewControler, animated: true, completion: nil)
    }
    
    fileprivate func submit(){
        if(selectedJobList.count == 0
            || (textView.text.isEmpty || textView.text == msgText)){
            showAlert(NSLocalizedString("key_infomation", comment: ""), NSLocalizedString("key_provide_input", comment: ""), NSLocalizedString("key_dismiss", comment: ""), viewController: self)
            return
        }
        
//        var message = TrainingMessage()
        message.Text = textView.text
        for selectedJob in selectedJobList{
            let job = Job(Key: selectedJob.JobId, Value: selectedJob.Name)
            message.Jobs.append(job)
        }
        let associate = AssociateHelper.get()
//        message.SenderId = associate?.employeeId ?? 0
        message.SenderFullName = associate?.fullName
        message.SenderEmail = associate?.email
        message.SenderJobTitle = associate?.jobTitle
        message.SenderType = SenderType.manager.path
        message.SenderProfileImageUrl = associate?.profilePictureUniqueId
        message.SourceLanguageId = getLocale()
        
        let site = SelectedSiteHelper.get()
        message.Sites.append(IdName(Key: site?.selectedSiteId, Value: site?.selectedSiteName))
        
//        message.Department = DepartmentRequestItem(departmentId: 18, name: "Me", departmentUrl: "00272466-804b-726e-502b-8256e03e7c13.png")
        
        message.Department = getDepartmentMe()
        
        if(message.Department == nil){
            isSubmitNow = true
            fetchDepartmentMe()
        }
        
        submit(message: message)
    }
    
    fileprivate func submit(message: TrainingMessage){
        if(remoteManager == nil){
            fatalError("Missing dependencies on submitMessage")
        }
        remoteManager!.submitItem(remoteManager: remoteManager!, item: message, apiRouter: APIRouter.trainingMessage, completion: submitCompletion)
    }
    
    fileprivate func fetchDepartmentMe(){
        fetchDepartments()
    }
    
    fileprivate func fetchDepartments(){
        print("*** *** fetchDepartments")
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetchTeamMembers")
        }
        remoteManager!.fetchDepartments(remoteManager: remoteManager!, apiRouter: APIRouter.department, completion: fetchDepartmentsCompletion)
    }
    
    fileprivate func fetchDepartmentsCompletion(_ departmentList: [Department]?, _ error: Error?, _ responseCode: Int){
        print("*** *** fetchDepartmentsCompletion")
        if(error != nil){
            print("*** *** fetchDepartmentsCompletion: responseCode: \(responseCode) error: \(error)")
        }else{
            print("*** *** fetchDepartmentsCompletion: responseCode: \(responseCode) ")
        }
        
        if(departmentList != nil && (departmentList?.count ?? 0) > 0){
            print("### ### size : \(departmentList!.count)")
            for dept in departmentList! {
                if(dept.name == getMeDepartmentName()){
                    message.Department = DepartmentRequestItem(departmentId: dept.departmentId, name: dept.name, departmentImageUrl: dept.departmentImageUrl)
                    break
                }
            }
            if(isSubmitNow){
                submit(message: message)
            }
        }else{
            print("### ### size : departmentList == nil")
        }
    }
    
    fileprivate func getDepartmentMe() -> DepartmentRequestItem{
        return DepartmentRequestItem(departmentId: 18, name: "Me", departmentImageUrl: "")
    }
    
    fileprivate func getMeDepartmentName() -> String {
        return "Me"
    }
}
