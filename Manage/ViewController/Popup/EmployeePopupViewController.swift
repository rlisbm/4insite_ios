//
//  EmployeePopupViewController.swift
//  Manage
//
//  Created by Rong Li on 3/20/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class EmployeeQuickEntryCell: UICollectionViewCell{

    @IBOutlet weak var entryImg: UIImageView!
    
    @IBOutlet weak var entryLabel: UILabel!
}

class EmployeePopupViewController: BaseViewController {
    let goAttendance = 0
    let goConduct = 1
    let goProfessionalism = 2
    let goTodos = 3
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var imgLabelView: ImgLabelView!
    
    var quickEntryObjects = [QuickEntryObject]()
    
    var tabBar: TabBarController?
    
    override func viewDidLoad() {
        //without remoteManager, app can not be continued login
        if(remoteManager == nil && selectedEmployee == nil && tabBar == nil){
            return
        }
        
        super.viewDidLoad()
        
        tabBar!.setEmplpyeePopupViewController(viewController: self)
        
        initEmployeeQuckEntry()
        initViews()
    }

    fileprivate func initEmployeeQuckEntry() {
        quickEntryObjects.append(QuickEntryObject(label: NSLocalizedString("key_attendance", comment: ""), img: UIImage(named: "icon-attendance.png")!))
        quickEntryObjects.append(QuickEntryObject(label: NSLocalizedString("key_conduct", comment: ""), img: UIImage(named: "icon-conduct.png")!))
        quickEntryObjects.append(QuickEntryObject(label: NSLocalizedString("key_professionalism", comment: ""), img: UIImage(named: "icon-Professionalim.png")!))
        quickEntryObjects.append(QuickEntryObject(label: NSLocalizedString("key_todos", comment: ""), img: UIImage(named: "icon-todo.png")!))
    }
    
    fileprivate func initViews(){
        var layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 4)
        layout.minimumInteritemSpacing = 8
        layout.itemSize = CGSize(width: (self.collectionView.frame.size.width - 20)/2, height: ((self.collectionView.frame.size.height - 20)/2))
        
        imgLabelView.label1.text = (selectedEmployee?.FirstName)! + " " + (selectedEmployee?.LastName)!
        imgLabelView.label2.text = selectedEmployee?.Job?.Value
        
        var avatarUrl = ""
        if(selectedEmployee!.ProfileImage != nil && selectedEmployee!.ProfileImage!.Value != nil){
            avatarUrl = selectedEmployee!.ProfileImage!.Value!
        }
        avatarUrl = remoteManager!.baseCDNUrl + avatarUrl
        setImage(imageView: imgLabelView.img, imgUrl: avatarUrl, isCircle: true)
    }
    
    @IBAction func closeBtnClicked(_ sender: Any) {
        dismiss(animated: true)
    }
    
    func goAttendancePageViewController() {
        let storyboard =  UIStoryboard(name: "Attendance", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AttendancePageViewController") as! AttendancePageViewController
        viewController.remoteManager = remoteManager
        viewController.selectedEmployee = selectedEmployee
        viewController.employeePopupViewController = self
        present(viewController, animated: true, completion: nil)
    }
    
    func goConductPageViewController() {
        let storyboard =  UIStoryboard(name: "Conduct", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ConductPageViewController") as! ConductPageViewController
        viewController.remoteManager = remoteManager
        viewController.selectedEmployee = selectedEmployee
        viewController.employeePopupViewController = self
        present(viewController, animated: true, completion: nil)
    }
    func goProfessionalismViewController() {
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ProfessionalismViewController") as! ProfessionalismViewController
        viewController.remoteManager = remoteManager
        viewController.selectedEmployee = selectedEmployee
        viewController.employeePopupViewController = self
        present(viewController, animated: true, completion: nil)
    }
    func goTodosPageViewController() {
        let storyboard =  UIStoryboard(name: "Todos", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "TodosPageViewController") as! TodosPageViewController
        viewController.remoteManager = remoteManager
        viewController.selectedEmployee = selectedEmployee
        viewController.employeePopupViewController = self
        present(viewController, animated: true, completion: nil)
    }
}

extension EmployeePopupViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return quickEntryObjects.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmployeeQuickEntryCell", for: indexPath) as! EmployeeQuickEntryCell
        
        cell.entryLabel.text = quickEntryObjects[indexPath.item].label
        cell.entryImg.image = quickEntryObjects[indexPath.item].img
        
        cell.layer.borderColor = UIColor.clear.cgColor
        cell.layer.borderWidth = 0.5
        cell.layer.cornerRadius = 8.0
        cell.layer.masksToBounds = true
        
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.layer.shadowRadius = 8.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        
        return cell
    
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.item {
            case goAttendance:
                goAttendancePageViewController()
                break
            case goConduct:
                goConductPageViewController()
                break
            case goProfessionalism:
                goProfessionalismViewController()
                break
            case goTodos:
                goTodosPageViewController()
                break
            default:
                break
        }
    }
    
}
