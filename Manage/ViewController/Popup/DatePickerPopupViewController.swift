//
//  DatePickerPopupViewController.swift
//  Manage
//
//  Created by Rong Li on 12/10/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class DatePickerPopupViewController: UIViewController {
    var tag: Int = 0   //0: default value for dateAndTime, 1: for date, 2: for time
    var dateStyle: DateFormatter.Style?
    let dateFormatter = DateFormatter()

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var onSelectedDate: ((_ date: Date) -> ())?
    
    var onSelectedDateText: ((_ formattedText: String, _ tag: Int) -> ())?
    
    var formattedDate: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        return formatter.string(from: datePicker.date)
    }

    var formattedTime: String {
        get {
            let calendar = datePicker.calendar
            let h = calendar?.component(.hour, from: datePicker.date) ?? 0
            let m = calendar?.component(.minute, from: datePicker.date) ?? 0
            let s = calendar?.component(.second, from: datePicker.date) ?? 0
            
            let hh = (h < 10 ? "0\(h)" : "\(h)")
            let mm = (m < 10 ? "0\(m)" : "\(m)")
            let ss = (s < 10 ? "0\(s)" : "\(s)")
            
            return "\(hh):\(mm):\(ss)"
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.touchAction (_:)))
        view.addGestureRecognizer(gesture)
        
        switch tag {
        case 1:
            datePicker.datePickerMode = .date
            break
        case 2:
            datePicker.datePickerMode = .time
            break
        case 3:
            datePicker.datePickerMode = .date
            break
        default:
            datePicker.datePickerMode = .dateAndTime
            break
        }
        
        dateFormatter.locale = NSLocale.current
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateStyle = dateStyle ?? .short
        
    }
    
    @objc func touchAction(_ sender: UITapGestureRecognizer){
        dismiss(animated: true)
    }
    
    @IBAction func selectBtnClicked(_ sender: Any) {
//        onSelectedDateText?(formattedDate)
//        onSelectedDate?(datePicker.date)
        
        switch tag {
        case 1:
            onSelectedDateText?(formattedDate, tag)
            break
        case 2:
            onSelectedDateText?(formattedTime, tag)
            break
        default:
            onSelectedDate?(datePicker.date)
            break
        }
        
        dismiss(animated: true)
    }
    
    
    @IBAction func closeBtnClicked(_ sender: Any) {
        dismiss(animated: true)
    }
}
