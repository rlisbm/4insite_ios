//
//  SentViewController.swift
//  Manage
//
//  Created by Rong Li on 2/11/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit
import RealmSwift

class SentViewController: BaseViewController {

    let realmDB: RealmDB = RealmDB.shared
    
    @IBOutlet weak var headerView: HeaderView!
    
    @IBOutlet weak var tableView: UITableView!
    
    var storedSentItems: [SubmitItemObject]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        storedSentItems = self.realmDB.getObjects(type: SubmitItemObject.self)?.toArray(ofType: SubmitItemObject.self) as! [SubmitItemObject]
        
        print("########## count: \(storedSentItems?.count)")
        print("########## count: \(((SubmitItemObject)(value: storedSentItems![0])).urlString)")
        
        initViews()
    }
    
    fileprivate func initViews(){
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_sent", comment: ""))
    }
    
//    func contextualToggleFlagAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
//        // 1
//        var email = data[indexPath.row]
//        // 2
//        let action = UIContextualAction(style: .normal,
//                                        title: "Flag") { (contextAction: UIContextualAction, sourceView: UIView, completionHandler: (Bool) -> Void) in
//                                            // 3
//                                            if email.toggleFlaggedFlag() {
//                                                // 4
//                                                self.data[indexPath.row] = email
//                                                self.tableView.reloadRows(at: [indexPath], with: .none)
//                                                // 5
//                                                completionHandler(true)
//                                            } else {
//                                                // 6
//                                                completionHandler(false)
//                                            }
//        }
//        // 7
//        action.image = UIImage(named: "flag")
//        action.backgroundColor = email.isFlagged ? UIColor.gray : UIColor.orange
//        return action
//    }
}

extension SentViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.storedSentItems?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed(StoredSentItemCell.identifier, owner: self, options: nil)?.first as! StoredSentItemCell

        cell.title.text = storedSentItems?[indexPath.row].urlString
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView( _ tableView: UITableView, commit edititingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath){
        if(edititingStyle == .delete){
            self.realmDB.deleteObject(storedSentItems![indexPath.row] )
            
            storedSentItems?.remove(at: indexPath.row)
            
            tableView.reloadData()
        }
    }
    
//    func tableView(_ tableView: UITableView,
//                   leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
//        -> UISwipeActionsConfiguration{
//
//            let deleteAction = self.contextualDeleteAction(forRowAtIndexPath: indexPath)
//            let flagAction = self.contextualToggleFlagAction(forRowAtIndexPath: indexPath)
//            let swipeConfig = UISwipeActionsConfiguration(actions: [deleteAction, flagAction])
//            return swipeConfig
//    }
//    func tableView(_ tableView: UITableView,
//                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
//        -> UISwipeActionsConfiguration{
//
//    }
    
}

extension Results {
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for i in 0 ..< count {
            if let result = self[i] as? T {
                array.append(result)
            }
        }
        return array
    }
}
