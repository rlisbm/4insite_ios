//
//  TabBarControllerViewController.swift
//  Manage
//
//  Created by Rong Li on 3/14/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    var remoteManager: RemoteManager?
    var isSuccess: Bool?
    var isCompletion: Bool?
    var emplpyeePopupViewController: EmployeePopupViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
    }
    
    func setEmplpyeePopupViewController(viewController: EmployeePopupViewController){
        emplpyeePopupViewController = viewController
    }
    
    func goSideMenuViewController(){
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let sideMenuVC = storyboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
        sideMenuVC.remoteManager = remoteManager
        self.view.addSubview(sideMenuVC.view)
        self.addChildViewController(sideMenuVC)
        sideMenuVC.view.layoutIfNeeded()
        
        sideMenuVC.view.frame=CGRect(x: 0 + UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            sideMenuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        }, completion:nil)
    }
}
extension TabBarController: UITabBarControllerDelegate{
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
    }
    func tabBarController(_ tabBarController: UITabBarController,shouldSelect viewController: UIViewController) -> Bool {
        if let index = tabBarController.viewControllers!.index(of:viewController) , index != 0 {
            if(emplpyeePopupViewController != nil){
                emplpyeePopupViewController?.dismiss(animated: true)
            }
        }
        
        if let index = tabBarController.viewControllers!.index(of:viewController) , index == 3 {
            goSideMenuViewController()
            return false
        }
        return true
    }
}
