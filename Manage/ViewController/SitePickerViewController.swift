//
//  SitePickerViewController.swift
//  Manage
//
//  Created by Rong Li on 10/30/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
import RealmSwift
import Realm

protocol ShowSitePickerDelegate: class{
    func showSitePicker()
}


class SitePickerViewController: BaseViewController {
    var selectedOne: SitePickerSelectedSite?

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var clientDropDown: DropDown!
    @IBOutlet weak var siteDropDown: DropDown!
    @IBOutlet weak var programDropDown: DropDown!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    
    var names = [String]()
    var ids = [Int]()
    var siteAry = [Site]()

    var list = [KeyValue1]()
    var siteDic: Dictionary = [Int:[Site]]()
    var siteIdDic: Dictionary = [Int:[Int]]()
    var siteNameDic: Dictionary = [Int:[String]]()
    var programIdDic: Dictionary = [Int:[Int]]()
    var programNameDic: Dictionary = [Int:[String]]()
    
    var selectedClientId: Int = -1
    var selectedSiteInfo: Site?
    var selectedProgramId: Int = -1
    
    var selectedClientIndex: Int = -1
    var selectedSiteIndex: Int = -1
    var selectedProgramIndex: Int = -1
    
    var selectedClient: KeyValue1?
    var selectedSite: KeyValue1?
    var selectedProgram: KeyValue1?
    
    
    var isClientOpen: Bool = false
    var isSiteOpen: Bool = false
    var isProgramOpen: Bool = false
    
    var fetchIndex: Int = 0
    var siteIds = [Int]()
    
    let clientText = NSLocalizedString("key_client", comment: "")
    let siteText = NSLocalizedString("key_site", comment: "")
    let programText = NSLocalizedString("key_program", comment: "")
    
    var isLaunchTabbarController = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(refreshTokenNotificationName != nil){
            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
        }else{
            initViews()
            
            if(AssociateHelper.get()?.employeeId == nil){
                fetchAssociateInfoNotificationName = Notification.Name(rawValue: fetchAssocateInfoNotificationKey)
                NotificationCenter.default.addObserver(self, selector: #selector(fetchAssociateInfoCompletion(notification:)), name: fetchAssociateInfoNotificationName, object: nil)
            }else{
                fetchClients()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(refreshTokenNotificationName != nil || fetchAssociateInfoNotificationName != nil ){
            NotificationCenter.default.removeObserver(self)
        }
    }
    
    func initViews(){
        progressIndicator.isHidden = true
        
        selectedOne = SelectedSiteHelper.get()
        
        isLaunchTabbarController = selectedOne == nil ? true : false
        
        name.text = AssociateHelper.get()?.firstName
        
        clientDropDown.setDropDown(rowBackgroundColor: Constants.dropDownRowBG, selectedRowColor: Constants.dropDownRowBG, rowTextColor: Constants.dropDownTextColor, rowHeight: Constants.drowDownRowHeight);
        siteDropDown.setDropDown(rowBackgroundColor: Constants.dropDownRowBG, selectedRowColor: Constants.dropDownRowBG, rowTextColor: Constants.dropDownTextColor, rowHeight: Constants.drowDownRowHeight);
        programDropDown.setDropDown(rowBackgroundColor: Constants.dropDownRowBG, selectedRowColor: Constants.dropDownRowBG, rowTextColor: Constants.dropDownTextColor, rowHeight: Constants.drowDownRowHeight);
        
        clientDropDown.isUserInteractionEnabled = false
        clientDropDown.didSelect{(selectedText , index , id) in
            print("Selected String: \(selectedText) \n index: \(index) \n Id: \(id)")
            self.selectedClient = KeyValue1(Key: id, Value: selectedText)
            self.selectedClientIndex = index
            self.selectedClientId = id
            self.updateSiteDropDown(selectedClientId: id, isResetTitle: true)
            self.updateProgramDropDown(selectedSiteId: nil, isResetTitle: true) // nil: indicate programDropDown needs to be reset after siteDropDown seletced
            self.isClientOpen = false
        }
        siteDropDown.isUserInteractionEnabled = false
        siteDropDown.didSelect{(selectedText , index , id) in
            print("Selected String: \(selectedText) \n index: \(index) \n Id: \(id)")
            self.selectedSite = KeyValue1(Key: id, Value: selectedText)
            self.selectedSiteIndex = index
            self.selectedSiteInfo = self.siteDic[self.selectedClientId]?[index]
            self.updateProgramDropDown(selectedSiteId: id, isResetTitle: true)
            self.isSiteOpen = false
        }
        programDropDown.isUserInteractionEnabled = false
        programDropDown.didSelect{(selectedText , index , id) in
            print("Selected String: \(selectedText) \n index: \(index) \n Id: \(id)")
            self.selectedProgram = KeyValue1(Key: id, Value: selectedText)
            self.selectedProgramIndex = index
            self.selectedProgramId = id
            self.isProgramOpen = false
        }
        
        let clientGesture = UITapGestureRecognizer(target: self, action:  #selector (self.clientDropDownAction(sender:)))
        clientDropDown.addGestureRecognizer(clientGesture)
        let siteGesture = UITapGestureRecognizer(target: self, action:  #selector (self.siteDropDownAction(sender:)))
        siteDropDown.addGestureRecognizer(siteGesture)
        let programGesture = UITapGestureRecognizer(target: self, action:  #selector (self.programDropDownAction(sender:)))
        programDropDown.addGestureRecognizer(programGesture)
        
    }
    
    @objc func refreshTokenCompletion(notification: NSNotification){
        if(fetchAssociateInfoNotificationName == nil){
            fetchClients()
        }
    }
    
    @objc func fetchAssociateInfoCompletion(notification: NSNotification){
        fetchClients()
    }
    
    @objc func clientDropDownAction(sender: UITapGestureRecognizer){

        if(isSiteOpen){
            isSiteOpen = !isSiteOpen
            siteDropDown.touchAction()
            return
        }
        if(isProgramOpen){
            isProgramOpen = !isProgramOpen
            programDropDown.touchAction()
            return
        }
        isClientOpen = !isClientOpen
        if(selectedOne != nil && clientDropDown.selectedIndex == nil){
            clientDropDown.selectedIndex = selectedClientIndex
        }
        clientDropDown.touchAction()
        
    }
    @objc func siteDropDownAction(sender: UITapGestureRecognizer){
        if(isClientOpen){
            isClientOpen = !isClientOpen
            clientDropDown.touchAction()
            return
        }
        if(isProgramOpen){
            isProgramOpen = !isProgramOpen
            programDropDown.touchAction()
            return
        }
        
        if(selectedOne != nil && siteDropDown.selectedIndex == nil){
            siteDropDown.selectedIndex = selectedSiteIndex
        }
        
        siteDropDown.touchAction()
        isSiteOpen = !isSiteOpen
        
    }
    
    @objc func programDropDownAction(sender: UITapGestureRecognizer){
        if(isClientOpen){
            isClientOpen = !isClientOpen
            clientDropDown.touchAction()
            return
        }
        if(isSiteOpen){
            isSiteOpen = !isSiteOpen
            siteDropDown.touchAction()
            return
        }
        programDropDown.touchAction()
        isProgramOpen = !isProgramOpen
    }
    
    @IBAction func selectSiteClicked(_ sender: Any) {
        
        if(clientDropDown.text == clientText || siteDropDown.text == siteText || programDropDown.text == programText){
            showAlert(NSLocalizedString("key_infomation", comment: ""), NSLocalizedString("key_select_site", comment: ""), NSLocalizedString("key_dismiss", comment: ""), viewController: self)
            return
        }
        
        SelectedSiteInfoHelper.save(selectedSiteInfo)
        selectedOne = SitePickerSelectedSite(selectedClientId: selectedClient!.Key, selectedClientName: selectedClient!.Value!,
                                   selectedSiteId: selectedSite!.Key, selectedSiteName: selectedSite!.Value!,
                                   selectedProgramId: selectedProgram!.Key, selectedProgramName: selectedProgram!.Value!)
        SelectedSiteHelper.save(selectedOne)
        
        if(isLaunchTabbarController){
            goTabBarController(isCompletion: false, isSuccess: false, tabIndex: 0)
        }else{
            dismiss(animated: true)
        }
        
//        goToLandingViewController()
    }
    
    
//    func goToLandingViewController(){
//        print("### ### goToLandingViewController")
//
//        let sitePickerVC = storyboard?.instantiateViewController(withIdentifier: "LandingViewController") as! LandingViewController
//        sitePickerVC.remoteManager = remoteManager
//        present(sitePickerVC, animated: true, completion: nil)
//    }
    
    func updateClientDropDown(){
        clientDropDown.text = clientText
        clientDropDown.optionArray = names
        clientDropDown.optionIds = ids
        
        if(selectedOne != nil){
            if let selectedIndex = ids.index(where: {$0 == selectedOne!.selectedClientId}){
//                let indexPath:IndexPath = IndexPath(row: selectedClientIndex, section: 0);
//                clientDropDown.table.selectRow(at: indexPath, animated: true, scrollPosition: UITableViewScrollPosition.none)
                selectedClientIndex = selectedIndex
                selectedClientId = ids[selectedClientIndex]
                let value = names[selectedClientIndex]
                selectedClient =  KeyValue1(Key: selectedClientId, Value: value)
                clientDropDown.text = names[selectedClientIndex]
            }
        }
        

    }
    
    func updateSiteDropDown(selectedClientId: Int, isResetTitle: Bool){
        siteDropDown.text = siteText
        siteDropDown.selectedIndex = nil
        siteDropDown.optionArray = siteNameDic[selectedClientId]!
        siteDropDown.optionIds = siteIdDic[selectedClientId]!
        
        if(!isResetTitle){
            if let selectedIndex = siteDropDown.optionIds?.index(where: {$0 == selectedOne!.selectedSiteId}){
                selectedSiteIndex = selectedIndex
                selectedSiteInfo = self.siteDic[self.selectedClientId]?[selectedSiteIndex]
                selectedSite =  KeyValue1(Key: selectedSiteInfo?.Id, Value: selectedSiteInfo?.Name)
                siteDropDown.text = siteDropDown.optionArray[selectedSiteIndex]
            }
        }
    }
    
    func updateProgramDropDown(selectedSiteId: Int?, isResetTitle: Bool){
        programDropDown.text = programText
        programDropDown.selectedIndex = nil
        // id == nil: after selected clents has been changed.  programDropDown needs to be reset after siteDropDown re-selected
        if(selectedSiteId == nil || programNameDic[selectedSiteId!] == nil || programNameDic[selectedSiteId!]?.count == 0){
            programDropDown.optionArray = [String]()
            programDropDown.optionIds = nil
            return
        }
        
        programDropDown.optionArray = programNameDic[selectedSiteId!]!
        programDropDown.optionIds = programIdDic[selectedSiteId!]!
        
        if(!isResetTitle){
            if let selectedIndex = programDropDown.optionIds?.index(where: {$0 == selectedOne!.selectedProgramId}){
                selectedProgramIndex = selectedIndex
                selectedProgramId = (programDropDown.optionIds?[selectedProgramIndex])!
                let value = programDropDown.optionArray[selectedProgramIndex]
                selectedProgram =  KeyValue1(Key: selectedProgramId, Value: value)
                programDropDown.text = value
            }
        }
    }

    func fetchClients(){
        progressIndicator.isHidden = false
        
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetche ClientList")
        }
        remoteManager!.fetchDataWithDate(remoteManager: remoteManager!, apiRouter: APIRouter.clients, completion: fetchClientsCompletion)
    }

    fileprivate func fetchClientsCompletion(_ data: Data?, _ error: Error?, _ responseCode: Int?, date: Date?){
        if(responseCode == Constants.ResponseFailed401){
            refreshTokenNotificationName = Notification.Name(rawValue: refreshTokenNotificationKey)
            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
            refreshToken()
            return
        }
        
        let jsonDecoder = JSONDecoder()
        guard let clients = try? jsonDecoder.decode(Client.self, from: data!) else{
            //TODO: handle error - failed to fetch data from server (204, 4xx, 5xx) and failed to fetch data from local database (it could be the first time to launch the app)
            return
        }
        
        //if success, fetch from server, otherwise fetch from database
        //empty data could happen on the very first time failed fetch from server.
        clientDropDown.isUserInteractionEnabled = true
        list.removeAll()
        list = (clients.Clients?.Options)!
        for item in list{
            ids.append(item.Key!)
            names.append(item.Value!)
        }
        updateClientDropDown()
        fetchIndex = 0;
        fetchSites(clientId: ids[fetchIndex])
    }
    
    
    fileprivate func fetchSites(clientId: Int){
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetche SiteList")
        }
        remoteManager!.fetchDataWithDate(remoteManager: remoteManager!, id: clientId, apiRouter: APIRouter.sites, completion: fetchSitesCompletion)
    }
    
    fileprivate func fetchSitesCompletion(_ data: Data?, _ error: Error?, _ responseCode: Int?, date: Date?){
        let jsonDecoder = JSONDecoder()
        guard let sites = try? jsonDecoder.decode([Site].self, from: data!) else{
            //TODO: handle error - failed to fetch data from server (204, 4xx, 5xx) and failed to fetch data from local database (it could be the first time to launch the app)
            return
        }
        print(sites)
        siteDropDown.isUserInteractionEnabled = true
        if(sites.count == 0){
            fetchIndex = fetchIndex + 1;
            if(fetchIndex < clientDropDown.optionIds!.count){
                fetchSites(clientId: clientDropDown.optionIds![fetchIndex])
            }
            return
        }
        ids.removeAll()
        names.removeAll()
        siteAry.removeAll()
        for site in sites {
            ids.append(site.Id!)
            names.append(site.Name!)
            siteAry.append(site)
        }
        
        siteDic[clientDropDown.optionIds![fetchIndex]] = siteAry
        siteIdDic[clientDropDown.optionIds![fetchIndex]] = ids
        siteNameDic[clientDropDown.optionIds![fetchIndex]] = names
        fetchIndex = fetchIndex + 1;
        if(fetchIndex < clientDropDown.optionIds!.count){
            fetchSites(clientId: clientDropDown.optionIds![fetchIndex])
        }else{
            siteDropDown.text = siteText
            for (key, list) in siteIdDic {
                for item in list {
                    siteIds.append(item)
                }
            }
            
            if(selectedOne != nil){
                self.updateSiteDropDown(selectedClientId: selectedClientId, isResetTitle: false)
            }
            
            fetchIndex = 0
            
            fetchPrograms(siteId: siteIds[fetchIndex])
        }
    }
    
    fileprivate func fetchPrograms(siteId: Int){
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetche ProgramList")
        }
        remoteManager!.fetchDataWithDate(remoteManager: remoteManager!, id: siteId, apiRouter: APIRouter.programs, completion: fetchProgramsCompletion)
    }
    fileprivate func fetchProgramsCompletion(_ data: Data?, _ error: Error?, _ responseCode: Int?, date: Date?){
        let jsonDecoder = JSONDecoder()
        guard let programs = try? jsonDecoder.decode([IdName].self, from: data!) else{
            //TODO: handle error - failed to fetch data from server (204, 4xx, 5xx) and failed to fetch data from local database (it could be the first time to launch the app)
            return
        }
        print(programs)
        
        progressIndicator.isHidden = true
        programDropDown.isUserInteractionEnabled = true
        
        ids.removeAll()
        names.removeAll()
        for program in programs {
            if(program.Name != nil){
                ids.append(program.Id!)
                names.append(program.Name!)
            }
        }
        
        programIdDic[siteIds[fetchIndex]] = ids
        programNameDic[siteIds[fetchIndex]] = names
        fetchIndex = fetchIndex + 1;
        if(fetchIndex < siteIds.count){
            fetchPrograms(siteId: siteIds[fetchIndex])
        }else{
            programDropDown.text = programText
            if(selectedOne != nil){
                self.updateProgramDropDown(selectedSiteId: (selectedSiteInfo?.Id)!, isResetTitle: false)
            }
        }
    }
}
