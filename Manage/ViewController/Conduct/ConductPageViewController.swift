//
//  ConductPageViewController.swift
//  Manage
//
//  Created by Rong Li on 12/17/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class ConductPageViewController: BasePageViewController {
    
//    lazy var vcs: [UIViewController] = {
//        return [UIStoryboard(name: "Conduct", bundle: nil).instantiateViewController(withIdentifier: "FirstConductViewController") as! FirstConductViewController,
//                UIStoryboard(name: "Conduct", bundle: nil).instantiateViewController(withIdentifier: "SecondConductViewController") as! SecondConductViewController]
//    }()
    
    
    var VC1 : FirstConductViewController?
    var VC2 : SecondConductViewController?
    
    var conduct = Conduct()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        self.dataSource = self
        
        
        VC1 = storyboard?.instantiateViewController(withIdentifier: "FirstConductViewController") as? FirstConductViewController
        VC2 = storyboard?.instantiateViewController(withIdentifier: "SecondConductViewController") as? SecondConductViewController
        
        VCs.append(VC1!)
        VCs.append(VC2!)
        
        VC1?.remoteManager = remoteManager
        VC2?.remoteManager = remoteManager
        
        VC1?.selectedEmployee = selectedEmployee
        VC2?.selectedEmployee = selectedEmployee
        
        VC1?.conduct = conduct
        VC2?.conduct = conduct
        
        setViewControllers([VCs[0]], direction: .forward, animated: true, completion: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func setSelectedEmployee(selectedEmployee: TeamMemberV2){
        VC2?.selectedEmployee = selectedEmployee
    }
    
    func setSelectedDate(selectedDate: Date){
        VC2?.selectedDate = selectedDate
    }
    
    func isReadyMoveNext(currentIndex: Int) -> Bool {
//        switch currentIndex {
//        case 0:
//            return VC1?.isReadyMoveNext() ?? false
//        default:
//            return false
//        }
        
        return true
    }
    
    //MARK: UIPageViewControllerDataSource, UIPageViewControllerDelegate
    override func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = VCs.index(of: viewController) ?? 0
        
        if(!isReadyMoveNext(currentIndex: currentIndex)){
            return nil
        }
        
        if(currentIndex >= VCs.count - 1){
            return nil
        }
        return VCs[currentIndex + 1]
    }

}
