//
//  FirstConductViewController.swift
//  Manage
//
//  Created by Rong Li on 12/17/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class FirstConductViewController: KeyboardBaseViewController {
    let verbalWarningKey = 1
    let writtenWarningKey = 2
    let finalWarningKey = 3
    let administrativeLeaveKey = 4
    let correctiveActionKey = 5
    
    let currentPageIndex = 0
    
    var conduct: Conduct?
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: TwoDotsView!
    @IBOutlet weak var assignToView: AssignToOneView!
    @IBOutlet weak var verbalWarningBtn: UIButton!
    @IBOutlet weak var writtenWarningBtn: UIButton!
    @IBOutlet weak var finalWarningBtn: UIButton!
    @IBOutlet weak var correctiveActionBtn: UIButton!
    @IBOutlet weak var administrativeLeaveBtn: UIButton!
    
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initViews()
        
        //Listen for keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidHide, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidChangeFrame, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    fileprivate func initViews(){
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_conduct", comment: ""))
        
        let dots = [dotsView.dot1, dotsView.dot2]
        initDotsView(dots: dots as! [UIButton], selectedIndex: currentPageIndex)
        
//        initAssignToOneView(assignToView: assignToView, featureName: FeatureName.Conduct)
        initAssignToOneView(assignToView: assignToView)
        assignToView.onEditBtnClicked = {
            self.popupSelectEmployeeViewController()
        }
        
        textView.layer.masksToBounds = true
        textView.layer.borderColor = UIColor.init(hexString: "#8785A1").cgColor
        textView.layer.borderWidth = 1
        
        textView.delegate = self
        textView.text = NSLocalizedString("key_description", comment: "")
        textView.textColor = UIColor.init(hexString: Constants.textDarkWhite)
        
        dateTextField.delegate = self
        dateTextField.layer.masksToBounds = true
        dateTextField.layer.borderColor = UIColor.init(hexString: "#8785A1").cgColor
        dateTextField.layer.borderWidth = 1
        
        
        //calendarBtn: var calendarBtn  = UIButton(type: .custom)
        calendarBtn.frame = CGRect(x:0, y:0, width:30, height:30)
        calendarBtn.addTarget(self, action: #selector(self.popupDatePickerBtnClicked), for: .touchUpInside)
        calendarBtn.setImage(UIImage(named: "Calendar-icon.png"), for: .normal)
        calendarBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
        dateTextField.rightViewMode = .always
        dateTextField.rightView = calendarBtn
    }
    
    fileprivate func onConfirmBtnClicked(_ selectedTeam: [TeamMemberV2]) {
        selectedEmployee = selectedTeam[0]
        initAssignToOneView(assignToView: assignToView)
    }
    
    fileprivate func popupSelectEmployeeViewController(){
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SelectEmployeeViewController") as! SelectEmployeeViewController
        viewController.remoteManager = remoteManager
        viewController.featureName = FeatureName.Conduct
        viewController.isEdit = true
        viewController.selectedEmployee = selectedEmployee
        viewController.onConfirmBtnClicked = onConfirmBtnClicked
        
        viewController.modalTransitionStyle = .crossDissolve
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: true, completion: nil)
    }
    
    fileprivate func onSelectedDate(_ date: Date){
        selectedDate = date
        dateTextField.text = selectedDate!.formattedDateShort
    }
    
    @objc func popupDatePickerBtnClicked(sender:UITapGestureRecognizer) {
        let storyboard =  UIStoryboard(name: "Popup", bundle: nil)
        let viewControler = storyboard.instantiateViewController(withIdentifier: "DatePickerPopupViewController") as! DatePickerPopupViewController
        viewControler.onSelectedDate = onSelectedDate
        present(viewControler, animated: true, completion: nil)
    }
    
    fileprivate func setBtnTag(sender: UIButton){
        if(sender.tag == 0){
            sender.tag = 1
            sender.backgroundColor = UIColor.init(hexString: Constants.btnLightGrayBG)
            sender.setTitleColor(UIColor.init(hexString: Constants.textBlack), for: .normal)
        }else{
            sender.tag = 0
            sender.backgroundColor = UIColor.init(hexString: Constants.btnDarkGrayBG)
            sender.setTitleColor(UIColor.init(hexString: Constants.btnLightGrayBG), for: .normal)
        }
        
    }
    
    func isReadyMoveNext() -> Bool {
        if(selectedEmployee == nil
            || selectedDate == nil
            || textView.text.isEmpty || textView.text == NSLocalizedString("key_description", comment: "")
            || (verbalWarningBtn.tag == 0 && writtenWarningBtn.tag == 0 && finalWarningBtn.tag == 0 && correctiveActionBtn.tag == 0 && administrativeLeaveBtn.tag == 0)){
            return false
        }
        
        let pageController = self.parent as! ConductPageViewController
        pageController.setSelectedEmployee(selectedEmployee: selectedEmployee!)
        pageController.setSelectedDate(selectedDate: selectedDate!)
        
        creatConduct()
        
        return true
    }
    
    fileprivate func creatConduct(){
        conduct?.orgUserId = (self.selectedEmployee?.EmployeeId)!
        conduct?.dateOfOccurrence = selectedDate!.formateDateUTC(date: selectedDate!, withFormat: Constants.DATE_UTC_SS)
        conduct?.reason = textView.text
        conduct?.conductTypes.removeAll()
        if(verbalWarningBtn.tag == 1){
            conduct?.conductTypes.append(ConductType(conductTypeId: verbalWarningKey, name: verbalWarningBtn.title(for: .normal), isSelected: true))
        }
        if(writtenWarningBtn.tag == 1){
            conduct?.conductTypes.append(ConductType(conductTypeId: writtenWarningKey, name: writtenWarningBtn.title(for: .normal), isSelected: true))
        }
        if(finalWarningBtn.tag == 1){
            conduct?.conductTypes.append(ConductType(conductTypeId: finalWarningKey, name: finalWarningBtn.title(for: .normal), isSelected: true))
        }
        if(administrativeLeaveBtn.tag == 1){
            conduct?.conductTypes.append(ConductType(conductTypeId: administrativeLeaveKey, name: administrativeLeaveBtn.title(for: .normal), isSelected: true))
        }
        if(correctiveActionBtn.tag == 1){
            conduct?.conductTypes.append(ConductType(conductTypeId: correctiveActionKey, name: correctiveActionBtn.title(for: .normal), isSelected: true))
        }
    }
    
    @IBAction func arrivedLateBtnClicked(_ sender: UIButton) {
        setBtnTag(sender: sender)
    }
    
    @IBAction func writtenWarningBtnClicked(_ sender: UIButton) {
        setBtnTag(sender: sender)
    }
    
    @IBAction func finalWarningBtnClicked(_ sender: UIButton) {
        setBtnTag(sender: sender)
    }
    
    @IBAction func correctiveActionBtnClicked(_ sender: UIButton) {
        setBtnTag(sender: sender)
    }
    
    @IBAction func administrativeLeaveBtnClicked(_ sender: UIButton) {
        setBtnTag(sender: sender)
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {

        if(!isReadyMoveNext()){
            showAlert(NSLocalizedString("key_infomation", comment: ""), NSLocalizedString("key_provide_input", comment: ""), NSLocalizedString("key_dismiss", comment: ""), viewController: self)
            return
        }
        
        let pageController = self.parent as! ConductPageViewController
        pageController.onNextBtnClicked(currentIndex: currentPageIndex)
        
    }
    
    //Touch screen outside textview to dismiss the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        textView.resignFirstResponder()
    }
    
    //MARK: UITextFeildDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == dateTextField {
            return false;
        }
        return true
    }
    
}

