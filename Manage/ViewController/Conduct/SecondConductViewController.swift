//
//  SecondViewController.swift
//  Manage
//
//  Created by Rong Li on 12/17/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class SecondConductViewController: BaseViewController {
    let currentPageIndex = 1
    
    var conduct: Conduct?
    
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: TwoDotsView!
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var descriptionContent: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()
    }
    
    fileprivate func initViews(){
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_conduct", comment: ""))
        
        let dots = [dotsView.dot1, dotsView.dot2]
        initDotsView(dots: dots as! [UIButton], selectedIndex: currentPageIndex)
        
        descriptionContent.isEditable = false
        descriptionContent.isScrollEnabled = true
    }
    
    fileprivate func updateViews(){
        var fullName = ""
        fullName.append(self.selectedEmployee?.FirstName ?? "")
        fullName.append(" ")
        fullName.append(self.selectedEmployee?.LastName ?? "")
        name.text = fullName
        date.text = selectedDate?.formattedDateShort
        if(conduct?.conductTypes != nil && conduct?.conductTypes.count ?? 0 > 0){
            var typeString = ""
            var index: Int = 0
            while (index < (conduct?.conductTypes.count)!){
                typeString.append(conduct?.conductTypes[index].Name ?? "")
                index += 1
                if(index < (conduct?.conductTypes.count)!){
                    typeString.append(", ")
                }
            }
            
           
            type.text = typeString
        }
        descriptionContent.text = conduct?.reason
    }
    
    fileprivate func submitConduct(){
        print("*** *** submitConduct")
        if(remoteManager == nil){
            fatalError("Missing dependencies on submitConduct")
        }
        
        remoteManager!.submitConduct(remoteManager: remoteManager!, conduct: conduct!, completion: submitCompletion)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    @IBAction func editBtnClicked(_ sender: Any) {
        let pageController = self.parent as! ConductPageViewController
        pageController.onBackBtnClicked(currentIndex: currentPageIndex)
    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        conduct?.conductTypes.append(ConductType(conductTypeId: 1, name: "Verbal Warning", isSelected: true))
        conduct?.dateOfOccurrence = "2019-01-18T17:18:37"
        conduct?.isExempt = false
        conduct?.orgUserId = 176920
        conduct?.reason = "tested by rli"
        submitConduct()
    }
    
}


