//
//  RateAuditViewController.swift
//  Manage
//
//  Created by Rong Li on 1/31/19.
//  Copyright © 2019 Insite. All rights reserved.
//
import Foundation
import UIKit

class RateAuditPhotoCollectionCell: UICollectionViewCell{
    @IBOutlet weak var photo: UIImageView!
    
    var onDeletePhotoBtnClicked: (() -> Void)?
    
    @IBAction func deletePhotoBtnClicked(_ sender: Any) {
        onDeletePhotoBtnClicked?()
    }
}

class RateAuditViewController: BaseViewController {
    //class RateAuditViewController: KeyboardBaseViewController {
    let notApplicableText = NSLocalizedString("key_n_a", comment: "")
    var maxScore: Double = 5
    var minScore: Double = 1
    var delta: Double = 1
    var score: Double = 0
    var meetScore: Double = 4
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var rateHeaderView: RateHeaderView!
    @IBOutlet weak var rateView: RateView!
    @IBOutlet weak var textView: UITextView!
    
    
    @IBOutlet weak var collectionSectionView: SectionHeaderView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableSectionView: SectionHeaderView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var tableSectionViewTop: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    //    @IBOutlet weak var scrollContentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollContentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    
    var pageIndex: Int?
    var indexPath: IndexPath?
    var scorecardDetail: ScorecardDetail?
    var scorecardDetails: [ScorecardDetail]?
    
    
    var tableSectionViewTopWithPhoto: CGFloat?
    let tableSectionViewTopNoPhoto: CGFloat = 8
    var scrollContentViewOriginalHeight: CGFloat?
    
    var images: [UIImage] = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        if(refreshTokenNotificationName != nil){
        //            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
        //        }else{
        //            initViews()
        //
        //            if(AssociateHelper.get()?.employeeId == nil){
        //                fetchAssociateInfoNotificationName = Notification.Name(rawValue: fetchAssocateInfoNotificationKey)
        //                NotificationCenter.default.addObserver(self, selector: #selector(fetchAssociateInfoCompletion(notification:)), name: fetchAssociateInfoNotificationName, object: nil)
        //            }else{
        //
        //            }
        //        }
        
        if(scorecardDetail == nil){
            //TODO: without scorecardDetails, it can not continue
            return
        }
        
        scrollContentViewOriginalHeight = scrollContentViewHeight.constant
        
        initViews()
        
        rateView.progress.setMaxScore(maxScore: maxScore)
        
        //        //Listen for keyboard events
        //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)
    }
    @objc func refreshTokenCompletion(notification: NSNotification){
        print("### ### RateAuditViewController refreshTokenCompletion")
        submitScorecardDetails()
    }
    
    //    @objc func fetchAssociateInfoCompletion(notification: NSNotification){
    //        print("### ### RateAuditViewController fetchAssociateInfoCompletion")
    //    }
    
    fileprivate func initViews(){
        headerView.screenName.text = NSLocalizedString("key_rate_score", comment: "")
        headerView.onCloseBtnClicked = {
            self.dismiss(animated: true)
        }
        
        rateHeaderView.auditInspectionItemName.text = scorecardDetail?.AuditInspectionItemName
        rateHeaderView.onNextBtnClicked = {
            self.onNextArrowClicked()
        }
        
        rateHeaderView.onPreviousBtnClicked = {
            self.onPreviousArrowClicked()
        }
        
        rateView.rate.text = notApplicableText
        rateView.onPlusClicked = {
            self.onPlusClicked()
        }
        rateView.onMinusClicked = {
            self.onMinusClicked()
        }
        rateView.onPassMeetGoalClicked = {
            self.onPassMeetGoalClicked()
        }
        
        textView.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        
        collectionSectionView.title.text = "photo"
        collectionSectionView.image.image = UIImage(named: "attach-icon.png")
        tableSectionView.title.text = "todos"
        tableSectionView.image.image = UIImage(named: "Add Todos - Audits.png")
        
        tableSectionViewTopWithPhoto = tableSectionViewTop.constant
        
        if(scorecardDetail?.ScorecardDetailImages?.count == 0){
            collectionView.isHidden = true
            tableSectionViewTop.constant = 8
        }
        
        tableView.separatorStyle = .none
        if(scorecardDetail?.TodoItems?.count == 0){
            tableView.isHidden = true
        }
        
        let photoTappedGesture = UITapGestureRecognizer(target: self, action:  #selector (self.addPhoto(sender:)))
        collectionSectionView.addGestureRecognizer(photoTappedGesture)
        
        let todoTappedGesture = UITapGestureRecognizer(target: self, action:  #selector (self.addTodo(sender:)))
        tableSectionView.addGestureRecognizer(todoTappedGesture)
        
    }
    
    fileprivate func updateCollectionView(image: UIImage?, indexPath: IndexPath?){
        if(image != nil){
            images.append(image!)
            collectionView.isHidden = false
            tableSectionViewTop.constant = self.tableSectionViewTopWithPhoto!
            if(images.count == 1){
                scrollContentViewHeight.constant += collectionViewHeight.constant
            }
        }else if (indexPath != nil){
            images.remove(at: indexPath!.row)
            if(images.count == 0){
                collectionView.isHidden = true
                scrollContentViewHeight.constant -= collectionViewHeight.constant
                tableSectionViewTop.constant = tableSectionViewTopNoPhoto
            }
        }
        
        collectionView.reloadData()
    }
    
    @objc fileprivate func addPhoto(sender: UITapGestureRecognizer){
        ImagePickerManager().pickImage(self){ image in
            self.updateCollectionView(image: image, indexPath: nil)
        }
    }
    
    fileprivate func onTodoCreated(){
        if((scorecardDetail?.TodoItems!.count ?? 0) > 0){
            tableView.isHidden = false
        }else{
            tableView.isHidden = true
        }
        
        if((scorecardDetail?.TodoItems!.count ?? 1) > 1){
            scrollContentViewHeight.constant += tableView.estimatedRowHeight
        }
        
        tableView.reloadData()
    }
    
    @objc fileprivate func addTodo(sender: UITapGestureRecognizer){
        let storyboard =  UIStoryboard(name: "Audit", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AuditTodoViewController") as! AuditTodoViewController
        viewController.remoteManager = remoteManager
        viewController.selectedEmployees = selectedEmployees
        viewController.onTodoCreated = onTodoCreated
        viewController.scorecardDetail = scorecardDetail
        
        viewController.modalTransitionStyle = .crossDissolve
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: true, completion: nil)
        
    }
    fileprivate func onPassMeetGoalClicked(){
        
    }
    fileprivate func onMinusClicked(){
        if(rateView.rate.text != notApplicableText){
            rateView.rate.text = ((rateView.rate.text?.toDouble())! > minScore) ? String((rateView.rate.text?.toDouble())! - delta): notApplicableText
        }
    }
    fileprivate func onPlusClicked(){
        if (rateView.rate.text == notApplicableText){
            rateView.rate.text = String(delta)
        }else{
            if((rateView.rate.text?.toDouble())! < maxScore){
                rateView.rate.text = String((rateView.rate.text?.toDouble())! + delta)
            }
        }
    }
    
    fileprivate func onNextArrowClicked(){
        let pageController = self.parent as! RateAuditPageViewController
        pageController.onNextBtnClicked(currentIndex: pageIndex ?? 0)
    }
    
    fileprivate func onPreviousArrowClicked(){
        let pageController = self.parent as! RateAuditPageViewController
        pageController.onBackBtnClicked(currentIndex: pageIndex ?? 0)
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        onNextArrowClicked()
    }
    
    var onDoneBtnClicked: ((_ indexPath: IndexPath) -> ())?
    @IBAction func doneBtnClicked(_ sender: Any) {
        if(rateView.rate.text == notApplicableText){
            //TODO: Show alert
            return
        }
        
        scorecardDetail?.Score = Double(rateView.rate.text!)
//        scorecardDetails![(indexPath?.row)!].Score = 4
        submitScorecardDetails()
        
        //        onDoneBtnClicked?(indexPath!)
        //        dismiss(animated: true)
        
//        parseData()
    }
//    let jsonResponse = "[{\"Status\":1,\"Body\":{\"ScorecardDetailId\":2331985,\"ScorecardSectionId\":1293322,\"Score\":null,\"SourceLanguageId\":null,\"Comment\":null,\"AuditInspectionItemName\":\"Overall Appearance\",\"TodoItems\":[],\"ScorecardDetailImages\":[]},\"Message\":null}]"
//
//    let jsonData = "[{\"Status\":1, \"Message\":null}, {\"Status\":2, \"Message\":null}]"

//    func parseData(){
//        do{
//            let jsonDecoder = JSONDecoder()
//            let items = try? jsonDecoder.decode(Array<ResponseScorecardDetail>.self,
//                                                from: jsonData.data(using: .utf8)!)
//
//            print(items?.count)
//            print(items?[0].Status)
//        }catch{
//            print("*** *** error: \(error)")
//        }
//
//        guard let jsonArray = jsonData.data(using: .utf8)! as? [[String: Any]] else {
//            print("*************** am i here")
//            return
//        }
//        print(jsonArray.count)
//        print(jsonArray)
//    }
    
    fileprivate func submitScorecardDetails(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on submitScorecardDetails")
        }
        
        print("*************** rate \(scorecardDetail?.Score)")
        remoteManager!.submitItemArray(remoteManager: remoteManager!, items: scorecardDetails!, apiRouter: APIRouter.scoreCardDetail, completion: submitCompletion)
    }
    
    func submitCompletion(_ responseAny: Any?, _ error: Error?, _ responseCode: Int) {
        print("### ### *** responseCode \(responseCode)")
        if(responseCode == 401){
            refreshTokenNotificationName = Notification.Name(rawValue: refreshTokenNotificationKey)
            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
            refreshToken()
            return
        }
        
        guard let jsonArray = responseAny as? [[String: Any]] else {
            return
        }
        print("********** 1")
        print(jsonArray)
        print("********** 2 \(jsonArray.count)")
        guard let item = jsonArray[0]["Body"] as? [String: Any] else {
            print("********** 21")
            return
        }
        print(item)
        
//        json: [String: Any]
        
        for i in 0..<jsonArray.count {
            guard let data = jsonArray[i]["Body"] as? [String: Any] else {
                return
            }
            let scorecardDetail = ScorecardDetail(json: data)
            print("********** 31")
            print(scorecardDetail)
            scorecardDetails![i] = scorecardDetail
        }
        
        
        onDoneBtnClicked?(indexPath!)
        dismiss(animated: true)
    }
}

//@IBAction func doneBtnClicked(_ sender: Any) {
//            print("************** \(String(describing: scorecardDetails))")
//            print("\(scorecardDetails![0])")
//            do {
//                let jsonEncoder = JSONEncoder()
//                var jsonString: String?
//                let jsonData = try jsonEncoder.encode(scorecardDetails![0] as! ScorecardDetail)
//                jsonString = String(data: jsonData, encoding: .utf8)
//                print("************** \(jsonString)")
//                let parameters = jsonString?.convertToDictionary()
//                print("************** \(parameters)")
//            }catch {
//                print("\(error)")
//            }
//            print("**************")
//
//
//            var items: [TestScorecardDetail] = [TestScorecardDetail]()
//            items.append(TestScorecardDetail())
//            items.append(TestScorecardDetail())
//            items.append(TestScorecardDetail())
//            do{
//            let jsonEncoder = JSONEncoder()
//            let jsonData = try jsonEncoder.encode(items as! [TestScorecardDetail])
//            var jsonString = String(data: jsonData, encoding: .utf8)
//            print("************** \(jsonString)")
//                let parameters = jsonString!.convertToDictionary()
//                print(parameters)
//            }catch{
//                print("************** \(error)")
//            }
//
//            submitScorecardDetails()
//
//            onDoneBtnClicked?(indexPath!)
//            dismiss(animated: true)
//}

extension RateAuditViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RateAuditPhotoCollectionCell", for: indexPath) as! RateAuditPhotoCollectionCell
        cell.photo.image = images[indexPath.row]
        cell.onDeletePhotoBtnClicked = {
            self.updateCollectionView(image: nil, indexPath: indexPath)
        }
        
        return cell
    }
    
}

extension RateAuditViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        print("************* indexpath: \(scorecardDetail?.TodoItems?.count ?? 0)")
        return scorecardDetail?.TodoItems?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        print("************* indexpath: \(indexPath.row)")
        let cell = Bundle.main.loadNibNamed(RateAuditTodoTableCell.identifier, owner: self, options: nil)?.first as! RateAuditTodoTableCell
        let todo = scorecardDetail?.TodoItems![indexPath.row]
        
        cell.comment.text = todo?.Comment
        cell.buildingFloor.text = "hahaha"
        cell.due.text = todo?.DueDate
        
        if(todo?.TodoItemAttachments == nil || todo?.TodoItemAttachments?.count == 0){
            cell.badge.isHidden = true
        }else{
            cell.badge.isHidden = false
            cell.badge.text = String(todo?.TodoItemAttachments?.count ?? 0)
        }
        
        if(scorecardDetail?.TodoItems?.count == (indexPath.row + 1)){
            cell.separator.isHidden = true
        }
        
        return cell
    }
    
    
}
