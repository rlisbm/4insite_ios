//
//  FirstAuditViewController.swift
//  Manage
//
//  Created by Rong Li on 1/18/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class FirstAuditViewController: BuildingLableKeyboardBaseViewController {
    let jointAuditor = NSLocalizedString("key_joint_auditor", comment: "")
    let addAuditors = NSLocalizedString("key_add_auditors", comment: "")
//    let editText = NSLocalizedString("key_edit", comment: "")
    let addEmployeeText = NSLocalizedString("key_add_employees", comment: "")
    
    let currentPageIndex = 0
    
    var isPortrait: Bool = true
    
    var scorecard: Scorecard?

    
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: TwoDotsView!
    @IBOutlet weak var buildingDropdown: DropDown!
    @IBOutlet weak var floorDropdown: DropDown!
    @IBOutlet weak var assignToView: AssignToView!
    @IBOutlet weak var nameContainerView: UIView!
    @IBOutlet weak var dateTextField: UITextField!
    
    @IBOutlet weak var scrollContentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nameLabelViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nameContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nameContainerViewTop: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        self.buildingDropDown = buildingDropdown
        self.floorDropDown = floorDropdown
        
        self.assignTo = assignToView
        self.nameContainer = nameContainerView
        
        self.scrollContentHeight = scrollContentViewHeight
        self.nameLabelHeight = nameLabelViewHeight
        self.nameContainerHeight = nameContainerViewHeight
        self.nameContainerTop = nameContainerViewTop
        
        yOriginalPos = Int(nameContainerViewTop.constant + nameContainerViewHeight.constant)
        scrollContentViewOriginalHeight = scrollContentViewHeight.constant
        nameLabelViewOriginalHeight = nameLabelViewHeight.constant
        nameContainerViewOriginalHeight = nameContainerViewHeight.constant
        
        super.viewDidLoad()

        initViews()
        
        isPortrait = getIsPortrait()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        updateNameLabelView()
    }
    
//    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        super.viewWillTransition(to: size, with: coordinator)
//        updateNameLabelViewAfterRotate()
//    }
    
//    func updateNameLabelViewAfterRotate(){
//        if(isPortrait != getIsPortrait()){
//            isPortrait = !isPortrait
//            perform(#selector(updateNameLabelView), with: nil, afterDelay: 0.5)
//        }
//    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        updateNameLabelView()
    }
    
//    func getIsPortrait() -> Bool{
//        return UIDevice.current.orientation.isLandscape == true ? false : true
//    }
//    fileprivate func fetchScorecards(){
//        if(remoteManager == nil){
//            fatalError("Missing dependencies on fetchScorecards")
//        }
//        remoteManager!.fetchScorecards(remoteManager: remoteManager!, apiRouter: APIRouter.scoreCard, completion: fetchScorecardsCompletion)
//    }
//
//    fileprivate func fetchScorecardsCompletion(_ scorecard: Scorecard?, _ error: Error?, _ responseCode: Int){
//        if(scorecard != nil){
//            print("### ### fetchScorecardsCompletion")
//            self.scorecard = scorecard
//            let pageController = self.parent as! AuditPageViewController
//            pageController.setScorecard(scorecard: scorecard!)
//        }
//    }
    
    fileprivate func initViews(){
        headerView.saveBtn.isHidden = false
        headerView.onSaveBtnClicked = {
            self.onSaveBtnClicked()
        }
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_audit", comment: ""))
        
        let dots = [dotsView.dot1, dotsView.dot2]
        initDotsView(dots: dots as! [UIButton], selectedIndex: currentPageIndex)
        
        if(selectedEmployees.count == 0){
            updateEditBtn(title: addAuditors)
        }
    
        assignToView.label.text = jointAuditor
        assignToView.onEditBtnClicked = {
            self.onEditBtnClicked()
        }
        assignToView.onToggleBtnClicked = {
            (toggleBtn) in
            self.onToggleBtnClicked(toggleBtn: toggleBtn)
        }
        
        buildingDropdown.borderColor = UIColor.init(hexString: Constants.boarderColor)
        floorDropdown.borderColor = UIColor.init(hexString: Constants.boarderColor)
        
        dateTextField.setPadding(left: 8, right: 0)
        dateTextField.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        
        initCalendarBtn(dateTextField: dateTextField)
        calendarBtn.addTarget(self, action: #selector(self.popupDatePickerBtnClicked), for: .touchUpInside)
    }
    fileprivate func onSaveBtnClicked(){
        
    }
    
    fileprivate func onSelectedDate(_ date: Date){
        selectedDate = date
        dateTextField.text = selectedDate!.formattedDateShort
    }

    @objc func popupDatePickerBtnClicked(sender:UITapGestureRecognizer) {
        let storyboard =  UIStoryboard(name: "Popup", bundle: nil)
        let viewControler = storyboard.instantiateViewController(withIdentifier: "DatePickerPopupViewController") as! DatePickerPopupViewController
        viewControler.tag = DatePickerType.Date.rawValue
        viewControler.onSelectedDate = onSelectedDate
        present(viewControler, animated: true, completion: nil)
    }
    
    fileprivate func onEditBtnClicked() {
        self.popupSelectCustomerRepresentativeViewController(featureName: FeatureName.Audit)
    }
    
    fileprivate func onToggleBtnClicked(toggleBtn: UIButton) {
        updateNameLabelView()
    }
    
    fileprivate func onConfirmBtnClicked(_ selectedTeam: [TeamMemberV2]) {
        selectedEmployees = selectedTeam
        updateNameContainerView(isShowAll: false)
        updateEditBtn(title: editText)
    }
    
    fileprivate func popupSelectCustomerRepresentativeViewController(featureName: FeatureName){
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SelectCustomerRepresentativeViewController") as! SelectCustomerRepresentativeViewController
        viewController.remoteManager = remoteManager
        viewController.featureName = featureName
        viewController.selectedEmployees = selectedEmployees
//        viewController.onConfirmBtnClicked = onConfirmBtnClicked
        
        viewController.modalTransitionStyle = .crossDissolve
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: true, completion: nil)
    }
    
    func isReadyMoveNext() -> Bool {
        if(selectedFloor != nil && selectedDate != nil){
            let pageController = self.parent as! AuditPageViewController
            pageController.setParams(selectedBuilding: selectedBuilding!, selectedFloor: selectedFloor!, selectedDate: selectedDate!)
            
            scorecard?.BuildingId = selectedBuilding?.Key
            scorecard?.BuildingName = selectedBuilding?.Value
            scorecard?.FloorId = selectedFloor?.Key
            scorecard?.FloorName = selectedFloor?.Value
            scorecard?.AuditDate = selectedDate?.formateDateUTC(date: selectedDate!, withFormat: Constants.DATE_UTC_SS)
//            scorecard?.CustomerRepresentatives = customerRepresentatives
            
            return true
        }
        return false
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        if(isReadyMoveNext()){
            let pageController = self.parent as! AuditPageViewController
            pageController.onNextBtnClicked(currentIndex: currentPageIndex)
        }
    }
}
