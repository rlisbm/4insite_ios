//
//  RateAuditPageViewController.swift
//  Manage
//
//  Created by Rong Li on 1/31/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class RateAuditPageViewController: BasePageViewController {
    
    var indexPath: IndexPath?
    var scorecardSections: [ScorecardSection]?

    var scorecardDetails: [ScorecardDetail]?
    
    var onDoneBtnClicked: ((_ indexPath: IndexPath) -> ())?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        scorecardDetails = scorecardSections![(indexPath?.section)!].ScorecardDetails
        if(scorecardDetails == nil || scorecardDetails!.count == 0){
            //TODO: without scorecardDetails, it can not continue
            return
        }
        
        self.delegate = self
        self.dataSource = self

        for i in 0..<scorecardDetails!.count{
            let storyboard = UIStoryboard(name: "Audit", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "RateAuditViewController") as! RateAuditViewController
            viewController.remoteManager = remoteManager
            viewController.pageIndex = i
            viewController.indexPath = indexPath
            viewController.scorecardDetail = scorecardDetails![i]
            viewController.scorecardDetails = scorecardDetails
            viewController.onDoneBtnClicked = onDoneBtnClicked
            VCs.append(viewController)
        }
        
        setViewControllers([VCs[indexPath?.row ?? 0]], direction: .forward, animated: true, completion: nil)
    }
    

    func viewControllerAtIndex(index: Int) -> RateAuditViewController {
        let storyboard = UIStoryboard(name: "Audit", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "RateAuditViewController") as! RateAuditViewController
        
        if (scorecardDetails!.count > 0) || (index < scorecardDetails!.count) {
            viewController.remoteManager = remoteManager
            viewController.pageIndex = index
            viewController.indexPath = indexPath
            viewController.scorecardDetail = scorecardDetails![index]
        }
        return viewController
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return (scorecardDetails?.count)!
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    override func onNextBtnClicked(currentIndex: Int){
        if(currentIndex + 1 < VCs.count){
            setViewControllers([VCs[currentIndex + 1]], direction: .forward, animated: true, completion: nil)
        }
    }
    
    override func onBackBtnClicked(currentIndex: Int){
        if(currentIndex - 1 >= 0){
            setViewControllers([VCs[currentIndex - 1]], direction: .reverse, animated: true, completion: nil)
        }
    }
}

class RateAuditContainerViewController: UIViewController, ReusableContainer {
    var containerView: UIView!
    
    var pageIndex: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addReusableViewController()
    }
}

protocol ReusableContainer {
    var containerView: UIView! { get }
    func addReusableViewController()
}

extension ReusableContainer where Self: UIViewController {
    func addReusableViewController() {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: String(describing: RateAuditViewController.self)) as? RateAuditViewController else { return }
        
        vc.willMove(toParentViewController: self)
        addChildViewController(vc)
        containerView.addSubview(vc.view)
        constraintViewEqual(view1: containerView, view2: vc.view)
        vc.didMove(toParentViewController: self)
    }
    
    /// Sticks child view (view1) to the parent view (view2) using constraints.
    private func constraintViewEqual(view1: UIView, view2: UIView) {
        view2.translatesAutoresizingMaskIntoConstraints = false
        let constraint1 = NSLayoutConstraint(item: view1, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view2, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0.0)
        let constraint2 = NSLayoutConstraint(item: view1, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view2, attribute: NSLayoutAttribute.trailing, multiplier: 1.0, constant: 0.0)
        let constraint3 = NSLayoutConstraint(item: view1, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view2, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0.0)
        let constraint4 = NSLayoutConstraint(item: view1, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view2, attribute: NSLayoutAttribute.leading, multiplier: 1.0, constant: 0.0)
        view1.addConstraints([constraint1, constraint2, constraint3, constraint4])
    }
}
