//
//  SecondAuditViewController.swift
//  Manage
//
//  Created by Rong Li on 1/18/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

//class AuditCellData {
//    var isCollapsed: Bool = true
//    var scorecardDetails = [ScorecardDetail]()
//
//    init(scorecardDetails: [ScorecardDetail]){
//        self.scorecardDetails = scorecardDetails
//    }
//}

class SecondAuditViewController: BaseViewController {
    let currentPageIndex = 1
    var scorecard: Scorecard?
    var selectedBuilding: KeyValueMappable?
    var selectedFloor: KeyValueMappable?
    
//    @IBOutlet weak var headerView: HeaderView!
//    @IBOutlet weak var dotsView: TwoDotsView!
//    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
//    @IBOutlet weak var tableView: UITableView!
    
//    @IBOutlet weak var scrollContentViewHeight: NSLayoutConstraint!
//    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: TwoDotsView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textView: UITextView!
    
    
    @IBOutlet weak var scrollContentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    var meetScore: Double = 4

    var scorecardSections: [ScorecardSection] = [ScorecardSection]()
    
//    var auditCellDatum: [AuditCellData] = [AuditCellData]()
    
    var auditCellDatum: [Bool] = [Bool]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(refreshTokenNotificationName != nil || fetchAssociateInfoNotificationName != nil){
            if(refreshTokenNotificationName != nil){
                NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
            }else{
                NotificationCenter.default.addObserver(self, selector: #selector(fetchAssociateInfoCompletion(notification:)), name: fetchAssociateInfoNotificationName, object: nil)
            }
        }else{
            fetchScorecards()
        }
        
        initViews()
        
//        let tableHeaderViewNib = UINib.init(nibName: "TableHeaderView", bundle: Bundle.main)
//        tableView.register(tableHeaderViewNib, forHeaderFooterViewReuseIdentifier: "TableHeaderView")
        
//        tableView?.register(TableHeaderView.nib, forHeaderFooterViewReuseIdentifier: TableHeaderView.identifier)
//        tableView.isHidden = true
        


    }
    
    @objc func refreshTokenCompletion(notification: NSNotification){
        if(fetchAssociateInfoNotificationName == nil){
            fetchScorecards()
        }
    }
    
    @objc func fetchAssociateInfoCompletion(notification: NSNotification){
        fetchScorecards()
    }
   
    fileprivate func initViews(){
        headerView.saveBtn.isHidden = false
        headerView.onSaveBtnClicked = {
            self.onSaveBtnClicked()
        }
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_audit", comment: ""))
        
        let dots = [dotsView.dot1, dotsView.dot2]
        initDotsView(dots: dots as! [UIButton], selectedIndex: currentPageIndex)
        
        textView.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        
        tableView?.register(TableHeaderView.nib, forHeaderFooterViewReuseIdentifier: TableHeaderView.identifier)
        tableView.isHidden = true
    
    }
    
    fileprivate func fetchScorecards(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetchScorecards")
        }
        remoteManager!.fetchScorecards(remoteManager: remoteManager!, apiRouter: APIRouter.scoreCard, completion: fetchScorecardsCompletion)
    }

    fileprivate func fetchScorecardsCompletion(_ scorecard: Scorecard?, _ error: Error?, _ responseCode: Int){
        tableView.isHidden = false
        activityIndicator.isHidden = true
        scorecardSections = (scorecard?.ScorecardSections)!

//        for scorecardSection in scorecardSections {
//            auditCellDatum.append(AuditCellData(scorecardDetails: scorecardSection.ScorecardDetails!))
//            //auditCellDatum.append(true)
//        }

        auditCellDatum = [Bool](repeating: true, count: scorecardSections.count)
        scrollContentViewHeight.constant += CGFloat(auditCellDatum.count * 60) - tableViewHeight.constant
        tableViewHeight.constant = CGFloat(auditCellDatum.count * 60)

        tableView.reloadData()

        fetchSiteReportSetting()

    }
    
    fileprivate func fetchSiteReportSetting(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetchSiteReportSetting")
        }
        if let selectedSiteId = SelectedSiteHelper.get()?.selectedSiteId {
            remoteManager!.fetchSiteReportSetting(remoteManager: remoteManager!, selectedSiteId: selectedSiteId, apiRouter: APIRouter.siteReportSetting, completion: fetchSiteReportSettingCompletion)
        }else{
            //TODO: go back to SitePicker??
        }
    }
    
    fileprivate func fetchSiteReportSettingCompletion(_ siteReportSetting: SiteReportSetting?, _ error: Error?, _ responseCode: Int){
    }
    
    fileprivate func onSaveBtnClicked(){
    
    }
    
//    fileprivate func goRateAuditViewController(){
//        let storyboard =  UIStoryboard(name: "Audit", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "RateAuditViewController") as! RateAuditViewController
//        viewController.remoteManager = remoteManager
//        present(viewController, animated: true, completion: nil)
//    }
    
    fileprivate func onDoneBtnClicked(_ indexPath: IndexPath){
//        print("*************** secondAuditVC: onDoneBtnClicked: \(indexPath)")
        print("*************** comment \(scorecardSections[indexPath.section].ScorecardDetails![indexPath.row].Score)")
        
        tableView.reloadSections(IndexSet(integer: indexPath.section), with: UITableViewRowAnimation.top)
    }
    
    fileprivate func goRateAuditPageViewController(indexPath: IndexPath, scorecardSections: [ScorecardSection]){
        let storyboard =  UIStoryboard(name: "Audit", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "RateAuditPageViewController") as! RateAuditPageViewController
        viewController.remoteManager = remoteManager
        viewController.indexPath = indexPath
        viewController.scorecardSections = scorecardSections
        viewController.onDoneBtnClicked = onDoneBtnClicked
//        present(viewController, animated: true, completion: nil)
        
        viewController.modalTransitionStyle = .crossDissolve
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: true, completion: nil)
    }
    
    fileprivate func onAuditBtnClicked(indexPath: IndexPath, scorecardSections: [ScorecardSection]){
//        goRateAuditViewController()
        goRateAuditPageViewController(indexPath: indexPath, scorecardSections: scorecardSections)
    }
    
}

extension SecondAuditViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return scorecardSections.count
//        return auditCellDatum.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return auditCellDatum[section].isCollapsed ? 0 : auditCellDatum[section].scorecardDetails.count
        
        return auditCellDatum[section] ? 0 : scorecardSections[section].ScorecardDetails!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "AuditCell", for: indexPath) as! TableCellView
        let cell = Bundle.main.loadNibNamed(TableCellView.identifier, owner: self, options: nil)?.first as! TableCellView

        let detail = scorecardSections[indexPath.section].ScorecardDetails![indexPath.row]
        
        cell.auditInspectionItemName?.text = detail.AuditInspectionItemName
        cell.areaName.text = scorecardSections[indexPath.section].AreaName
        
        cell.commentTextView.text = detail.Comment
//        if( (scorecardSections[indexPath.section].ScorecardDetails![indexPath.row].Comment ?? "").isEmpty){
//            cell.commentTextViewHeight.constant = 0
//        }else{
//            cell.commentTextViewHeight.constant = 50
//        }
        
        
        
        if(detail.Score == nil){
            cell.areaScore.isHidden = true
            cell.scoreBarView.backgroundColor = UIColor.init(hexString: Constants.passNilGray)
        }else{
            cell.areaScore.isHidden = false
            cell.areaScore.text = String(scorecardSections[indexPath.section].ScorecardDetails![indexPath.row].Score ?? 0)
            cell.scoreBarView.backgroundColor = (detail.Score! < meetScore) ? UIColor.init(hexString: Constants.failRed) : UIColor.init(hexString: Constants.passGreen)
        }
        

        cell.photoView.bageLable.isHidden = (scorecardSections[indexPath.section].ScorecardDetails![indexPath.row].ScorecardDetailImages == nil || scorecardSections[indexPath.section].ScorecardDetails![indexPath.row].ScorecardDetailImages?.count == 0)
        if(!cell.photoView.bageLable.isHidden){
            cell.photoView.bageLable.text = String((scorecardSections[indexPath.section].ScorecardDetails![indexPath.row].ScorecardDetailImages?.count)!)
        }
        
        cell.todoView.bageName.text = NSLocalizedString("key_todo", comment: "")
        cell.todoView.bageIcon.image = UIImage(named: "Audits Card - To Dos Icon.png")
        cell.todoView.bageLable.isHidden = (scorecardSections[indexPath.section].ScorecardDetails![indexPath.row].TodoItems == nil || scorecardSections[indexPath.section].ScorecardDetails![indexPath.row].TodoItems?.count == 0)
        
        if(!cell.todoView.bageLable.isHidden){
            cell.todoView.bageLable.text = String((scorecardSections[indexPath.section].ScorecardDetails![indexPath.row].TodoItems?.count)!)
        }
        
        cell.onAuditBtnClicked = {
            self.onAuditBtnClicked(indexPath: indexPath, scorecardSections: self.scorecardSections)
        }
        
        return cell
    }
}

extension SecondAuditViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TableHeaderView") as! TableHeaderView
        
        let scorecardSection = scorecardSections[section]
        headerView.title.text = scorecardSection.AreaName
        headerView.section = section
        headerView.delegate = self
        
        guard let details = scorecardSection.ScorecardDetails else {
            return headerView
        }
        var counter: Int = 0
        var total: Double = 0
        for detail in details {
            if(detail.Score != nil){
                total += detail.Score!
                counter += 1
            }
        }
        
        if(counter == 0){
            headerView.average.isHidden = true
        }else{
            headerView.average.isHidden = false
            let average = total/Double(counter)
            headerView.average.text = String(average)
            headerView.average.backgroundColor = (average < meetScore) ? UIColor.init(hexString: Constants.failRed) : UIColor.init(hexString: Constants.passGreen)
        }
        
        return headerView
    }
}

extension SecondAuditViewController: TableHeaderViewDelegate {
    func toggleSection(header: TableHeaderView, section: Int) {
        
//        var auditCellData = auditCellDatum[section]
//        auditCellData.isCollapsed = !auditCellData.isCollapsed
//        header.setCollapsed(collapsed: auditCellData.isCollapsed)
        
        auditCellDatum[section] = !auditCellDatum[section]
        header.setCollapsed(collapsed: auditCellDatum[section])

        self.tableView?.beginUpdates()
        self.tableView?.reloadSections([section], with: .fade)
        self.tableView?.endUpdates()
    }
}
