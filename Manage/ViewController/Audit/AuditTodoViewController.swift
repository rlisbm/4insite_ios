//
//  AuditTodoViewController.swift
//  Manage
//
//  Created by Rong Li on 2/5/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit
class AuditAssignTodoPhotoCollectionCell: UICollectionViewCell{
    @IBOutlet weak var photo: UIImageView!
    
    var onDeletePhotoBtnClicked: (() -> Void)?
    
    @IBAction func deletePhotoBtnClicked(_ sender: Any) {
        onDeletePhotoBtnClicked?()
    }
    
}

class AuditTodoViewController: BaseViewController {
    let addEmployeeText = NSLocalizedString("key_add_employee", comment: "")
    let editText = NSLocalizedString("key_edit", comment: "")
    var scorecardDetail: ScorecardDetail?
    
    @IBOutlet weak var assignTo: AssignToOneView!
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var dueBtn: UIButton!
    @IBOutlet weak var collectionSectionView: SectionHeaderView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var images: [UIImage] = [UIImage]()
    
    var todoItem: TodoItem = TodoItem()
    
    var onTodoCreated: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //without ScorecardDetailId, todo can not be submit
        if(scorecardDetail == nil || scorecardDetail?.ScorecardDetailId == nil){
            return
        }
        initViews()
        
    }
    
    fileprivate func initViews(){
        
        if(selectedEmployee == nil){
            assignTo.fullNameLable.isHidden = true
            updateEditBtn(title: addEmployeeText)
        }else{
            assignTo.fullNameLable.isHidden = false
            updateEditBtn(title: editText)
        }
        assignTo.onEditBtnClicked = {
            self.popupSelectEmployeeViewController()
        }
        
        textView.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        
        dateTextField.setPadding(left: 8, right: 0)
        dateTextField.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        
        initCalendarBtn(dateTextField: dateTextField)
        calendarBtn.addTarget(self, action: #selector(self.popupDatePickerBtnClicked), for: .touchUpInside)
        
        collectionSectionView.title.text = NSLocalizedString("key_add_photo_optional", comment: "")
        collectionSectionView.image.image = UIImage(named: "attach-icon.png")
        let photoTappedGesture = UITapGestureRecognizer(target: self, action:  #selector (self.addPhoto(sender:)))
        collectionSectionView.addGestureRecognizer(photoTappedGesture)
    }
    fileprivate func onConfirmBtnClicked(_ selectedTeam: [TeamMemberV2]) {
        selectedEmployee = selectedTeam[0]
        assignTo.fullNameLable.isHidden = false
        initAssignToOneView(assignToView: assignTo)
        updateEditBtn(title: editText)
    }
    
    fileprivate func popupSelectEmployeeViewController(){
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SelectEmployeeViewController") as! SelectEmployeeViewController
        viewController.remoteManager = remoteManager
        viewController.featureName = FeatureName.AuditAssignTodo
        viewController.isEdit = true
        viewController.selectedEmployee = selectedEmployee
        viewController.onConfirmBtnClicked = onConfirmBtnClicked

        viewController.modalTransitionStyle = .crossDissolve
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: true, completion: nil)
        
    }
    func updateEditBtn(title: String){
        if let attributedTitle = assignTo.editBtn.attributedTitle(for: .normal) {
            let mutableAttributedTitle = NSMutableAttributedString(attributedString: attributedTitle)
            mutableAttributedTitle.replaceCharacters(in: NSMakeRange(0, mutableAttributedTitle.length), with: title)
            assignTo.editBtn.setAttributedTitle(mutableAttributedTitle, for: .normal)
        }
    }
    
    fileprivate func onSelectedDate(_ date: Date){
        selectedDate = date
        dateTextField.text = selectedDate!.formattedDateShort
    }
    @objc func popupDatePickerBtnClicked(sender:UITapGestureRecognizer) {
        let storyboard =  UIStoryboard(name: "Popup", bundle: nil)
        let viewControler = storyboard.instantiateViewController(withIdentifier: "DatePickerPopupViewController") as! DatePickerPopupViewController
        viewControler.tag = DatePickerType.Date.rawValue
        viewControler.onSelectedDate = onSelectedDate
        present(viewControler, animated: true, completion: nil)
    }
    
    @objc fileprivate func addPhoto(sender: UITapGestureRecognizer){
        ImagePickerManager().pickImage(self){ image in
            self.updateCollectionView(image: image, indexPath: nil)
        }
    }

    fileprivate func updateCollectionView(image: UIImage?, indexPath: IndexPath?){
        if(image != nil){
            images.append(image!)
            collectionView.isHidden = false
        }else if (indexPath != nil){
            images.remove(at: indexPath!.row)
            if(images.count == 0){
                collectionView.isHidden = true
            }
        }
        collectionView.reloadData()
    }
    
    @IBAction func cancelBtnClicked(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func createTodoBtnClicked(_ sender: Any) {
        todoItem.Comment = "test rli 123"
        todoItem.DueDate = "2019-02-07T07:59:59.999Z"
        todoItem.FloorId = 28998
        todoItem.IsComplete = false
        todoItem.OrgUserId = selectedEmployee?.EmployeeId
        todoItem.ScorecadDetailId = scorecardDetail?.ScorecardDetailId
        todoItem.TodoItemId = 0
        
        
        
        submitAuditTodoItem()
        
    }
    
    fileprivate func submitAuditTodoItem(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on submitTodoItem")
        }
       
        
        remoteManager!.submitItem(remoteManager: remoteManager!, item: todoItem, apiRouter: APIRouter.todoItem, completion: submitCompletion)
        
    }
    
    override func submitCompletion (_ responseString: String?, _ error: Error?, _ responseCode: Int){
        
        if(responseCode == Constants.ResponseOK){
//            todoItem.TodoItemId = responseString!.convertToDictionary()!["TodoItemId"] as? Int
//            scorecardDetail?.TodoItems?.append(todoItem)
            
            guard let dict = responseString!.convertToDictionary() else {
                fatalError("200 response code without TodoItemId in response payload")
            }
            
            todoItem.TodoItemId = (dict["TodoItemId"] as! Int)
            
        }
        scorecardDetail?.TodoItems?.append(todoItem)
        onTodoCreated?()
        dismiss(animated: true)
    }
    
}

extension AuditTodoViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AuditAssignTodoPhotoCollectionCell", for: indexPath) as! AuditAssignTodoPhotoCollectionCell
        cell.photo?.image = images[indexPath.row]
        
        cell.onDeletePhotoBtnClicked = {
            self.updateCollectionView(image: nil, indexPath: indexPath)
        }
        
        return cell
    }
    
}
