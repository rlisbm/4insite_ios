//
//  AuditPageViewController.swift
//  Manage
//
//  Created by Rong Li on 1/18/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class AuditPageViewController: BasePageViewController {
    
    var VC1 : FirstAuditViewController?
    var VC2 : SecondAuditViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        self.dataSource = self
        
        VC1 = storyboard?.instantiateViewController(withIdentifier: "FirstAuditViewController") as? FirstAuditViewController
        VC2 = storyboard?.instantiateViewController(withIdentifier: "SecondAuditViewController") as? SecondAuditViewController
        
        VCs.append(VC1!)
        VCs.append(VC2!)
        
        VC1?.remoteManager = remoteManager
        VC2?.remoteManager = remoteManager
        
        setViewControllers([VCs[0]], direction: .forward, animated: true, completion: nil)

    }
    
    func setScorecard(scorecard: Scorecard){
        VC1?.scorecard = scorecard
    }
    
    func setParams(selectedBuilding: KeyValueMappable, selectedFloor: KeyValueMappable, selectedDate: Date){
        VC2?.selectedDate = selectedDate
        VC2?.selectedBuilding = selectedBuilding
        VC2?.selectedFloor = selectedFloor
    }
    
    func isReadyMoveNext(currentIndex: Int) -> Bool {
//        switch currentIndex {
//        case 0:
//            return VC1?.isReadyMoveNext() ?? false
//        default:
//            return false
//        }
        
        return true
    }
    
    //MARK: UIPageViewControllerDataSource, UIPageViewControllerDelegate
    override func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = VCs.index(of: viewController) ?? 0
        
        if(!isReadyMoveNext(currentIndex: currentIndex) || currentIndex >= VCs.count - 1){
            return nil
        }
        
        return VCs[currentIndex + 1]
    }


}
