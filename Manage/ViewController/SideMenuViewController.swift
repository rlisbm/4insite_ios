//
//  SlideMenuViewController.swift
//  Manage
//
//  Created by Rong Li on 11/14/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import RealmSwift

class SideMenuCell: UITableViewCell {
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var cellImg: UIImageView!
}

class SideMenuViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    let cellData = [NSLocalizedString("key_sent", comment: ""), NSLocalizedString("key_audit_drafts", comment: ""), NSLocalizedString("key_settings", comment: ""), NSLocalizedString("key_help", comment: "")]
    
    //    var sideMenuBtn : UIButton!  //original hangburg more button which is replaced by tabBar button
    
    @IBOutlet weak var sideMenuTableView: UITableView!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var clientLabel: UILabel!
    @IBOutlet weak var siteLabel: UILabel!
    @IBOutlet weak var programLabel: UILabel!
    @IBOutlet weak var closeSideMenuBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        sideMenuTableView.delegate = self
//        sideMenuTableView.backgroundColor = .clear
//        sideMenuTableView.layoutMargins = UIEdgeInsets.zero
//        sideMenuTableView.separatorInset = UIEdgeInsets.zero
        
   
        
        initViews()
        
//        fetchAvatar()
    }
    
    func initViews(){
        sideMenuTableView.delegate = self
        sideMenuTableView.backgroundColor = .clear
        sideMenuTableView.layoutMargins = UIEdgeInsets.zero
        sideMenuTableView.separatorInset = UIEdgeInsets.zero
        
        guard let associate = AssociateHelper.get() else {
            //TODO: re-fetch it or back to login???
            print("### ### this needs to be taken care")
            return
        }
        
        guard let selectedSite = SelectedSiteHelper.get() else {
            //TODO: re-fetch it from database
            print("### ### this needs to be taken care")
            return
        }
        
        nameLabel.text = associate.fullName
        positionLabel.text = associate.jobTitle
        clientLabel.text = selectedSite.selectedClientName
        siteLabel.text = selectedSite.selectedSiteName
        programLabel.text = selectedSite.selectedProgramName
        
        sideMenuTableView.isScrollEnabled = false
        
        if(remoteManager == nil){
            fatalError("Missing dependencies on login")
        }
//        let avatarUrl = remoteManager!.baseCDNUrl + AssociateHelper.get()!.profilePictureUniqueId
//        avatar.af_setImage(withURL: URL(string: avatarUrl)!, placeholderImage: UIImage(named: "Logo_Circle.png")!)
//        avatar.layer.cornerRadius = avatar.frame.size.width / 2
//        avatar.clipsToBounds = true
        
        setImage(imageView: avatar, imgUrl: remoteManager!.baseCDNUrl + AssociateHelper.get()!.profilePictureUniqueId, isCircle: true)
    }
    
//    func fetchAvatar(){
//        avatar.image = UIImage(named: "Password_See")
//        if(remoteManager == nil){
//            fatalError("Missing dependencies on login")
//        }
//
//        remoteManager!.fetchAvatar(remoteManager: remoteManager!, completion: fetchAvatarCompletion)
//
//    }
//
//    fileprivate func fetchAvatarCompletion(_ image: Image?, _ error: Error?, _ responseCode: Int){
//        if(image != nil){
//            avatar.image = image!.af_imageRoundedIntoCircle()
//        }
//    }
    
    func goSitePickViewController(){
        print("### ### goToSitePickViewController")
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let sitePickerVC = storyboard.instantiateViewController(withIdentifier: "SitePickerViewController") as! SitePickerViewController
        sitePickerVC.remoteManager = remoteManager
        present(sitePickerVC, animated: true, completion: nil)
        
        closeSideMenuBtn.sendActions(for: .touchUpInside)
    }

    
    fileprivate func goSettingsViewController(){
        print("### ### goSettingsViewController")
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        viewController.remoteManager = remoteManager
        present(viewController, animated: true, completion: nil)
        
        closeSideMenuBtn.sendActions(for: .touchUpInside)
    }
    
     fileprivate func goSentViewController() {
        print("### ### goSentViewController")
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SentViewController") as! SentViewController
        viewController.remoteManager = remoteManager
        present(viewController, animated: true, completion: nil)
        
        closeSideMenuBtn.sendActions(for: .touchUpInside)
    }
    
    func performOnCloseBtnClicked(){
        closeSideMenuBtn?.sendActions(for: .touchUpInside)
    }
    
    @IBAction func changeSiteBtnClicked(_ sender: Any) {
        goSitePickViewController()
    }
    
    @IBAction func closeSideMenuBtnClicked(_ sender: UIButton) {
//        sideMenuBtn.tag = 0
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: +UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
        })
    }
    
    //MARK: table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuCell
        cell.backgroundColor = .clear
        cell.layoutMargins = UIEdgeInsets.zero
        cell.cellLabel?.text = cellData[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            goSentViewController()
        case 1:
            return
        case 2:
            goSettingsViewController()
        default:
            return
        }
    }

}
