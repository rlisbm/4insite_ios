//
//  ViewController.swift
//  Manage
//
//  Created by Rong Li on 10/24/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import SwiftKeychainWrapper

class LoginViewController: KeyboardBaseViewController {

    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var logoCircle: UIImageView!
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    
    
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var forgotPassword: UILabel!
    //eyeball - toggle password
    var toggleBtn  = UIButton(type: .custom)
    let passwordUnsee = UIImage(named: "Password_Unsee.png") as UIImage?
    let passwordSee = UIImage(named: "Password_See.png") as UIImage?
    
    var locationManager: LocationManager {
        return AppDelegate.locationManager
    }
    
    lazy var locationObserver: LocationManager.LocationObserver = Observer { [weak self] in
       
        if(self != nil){
            self!.mapView.camera = GMSCameraPosition(target: $0.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        }
    }
    
    override func viewDidLoad() {
        //clear SBMToken and LoginCredentails only in KeyChain
        clearKeychainData()
        clearDefaults()
        
//        super.viewDidLoad()
        
        //without remoteManager, app can not be continued login
        if(remoteManager == nil){
            return
        }
        refreshTokenNotificationName = Notification.Name(rawValue: refreshTokenNotificationKey)
        NotificationCenter.default.addObserver(self, selector: #selector(loginCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
        fetchAssociateInfoNotificationName = Notification.Name(rawValue: fetchAssocateInfoNotificationKey)
        NotificationCenter.default.addObserver(self, selector: #selector(fetchAssociateInfoCompletion(notification:)), name: fetchAssociateInfoNotificationName, object: nil)
        
        progressIndicator.isHidden = true
        
        initMap()
        initViews()

    }
    
    fileprivate func initMap(){
        //setup location map
        locationManager.requestAuthorization()
        mapView.isMyLocationEnabled = true
        locationManager.didUpdateLocations += locationObserver
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "login_map_style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
    
    fileprivate func initViews(){
        
        self.view.addSubview(loginView)
        
        //forgot password
        let attributedString  = NSMutableAttributedString(string: "Forgot Password", attributes: [NSAttributedString.Key.underlineStyle : true])
        forgotPassword.attributedText = attributedString
        let tap = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.forgotPasswordClicked))
        forgotPassword.isUserInteractionEnabled = true
        forgotPassword.addGestureRecognizer(tap)
        
        //textfields
        userName.delegate = self
        password.delegate = self
        self.userName.setPadding(left: 8, right: 8)
        self.password.setPadding(left: 8, right: 8)
        userName.becomeFirstResponder()
        UITextField.connectTextField(textFields: [userName, password])
        
        //Listen for keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)
        
        //eyeball: toggle password. var rightButton  = UIButton(type: .custom)
        toggleBtn.frame = CGRect(x:0, y:0, width:30, height:30)
        toggleBtn.addTarget(self, action: #selector(togglePassword), for: .touchUpInside)
        toggleBtn.setImage(passwordUnsee, for: .normal)
        toggleBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
        password.rightViewMode = .always
        password.rightView = toggleBtn
        
        setLogo()
    }
    
    deinit {
        locationManager.didUpdateLocations -= locationObserver
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidHide, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidChangeFrame, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
//    //moved to BaseViewController
//    override func viewWillDisappear(_ animated: Bool) {
//        if(refreshTokenNotificationName != nil || fetchAssociateInfoNotificationName != nil ){
//            NotificationCenter.default.removeObserver(self)
//        }
//    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        setLogo()
    }
    
    fileprivate func setLogo(){
        if(UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight){
            logo.isHidden = true
            logoCircle.isHidden = false
        }else{
            if(isKeyboardOpen){
                logo.isHidden = true
                logoCircle.isHidden = false
            }else{
                logo.isHidden = false
                logoCircle.isHidden = true
            }
        }
    }
    
    func goToLandingViewController(){
        print("### ### goToLandingViewController")
        
        let sitePickerVC = storyboard?.instantiateViewController(withIdentifier: "LandingViewController") as! LandingViewController
        sitePickerVC.remoteManager = remoteManager
        present(sitePickerVC, animated: true, completion: nil)
    }
    
    fileprivate func clearKeychainData(){
        let isRemoved: Bool = KeychainWrapper.standard.removeAllKeys()
    }
    
    fileprivate func clearDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    
    @objc func togglePassword(_ sender : UIButton ){
        if password.isSecureTextEntry{
            password.isSecureTextEntry = false
            toggleBtn.setImage(passwordSee, for: .normal)
            
        }else{
            password.isSecureTextEntry = true;
            toggleBtn.setImage(passwordUnsee, for: .normal)
        }
    }
 
    @IBAction func loginBtnClicked(_ sender: Any) {
        hideKeyboard(hideKeyboardType:HideKeyboardType.ScreenTounch, textView: nil)
        progressIndicator.isHidden = false
        
        userName.text = "201306"
        password.text = "Sbm123!"
        
        let userNameText:String = userName.text!
        let passwordText:String = password.text!
        
        if(userNameText.isEmpty || passwordText.isEmpty){
            showAlert(NSLocalizedString("key_infomation", comment: ""), NSLocalizedString("key_empty_username_password", comment: ""), NSLocalizedString("key_dismiss", comment: ""), viewController: self)
            return
        }
      
        login(userName: userNameText, password: passwordText)
    }
    
    @objc func loginCompletion(notification: NSNotification){
        print("### ### responseCode \(notification.userInfo!["responseCode"] as? Int)")
        if let userInfo = notification.userInfo {
            guard let responseCode = userInfo["responseCode"] as? Int, responseCode == Constants.ResponseOK else {
                print("### ### 333")
                progressIndicator.isHidden = true
                return
            }
            fetchAssociateInfo()
        }
    }
    
    @objc func fetchAssociateInfoCompletion(notification: NSNotification){
        gotoSitePickViewController()
    }
    
    func gotoSitePickViewController(){
        progressIndicator.isHidden = true
        
//        performSegue(withIdentifier: "SitePickerViewController", sender: self)

        let sitePickerVC = storyboard?.instantiateViewController(withIdentifier: "SitePickerViewController") as! SitePickerViewController
        sitePickerVC.remoteManager = remoteManager
        present(sitePickerVC, animated: true, completion: nil)
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
////        let sitePickViewController = segue.destination as! SitePickViewController
////        sitePickViewController.remoteManager = remoteManager
//    }
    
    //Handle forgot password clicked
    @objc func forgotPasswordClicked(sender:UITapGestureRecognizer) {
        if let url = URL(string:"https://prod-web-cdn.sbminsite.com/dashboard"){
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    override func keyboardWillChange(notification: Notification){
        super.keyboardWillChange(notification: notification)
        setLogo()
    }
    
    //UITextFieldDelegate Methods
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        hideKeyboard(hideKeyboardType:HideKeyboardType.KeyboradReturn, textView: nil)
        if (textField.returnKeyType == .done){
            submitBtn.sendActions(for: .touchUpInside)
        }
        
        return true
    }
    
    //Touch screen outside textfield to dismiss the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        hideKeyboard(hideKeyboardType:HideKeyboardType.ScreenTounch, textView: nil)
    }
}

extension UIViewController {
    class func displaySpinner(onView : UIView) -> UIView {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        return spinnerView
    }
    
    class func removeSpinner(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
}
