//
//  SecondReportItViewController.swift
//  Manage
//
//  Created by Rong Li on 1/2/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class SecondReportItViewController: BaseViewController {
    let currentPageIndex = 1
    
    var ownership: OwnershipRequestItem?

    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: ThreeDotsView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
    }
    
    
    fileprivate func initViews(){
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_report_it", comment: ""))
        
        let dots = [dotsView.dot1, dotsView.dot2, dotsView.dot3]
        initDotsView(dots: dots as! [UIButton], selectedIndex: currentPageIndex)
    }

    func isReadyMoveNext() -> Bool{
        return true
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        let pageController = self.parent as! ReportItPageViewController
        pageController.onBackBtnClicked(currentIndex: currentPageIndex)
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        let pageController = self.parent as! ReportItPageViewController
        pageController.onNextBtnClicked(currentIndex: currentPageIndex)
    }
}
