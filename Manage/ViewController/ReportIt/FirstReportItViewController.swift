//
//  FirstReportItViewController.swift
//  Manage
//
//  Created by Rong Li on 1/2/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit
import RealmSwift

class FirstReportItViewController: BaseViewController, UITextViewDelegate {
    let currentPageIndex = 0
    
    var ownership: OwnershipRequestItem?
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: ThreeDotsView!
    @IBOutlet weak var assignToView: AssignToOneView!
    @IBOutlet weak var buildingDropDown: DropDown!
    @IBOutlet weak var floorDropDown: DropDown!
    @IBOutlet weak var textView: UITextView!
    
    var names = [String]()
    var ids = [Int]()
    
    var buildingList = List<KeyValueMappable>()
    var floorDic: Dictionary = [Int:[Floor]]()
    var floorIdDic: Dictionary = [Int:[Int]]()
    var floorNameDic: Dictionary = [Int:[String]]()
    
    var selectedBuilding: KeyValueMappable?
    var selectedFloor: KeyValueMappable?
    
    var isBuildingOpen: Bool = false
    var isFloorOpen: Bool = false
    var fetchIndex = 0;
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(refreshTokenNotificationName != nil){
            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
        }else{
            initViews()
            
            if(AssociateHelper.get()?.employeeId == nil){
                fetchAssociateInfoNotificationName = Notification.Name(rawValue: fetchAssocateInfoNotificationKey)
                NotificationCenter.default.addObserver(self, selector: #selector(fetchAssociateInfoCompletion(notification:)), name: fetchAssociateInfoNotificationName, object: nil)
            }else{
                fetchBuildings()
            }
        }
        
        //Listen for keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidHide, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidChangeFrame, object: nil)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            if(isBuildingOpen){
                buildingDropDown.touchAction()
            }
        } else {
            print("Portrait")
        }
    }
    
    @objc func refreshTokenCompletion(notification: NSNotification){
        print("### ### FirstTodosViewController refreshTokenCompletion")
        if(self.isTokenExpirationChanged){
            fetchBuildings()
        }else{
            initViews()
        }
    }
    
    @objc func fetchAssociateInfoCompletion(notification: NSNotification){
        print("### ### FirstTodosViewController fetchAssociateInfoCompletion")
        fetchBuildings()
    }
    
    func initViews(){
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_report_it", comment: ""))
        
        let dots = [dotsView.dot1, dotsView.dot2, dotsView.dot3]
        initDotsView(dots: dots as! [UIButton], selectedIndex: currentPageIndex)
        
        initAssignToOneView(assignToView: assignToView, featureName: FeatureName.ReportIt)
        
        textView.delegate = self
        textView.text = NSLocalizedString("key_description", comment: "") 
        textView.textColor = UIColor.init(hexString: Constants.textDarkWhite)
    
        buildingDropDown.setDropDown(rowBackgroundColor: Constants.dropDownRowBG, selectedRowColor: Constants.dropDownRowBG, rowTextColor: Constants.dropDownTextColor, rowHeight: 50);
        floorDropDown.setDropDown(rowBackgroundColor: Constants.dropDownRowBG, selectedRowColor: Constants.dropDownRowBG, rowTextColor: Constants.dropDownTextColor, rowHeight: 50);

        buildingDropDown.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        floorDropDown.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        
        buildingDropDown.didSelect{(selectedText , index , id) in
            print("Selected String: \(selectedText) \n index: \(index) \n Id: \(id)")
            self.selectedBuilding = KeyValueMappable(Key: id, Value: selectedText)
            self.isBuildingOpen = false
            self.updateFloorDropDown(selectedBuildingId: id, isResetTitle: true)
//            self.todoItem!.buildingId = id
//            self.todoItem!.buildingName = selectedText
        }
        
        floorDropDown.didSelect{(selectedText , index , id) in
            print("Selected String: \(selectedText) \n index: \(index) \n Id: \(id)")
            self.selectedFloor = KeyValueMappable(Key: id, Value: selectedText)
            self.isFloorOpen = false
//            self.todoItem!.floorId = id
//            self.todoItem!.floorName = selectedText
        }
        
        let buildingGesture = UITapGestureRecognizer(target: self, action:  #selector (self.buildingDropDownAction(sender:)))
        buildingDropDown.addGestureRecognizer(buildingGesture)
        let floorGesture = UITapGestureRecognizer(target: self, action:  #selector (self.floorDropDownAction(sender:)))
        floorDropDown.addGestureRecognizer(floorGesture)
    }
    
    @objc func buildingDropDownAction(sender: UITapGestureRecognizer){
        if(isFloorOpen){
            isFloorOpen = !isFloorOpen
            floorDropDown.touchAction()
            return
        }
        
        isBuildingOpen = !isBuildingOpen
        buildingDropDown.touchAction()
    }
    
    @objc func floorDropDownAction(sender: UITapGestureRecognizer){
        if(isBuildingOpen){
            isBuildingOpen = !isBuildingOpen
            buildingDropDown.touchAction()
            return
        }
        
        isFloorOpen = !isFloorOpen
        floorDropDown.touchAction()
    }
    
    func updateBuildingDropDown(_ buildings: NameKeyValueMappable){
        buildingDropDown.text = buildingText
        buildingDropDown.optionArray = names
        buildingDropDown.optionIds = ids
    }
    
    func updateFloorDropDown(selectedBuildingId: Int, isResetTitle: Bool){
        floorDropDown.text = floorText
        floorDropDown.selectedIndex = nil
        if(floorNameDic[selectedBuildingId] != nil && floorNameDic[selectedBuildingId]!.count > 0){
            floorDropDown.optionArray = floorNameDic[selectedBuildingId]!
            floorDropDown.optionIds = floorIdDic[selectedBuildingId]
        }else{
            floorDropDown.optionArray = []
            floorDropDown.optionIds = []
        }
        
        if(!isResetTitle){
            //TODO
        }
    }
    func fetchBuildings(){
        print("*** *** fetchBuildings")
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetche Buildings")
        }
        
        remoteManager!.fetchBuildings(remoteManager: remoteManager!, completion: fetchBuildingsCompletion)
        
    }
    
    fileprivate func fetchBuildingsCompletion(_ buildings: NameKeyValueMappable?, _ error: Error?, _ responseCode: Int){
        if(buildings != nil && buildings?.Options != nil && (buildings?.Options.count)! > 0){
            buildingList = (buildings?.Options)!
            for keyValue in buildingList {
                ids.append(keyValue.Key)
                names.append(keyValue.Value!)
            }
            updateBuildingDropDown(buildings!)
            
            fetchIndex = 0;
            fetchBuildingFloors(buildingId: buildingList[0].Key)
        }else{
            //TODO: Handle Error here
            if(responseCode == 401){
                //TODO: it could be token expired. This is not likely to happen, because it's validated in viewDidLoad().
                //only if expiration duration has been changed on server side
                refreshTokenNotificationName = Notification.Name(rawValue: refreshTokenNotificationKey)
                NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
                refreshToken()
            }
        }
    }
    
    fileprivate func fetchBuildingFloors(buildingId: Int) {
        print("*** *** fetchFloors")
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetche Floors")
        }
        remoteManager!.fetchBuildingFloors(remoteManager: remoteManager!, buildingId: buildingId, completion: fetchBuildingFloorsCompletion)
    }
    
    func fetchBuildingFloorsCompletion(_ floors: NameKeyValueMappable?, _ error: Error?, _ responseCode: Int){
        print("*** *** fetchBuildingFloorsCompletion")
        if(floors != nil && floors?.Options != nil && (floors?.Options.count)! > 0){
            ids.removeAll()
            names.removeAll()
            
            for keyValue in (floors?.Options)! {
                ids.append(keyValue.Key)
                names.append(keyValue.Value!)
            }
            floorIdDic[buildingDropDown.optionIds![fetchIndex]] = ids
            floorNameDic[buildingDropDown.optionIds![fetchIndex]] = names
            
            fetchIndex += 1
            if(fetchIndex < buildingDropDown.optionIds!.count){
                fetchBuildingFloors(buildingId: buildingDropDown.optionIds![fetchIndex])
            }else{
                floorDropDown.text = floorText
            }
        }else{
            
            fetchIndex += 1
            if(fetchIndex < buildingDropDown.optionIds!.count){
                fetchBuildingFloors(buildingId: buildingDropDown.optionIds![fetchIndex])
            }else{
                floorDropDown.text = floorText
            }
        }
    }
        
    func isReadyMoveNext() -> Bool{
//        if((buildingDropDown.text != buildingText)
//            && (floorDropDown.text != floorText)
//            && !textView.text.isEmpty){
//            ownership?.BuildingId = selectedBuilding?.Key
//            ownership?.BuildingName = selectedBuilding?.Value
//            ownership?.FloorId = selectedFloor?.Key
//            ownership?.FloorName = selectedFloor?.Value
//            ownership?.Description = textView.text
//
//            print("\(ownership?.BuildingId)  \(ownership?.BuildingName)  \(ownership?.FloorId)  \(ownership?.FloorName)   \(ownership?.Description)")
//            return true
//        }
//
//        showInfoAlert(message: NSLocalizedString("key_provide_input", comment: ""))
//        return false
        
        return true
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        if(isReadyMoveNext()){
            let pageController = self.parent as! ReportItPageViewController
            pageController.onNextBtnClicked(currentIndex: currentPageIndex)
        }
    }
    
    //Handle Keyboard
    func hideKeyboard(hideKeyboardType:HideKeyboardType){
        switch hideKeyboardType {
        case HideKeyboardType.KeyboradReturn:
            textView.resignFirstResponder()
            break
        case HideKeyboardType.ScreenTounch:
            self.view.endEditing(true)
            break
        default:
            break
        }
    }
    
    @objc func keyboardWillChange(notification: Notification){
        guard let keyboardRect = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        if (notification.name == Notification.Name.UIKeyboardWillShow || notification.name == Notification.Name.UIKeyboardWillChangeFrame
            || notification.name == Notification.Name.UIKeyboardDidShow || notification.name == Notification.Name.UIKeyboardDidChangeFrame) {
            
            view.frame.origin.y = -keyboardRect.height
            isKeyboardOpen = true
        }else{
            view.frame.origin.y = 0
            isKeyboardOpen = false
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.init(hexString: Constants.textDarkWhite) {
            textView.text = nil
            textView.textColor = UIColor.init(hexString: Constants.textWhite)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    //Touch screen outside textfield to dismiss the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        hideKeyboard(hideKeyboardType:HideKeyboardType.ScreenTounch)
    }

    
}
