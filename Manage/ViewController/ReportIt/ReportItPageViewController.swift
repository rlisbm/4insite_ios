//
//  ReportItPageViewController.swift
//  Manage
//
//  Created by Rong Li on 1/2/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class ReportItPageViewController: BasePageViewController {
    
    var ownership: OwnershipRequestItem = OwnershipRequestItem()
    
    var VC1 : FirstReportItViewController?
    var VC2 : SecondReportItViewController?
    var VC3 : ThirdReportItViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        self.dataSource = self
        
        
        VC1 = storyboard?.instantiateViewController(withIdentifier: "FirstReportItViewController") as? FirstReportItViewController
        VC2 = storyboard?.instantiateViewController(withIdentifier: "SecondReportItViewController") as? SecondReportItViewController
        VC3 = storyboard?.instantiateViewController(withIdentifier: "ThirdReportItViewController") as? ThirdReportItViewController
        
        VCs.append(VC1!)
        VCs.append(VC2!)
        VCs.append(VC3!)
        
        VC1?.remoteManager = remoteManager
        VC2?.remoteManager = remoteManager
        VC3?.remoteManager = remoteManager
        
        VC1?.selectedEmployee = selectedEmployee
        VC2?.selectedEmployee = selectedEmployee
        VC3?.selectedEmployee = selectedEmployee
        
        VC1?.ownership = ownership
        VC2?.ownership = ownership
        VC3?.ownership = ownership
        
        setViewControllers([VCs[0]], direction: .forward, animated: true, completion: nil)
        
    }
    
    func isReadyMoveNext(currentIndex: Int) -> Bool {
//        switch currentIndex {
//        case 0:
//            return VC1?.isReadyMoveNext() ?? false
//        case 1:
//            return VC2?.isReadyMoveNext() ?? false
//        default:
//            return false
//        }
        
        return true
    }
    
    //MARK: UIPageViewControllerDataSource, UIPageViewControllerDelegate
    override func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = VCs.index(of: viewController) ?? 0
        
        if(!isReadyMoveNext(currentIndex: currentIndex)){
            return nil
        }
        
        if(currentIndex >= VCs.count - 1){
            return nil
        }
        return VCs[currentIndex + 1]
    }
}
