//
//  ThirdReportItViewController.swift
//  Manage
//
//  Created by Rong Li on 1/2/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class ThirdReportItViewController: BaseViewController {
    let currentPageIndex = 2
    
    var ownership: OwnershipRequestItem?
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: ThreeDotsView!
    @IBOutlet weak var descriptionView: DescriptionView!
    @IBOutlet weak var buildingNameLabel: UILabel!
    @IBOutlet weak var floorNameLabel: UILabel!
    @IBOutlet weak var employeeFullNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()

    }
    
    fileprivate func initViews(){
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_report_it", comment: ""))
        
        let dots = [dotsView.dot1, dotsView.dot2, dotsView.dot3]
        initDotsView(dots: dots as! [UIButton], selectedIndex: currentPageIndex)
        
        descriptionView.content.text = ownership?.Description
        descriptionView.content.layer.borderWidth = 0
        buildingNameLabel.text = ownership?.BuildingName
        floorNameLabel.text = ownership?.FloorName
        
        let firstName = self.selectedEmployee!.FirstName ?? ""
        let lastName = self.selectedEmployee!.LastName!
        let fullName = "\(firstName) \(lastName)"
        employeeFullNameLabel.text = fullName
        
        
        ownership?.BuildingId = 11464
        ownership?.FloorId = 35629
        ownership?.CreateDate = "2019-01-03T22:07:36.684Z"
        ownership?.Description = "rli testing xxx"
        ownership?.EmployeeId = 176920
        
        var ownershipAttachment = OwnershipAttachmentRequestItem()
        ownershipAttachment.AttachmentType = "Image"
        ownershipAttachment.OwnershipAttachmentType = "Misc"
        ownershipAttachment.OwnershipAttachmentTypeId = 1
        ownershipAttachment.UniqueId = "image/678c20d3-347f-c6f0-ed0f-3f7d7eac634f.png"
        
        ownership?.OwnershipAttachments.append(ownershipAttachment)
    }
    
    fileprivate func submitReportIt(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on submitAReportIt")
        }
        
        remoteManager!.submitItem(remoteManager: remoteManager!, item: ownership, apiRouter: APIRouter.reportIt, completion: submitCompletion)
    }
    
//    fileprivate func submitCompletion (_ responseString: String?, _ error: Error?, _ responseCode: Int){
//        if(responseCode == Constants.ResponseOK){
//            goLandingViewController(isCompletion: true, isSuccess: true)
//        }else{
//            goLandingViewController(isCompletion: true, isSuccess: false)
//        }
//    }
    
    
    @IBAction func editBtnClicked(_ sender: Any) {
        let pageController = self.parent as! ReportItPageViewController
        pageController.onBackBtnClicked(currentIndex: currentPageIndex)
    }
    
    
//    @POST("api/services/ownership")
//    Call<ReportItResponse> submitReportIt(
//    @Body ReportItem reportItem
//    );
    @IBAction func submitBtnClicked(_ sender: Any) {
        submitReportIt()
    }
    
    
}
