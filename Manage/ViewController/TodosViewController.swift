//
//  TodosViewController.swift
//  Manage
//
//  Created by Rong Li on 11/26/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

class TodosViewController: BaseViewController {

    var featureName: FeatureName?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.statusBarBackgroundColor = .black
 
        fetchDepartments()
    }
    
    func fetchDepartments(){
        print("*** *** fetchTeamMembers")
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetchTeamMembers")
        }
//        remoteManager!.fetchDepartments(remoteManager: remoteManager!, completion: fetchDepartmentsCompletion)
    }
    
    fileprivate func fetchDepartmentsCompletion(_ departments: [Department]?, _ error: Error?, _ responseCode: Int){
        
    }
    
}
