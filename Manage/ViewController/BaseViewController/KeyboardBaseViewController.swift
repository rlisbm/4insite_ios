//
//  KeyboardBaseViewController.swift
//  Manage
//
//  Created by Rong Li on 1/9/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class KeyboardBaseViewController: BaseViewController, UITextFieldDelegate, UITextViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    //Handle Keyboard
    func hideKeyboard(hideKeyboardType:HideKeyboardType, textView: UITextView?){
        switch hideKeyboardType {
        case HideKeyboardType.KeyboradReturn:
            if(textView != nil){
                textView!.resignFirstResponder()
            }
            break
        case HideKeyboardType.ScreenTounch:
            self.view.endEditing(true)
            break
        default:
            break
        }
    }
    
    @objc func keyboardWillChange(notification: Notification){
        guard let keyboardRect = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        if (notification.name == Notification.Name.UIKeyboardWillShow || notification.name == Notification.Name.UIKeyboardWillChangeFrame
            || notification.name == Notification.Name.UIKeyboardDidShow || notification.name == Notification.Name.UIKeyboardDidChangeFrame) {
            
            view.frame.origin.y = -keyboardRect.height
            isKeyboardOpen = true
        }else{
            view.frame.origin.y = 0
            isKeyboardOpen = false
        }
    }
    
    //MARK: UITextFieldDelegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    //MARK: UITextViewDelegate Methods
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.init(hexString: Constants.textDarkWhite) {
            textView.text = nil
            textView.textColor = UIColor.init(hexString: Constants.textWhite)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }

}
