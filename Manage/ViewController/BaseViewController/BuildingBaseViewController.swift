//
//  BuildingBaseViewController.swift
//  Manage
//
//  Created by Rong Li on 1/11/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit
import RealmSwift

class BuildingBaseViewController: BaseViewController {

    var buildingDropDown: DropDown!
    var floorDropDown: DropDown!
    
    var names = [String]()
    var ids = [Int]()
    
    var buildingList = List<KeyValueMappable>()
    var floorDic: Dictionary = [Int:[Floor]]()
    var floorIdDic: Dictionary = [Int:[Int]]()
    var floorNameDic: Dictionary = [Int:[String]]()
    
    var selectedBuilding: KeyValueMappable?
    var selectedFloor: KeyValueMappable?
    
    var isBuildingOpen: Bool = false
    var isFloorOpen: Bool = false
    var fetchIndex = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(refreshTokenNotificationName != nil){
            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
        }else{
            initBuildingViews()
            
            if(AssociateHelper.get()?.employeeId == nil){
                fetchAssociateInfoNotificationName = Notification.Name(rawValue: fetchAssocateInfoNotificationKey)
                NotificationCenter.default.addObserver(self, selector: #selector(fetchAssociateInfoCompletion(notification:)), name: fetchAssociateInfoNotificationName, object: nil)
            }else{
                fetchBuildings()
            }
        }
    }

    fileprivate func initBuildingViews(){
        
        buildingDropDown.setDropDown(rowBackgroundColor: Constants.dropDownRowBG, selectedRowColor: Constants.dropDownRowBG, rowTextColor: Constants.dropDownTextColor, rowHeight: 50);
        buildingDropDown.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        floorDropDown.setDropDown(rowBackgroundColor: Constants.dropDownRowBG, selectedRowColor: Constants.dropDownRowBG, rowTextColor: Constants.dropDownTextColor, rowHeight: 50);
        floorDropDown.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        
        buildingDropDown.didSelect{(selectedText , index , id) in
            print("Selected String: \(selectedText) \n index: \(index) \n Id: \(id)")
            self.selectedBuilding = KeyValueMappable(Key: id, Value: selectedText)
            self.isBuildingOpen = false
            self.updateFloorDropDown(selectedBuildingId: id, isResetTitle: true)
        }
        
        floorDropDown.didSelect{(selectedText , index , id) in
            print("Selected String: \(selectedText) \n index: \(index) \n Id: \(id)")
            self.selectedFloor = KeyValueMappable(Key: id, Value: selectedText)
            self.isFloorOpen = false
        }
        
        let buildingGesture = UITapGestureRecognizer(target: self, action:  #selector (self.buildingDropDownAction(sender:)))
        buildingDropDown.addGestureRecognizer(buildingGesture)
        let floorGesture = UITapGestureRecognizer(target: self, action:  #selector (self.floorDropDownAction(sender:)))
        floorDropDown.addGestureRecognizer(floorGesture)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if(isBuildingOpen){
            isBuildingOpen = false
            buildingDropDown.touchAction()
        }else if (isFloorOpen){
            isFloorOpen = false
            floorDropDown.touchAction()
        }
    }
    
    @objc func buildingDropDownAction(sender: UITapGestureRecognizer){
        if(isFloorOpen){
            isFloorOpen = !isFloorOpen
            floorDropDown.touchAction()
            return
        }
        
        isBuildingOpen = !isBuildingOpen
        buildingDropDown.touchAction()
    }
    
    @objc func floorDropDownAction(sender: UITapGestureRecognizer){
        if(isBuildingOpen){
            isBuildingOpen = !isBuildingOpen
            buildingDropDown.touchAction()
            return
        }
        
        isFloorOpen = !isFloorOpen
        floorDropDown.touchAction()
    }
    
    func updateBuildingDropDown(_ buildings: NameKeyValueMappable){
        buildingDropDown.text = buildingText
        buildingDropDown.optionArray = names
        buildingDropDown.optionIds = ids
    }
    
    func updateFloorDropDown(selectedBuildingId: Int, isResetTitle: Bool){
        floorDropDown.text = floorText
        floorDropDown.selectedIndex = nil
        if(floorNameDic[selectedBuildingId] != nil && floorNameDic[selectedBuildingId]!.count > 0){
            floorDropDown.optionArray = floorNameDic[selectedBuildingId]!
            floorDropDown.optionIds = floorIdDic[selectedBuildingId]
        }else{
            floorDropDown.optionArray = []
            floorDropDown.optionIds = []
        }
        
        if(!isResetTitle){
            //TODO
        }
        
    }
    
    func fetchBuildings(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetche Buildings")
        }
        
        remoteManager!.fetchBuildings(remoteManager: remoteManager!, completion: fetchBuildingsCompletion)
        
    }
    
    fileprivate func fetchBuildingsCompletion(_ buildings: NameKeyValueMappable?, _ error: Error?, _ responseCode: Int){
        if(buildings != nil && buildings?.Options != nil && (buildings?.Options.count)! > 0){
            buildingList = (buildings?.Options)!
            for keyValue in buildingList {
                ids.append(keyValue.Key)
                names.append(keyValue.Value!)
            }
            updateBuildingDropDown(buildings!)
            
            fetchIndex = 0;
            fetchBuildingFloors(buildingId: buildingList[0].Key)
        }else{
            //TODO: Handle Error here
            if(responseCode == 401){
                //TODO: it could be token expired. This is not likely to happen, because it's validated in viewDidLoad().
                //only if expiration duration has been changed on server side
                refreshTokenNotificationName = Notification.Name(rawValue: refreshTokenNotificationKey)
                NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
                refreshToken()
            }
        }
    }
    
    fileprivate func fetchBuildingFloors(buildingId: Int) {
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetche Floors")
        }
        remoteManager!.fetchBuildingFloors(remoteManager: remoteManager!, buildingId: buildingId, completion: fetchBuildingFloorsCompletion)
    }
    
    func fetchBuildingFloorsCompletion(_ floors: NameKeyValueMappable?, _ error: Error?, _ responseCode: Int){
        if(floors != nil && floors?.Options != nil && (floors?.Options.count)! > 0){
            ids.removeAll()
            names.removeAll()
            
            for keyValue in (floors?.Options)! {
                ids.append(keyValue.Key)
                names.append(keyValue.Value!)
            }
            print("************** fetchIndex \(fetchIndex)")
            print("************** fetchIndex \(buildingDropDown.optionIds!.count)")
            print("************** buildingDropDown.optionIds![fetchIndex]  \(buildingDropDown.optionIds![fetchIndex])")
            floorIdDic[buildingDropDown.optionIds![fetchIndex]] = ids
            floorNameDic[buildingDropDown.optionIds![fetchIndex]] = names
            
            fetchIndex += 1
            if(fetchIndex < buildingDropDown.optionIds!.count){
                fetchBuildingFloors(buildingId: buildingDropDown.optionIds![fetchIndex])
            }else{
                floorDropDown.text = floorText
            }
        }else{
            
            fetchIndex += 1
            if(fetchIndex < buildingDropDown.optionIds!.count){
                fetchBuildingFloors(buildingId: buildingDropDown.optionIds![fetchIndex])
            }else{
                floorDropDown.text = floorText
            }
        }
    }
    
    @objc func refreshTokenCompletion(notification: NSNotification){
        if(self.isTokenExpirationChanged){
            fetchBuildings()
        }else{
            initBuildingViews()
        }
    }
    
    @objc func fetchAssociateInfoCompletion(notification: NSNotification){
        fetchBuildings()
    }
    

}
