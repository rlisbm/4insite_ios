//
//  BasePageViewController.swift
//  Manage
//
//  Created by Rong Li on 12/17/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit




class BasePageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate{
    var selectedEmployee: TeamMemberV2?
    var selectedEmployees: [TeamMemberV2] = [TeamMemberV2]()
    var remoteManager: RemoteManager?
    
    var selectEmployeeViewController : SelectEmployeeViewController?
    var employeePopupViewController: EmployeePopupViewController?
    
    var VCs = [UIViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        testGenericAry()
    }
    
    func testGenericAry() {
        var containers = [Container]()
        
        containers.append(Container(1, 4, 6, 2, 6))
        containers.append(Container(1.2, 3.5))
        
        for container in containers {
            print(container.myMethod())
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(employeePopupViewController != nil){
            employeePopupViewController?.dismiss(animated: true)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if(selectEmployeeViewController != nil){
            selectEmployeeViewController?.dismiss(animated: true)
        }
    }
    
    //MARK: UIPageViewControllerDataSource, UIPageViewControllerDelegate
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return VCs.count
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = VCs.index(of: viewController) ?? 0
        if(currentIndex <= 0){
            return nil
        }
        return VCs[currentIndex - 1]
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = VCs.index(of: viewController) ?? 0
        
        if(currentIndex >= VCs.count - 1){
            return nil
        }
        return VCs[currentIndex + 1]
    }

    func onNextBtnClicked(currentIndex: Int){
        setViewControllers([VCs[currentIndex + 1]], direction: .forward, animated: true, completion: nil)
    }
    
    func onBackBtnClicked(currentIndex: Int){
        setViewControllers([VCs[currentIndex - 1]], direction: .reverse, animated: true, completion: nil)
    }
}



////////////////////
class Container {
    var values: [MyProtocol]
    
    init(_ values: MyProtocol...) {
        self.values = values
    }
    
    func myMethod() -> [MyProtocol] {
        return values
    }
}

////////////////
protocol MyProtocol {
    func getValue() -> Self
}

extension Int: MyProtocol {
    func getValue() -> Int {
        return self
    }
}

extension Double: MyProtocol {
    func getValue() -> Double {
        return self
    }
}


//protocol ViewControllerProtocol {
////    func getValue() -> ViewControllerProtocol
//    var value: Self { get }
//}
//extension FirstConductViewController: ViewControllerProtocol{
//    func getValue() -> FirstConductViewController {
//        return self
//    }
//}
//extension SecondConductViewController: ViewControllerProtocol{
//    func getValue() -> SecondConductViewController {
//        return self
//    }
//}
