//
//  BaseViewController.swift
//  Manage
//
//  Created by Rong Li on 11/19/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit
import Alamofire
import SwiftKeychainWrapper
import MaterialComponents.MaterialSnackbar
import MaterialComponents.MaterialSnackbar_ColorThemer
import MaterialComponents.MaterialSnackbar_TypographyThemer

class BaseViewController: UIViewController {
    let refreshTokenNotificationKey = "com.sbm.insite.manager.refreshToken"
    let fetchAssocateInfoNotificationKey = "com.sbm.insite.manager.fetchAssocateInfo"

    let buildingText = NSLocalizedString("key_building", comment: "")
    let floorText = NSLocalizedString("key_floor", comment: "")
    
    var isKeyboardOpen: Bool = false
    
    //calendar icon
    var calendarBtn  = UIButton(type: .custom)
    var selectedDate: Date?
    
    var remoteManager: RemoteManager?
    var feature: FeatureName?
    var selectedEmployee: TeamMemberV2?
    var selectedEmployees: [TeamMemberV2] = []
    var selectedJobList: [JobForSite] = []
    var selectedDepartmentList: [Department]?
    
    var isTokenExpirationChanged = false;
    var refreshTokenNotificationName: Notification.Name?
    var fetchAssociateInfoNotificationName: Notification.Name?
    
    var selectEmployeeViewController : SelectEmployeeViewController?
    var employeePopupViewController: EmployeePopupViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.statusBarBackgroundColor = .black
        
        validateToken()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        if(employeePopupViewController != nil){
            employeePopupViewController?.dismiss(animated: true)
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        if(refreshTokenNotificationName != nil || fetchAssociateInfoNotificationName != nil ){
            NotificationCenter.default.removeObserver(self, name: refreshTokenNotificationName, object: nil)
        }
        if(fetchAssociateInfoNotificationName != nil){
            NotificationCenter.default.removeObserver(self, name: fetchAssociateInfoNotificationName, object: nil)
        }
        
        if(selectEmployeeViewController != nil){
            selectEmployeeViewController?.dismiss(animated: true)
        }
    }

    func topMostController() -> UIViewController {
        var topController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        while (topController.presentedViewController != nil) {
            topController = topController.presentedViewController!
        }
        return topController
    }
    
    func initHeaderView(headerView: HeaderView, screenName: String){
        headerView.screenName.text = screenName
        headerView.onCloseBtnClicked = {
            self.discardWarningAlert()
        }
    }
    func initDotsView(dots: [UIButton], selectedIndex: Int){
        for i in 0..<dots.count {
            if i <= selectedIndex {
                dots[i].backgroundColor = UIColor.init(hexString: Constants.pageSelectedDotBGColor)
                dots[i].setTitleColor(UIColor.init(hexString: Constants.pageSelectedDotTextColor), for: .normal)
            }else{
                dots[i].backgroundColor = UIColor.init(hexString: Constants.pageUnSelectedDotBGColor)
                dots[i].setTitleColor(UIColor.init(hexString: Constants.pageUnSelectedDotTextColor), for: .normal)
            }
        }
    }
    
    func initAssignToOneView(assignToView: AssignToOneView, featureName: FeatureName) {
        let firstName = self.selectedEmployee!.FirstName ?? ""
        let lastName = self.selectedEmployee!.LastName ?? ""
        let fullName = firstName + " " + lastName
        
        
        //TODO: needs to be removed from here later
        assignToView.onEditBtnClicked = {
            self.goSelectEmployeeViewController(featureName: featureName)
        }
        
        assignToView.fullNameLable.text = "\(fullName)      "
        assignToView.fullNameLable.sizeToFit()
        assignToView.fullNameLable.padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        assignToView.fullNameLable.layer.masksToBounds = true
        assignToView.fullNameLable.layer.cornerRadius = assignToView.fullNameLabelHeight.constant/2
//        assignToView.fullNameLable.frame.size.width = assignToView.fullNameLable.intrinsicContentSize.width + 100
    }
    
    func initAssignToOneView(assignToView: AssignToOneView) {
//        let firstName = self.selectedEmployee!.firstName ?? ""
//        let lastName = self.selectedEmployee!.lastName ?? ""
//        let fullName = firstName + " " + lastName
        
        if(selectedEmployee == nil){
            return
        }
        assignToView.fullNameLable.text = "\(self.selectedEmployee!.FirstName ?? "") \(self.selectedEmployee!.LastName ?? "")      "
        assignToView.fullNameLable.sizeToFit()
        assignToView.fullNameLable.padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        assignToView.fullNameLable.layer.masksToBounds = true
        assignToView.fullNameLable.layer.cornerRadius = assignToView.fullNameLabelHeight.constant/2
        //        assignToView.fullNameLable.frame.size.width = assignToView.fullNameLable.intrinsicContentSize.width + 100
    }
    
    func initCalendarBtn(dateTextField: UITextField){
        //calendarBtn: var calendarBtn  = UIButton(type: .custom)
        calendarBtn.frame = CGRect(x:0, y:0, width:30, height:30)
        calendarBtn.setImage(UIImage(named: "Calendar-icon.png"), for: .normal)
        calendarBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
        dateTextField.rightViewMode = .always
        dateTextField.rightView = calendarBtn
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(refreshTokenNotificationName != nil || fetchAssociateInfoNotificationName != nil ){
            NotificationCenter.default.removeObserver(self)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setImage(imageView: UIImageView, imgUrl: String, isCircle: Bool) {
        if(isCircle){
            imageView.layer.cornerRadius = imageView.frame.size.width / 2
            imageView.clipsToBounds = true
        }
        imageView.af_setImage(withURL: URL(string: imgUrl)!, placeholderImage: UIImage(named: "Profile Default Pic.png")!)
    }
    
    func getIsPortrait() -> Bool{
        return UIDevice.current.orientation.isLandscape == true ? false : true
    }
    
    func getToken() -> String? {
        let token: String = KeychainWrapper.standard.string(forKey: "access_token") ?? ""
//        let token: String = (SBMTokenHelper.get()?.token_type)! + " " + (SBMTokenHelper.get()?.access_token)!
        return token
    }
    
//    func validateToken() {
//        if(getToken() != nil){
//            let expiration = Int64(KeychainWrapper.standard.string(forKey: "expires_in")!)!
//            let expiration = SBMTokenHelper.get()?.expires_in
//            let now: Int64  = Date().toSeconds()
//            if(expiration! > now + 2 * 24 * 60){  //2 days buffer for refresh token
//                return
//            }
//        }
//        //TODO: relogin to refresh token
//        isRefreshTokenRequired = true
//        refreshToken()
//        return
//    }
    
    //only if the fresh new install, token and expiration can be unset, it will continue without refresh token
    //so refresh token only if token was not nil
    func validateToken() {
        if(getToken() != ""){
            let expirationText = KeychainWrapper.standard.string(forKey: "expires_in") ?? ""
            if(expirationText != ""){
                let expiration = Int64(expirationText)
                //let expiration = SBMTokenHelper.get()?.expires_in
                let now: Int64  = Date().toSeconds()
                if(expiration! > now + 2 * 24 * 60){  //2 days buffer for refresh token
                    //Check associate object
                    validateAssociate()
                    return
                }else{
                    //TODO: relogin to refresh token
//                    isTokenExpired = true
                    refreshTokenNotificationName = Notification.Name(rawValue: refreshTokenNotificationKey)
                    refreshToken()
                }
            }
        }

        return
    }
    
    fileprivate func validateAssociate(){
        if(AssociateHelper.get()?.employeeId == nil){
            fetchAssociateInfoNotificationName = Notification.Name(rawValue: fetchAssocateInfoNotificationKey)
            fetchAssociateInfo()
        }
    }
    fileprivate func clearKeychainData(){
        let isRemoved: Bool = KeychainWrapper.standard.removeAllKeys()
    }
    func showSnackBar(messageText: String, messageTextColor: UIColor, actionTitle: String, actionTitleColor: UIColor, snackBarBackgroundColor: UIColor){
    
        let snackBar = MDCSnackbarMessage()
        MDCSnackbarMessage.usesLegacySnackbar = false
        snackBar.text = messageText
        
        let action = MDCSnackbarMessageAction()
        action.title = actionTitle
        snackBar.action = action
        snackBar.buttonTextColor = actionTitleColor
        
        MDCSnackbarManager.show(snackBar)
        
        let colorScheme = MDCSemanticColorScheme()
        colorScheme.surfaceColor = messageTextColor
        colorScheme.onSurfaceColor = snackBarBackgroundColor
        MDCSnackbarColorThemer.applySemanticColorScheme(colorScheme)
        
    }
    
    func discardWarningAlert(){
        showAlert(title: NSLocalizedString("key_infomation", comment: ""), message: NSLocalizedString("key_discard_warning_message", comment: ""), positiveBtn: NSLocalizedString("key_ok", comment: ""), nagetiveBtn: NSLocalizedString("key_cancel", comment: ""),  viewController: self, completion: discardWarningAlertCompletion)
    }
    
    func showInfoAlert(message: String){
        showAlert(title: NSLocalizedString("key_infomation", comment: ""), message: message, positiveBtn: NSLocalizedString("key_ok", comment: ""), nagetiveBtn: NSLocalizedString("key_cancel", comment: ""),  viewController: self, completion: discardWarningAlertCompletion)
    }
    
//    func provideInputAlert(){
//        showAlert(title: NSLocalizedString("key_infomation", comment: ""), message: NSLocalizedString("key_provide_input", comment: ""), positiveBtn: NSLocalizedString("key_ok", comment: ""), nagetiveBtn: NSLocalizedString("key_cancel", comment: ""),  viewController: self, completion: discardWarningAlertCompletion)
//    }
    
    func discardWarningAlertCompletion(isPositiveAnswer: Bool){
        print("### ### warningAlertCompletion isPositiveAnswer: \(isPositiveAnswer)")
        if(isPositiveAnswer){
            goLandingViewController()
        }
    }
//    fileprivate func popupSelectEmployeeViewController(featureName: FeatureName, isEdit: Bool){
//        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "SelectEmployeeViewController") as! SelectEmployeeViewController
//        viewController.remoteManager = remoteManager
//        viewController.featureName = featureName
//        viewController.selectedEmployees = selectedEmployees
//        viewController.onConfirmBtnClicked = onConfirmBtnClicked
//
//        viewController.modalTransitionStyle = .crossDissolve
//        viewController.modalPresentationStyle = .overCurrentContext
//        present(viewController, animated: true, completion: nil)
//    }
    
    func goSelectEmployeeViewController(featureName: FeatureName){
        print("### ### goSelectEmployeeViewController")
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SelectEmployeeViewController") as! SelectEmployeeViewController
        viewController.remoteManager = remoteManager
        viewController.featureName = featureName
        if(selectedEmployee != nil){
            viewController.selectedEmployees.append(selectedEmployee!)
        }
//        if(featureName == FeatureName.Complaint){
//            viewController.selectedEmployees = selectedEmployees
//        }
        present(viewController, animated: true, completion: nil)
    }
    
    func goTabBarController(isCompletion: Bool, isSuccess: Bool, tabIndex: Int){
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        
        let tabBarController = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
        tabBarController.remoteManager = remoteManager
        tabBarController.isCompletion = isCompletion
        tabBarController.isSuccess = isSuccess
        tabBarController.selectedIndex = tabIndex
        present(tabBarController, animated: true, completion: nil)
    }
    
    func goLandingViewController(isCompletion: Bool, isSuccess: Bool){
        
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        if (appDelegate.window?.rootViewController?.presentedViewController) != nil
//        {
//            // Array of all viewcontroller even after presented
//            let vc = appDelegate.window?.rootViewController?.presentedViewController
//            print(vc?.nibName)
//        }
//        if (appDelegate.window?.rootViewController?.childViewControllers) != nil
//        {
//            // Array of all viewcontroller after push
//            let vcs = appDelegate.window?.rootViewController?.childViewControllers
//            print(vcs?.count)
//        }
        
//        self.dismiss(animated: true, completion: {
//            if(self.selectEmployeeViewController != nil){
//                self.selectEmployeeViewController?.dismiss(animated: true)
//            }
//        })
        
//        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
//        let landingVC = storyboard.instantiateViewController(withIdentifier: "LandingViewController") as! LandingViewController
//        landingVC.remoteManager = remoteManager
//        landingVC.isCompletion = isCompletion
//        landingVC.isSuccess = isSuccess
//        present(landingVC, animated: true, completion: nil)
        
        self.dismiss(animated: true)
    }
    
    func goLandingViewController(){
        goLandingViewController(isCompletion: false, isSuccess: false)
    }
    
    func goLoginViewController(){
        print("### ### goToLoginViewController")
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        loginVC.remoteManager = remoteManager
        present(loginVC, animated: true, completion: nil)
    }
    func reLaunchLogin(){
        showAlert(NSLocalizedString("key_infomation", comment: ""), NSLocalizedString("key_login_again", comment: ""), NSLocalizedString("key_dismiss", comment: ""), viewController: self)
        clearKeychainData()
        goLoginViewController()
    }
    
    func refreshToken(){
        print("### ### refreshToken")
        if(refreshTokenNotificationName == nil){
            print("********** 1 refreshTokenNotificationName == nil")
        }else{
            print("********** 1 refreshTokenNotificationName != nil")
        }
//        let userName: String = (LoginCredentialsHelper.get()?.userName)!
//        let password: String = (LoginCredentialsHelper.get()?.password)!
        let userName: String = KeychainWrapper.standard.string(forKey: "user_name") ?? ""
        let password: String = KeychainWrapper.standard.string(forKey: "password") ?? ""
        login(userName: userName, password: password)
    }
    
    func login(userName: String, password: String){
        
        if(remoteManager == nil){
            fatalError("Missing dependencies on login")
        }
        
        remoteManager!.login(remoteManager: remoteManager!, userName: userName, password: password, completion: loginCompletion)
    }
    
    fileprivate func loginCompletion(_ sbmToken: SBMToken?, _ error: Error?, _ responseCode: Int){
//        if(sbmToken != nil){
//            if(refreshTokenNotificationName != nil){
//                NotificationCenter.default.post(name: refreshTokenNotificationName!, object: nil)
//                refreshTokenNotificationName = nil
//            }
//        }else{
//            var messageText = NSLocalizedString("key_unable_login", comment: "")
//            if(responseCode == Constants.ResponseFailed401){
//                messageText = NSLocalizedString("key_username_password_incorrect", comment: "")
//            }else if (responseCode == Constants.OffLineError){
//                messageText = NSLocalizedString("key_device_off_line", comment: "")
//            }
//            showSnackBar(messageText: messageText,
//                         messageTextColor: UIColor.init(hexString: Constants.textWhite),
//                         actionTitle: NSLocalizedString("key_dismiss", comment: ""),
//                         actionTitleColor: UIColor.init(hexString: Constants.btnTextBlue),
//                         snackBarBackgroundColor: UIColor.init(hexString: Constants.dropDownRowBG))
//        }
        
        
        print("### ### 111")
//        if(refreshTokenNotificationName != nil){
//            print("### ### 222")
////      NotificationCenter.default.post(name: NSNotification.Name("userUpdate"), object: self, userInfo: ["name": "Jim"])
//
//            NotificationCenter.default.post(name: refreshTokenNotificationName!, object: self, userInfo: ["responseCode": responseCode])
//            refreshTokenNotificationName = nil
//        }
        
        NotificationCenter.default.post(name: refreshTokenNotificationName!, object: self, userInfo: ["responseCode": responseCode])
        
        
        if(sbmToken == nil){
            var messageText = NSLocalizedString("key_unable_login", comment: "")
            if(responseCode == Constants.ResponseFailed401){
                messageText = NSLocalizedString("key_username_password_incorrect", comment: "")
            }else if (responseCode == Constants.OffLineError){
                messageText = NSLocalizedString("key_device_off_line", comment: "")
            }
            showSnackBar(messageText: messageText,
                         messageTextColor: UIColor.init(hexString: Constants.snackbarMsgColor),
                         actionTitle: NSLocalizedString("key_dismiss", comment: ""),
                         actionTitleColor: UIColor.init(hexString: Constants.snackbarActionColor),
                         snackBarBackgroundColor: UIColor.init(hexString: Constants.snackbarBgColor))
        }
    }
    
    func fetchAssociateInfo(){
        let token: String = KeychainWrapper.standard.string(forKey: "access_token") ?? ""
        if(token == ""){
            goLoginViewController()
        }else{
            remoteManager!.fetchAssociateInfo(remoteManager: remoteManager! , token: token, completion: fetchAssociateInfoCompletion)
        }
    }
  
    fileprivate func fetchAssociateInfoCompletion(_ associateInfo: AssociateInfo?, _ error: Error?, _ responseCode: Int){
//        NotificationCenter.default.post(name: Notification.Name(rawValue: fetchAssocateInfoNotificationKey), object: nil)
        if let error = error{
            //if connnected, it must be something wrong
            if(Connectivity.isConnectedToInternet){
                reLaunchLogin()
            }
        }else{
            NotificationCenter.default.post(name: Notification.Name(rawValue: fetchAssocateInfoNotificationKey), object: nil)
        }
    }
    
    
    //This could be for all post request completion as long as get jstring of response
    func submitCompletion (_ responseString: String?, _ error: Error?, _ responseCode: Int){
        if(responseCode == Constants.ResponseOK){
            goLandingViewController(isCompletion: true, isSuccess: true)
        }else{
            goLandingViewController(isCompletion: true, isSuccess: false)
        }
    }
}

extension UIApplication {
    class var statusBarBackgroundColor: UIColor? {
        get {
            return (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor
        } set {
            (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor = newValue
        }
    }
}

extension UITabBarController {
    
    open override var childViewControllerForStatusBarStyle: UIViewController? {
        return selectedViewController
    }
    
//    //swift 4.2
//    open override var childForStatusBarStyle: UIViewController? {
//        return selectedViewController
//    }
}

extension UINavigationController {
    open override var childViewControllerForStatusBarStyle: UIViewController? {
        return visibleViewController
    }
    
//    //swift 4.2
//    open override var childForStatusBarStyle: UIViewController? {
//        return visibleViewController
//    }
}


//protocol TopUIViewController {
//    func topUIViewController() -> UIViewController?
//}
//
//extension UIWindow : TopUIViewController {
//    func topUIViewController() -> UIViewController? {
//        if let rootViewController = self.rootViewController {
//            return self.recursiveTopUIViewController(from: rootViewController)
//        }
//
//        return nil
//    }
//
//    private func recursiveTopUIViewController(from: UIViewController?) -> UIViewController? {
//        if let topVC = from?.topUIViewController() { return recursiveTopUIViewController(from: topVC) ?? from }
//        return from
//    }
//}
//
//extension UIViewController : TopUIViewController {
//    @objc open func topUIViewController() -> UIViewController? {
//        return self.presentedViewController
//    }
//}
//
//extension UINavigationController {
//    override open func topUIViewController() -> UIViewController? {
//        return self.visibleViewController
//    }
//}
//
//extension UITabBarController {
//    override open func topUIViewController() -> UIViewController? {
//        return self.selectedViewController ?? presentedViewController
//    }
//}
