//
//  AddLabelBaseViewController.swift
//  Manage
//
//  Created by Rong Li on 1/11/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class AddLabelKeyboardBaseViewController: KeyboardBaseViewController {
    let editText = NSLocalizedString("key_edit", comment: "")
    
    let labelHeight = 20
    let margin:  Int = 5
    let horizontalPadding:  Int = 20
    var width: CGFloat = 0
    var yPos: Int = 0
    var xPos: Int = 0
    var yOriginalPos: Int = 0
    var scrollContentViewOriginalHeight: CGFloat = 0.0
    var nameLabelViewOriginalHeight: CGFloat = 0.0
    var nameContainerViewOriginalHeight: CGFloat = 90
    
    var assignTo: AssignToView!
    var nameContainer: UIView!
    
    var scrollContentHeight: NSLayoutConstraint!
    var nameLabelHeight: NSLayoutConstraint!
    var nameContainerHeight: NSLayoutConstraint!
    var nameContainerTop: NSLayoutConstraint!


    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //add employees
    func addNames(nameContainerView: UIView, isShowAll: Bool){
        width = 0
        xPos = 0
        yPos = 0
        var rows = 0
        
        let font = UIFont.boldSystemFont(ofSize: 10.0)
        for i in 0..<selectedEmployees.count {
            let name = "\(selectedEmployees[i].FirstName ?? "") \(selectedEmployees[i].LastName!)"
            
            let label = UILabel()
            label.backgroundColor = UIColor.init(hexString: Constants.btnDarkGrayBG)
            label.textColor = UIColor.init(hexString: Constants.btnLightGrayBG)
            label.font = font
            label.textAlignment = .left
            
            label.text = name
            let text = label.text! as NSString
            let size = text.size(withAttributes: [NSAttributedStringKey.font:font])
            
            label.frame = CGRect( x:Int(xPos), y:yPos, width:Int(size.width) + horizontalPadding, height: labelHeight)
            label.layer.cornerRadius = CGFloat(labelHeight/2)
            label.layer.masksToBounds = true
            label.padding = UIEdgeInsets(top: 0, left: CGFloat(horizontalPadding/2), bottom: 0, right: 0)
            
            width += label.frame.size.width + CGFloat(margin)
            
            if(width > nameContainerView.frame.width){
                rows += 1
                width = 0
                xPos = 0
                yPos += labelHeight + Int(margin)
                
                print("\(yPos + Int(margin) + labelHeight) \(nameContainerViewOriginalHeight)")
                if(!isShowAll && yPos + Int(margin) + labelHeight > Int(nameContainerViewOriginalHeight)){
                    break
                }
                label.frame = CGRect( x:Int(xPos), y:yPos, width:Int(size.width) + horizontalPadding, height: labelHeight)
                width += label.frame.size.width + CGFloat(margin)
            }
            
            xPos = Int(width + CGFloat(margin * 2))
            nameContainerView.addSubview(label)
            
        }
        yPos += margin * 4
    }

    //add positions/jobs
    func addNames(nameContainerView: UIView, isShowAll: Bool, featureName: FeatureName){
        width = 0
        xPos = 0
        yPos = 0
        var rows = 0
        
        let font = UIFont.boldSystemFont(ofSize: 10.0)
        for i in 0..<selectedJobList.count {
            let name = "\(selectedJobList[i].Name ?? "")"
            
            let label = UILabel()
            label.backgroundColor = UIColor.init(hexString: Constants.btnDarkGrayBG)
            label.textColor = UIColor.init(hexString: Constants.btnLightGrayBG)
            label.font = font
            label.textAlignment = .left
            
            label.text = name
            let text = label.text! as NSString
            let size = text.size(withAttributes: [NSAttributedStringKey.font:font])
            
            label.frame = CGRect( x:Int(xPos), y:yPos, width:Int(size.width) + horizontalPadding, height: labelHeight)
            label.layer.cornerRadius = CGFloat(labelHeight/2)
            label.layer.masksToBounds = true
            label.padding = UIEdgeInsets(top: 0, left: CGFloat(horizontalPadding/2), bottom: 0, right: 0)
            
            width += label.frame.size.width + CGFloat(margin)
            
            if(width > nameContainerView.frame.width){
                rows += 1
                width = 0
                xPos = 0
                yPos += labelHeight + Int(margin)
                
                print("\(yPos + Int(margin) + labelHeight) \(nameContainerViewOriginalHeight)")
                if(!isShowAll && yPos + Int(margin) + labelHeight > Int(nameContainerViewOriginalHeight)){
                    break
                }
                label.frame = CGRect( x:Int(xPos), y:yPos, width:Int(size.width) + horizontalPadding, height: labelHeight)
                width += label.frame.size.width + CGFloat(margin)
            }
            
            xPos = Int(width + CGFloat(margin * 2))
            nameContainerView.addSubview(label)
            
        }
        yPos += margin * 1
    }
    
    //update selected employees
    func updateNameContainerView(isShowAll: Bool){
        updateNameContainerView(isShowAll: isShowAll, featureName: nil)
    }

    //FeatureName.SendMessage: update selected positons/jobs
    func updateNameContainerView(isShowAll: Bool, featureName: FeatureName?){
        for label in nameContainer.subviews {
            label.removeFromSuperview()
        }
        nameContainerHeight.constant = nameContainerViewOriginalHeight
        
        if(featureName != nil && featureName == FeatureName.SendMessage){
            addNames(nameContainerView: nameContainer, isShowAll: isShowAll, featureName: FeatureName.SendMessage)
        }else{
            addNames(nameContainerView: nameContainer, isShowAll: isShowAll)
        }
    }
    
    func updateNameLabelView(){
        var title: String = ""
        
        if let attributedTitle = assignTo.toggleBtn.attributedTitle(for: .normal) {
            let mutableAttributedTitle = NSMutableAttributedString(attributedString: attributedTitle)
            
            if (assignTo.toggleBtn.tag == 1){
                title = NSLocalizedString("key_see_less", comment: "")
                updateNameContainerView(isShowAll: true)
                
                nameLabelHeight.constant = nameLabelViewOriginalHeight + CGFloat(yPos - yOriginalPos) + CGFloat(margin * 1)
                scrollContentHeight.constant = scrollContentViewOriginalHeight + CGFloat(yPos - yOriginalPos)
            }else{
                title = NSLocalizedString("key_see_all", comment: "")
                updateNameContainerView(isShowAll: false)
                nameLabelHeight.constant = nameLabelViewOriginalHeight
                scrollContentHeight.constant = scrollContentViewOriginalHeight
            }
            
            mutableAttributedTitle.replaceCharacters(in: NSMakeRange(0, mutableAttributedTitle.length), with: title)
            assignTo.toggleBtn.setAttributedTitle(mutableAttributedTitle, for: .normal)
        }
    }
    
    func updateEditBtn(title: String){
        if let attributedTitle = assignTo.editBtn.attributedTitle(for: .normal) {
            let mutableAttributedTitle = NSMutableAttributedString(attributedString: attributedTitle)
            mutableAttributedTitle.replaceCharacters(in: NSMakeRange(0, mutableAttributedTitle.length), with: title)
            assignTo.editBtn.setAttributedTitle(mutableAttributedTitle, for: .normal)
            
            if(title == editText){
                assignTo.toggleBtn.isHidden = false
            }else{
                assignTo.toggleBtn.isHidden = true
            }
        }
    }
}
