//
//  CompletionViewController.swift
//  Manage
//
//  Created by Rong Li on 12/19/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class CompletionViewController: UIViewController {
    var isSuccess: Bool?

    @IBOutlet weak var completionImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.touchAction (_:)))
        view.addGestureRecognizer(gesture)
        
        if(!(isSuccess ?? true)){
            //TODO: set Fail image
        }
    }
    

    @objc func touchAction(_ sender: UITapGestureRecognizer){
        print("************** touchAction")
        dismiss(animated: true)
    }

}
