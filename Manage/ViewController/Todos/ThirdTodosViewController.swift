//
//  ThirdViewController.swift
//  Manage
//
//  Created by Rong Li on 12/5/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit
import AWSS3
import AWSCore

import Alamofire
import SwiftyJSON

class ThirdTodosViewController: BaseViewController {
    let currentPageIndex = 2
    
    var todoItem: TodoItemMappable?

    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: FourDotsView!
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()

    }

//    func discardWarningAlert(){
//        showAlert(title: NSLocalizedString("key_infomation", comment: ""), message: NSLocalizedString("key_discard_warning_message", comment: ""), positiveBtn: NSLocalizedString("key_ok", comment: ""), nagetiveBtn: NSLocalizedString("key_cancel", comment: ""),  viewController: self, completion: discardWarningAlertCompletion)
//    }
//
//    func discardWarningAlertCompletion(isPositiveAnswer: Bool){
//        print("### ### warningAlertCompletion isPositiveAnswer: \(isPositiveAnswer)")
//        if(isPositiveAnswer){
//            goLandingViewController()
//        }
//    }
    
    fileprivate func initViews(){
        
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_todos", comment: ""))
        
        let dots = [dotsView.dot1, dotsView.dot2, dotsView.dot3, dotsView.dot4]
        initDotsView(dots: dots as! [UIButton], selectedIndex: currentPageIndex)
        
//        UIApplication.statusBarBackgroundColor = .black
//        
//        headerView.screenName.text = NSLocalizedString("key_todos", comment: "")
//        headerView.onCloseBtnClicked = {
////            self.goLandingViewController()
//            self.discardWarningAlert()
//        }
//        
//        dotsView.dot1.backgroundColor = UIColor.init(hexString: "#7680C8")
//        dotsView.dot2.backgroundColor = UIColor.init(hexString: "#7680C8")
//        dotsView.dot3.backgroundColor = UIColor.init(hexString: "#7680C8")
//        dotsView.dot4.backgroundColor = UIColor.init(hexString: "#3D3C4D")
//        
//        dotsView.dot1.setTitleColor(UIColor.init(hexString: "#04040E"), for: .normal)
//        dotsView.dot2.setTitleColor(UIColor.init(hexString: "#04040E"), for: .normal)
//        dotsView.dot3.setTitleColor(UIColor.init(hexString: "#04040E"), for: .normal)
//        dotsView.dot4.setTitleColor(UIColor.init(hexString: "#E0E0E0"), for: .normal)
    }
    
    fileprivate func attachImage(){
        ImagePickerManager().pickImage(self){ image in
            self.imageView.image = image
            
//            self.uploadImageToAWS()
            
//            self.uploadImage()
            
//            self.upload()
            
//            self.fetchAwsCredentials(apiRouter: APIRouter.awsCredentials)
            
            self.uploadImage_1()
        }
    }
    
    func isReadyMoveNext() -> Bool{
        return true
    }

    @IBAction func attachmentBtnClicked(_ sender: Any) {
        attachImage()
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        let pageController = self.parent as! TodosPageViewController
        pageController.onNextBtnClicked(currentIndex: currentPageIndex)
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        let pageController = self.parent as! TodosPageViewController
        pageController.onBackBtnClicked(currentIndex: currentPageIndex)
    }
    
//    let bucketName = "sbm-insight"
    let bucketName = "sbm-insite"
    var contentUrl: URL!
    var s3Url: URL!
    
    
    func uploadImage_1(){
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType:.USEast1,
                                                                identityPoolId:"us-east-1:aa886eca-06e4-48ce-9f1d-e6011540234d")
        
        let configuration = AWSServiceConfiguration(region:.USEast1, credentialsProvider:credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        var awsServiceManger = AWSServiceManager.default()
        var awsServiceConfiguration = awsServiceManger?.defaultServiceConfiguration
            awsServiceConfiguration = configuration
        
        
        print("********** endpoint \(AWSServiceManager.default().defaultServiceConfiguration.endpoint)")
        
        s3Url = AWSS3.default().configuration.endpoint.url
//        AWSS3.default().configuration.endpoint.url = "\(bucketName)" + "." + AWSS3.default().configuration.endpoint.url
        
        print("********** s3Url \(s3Url)")
        print("********** debug: \(AWSS3.default().configuration.endpoint.debugDescription)")
        
        let key = "test.png"
        let localImagePath = Bundle.main.path(forResource: "test", ofType: "png")!
        let localImageUrl = URL(fileURLWithPath: localImagePath)
        
        let request = AWSS3TransferManagerUploadRequest()!
        request.bucket = bucketName
        request.key = key
        request.body = localImageUrl
        request.acl = .publicReadWrite
        
        
        
        
        let transferManager = AWSS3TransferManager.default()
//        do{
//            let asUrl = try transferManager.debugDescription.asURL()
//            print("************ debugDescription: \(asUrl)")
//            print("************")
//        }catch{
//            print("************ error \(error)")
//        }
        transferManager.upload(request).continueWith(executor: AWSExecutor.mainThread()) { (task) -> Any? in
           
            let contentUrl = self.s3Url.appendingPathComponent(self.bucketName).appendingPathComponent(key)
            self.contentUrl = contentUrl
            print("********** \(contentUrl)")
            if let error = task.error {
                print("Upload failed ❌ (\(error))")
            }
            if task.result != nil {
                print("Uploaded \(key)")
                let contentUrl = self.s3Url.appendingPathComponent(self.bucketName).appendingPathComponent(key)
                self.contentUrl = contentUrl
                print(contentUrl)
            }
            
            return nil
        }
    }
    
    
    
    func getUrl(at route: APIRouter, baseUrl: String) -> String{
        return route.url(baseUrl)
    }
    func getHeaders() -> [String: String]{
        let token = getToken()!
        let selectedSite = SelectedSiteHelper.get()!
        print("#### clientId: \(String(selectedSite.selectedClientId!))  siteId: \(String(selectedSite.selectedSiteId!))  programId: \(String(selectedSite.selectedProgramId!))" )
//        let languageId = getLanguage()
        let languageId = 1
        let headers = [
            "Authorization": token,
            "Content-Type": "application/json",
            "clientId": String(selectedSite.selectedClientId!),
            "siteId": String(selectedSite.selectedSiteId!),
            "programId": String(selectedSite.selectedProgramId!),
            "languageId": String(languageId)
            ] as [String : String]
        
        return headers
    }
    func fetchAwsCredentials(apiRouter: APIRouter){
        let url = getUrl(at: apiRouter, baseUrl: remoteManager!.baseUrl)
        let headers = getHeaders()
        
        print("### ### url: \(String(describing: url))")
        
        Alamofire.request(url, method:.get, headers: headers)
            .responseString { response in
                print("### ### Success: \(response.result.isSuccess)")
                print("### ### Response String: \(String(describing: response.result.value))")
                
                
                
                do {
                    if let body = try JSONSerialization.jsonObject(with: response.data!) as? [String: Any] {
                        let accessKey = body["AccessKey"] as! String
                        let accessSecret = body["AccessSecret"] as! String
                        print("### ### accessKey: \(accessKey)")
                        print("### ### accessSecret: \(accessSecret)")
                        
//                        self.uploadToAWS(accessKey: accessKey, accessSecret: accessSecret)
                        
//                        self.upload(accessKey: accessKey, accessSecret: accessSecret)
                        
//                        self.uploadImage(accessKey: accessKey, accessSecret: accessSecret)
                        
                    }
                } catch {
                    print("Error deserializing JSON: \(error)")
                }
        }
    }

    
    func uploadToAWS(accessKey: String, accessSecret: String){
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: accessSecret)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.USWest1, credentialsProvider: credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        let S3BucketName = "sbm-insight"
        let remoteName = "rli_test.png"
        
        let localImagePath = Bundle.main.path(forResource: "test", ofType: "png")!
        let localImageUrl = URL(fileURLWithPath: localImagePath)
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()!
//        uploadRequest.body = fileURL
        uploadRequest.body = localImageUrl
        uploadRequest.key = remoteName
        uploadRequest.bucket = S3BucketName
        uploadRequest.contentType = "image/png"
        uploadRequest.acl = .publicReadWrite
        
        let transferManager = AWSS3TransferManager.default()
        
        transferManager.upload(uploadRequest).continueWith { [weak self] (task) -> Any? in
//            DispatchQueue.main.async {
//                self?.uploadButton.isHidden = false
//                self?.activityIndicator.stopAnimating()
//            }
            
            if let error = task.error {
                print("Upload failed with error: (\(error.localizedDescription))")
            }
            
            if task.result != nil {
                let url = AWSS3.default().configuration.endpoint.url
                let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                if let absoluteString = publicURL?.absoluteString {
                    print("Uploaded to:\(absoluteString)")
                }
            }
            
            return nil
        }
    }
    func upload(accessKey: String, accessSecret: String){
//        let credentialsProviderForS3 = AWSCognitoCredentialsProvider(
//            regionType:.USWest2,
//            identityPoolId: "us-west-2:d761e478-2f3f-4d9c-87ef-0c23dd8acaf8"
//        )
        
        
        let credentialsProviderForS3 = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: accessSecret)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.USWest1, credentialsProvider: credentialsProviderForS3)
        
        let awsConfigurationForS3 = AWSServiceConfiguration(
            region: .USWest1,
            credentialsProvider: credentialsProviderForS3
        )
        
        AWSServiceManager.default().defaultServiceConfiguration = awsConfigurationForS3
        s3Url = AWSS3.default().configuration.endpoint.url
   
        let transferManager = AWSS3TransferManager.default()
        
        let key = "test.png"
        let localImagePath = Bundle.main.path(forResource: "test", ofType: "png")!
        let localImageUrl = URL(fileURLWithPath: localImagePath)
        
        let request = AWSS3TransferManagerUploadRequest()!
        request.bucket = bucketName
        request.key = key
        request.body = localImageUrl
        request.acl = .publicReadWrite
        
        // `- upload:` is an asynchronous request. This means you need to retain a strong reference to `transferManager` until the async call completes.
        //        transferManager.upload(request)
        
        transferManager.upload(request).continueWith(executor: AWSExecutor.mainThread()) { (task) -> Any? in
            let contentUrl = self.s3Url.appendingPathComponent(self.bucketName).appendingPathComponent(key)
            self.contentUrl = contentUrl
            print(contentUrl)
            if let error = task.error {
                print("Upload failed ❌ (\(error))")
            }
            if task.result != nil {
                print("Uploaded \(key)")
                let contentUrl = self.s3Url.appendingPathComponent(self.bucketName).appendingPathComponent(key)
                self.contentUrl = contentUrl
                print(contentUrl)
            }
            
            return nil
        }
    }
    
    
////    let bucketName = "messing-with-aws"
//    let bucketName = "sbm-insight"
//    var contentUrl: URL!
//    var s3Url: URL!
    
    
//    func uploadImage(accessKey: String, accessSecret: String){
    func uploadImage(){
        
        let bucketName = "messing-with-aws"
        
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType:.USWest2,
                                                                identityPoolId:"us-west-2:97a13634-bb79-4043-b08e-4e59635bce6c")
        let configuration = AWSServiceConfiguration(region:.USWest2, credentialsProvider:credentialsProvider)
        
//        let credentialsProvider = AWSCognitoCredentialsProvider(regionType:.USWest2,
//                                                                identityPoolId:"us-west-2:d761e478-2f3f-4d9c-87ef-0c23dd8acaf8")
        
        
        
        
//        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: accessSecret)
//        let configuration = AWSServiceConfiguration(region:.USWest1, credentialsProvider:credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        s3Url = AWSS3.default().configuration.endpoint.url
        
        print("********** endpoint: \(AWSS3.default().configuration.endpoint.hostName)")
        print("********** s3Url: \(s3Url)")
        
        let key = "test.png"
        let localImagePath = Bundle.main.path(forResource: "test", ofType: "png")!
        let localImageUrl = URL(fileURLWithPath: localImagePath)
        
        let request = AWSS3TransferManagerUploadRequest()!
        request.bucket = bucketName
        request.key = key
        request.body = localImageUrl
        request.acl = .publicReadWrite
        
//        print("debugDescription:\n\(request.debugDescription)")
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(request).continueWith(executor: AWSExecutor.mainThread()) { (task) -> Any? in
            let contentUrl = self.s3Url.appendingPathComponent(self.bucketName).appendingPathComponent(key)
            self.contentUrl = contentUrl
            print(contentUrl)
            if let error = task.error {
                print("Upload failed ❌ (\(error))")
                
//                self.uploadImage(accessKey: accessKey, accessSecret: accessSecret)
            }
            if task.result != nil {
                print("Uploaded \(key)")
                let contentUrl = self.s3Url.appendingPathComponent(self.bucketName).appendingPathComponent(key)
                self.contentUrl = contentUrl
                print(contentUrl)
            }
            
            return nil
        }
    }
    
    func uploadImageToAWS() {
        let S3BucketName = "sbm-insight"
        let CognitoPoolID = "us-west-2:d761e478-2f3f-4d9c-87ef-0c23dd8acaf8"
//        let Region = AWSRegionType.USWest2

//        let CognitoPoolID = "us-west-1:d761e478-2f3f-4d9c-87ef-0c23dd8acaf8"
        let Region = AWSRegionType.USWest1
        
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType:Region,
                                                                identityPoolId:CognitoPoolID)
        let configuration = AWSServiceConfiguration(region:Region, credentialsProvider:credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        let imagePath = Bundle.main.path(forResource: "test", ofType: "png")
        print(imagePath)
        
        let ext = "png"
        let imageURL = Bundle.main.url(forResource: "test", withExtension: ext)!
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest!.body = imageURL
//        uploadRequest!.key = ProcessInfo.processInfo.globallyUniqueString + "." + ext
        uploadRequest!.key = "test.png"
        uploadRequest!.bucket = S3BucketName
        uploadRequest!.contentType = "image/" + ext
        
        
        print(print("debugDescription:\n\(uploadRequest.debugDescription)"))
        print(print("imageURL:\n\(imageURL)"))
        print(print("uploadRequest!.key:\n\(uploadRequest!.key)"))
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest!).continueWith { (task) -> AnyObject? in
            if let error = task.error {
                print("Upload failed ❌ (\(error))")
            }
            //            if let exception = task.description {
            //                print("Upload failed ❌ (\(exception))")
            //            }
            if task.result != nil {
                let s3URL = NSURL(string: "http://s3.amazonaws.com/\(S3BucketName)/\(uploadRequest!.key!)")!
                print("Uploaded to:\n\(s3URL)")
                
            }
            else {
                print("Unexpected empty result.")
            }
            return nil
        }
        
        
        //        let path = NSTemporaryDirectory().stringByAppendingString("image.jpeg")
        //
        //        if let data = UIImageJPEGRepresentation(image, 0.8) {
        //            data.writeToFile(path, atomically: true)
        //
        //        }
        //        self.dismissViewControllerAnimated(true, completion: {})
        //
        //        let credentialsProvider = AWSCognitoCredentialsProvider(regionType:CognitoRegionType,
        //                                                                identityPoolId:CognitoIdentityPoolId)
        //        let configuration = AWSServiceConfiguration(region:CognitoRegionType, credentialsProvider:credentialsProvider)
        //        AWSServiceManager.defaultServiceManager().defaultServiceConfiguration = configuration
        //
        //        let ext = "jpeg"
        //
        //        let uploadRequest = AWSS3TransferManagerUploadRequest()
        ////        uploadRequest.body = path
        //        uploadRequest.body = NSURL(string: "file://"+path)
        //        uploadRequest.key = NSProcessInfo.processInfo().globallyUniqueString + "." + ext
        //        uploadRequest.bucket = S3BucketName
        //        uploadRequest.contentType = "image/" + ext
        //
        ////        let s3URL = NSURL(string: "http://s3.amazonaws.com/\(S3BucketName)/\(uploadRequest.key!)")! print("Uploaded to:\n\(s3URL)")
        
    }
}
