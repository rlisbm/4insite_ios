//
//  FourthViewController.swift
//  Manage
//
//  Created by Rong Li on 12/5/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class FourthTodosViewController: BaseViewController {
    let currentPageIndex = 3
    
    var todoItem: TodoItemMappable?
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: FourDotsView!
    
    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var buildingName: UILabel! 
    @IBOutlet weak var floorName: UILabel!
    @IBOutlet weak var dueDate: UILabel!
    @IBOutlet weak var descriptionContent: UILabel!
    @IBOutlet weak var dueIn24: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()

    }
    
    
    fileprivate func initViews(){
        
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_todos", comment: ""))
        
        let dots = [dotsView.dot1, dotsView.dot2, dotsView.dot3, dotsView.dot4]
        initDotsView(dots: dots as! [UIButton], selectedIndex: currentPageIndex)
        
        fullName.text = self.selectedEmployee!.FirstName ?? "" + " " + self.selectedEmployee!.LastName!
        buildingName.text = todoItem?.buildingName
        floorName.text = todoItem?.floorName
        dueDate.text = todoItem?.dueDate
        dueIn24.text = todoItem?.dueIn24 ?? false ? "Yes" : "No"
        descriptionContent.text = todoItem?.comment
        
        
        todoItem?.orgUserId = (self.selectedEmployee?.EmployeeId)!
        
//        print(todoItem?.comment)
//        print(todoItem?.dueDate)
//        print(todoItem?.buildingId)
//        print(todoItem?.floorId)
//        print(todoItem?.orgUserId)
        
        todoItem?.comment = "test rli hhhh"
        todoItem?.dueDate = "2018-12-13T21:02:46"
        todoItem?.buildingId = 15688
        todoItem?.floorId = 35632
        todoItem?.orgUserId = 230954
    }
    
    
    func isReadyMoveNext() -> Bool{
        return false
    }

    @IBAction func editBtnClicked(_ sender: Any) {
    }

    @IBAction func submitBtnClicked(_ sender: Any) {
        submitTodoItem()
    }
    
    fileprivate func submitTodoItem(){
        print("*** *** submitTodoItem")
        if(remoteManager == nil){
            fatalError("Missing dependencies on submitTodoItem")
        }
        
        remoteManager!.submitTodoItem(remoteManager: remoteManager!, todoItem: todoItem!, completion: submitTodoItemCompletion)
        
    }
    
    fileprivate func submitTodoItemCompletion (_ todoItemId: TodoItemIdMappable?, _ error: Error?, _ responseCode: Int){
        if(responseCode == Constants.ResponseOK){
            goLandingViewController(isCompletion: true, isSuccess: true)
        }else{
            goLandingViewController(isCompletion: true, isSuccess: false)
        }
    }
    
   

}
