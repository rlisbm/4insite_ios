//
//  TodosPageViewController.swift
//  Manage
//
//  Created by Rong Li on 12/5/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class TodosPageViewController: BasePageViewController {

//    var selectedEmployee: TeamMember?
//    var remoteManager: RemoteManager?
    
    var todoItem: TodoItemMappable = TodoItemMappable()
    
//    var todosVCs = [UIViewController]()
    
    var todosVC1 : FirstTodosViewController?
    var todosVC2 : SecondTodosViewController?
    var todosVC3 : ThirdTodosViewController?
    var todosVC4 : FourthTodosViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        dataSource = self
        
//        self.removeGestureRecognizers()
        
        todosVC1 = storyboard?.instantiateViewController(withIdentifier: "FirstTodosViewController") as? FirstTodosViewController
        todosVC2 = storyboard?.instantiateViewController(withIdentifier: "SecondTodosViewController") as? SecondTodosViewController
        todosVC3 = storyboard?.instantiateViewController(withIdentifier: "ThirdTodosViewController") as? ThirdTodosViewController
        todosVC4 = storyboard?.instantiateViewController(withIdentifier: "FourthTodosViewController") as? FourthTodosViewController
        
        VCs.append(todosVC1!)
        VCs.append(todosVC2!)
        VCs.append(todosVC3!)
        VCs.append(todosVC4!)
        
        todosVC1?.remoteManager = remoteManager
        todosVC2?.remoteManager = remoteManager
        todosVC3?.remoteManager = remoteManager
        todosVC4?.remoteManager = remoteManager
        
        todosVC1?.selectedEmployee = selectedEmployee
        todosVC2?.selectedEmployee = selectedEmployee
        todosVC3?.selectedEmployee = selectedEmployee
        todosVC4?.selectedEmployee = selectedEmployee
        
        todosVC1?.todoItem = todoItem
        todosVC2?.todoItem = todoItem
        todosVC3?.todoItem = todoItem
        todosVC4?.todoItem = todoItem
        
        setViewControllers([VCs[0]], direction: .forward, animated: true, completion: nil)
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(pageTapped(sender:)))
//        self.view.subviews[0].addGestureRecognizer(tapGesture)
        
        
//        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
//        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
//
//        leftSwipe.direction = .left
//        rightSwipe.direction = .right
//
//        view.addGestureRecognizer(leftSwipe)
//        view.addGestureRecognizer(rightSwipe)
        
    }
    
//    @objc func pageTapped(sender: UITapGestureRecognizer) {
//        print("pageTapped")
//    }
    
//    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
//        
//        if (sender.direction == .left) {
//            print("Swipe Left")
//        }
//        
//        if (sender.direction == .right) {
//            print("Swipe Right")
//        }
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func isReadyMoveNext(currentIndex: Int) -> Bool {
//        switch currentIndex {
//        case 0:
//            return todosVC1?.isReadyMoveNext() ?? false
//        case 1:
//            return todosVC2?.isReadyMoveNext() ?? false
//        case 2:
//            return todosVC3?.isReadyMoveNext() ?? false
//        case 3:
//            return todosVC4?.isReadyMoveNext() ?? false
//        default:
//            return false
//        }
        
        return true
    }
    
    //MARK: UIPageViewControllerDataSource, UIPageViewControllerDelegate
    override func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = VCs.index(of: viewController) ?? 0
        
        if(!isReadyMoveNext(currentIndex: currentIndex)){
            return nil
        }
        
        if(currentIndex >= VCs.count - 1){
            return nil
        }

        return VCs[currentIndex + 1]
    }
}

extension UIPageViewController{
    func removeGestureRecognizers(){
        view.gestureRecognizers?.forEach({ (gesture) in
            view.removeGestureRecognizer(gesture)
        })
    }
}
