//
//  SecondTodosViewController.swift
//  Manage
//
//  Created by Rong Li on 12/5/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class SecondTodosViewController: BaseViewController, UITextViewDelegate {
    let currentPageIndex = 1
    
    var todoItem: TodoItemMappable?
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: FourDotsView!

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var dueBtn: UIButton!
    
//    //calendar icon
//    var calendarBtn  = UIButton(type: .custom)
//    var selectedDate: Date?
    
    let nextDayMidNight: Date = (Date().midNightOfNextDay)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()
        
        dateTextField.inputView = UIView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.init(hexString: Constants.textDarkWhite) {
            textView.text = nil
            textView.textColor = UIColor.init(hexString: Constants.textWhite)
        }
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
//    func textViewDidChangeSelection(_ textView: UITextView) {
//        if self.view.window != nil {
//            if (textView.textColor == UIColor.init(hexString: Constants.textDarkWhite)) {
//                textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
//            }
//        }
//    }
    //Touch screen outside textview to dismiss the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        textView.resignFirstResponder()
    }

    fileprivate func initViews(){
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_todos", comment: ""))
        
        let dots = [dotsView.dot1, dotsView.dot2, dotsView.dot3, dotsView.dot4]
        initDotsView(dots: dots as! [UIButton], selectedIndex: currentPageIndex)
        
        textView.layer.masksToBounds = true
        textView.layer.borderColor = UIColor.init(hexString: "#8785A1").cgColor
        textView.layer.borderWidth = 1
        
        textView.delegate = self
        textView.text = NSLocalizedString("key_description", comment: "")
        textView.textColor = UIColor.init(hexString: Constants.textDarkWhite)
        
        dateTextField.layer.masksToBounds = true
        dateTextField.layer.borderColor = UIColor.init(hexString: "#8785A1").cgColor
        dateTextField.layer.borderWidth = 1
        
        
        //calendarBtn: var calendarBtn  = UIButton(type: .custom)
        calendarBtn.frame = CGRect(x:0, y:0, width:30, height:30)
        calendarBtn.addTarget(self, action: #selector(self.popupDatePickerBtnClicked), for: .touchUpInside)
        calendarBtn.setImage(UIImage(named: "Calendar-icon.png"), for: .normal)
        calendarBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
        dateTextField.rightViewMode = .always
        dateTextField.rightView = calendarBtn
        
    }
    
//web    DueDate: "2018-12-14T07:59:59.999Z"
//mobile    DueDate: "2018-12-14T07:59:59"
    fileprivate func onSelectedDate(_ date: Date){
        selectedDate = date
        dateTextField.text = selectedDate!.formattedDateShort
        
//        var dateText = date.formateDate(date: date, withFormat: Constants.DATE_UTC_SS)
//        print("################ dateText: \(dateText)")
//        print("################ start of date: \(Date().startOfDay)")
//        print("################ end of date: \(Date().midNightOfNextDay)")
//        todoItem?.dueDate = dateText
    }
    
    @objc func popupDatePickerBtnClicked(sender:UITapGestureRecognizer) {
        let storyboard =  UIStoryboard(name: "Popup", bundle: nil)
        let viewControler = storyboard.instantiateViewController(withIdentifier: "DatePickerPopupViewController") as! DatePickerPopupViewController
        viewControler.onSelectedDate = onSelectedDate
        present(viewControler, animated: true, completion: nil)
    }
    
    func isReadyMoveNext() -> Bool{
//        todoItem?.comment = textView.text
//        if(dueBtn.tag == 1){
//            todoItem?.dueDate = nextDayMidNight.formateDate(date: nextDayMidNight, withFormat: Constants.DATE_UTC_SS)
//        }else {
//            todoItem?.dueDate = selectedDate == nil ? "" : selectedDate!.formateDate(date: selectedDate!, withFormat: Constants.DATE_UTC_SS)
//        }
//
//        if(todoItem?.comment?.isEmpty ?? true || todoItem?.dueDate?.isEmpty ?? true){
//            //Popup info dialog
//            showAlert(NSLocalizedString("key_infomation", comment: ""), NSLocalizedString("key_provide_input", comment: ""), NSLocalizedString("key_dismiss", comment: ""), viewController: self)
//            return false
//        }
//
//        return true
        
        return true
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        if(isReadyMoveNext()){
            let pageController = self.parent as! TodosPageViewController
            pageController.onNextBtnClicked(currentIndex: currentPageIndex)
        }
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        let pageController = self.parent as! TodosPageViewController
        pageController.onBackBtnClicked(currentIndex: currentPageIndex)
    }
    
    @IBAction func DueBtnClicked(_ sender: UIButton) {
        if(sender.tag == 0){
            sender.setImage(UIImage(named: "check-box-selected.png"), for: .normal)
            sender.tag = 1
            dateTextField.isUserInteractionEnabled = false
            dateTextField.textColor = UIColor.init(hexString: Constants.textBlack)
            dateTextField.placeholderColor(UIColor.init(hexString: Constants.textBlack))
            if(selectedDate != nil){
                todoItem?.dueDate = selectedDate!.formateDateUTC(date: selectedDate!, withFormat: Constants.DATE_UTC_SS)
                print("########## due1: \(todoItem?.dueDate)")
            }else{
                todoItem?.dueDate = ""
            }
            todoItem?.dueIn24 = true
        }else{
            sender.setImage(UIImage(named: "check-box-unselected.png"), for: .normal)
            sender.tag = 0
            dateTextField.isUserInteractionEnabled = true
            dateTextField.textColor = UIColor.init(hexString: Constants.textDarkWhite)
            dateTextField.placeholderColor(UIColor.init(hexString: Constants.textDarkWhite))
            todoItem?.dueDate = nextDayMidNight.formateDateUTC(date: nextDayMidNight, withFormat: Constants.DATE_UTC_SS)
            print("########## due2: \(todoItem?.dueDate)")
            todoItem?.dueIn24 = false
        }
        
    }
}

extension UITextField {
    func placeholderColor(_ color: UIColor){
        var placeholderText = ""
        if self.placeholder != nil{
            placeholderText = self.placeholder!
        }
        self.attributedPlaceholder = NSAttributedString(string: placeholderText, attributes: [NSAttributedStringKey.foregroundColor : color])
    }
}


//extension Date {
//    func localString(dateStyle: DateFormatter.Style = .medium,
//                     timeStyle: DateFormatter.Style = .medium) -> String {
//        return DateFormatter.localizedString(
//            from: self,
//            dateStyle: dateStyle,
//            timeStyle: timeStyle)
//    }
//    
//    var midnight: Date{
//        let cal = Calendar(identifier: .gregorian)
//        return cal.startOfDay(for: self)
//    }
//}
