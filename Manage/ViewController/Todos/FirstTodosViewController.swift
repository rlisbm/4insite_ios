//
//  FirstTodosViewController.swift
//  Manage
//
//  Created by Rong Li on 12/5/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit
import RealmSwift

class FirstTodosViewController: BaseViewController {
    let currentPageIndex = 0
    
    var todoItem: TodoItemMappable?
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: FourDotsView!
    @IBOutlet weak var assignToView: AssignToOneView!
    @IBOutlet weak var todoInfoLabel: UILabel!
    
    @IBOutlet weak var buildingDropDown: DropDown!
    @IBOutlet weak var floorDropDown: DropDown!
    
    var names = [String]()
    var ids = [Int]()
    
    var buildingList = List<KeyValueMappable>()
    var floorDic: Dictionary = [Int:[Floor]]()
    var floorIdDic: Dictionary = [Int:[Int]]()
    var floorNameDic: Dictionary = [Int:[String]]()
    
    var selectedBuilding: KeyValueMappable?
    var selectedFloor: KeyValueMappable?
    
    var isBuildingOpen: Bool = false
    var isFloorOpen: Bool = false
    var fetchIndex = 0;
    
//    let buildingText = NSLocalizedString("key_building", comment: "")
//    let floorText = NSLocalizedString("key_floor", comment: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if(refreshTokenNotificationName != nil){
            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
        }else{
            initViews()

            if(AssociateHelper.get()?.employeeId == nil){
                fetchAssociateInfoNotificationName = Notification.Name(rawValue: fetchAssocateInfoNotificationKey)
                NotificationCenter.default.addObserver(self, selector: #selector(fetchAssociateInfoCompletion(notification:)), name: fetchAssociateInfoNotificationName, object: nil)
            }else{
                fetchBuildings()
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
    }
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            if(isBuildingOpen){
                buildingDropDown.touchAction()
            }
        } else {
            print("Portrait")
        }
    }
    
    @objc func refreshTokenCompletion(notification: NSNotification){
        print("### ### FirstTodosViewController refreshTokenCompletion")
        if(self.isTokenExpirationChanged){
            fetchBuildings()
        }else{
            initViews()
        }
    }
    
    @objc func fetchAssociateInfoCompletion(notification: NSNotification){
        print("### ### FirstTodosViewController fetchAssociateInfoCompletion")
        fetchBuildings()
    }
    
    func initViews(){
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_todos", comment: ""))
        
        let dots = [dotsView.dot1, dotsView.dot2, dotsView.dot3, dotsView.dot4]
        initDotsView(dots: dots as! [UIButton], selectedIndex: currentPageIndex)
        
        initAssignToOneView(assignToView: assignToView, featureName: FeatureName.Todos)
//        setPadding()
        
//        buildingDropDown.setDropDown(rowBackgroundColor: "#212033", selectedRowColor: "#212033", rowTextColor: "#A7A7B2", separatorColor: "#A7A7B2", rowHeight: 50);
        buildingDropDown.setDropDown(rowBackgroundColor: Constants.dropDownRowBG, selectedRowColor: Constants.dropDownRowBG, rowTextColor: Constants.dropDownTextColor, rowHeight: 50);
        floorDropDown.setDropDown(rowBackgroundColor: Constants.dropDownRowBG, selectedRowColor: Constants.dropDownRowBG, rowTextColor: Constants.dropDownTextColor, rowHeight: 50);
        
//        buildingDropDown.layer.borderColor = UIColor(hexString: "#A7A7B2").cgColor
//        floorDropDown.layer.borderColor = UIColor(hexString: "#A7A7B2").cgColor
//        buildingDropDown.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
//        floorDropDown.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        
        buildingDropDown.didSelect{(selectedText , index , id) in
            print("Selected String: \(selectedText) \n index: \(index) \n Id: \(id)")
            self.selectedBuilding = KeyValueMappable(Key: id, Value: selectedText)
            self.isBuildingOpen = false
            self.updateFloorDropDown(selectedBuildingId: id, isResetTitle: true)
            self.todoItem!.buildingId = id
            self.todoItem!.buildingName = selectedText
        }
        
        floorDropDown.didSelect{(selectedText , index , id) in
            print("Selected String: \(selectedText) \n index: \(index) \n Id: \(id)")
            self.selectedFloor = KeyValueMappable(Key: id, Value: selectedText)
            self.isFloorOpen = false
            self.todoItem!.floorId = id
            self.todoItem!.floorName = selectedText
        }
        
        let buildingGesture = UITapGestureRecognizer(target: self, action:  #selector (self.buildingDropDownAction(sender:)))
        buildingDropDown.addGestureRecognizer(buildingGesture)
        let floorGesture = UITapGestureRecognizer(target: self, action:  #selector (self.floorDropDownAction(sender:)))
        floorDropDown.addGestureRecognizer(floorGesture)
        
    }
    
    func setPadding(){
        print("############ 0 \(assignToView.fullNameLable.frame.width)")
        assignToView.fullNameLable.frame.size.width = assignToView.fullNameLable.intrinsicContentSize.width + 100
        
        print("############ 1 \(assignToView.fullNameLable.frame.width)")
        var frame: CGRect = assignToView.fullNameLable.frame
        frame.size.width += 100;
        assignToView.fullNameLable.frame = frame;
        print("############ 2 \(assignToView.fullNameLable.frame.width)")
        let size = CGSize(width: frame.width, height: frame.height)
        assignToView.fullNameLable.sizeThatFits(size)
        assignToView.fullNameLable.padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)

    }
    
    @objc func buildingDropDownAction(sender: UITapGestureRecognizer){
        if(isFloorOpen){
            isFloorOpen = !isFloorOpen
            floorDropDown.touchAction()
            return
        }
        
        isBuildingOpen = !isBuildingOpen
        buildingDropDown.touchAction()
    }
    
    @objc func floorDropDownAction(sender: UITapGestureRecognizer){
        if(isBuildingOpen){
            isBuildingOpen = !isBuildingOpen
            buildingDropDown.touchAction()
            return
        }
        
        isFloorOpen = !isFloorOpen
        floorDropDown.touchAction()
    }
    
    func updateBuildingDropDown(_ buildings: NameKeyValueMappable){
        buildingDropDown.text = buildingText
        buildingDropDown.optionArray = names
        buildingDropDown.optionIds = ids
    }
    
    func updateFloorDropDown(selectedBuildingId: Int, isResetTitle: Bool){
        floorDropDown.text = floorText
        floorDropDown.selectedIndex = nil
        if(floorNameDic[selectedBuildingId] != nil && floorNameDic[selectedBuildingId]!.count > 0){
            floorDropDown.optionArray = floorNameDic[selectedBuildingId]!
            floorDropDown.optionIds = floorIdDic[selectedBuildingId]
        }else{
            floorDropDown.optionArray = []
            floorDropDown.optionIds = []
        }
        
        if(!isResetTitle){
            //TODO
        }
        
    }
    
//    func goSelectEmployeeViewController() {
//        print("### ### goSelectEmployeeViewController")
//        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "SelectEmployeeViewController") as! SelectEmployeeViewController
//        viewController.remoteManager = remoteManager
//        viewController.featureName = FeatureName.Todos
////        viewController.selectedEmployee = selectedEmployee
//        viewController.selectedEmployees.append(selectedEmployee!)
//        present(viewController, animated: true, completion: nil)
//    }
    
    func isReadyMoveNext() -> Bool{
//        return (buildingDropDown.text != buildingText) && (floorDropDown.text != floorText)
        
//        return todoItem?.buildingId != 0 && todoItem?.floorId != 0
        
        return true
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        if(isReadyMoveNext()){
            let pageController = self.parent as! TodosPageViewController
            pageController.onNextBtnClicked(currentIndex: 0)
        }
    }
    
    
    func fetchBuildings(){
        print("*** *** fetchBuildings")
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetche Buildings")
        }
        
        remoteManager!.fetchBuildings(remoteManager: remoteManager!, completion: fetchBuildingsCompletion)
        
    }
    
    fileprivate func fetchBuildingsCompletion(_ buildings: NameKeyValueMappable?, _ error: Error?, _ responseCode: Int){
        if(buildings != nil && buildings?.Options != nil && (buildings?.Options.count)! > 0){
            buildingList = (buildings?.Options)!
            for keyValue in buildingList {
                ids.append(keyValue.Key)
                names.append(keyValue.Value!)
            }
            updateBuildingDropDown(buildings!)
            
            fetchIndex = 0;
            fetchBuildingFloors(buildingId: buildingList[0].Key)
        }else{
            //TODO: Handle Error here
            if(responseCode == 401){
                //TODO: it could be token expired. This is not likely to happen, because it's validated in viewDidLoad().
                //only if expiration duration has been changed on server side
                refreshTokenNotificationName = Notification.Name(rawValue: refreshTokenNotificationKey)
                NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
                refreshToken()
            }
        }
    }
    
    fileprivate func fetchBuildingFloors(buildingId: Int) {
        print("*** *** fetchFloors")
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetche Floors")
        }
        remoteManager!.fetchBuildingFloors(remoteManager: remoteManager!, buildingId: buildingId, completion: fetchBuildingFloorsCompletion)
    }
    
    func fetchBuildingFloorsCompletion(_ floors: NameKeyValueMappable?, _ error: Error?, _ responseCode: Int){
        print("*** *** fetchBuildingFloorsCompletion")
        if(floors != nil && floors?.Options != nil && (floors?.Options.count)! > 0){
            ids.removeAll()
            names.removeAll()
            
            for keyValue in (floors?.Options)! {
                ids.append(keyValue.Key)
                names.append(keyValue.Value!)
            }
            floorIdDic[buildingDropDown.optionIds![fetchIndex]] = ids
            floorNameDic[buildingDropDown.optionIds![fetchIndex]] = names
            
            fetchIndex += 1
            if(fetchIndex < buildingDropDown.optionIds!.count){
                fetchBuildingFloors(buildingId: buildingDropDown.optionIds![fetchIndex])
            }else{
                floorDropDown.text = floorText
            }
        }else{
            
            fetchIndex += 1
            if(fetchIndex < buildingDropDown.optionIds!.count){
                fetchBuildingFloors(buildingId: buildingDropDown.optionIds![fetchIndex])
            }else{
                floorDropDown.text = floorText
            }
        }
    }
    
}

