//
//  FirstAttendanceViewController.swift
//  Manage
//
//  Created by Rong Li on 12/19/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class FirstAttendanceViewController: KeyboardBaseViewController {
    let currentPageIndex = 0
    let arrivedLateTypeId = 1
    let leftEarlyTypeId = 2
    let absentTypeId = 3
    
    let x = 0
    let y = 280
    let h = 140
    var absentReasonView: AbsentReasonView?
    
//    var issue: IssueMappable?
    var issue: Issue?
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var reasonConstraint: NSLayoutConstraint!
    @IBOutlet weak var reasonView: UIView!
    
    @IBOutlet weak var arrivedLateBtn: UIButton!
    @IBOutlet weak var leftEarlyBtn: UIButton!
    @IBOutlet weak var absentBtn: UIButton!
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: TwoDotsView!
    @IBOutlet weak var assignToView: AssignToOneView!
    
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var timeTextField: UITextField!
    @IBOutlet weak var textView: UITextView!
    
//    var issueTypeId: Int?
//    var reasonId: Int?
    
    //calendar icon
//    var calendarBtn  = UIButton(type: .custom)
    var clockBtn  = UIButton(type: .custom)
    
//    var selectedDate: String?
//    var selectedTime: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()
        
        //Listen for keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)
        
    }
    fileprivate func initTopViews(){
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_attendance", comment: ""))
        
        let dots = [dotsView.dot1, dotsView.dot2]
        initDotsView(dots: dots as! [UIButton], selectedIndex: currentPageIndex)
        
        initAssignToOneView(assignToView: assignToView)
        assignToView.onEditBtnClicked = {
            self.popupSelectEmployeeViewController()
        }
    }
    
    fileprivate func initViews(){
        initTopViews()
        
        reasonView.removeFromSuperview()
        reasonConstraint.constant = 18
        contentViewHeight.constant = contentView.frame.size.height - CGFloat(h)
        
        textView.delegate = self
        textView.text = "Description"
        textView.textColor = UIColor.init(hexString: Constants.textDarkWhite)
        
        textView.layer.masksToBounds = true
        textView.layer.borderColor = UIColor.init(hexString: "#8785A1").cgColor
        textView.layer.borderWidth = 1
        
        dateTextField.layer.masksToBounds = true
        dateTextField.layer.borderColor = UIColor.init(hexString: "#8785A1").cgColor
        dateTextField.layer.borderWidth = 1
        dateTextField.setPadding(left: 8, right: 8)

        //calendarBtn: var calendarBtn  = UIButton(type: .custom)
        calendarBtn.frame = CGRect(x:0, y:0, width:30, height:30)
        calendarBtn.tag = 1
        calendarBtn.addTarget(self, action: #selector(self.popupDatePickerBtnClicked), for: .touchUpInside)
        calendarBtn.setImage(UIImage(named: "Calendar-icon.png"), for: .normal)
        calendarBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
        dateTextField.rightViewMode = .always
        dateTextField.rightView = calendarBtn
        
        timeTextField.layer.masksToBounds = true
        timeTextField.layer.borderColor = UIColor.init(hexString: "#8785A1").cgColor
        timeTextField.layer.borderWidth = 1
        timeTextField.setPadding(left: 8, right: 8)
        
        //calendarBtn: var calendarBtn  = UIButton(type: .custom)
        clockBtn.frame = CGRect(x:0, y:0, width:30, height:30)
        clockBtn.tag = 2
        clockBtn.addTarget(self, action: #selector(self.popupDatePickerBtnClicked), for: .touchUpInside)
        clockBtn.setImage(UIImage(named: "Clock Icon"), for: .normal)
        clockBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
        timeTextField.rightViewMode = .always
        timeTextField.rightView = clockBtn
    }
    
    fileprivate func addReasonView(){
        absentReasonView = AbsentReasonView(frame: CGRect(x: x, y: y, width: Int(UIScreen.main.bounds.size.width), height: h))
        contentView.addSubview(absentReasonView!)

        reasonConstraint.constant = CGFloat(h + 18)
        contentViewHeight.constant = contentView.frame.size.height + CGFloat(h)
        
        absentReasonView!.onAbsentReasonClicked = {
            (reasonName, reasonId) in
            self.issue?.reasonId = reasonId
            self.issue?.reasonName = reasonName
        }
    }
    
    fileprivate func updateTimeVisibility(isHidden: Bool){
        timeTextField.isHidden = isHidden
    }
    
    fileprivate func removeReasonView(){
        if(absentReasonView != nil){
            absentReasonView?.removeFromSuperview()
            reasonConstraint.constant = 18
            contentViewHeight.constant = contentView.frame.size.height - CGFloat(h)
        }
    }
    
    //web    DueDate: "2018-12-14T07:59:59.999Z"
    //mobile    DueDate: "2018-12-14T07:59:59"
    fileprivate func onSelectedDateText(_ formattedText: String, _ tag: Int){
        print("### ### tag: \(tag)  \(formattedText) \(timeTextField.text)")
        switch tag {
        case 2:
            timeTextField.text = formattedText
            break
        default:
            dateTextField.text = formattedText
            break
        }
    }
    
    fileprivate func onConfirmBtnClicked(_ selectedTeam: [TeamMemberV2]) {
        selectedEmployee = selectedTeam[0]
        initAssignToOneView(assignToView: assignToView)
    }
    
    fileprivate func popupSelectEmployeeViewController(){
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SelectEmployeeViewController") as! SelectEmployeeViewController
        viewController.remoteManager = remoteManager
        viewController.featureName = FeatureName.Attendance
        viewController.isEdit = true
        viewController.selectedEmployee = selectedEmployee
        viewController.onConfirmBtnClicked = onConfirmBtnClicked
        
        viewController.modalTransitionStyle = .crossDissolve
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: true, completion: nil)
    }
    
    func isReadyMoveNext() -> Bool {
        if(selectedEmployee == nil
            || (dateTextField.text?.isEmpty ?? true)
            || (textView.text?.isEmpty ?? true)
            || issue?.issueTypeId == nil
            || ((timeTextField.text?.isEmpty ?? true) && (issue?.issueTypeId != absentTypeId))
            || (issue?.issueTypeId == absentTypeId && issue?.reasonId == nil)) {
            
            return false
        }
        
        let pageController = self.parent as! AttendancePageViewController
        pageController.setSelectedEmployee(selectedEmployee: selectedEmployee!)
        
        issue?.comment = textView.text
        
        let dateString = "\(dateTextField.text ?? "" ) \(timeTextField.text ?? "" )"
        
         print("### ### dateString: \(dateString)")
        issue?.dateOfOccurrence = dateString
        
//        issue?.dateOfOccurrence = dateString.formattedToUTC(dateString: dateString, fromFormat: Constants.DATE_LOCAL_SS, toFormat: Constants.DATE_UTC_SS)
//        print("### ### issue?.dateOfOccurrence: \(issue?.dateOfOccurrence)")
        return true;
    }

    @objc func popupDatePickerBtnClicked(sender: UIButton) {
        let storyboard =  UIStoryboard(name: "Popup", bundle: nil)
        let viewControler = storyboard.instantiateViewController(withIdentifier: "DatePickerPopupViewController") as! DatePickerPopupViewController
        viewControler.tag = sender.tag
        viewControler.dateStyle = .short
        viewControler.onSelectedDateText = onSelectedDateText
        present(viewControler, animated: true, completion: nil)
    }

    @IBAction func arrivedLateBtnClicked(_ sender: UIButton) {
        if(sender.tag == 0){
            issue?.issueTypeId = arrivedLateTypeId
            issue?.issueType = sender.titleLabel?.text
            issue?.reasonId = nil
            issue?.reasonName = ""
            if(absentBtn.tag == 1){
                removeReasonView()
                updateTimeVisibility(isHidden: false)
            }
            
            sender.tag = 1
            sender.backgroundColor = UIColor.init(hexString: Constants.btnLightGrayBG)
            sender.setTitleColor(UIColor.init(hexString: Constants.textBlack), for: .normal)
            
            leftEarlyBtn.tag = 0
            leftEarlyBtn.backgroundColor = UIColor.init(hexString: Constants.btnDarkGrayBG)
            leftEarlyBtn.setTitleColor(UIColor.init(hexString: Constants.textWhite), for: .normal)
            absentBtn.tag = 0
            absentBtn.backgroundColor = UIColor.init(hexString: Constants.btnDarkGrayBG)
            absentBtn.setTitleColor(UIColor.init(hexString: Constants.textWhite), for: .normal)
        }
    }
    
    @IBAction func leftEarlyBtnClicked(_ sender: UIButton) {
        if(sender.tag == 0){
            issue?.issueTypeId = leftEarlyTypeId
            issue?.issueType = sender.titleLabel?.text
            issue?.reasonId = nil
            issue?.reasonName = ""
            if(absentBtn.tag == 1){
                removeReasonView()
                updateTimeVisibility(isHidden: false)
            }
            
            sender.tag = 1
            sender.backgroundColor = UIColor.init(hexString: Constants.btnLightGrayBG)
            sender.setTitleColor(UIColor.init(hexString: Constants.textBlack), for: .normal)
            
            arrivedLateBtn.tag = 0
            arrivedLateBtn.backgroundColor = UIColor.init(hexString: Constants.btnDarkGrayBG)
            arrivedLateBtn.setTitleColor(UIColor.init(hexString: Constants.textWhite), for: .normal)
            absentBtn.tag = 0
            absentBtn.backgroundColor = UIColor.init(hexString: Constants.btnDarkGrayBG)
            absentBtn.setTitleColor(UIColor.init(hexString: Constants.textWhite), for: .normal)
        }
    }
    

    @IBAction func absentBtnClicked(_ sender: UIButton) {
        
        if(sender.tag == 0){
            issue?.issueTypeId = absentTypeId
            issue?.issueType = sender.titleLabel?.text
            updateTimeVisibility(isHidden: true)
            addReasonView()
            
            sender.tag = 1
            sender.backgroundColor = UIColor.init(hexString: Constants.btnLightGrayBG)
            sender.setTitleColor(UIColor.init(hexString: Constants.textBlack), for: .normal)
            
            arrivedLateBtn.tag = 0
            arrivedLateBtn.backgroundColor = UIColor.init(hexString: Constants.btnDarkGrayBG)
            arrivedLateBtn.setTitleColor(UIColor.init(hexString: Constants.textWhite), for: .normal)
            leftEarlyBtn.tag = 0
            leftEarlyBtn.backgroundColor = UIColor.init(hexString: Constants.btnDarkGrayBG)
            leftEarlyBtn.setTitleColor(UIColor.init(hexString: Constants.textWhite), for: .normal)

        }
        
    }
    
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        if(isReadyMoveNext()){
            let pageController = self.parent as! AttendancePageViewController
            pageController.onNextBtnClicked(currentIndex: currentPageIndex)
        }
    }
    
    //Touch screen outside textview to dismiss the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        textView.resignFirstResponder()
    }
    
    //Touch screen outside textfield to dismiss the keyboard and cursor
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == dateTextField {
            return false; //do not show keyboard nor cursor
        }
        return true
    }

}
