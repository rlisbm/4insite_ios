//
//  SecondAttendanceViewController.swift
//  Manage
//
//  Created by Rong Li on 12/19/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class SecondAttendanceViewController: BaseViewController {
    let currentPageIndex = 1
    
//    var issue: IssueMappable?
    var issue: Issue?
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: TwoDotsView!
    @IBOutlet weak var assignToView: ReviewAssignedTopView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var reasonLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var descriptionView: DescriptionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateViews()
    }
    
    fileprivate func initViews(){
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_attendance", comment: ""))
        
        let dots = [dotsView.dot1, dotsView.dot2]
        initDotsView(dots: dots as! [UIButton], selectedIndex: currentPageIndex)
        
    }
    
    
    fileprivate func updateViews(){
        let firstName = self.selectedEmployee!.FirstName ?? ""
        let lastName = self.selectedEmployee!.LastName ?? ""
        let fullName = firstName + " " + lastName
        
        assignToView.title.text = NSLocalizedString("key_review_attendance", comment: "")
        assignToView.fullName.text = fullName
        
        typeLabel.text = issue?.issueType
        reasonLabel.text = issue?.reasonName
        descriptionView.content.text = issue?.comment
        
        var dateParts = issue?.dateOfOccurrence?.components(separatedBy: " ") // splits into lines
        if(dateParts?.count == 2){
            dateLabel.text = dateParts?[0]
            timeLabel.text = dateParts?[1]
        }else{
            //TODO: error handling here
            print("Should not come to here, otherwise something wrong with datePicker and date parse.")
        }
    }

    fileprivate func submitAttendance(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on submitAttendance")
        }
        
//        remoteManager!.submitAttendance(remoteManager: remoteManager!, issue: issue!, completion: submitCompletion)
        
        remoteManager!.submitItem(remoteManager: remoteManager!, item: issue, apiRouter: APIRouter.attendance, completion: submitCompletion)
    }
    
//    fileprivate func submitCompletion (_ issue: IssueMappable?, _ error: Error?, _ responseCode: Int){
//        if(responseCode == Constants.ResponseOK){
//            goLandingViewController(isCompletion: true, isSuccess: true)
//        }else{
//            goLandingViewController(isCompletion: true, isSuccess: false)
//        }
//    }
    
    @IBAction func editBtnClicked(_ sender: Any) {
        let pageController = self.parent as! AttendancePageViewController
        pageController.onBackBtnClicked(currentIndex: currentPageIndex)
    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        issue?.orgUserId = (selectedEmployee?.EmployeeId)!
        submitAttendance()
    }
    
}
