//
//  AttendancePageViewController.swift
//  Manage
//
//  Created by Rong Li on 12/19/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class AttendancePageViewController: BasePageViewController {
    
//    var issue: IssueMappable = IssueMappable()
    var issue: Issue = Issue()

    var VC1 : FirstAttendanceViewController?
    var VC2 : SecondAttendanceViewController?
    
//    var selectEmployeeViewController : SelectEmployeeViewController?
//    var employeePopupViewController: EmployeePopupViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.removeGestureRecognizers() //disable swipe
        
        self.delegate = self
        self.dataSource = self
        
        
        VC1 = storyboard?.instantiateViewController(withIdentifier: "FirstAttendanceViewController") as? FirstAttendanceViewController
        VC2 = storyboard?.instantiateViewController(withIdentifier: "SecondAttendanceViewController") as? SecondAttendanceViewController
        
        VCs.append(VC1!)
        VCs.append(VC2!)
        
        VC1?.remoteManager = remoteManager
        VC2?.remoteManager = remoteManager
        
        VC1?.selectedEmployee = selectedEmployee
        VC2?.selectedEmployee = selectedEmployee
        
        VC1?.issue = issue
        VC2?.issue = issue
        
        setViewControllers([VCs[0]], direction: .forward, animated: true, completion: nil)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func setSelectedEmployee(selectedEmployee: TeamMemberV2){
        VC2?.selectedEmployee = selectedEmployee
    }
    
    func setSelectedDate(selectedDate: Date){
        VC2?.selectedDate = selectedDate
    }
    
    func isReadyMoveNext(currentIndex: Int) -> Bool {
//        switch currentIndex {
//        case 0:
//            return VC1?.isReadyMoveNext() ?? false
//        default:
//            return false
//        }
        
        return true
    }
    
    //MARK: UIPageViewControllerDataSource, UIPageViewControllerDelegate
    override func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = VCs.index(of: viewController) ?? 0
        
        if(!isReadyMoveNext(currentIndex: currentIndex) || currentIndex >= VCs.count - 1){
            return nil
        }
        
        return VCs[currentIndex + 1]
    }
    
}
