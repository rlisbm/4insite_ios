//
//  QuickEntryViewController.swift
//  Manage
//
//  Created by Rong Li on 11/5/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class QuickEntryCell: UICollectionViewCell {
    @IBOutlet weak var quickEntryImg: UIImageView!
    @IBOutlet weak var quickEntryLabel: UILabel!
}

struct QuickEntryObject {
    var label: String!
    var img: UIImage!
    
    init(label: String, img: UIImage){
        self.label = label
        self.img = img
    }
}

//struct FeatureName {
//    let Attendance = "Attendance"
//    let Audits = "Audits"
//    let Complaints = "Complaints"
//    let Compliments = "Compliments"
//    let Conduct = "Conduct"
//    let Professionalism = "Professionalism"
//    let ReportIt = "ReportIt"
//    let Safety = "Safety"
//    let SendMessage = "SendMessage"
//    let Todos = "Todos"
//}

class QuickEntryViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    let goAttendance = 0
    let goAudits = 1
    let goComplaints = 2
    let goCompliments = 3
    let goConduct = 4
    let goProfessionalism = 5
    let goReportIt = 6
    let goSafety = 7
    let goSendMessage = 8
    let goTodos = 9
    
    var quickEntryObjects = [QuickEntryObject]()
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tabBar = tabBarController as! TabBarController
        remoteManager = tabBar.remoteManager
    
        initQuickEntryObject()
        initViews()
    }
    
    fileprivate func initQuickEntryObject(){
        quickEntryObjects.append(QuickEntryObject(label: NSLocalizedString("key_attendance", comment: ""), img: UIImage(named: "icon-attendance.png")!))
        quickEntryObjects.append(QuickEntryObject(label: NSLocalizedString("key_audits", comment: ""), img: UIImage(named: "icon-audits.png")!))
        quickEntryObjects.append(QuickEntryObject(label: NSLocalizedString("key_complaints", comment: ""), img: UIImage(named: "icon complaints.png")!))
        quickEntryObjects.append(QuickEntryObject(label: NSLocalizedString("key_compliments", comment: ""), img: UIImage(named: "icon-compliments.png")!))
        quickEntryObjects.append(QuickEntryObject(label: NSLocalizedString("key_conduct", comment: ""), img: UIImage(named: "icon-conduct.png")!))
        quickEntryObjects.append(QuickEntryObject(label: NSLocalizedString("key_professionalism", comment: ""), img: UIImage(named: "icon-Professionalim.png")!))
        quickEntryObjects.append(QuickEntryObject(label: NSLocalizedString("key_report_it", comment: ""), img: UIImage(named: "icon-report-it.png")!))
        quickEntryObjects.append(QuickEntryObject(label: NSLocalizedString("key_safety", comment: ""), img: UIImage(named: "icon-safety.png")!))
        quickEntryObjects.append(QuickEntryObject(label: NSLocalizedString("key_send_msg", comment: ""), img: UIImage(named: "icon-send-a-message.png")!))
        quickEntryObjects.append(QuickEntryObject(label: NSLocalizedString("key_todos", comment: ""), img: UIImage(named: "icon-todo.png")!))
    }
    
    fileprivate func initViews(){
        var layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 0, left: 22, bottom: 0, right: 22)
        layout.minimumInteritemSpacing = 8
        layout.itemSize = CGSize(width: (self.collectionView.frame.size.width - 60)/2, height: ((self.collectionView.frame.size.height - 60)/5))
    }
    
//    @IBAction func closeBtnClicked(_ sender: Any) {
//        dismiss(animated: true, completion: nil)
//    }
    
//    fileprivate func goSelectEmployeeViewController(featureName: FeatureName){
//        print("### ### goSelectEmployeeViewController")
//        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
//        let selectEmployeeVC = storyboard.instantiateViewController(withIdentifier: "SelectEmployeeViewController") as! SelectEmployeeViewController
//        selectEmployeeVC.remoteManager = remoteManager
//        selectEmployeeVC.featureName = featureName
//        present(selectEmployeeVC, animated: true, completion: nil)
//    }
    
    fileprivate func goAttendanceViewController(){
//        let storyboard =  UIStoryboard(name: "First", bundle: nil)
//        let secondVC = storyboard.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
//        present(secondVC, animated: true, completion: nil)
    }
    fileprivate func goAuditsViewController(){
        let storyboard =  UIStoryboard(name: "Audit", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AuditPageViewController") as! AuditPageViewController
        viewController.remoteManager = remoteManager
        present(viewController, animated: true, completion: nil)
    }
    fileprivate func goComplaintViewController(){
        let storyboard =  UIStoryboard(name: "Complaint", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ComplaintPageViewController") as! ComplaintPageViewController
        viewController.remoteManager = remoteManager
        present(viewController, animated: true, completion: nil)
        
    }
    fileprivate func goComplimentViewController(){
        let storyboard =  UIStoryboard(name: "Compliment", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ComplimentPageViewController") as! ComplimentPageViewController
        viewController.remoteManager = remoteManager
        present(viewController, animated: true, completion: nil)
    }
    
    fileprivate func goSelectDepartmentViewController(featureName: FeatureName){
//        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "SelectDepartmentViewController") as! SelectDepartmentViewController
//        viewController.remoteManager = remoteManager
//        viewController.featureName = featureName
//        present(viewController, animated: true, completion: nil)
    }
    
    fileprivate func goSelectPositionViewController(featureName: FeatureName){
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SelectPositionViewController") as! SelectPositionViewController
        viewController.remoteManager = remoteManager
        viewController.featureName = featureName
        present(viewController, animated: true, completion: nil)
    }
    fileprivate func goTodosViewController(){
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "TodosViewController") as! TodosViewController
        viewController.remoteManager = remoteManager
        present(viewController, animated: true, completion: nil)
    }
    
    //MARK: CollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return quickEntryObjects.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuickEntryCell", for: indexPath) as! QuickEntryCell
    
        cell.quickEntryLabel.text = quickEntryObjects[indexPath.item].label
        cell.quickEntryImg.image = quickEntryObjects[indexPath.item].img
        
        cell.layer.borderColor = UIColor.clear.cgColor
        cell.layer.borderWidth = 0.5
        cell.layer.cornerRadius = 8.0
        cell.layer.masksToBounds = true
        
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.layer.shadowRadius = 8.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        
        return cell
    }
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        goSelectEmployeeViewController(isSingleSelection: false)
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        

        switch indexPath.item {
        case goAttendance:
            goSelectEmployeeViewController(featureName: FeatureName.Attendance)
            break
        case goAudits:
            goAuditsViewController()
//            goSelectEmployeeViewController(featureName: FeatureName.Audit)
            break
        case goComplaints:
            goComplaintViewController()
            break
        case goCompliments:
            goComplimentViewController()
            break
        case goConduct:
            goSelectEmployeeViewController(featureName: FeatureName.Conduct)
            break
        case goProfessionalism:
            goSelectEmployeeViewController(featureName: FeatureName.Professionalism)
            break
        case goReportIt:
            goSelectEmployeeViewController(featureName: FeatureName.ReportIt)
            break
        case goSafety:
            goSelectEmployeeViewController(featureName: FeatureName.Safety)
            break
        case goSendMessage:
            goSelectPositionViewController(featureName: FeatureName.SendMessage)
            break
        case goTodos:
            goSelectEmployeeViewController(featureName: FeatureName.Todos)
            break
        default:
            break
        }

    }
}


