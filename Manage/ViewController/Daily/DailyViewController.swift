//
//  DailyViewController.swift
//  Manage
//
//  Created by Rong Li on 3/14/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

enum Direction: String {
    case Left = "left"
    case Right = "right"
}

enum DailyTimeCardStatus: String {
    case Finalized = "Finalized"
}

class DailyCollectionCell: UICollectionViewCell{
    @IBOutlet weak var cellBg: UIImageView!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var cellBage: UILabel!
    @IBOutlet weak var cellBadgeHeight: NSLayoutConstraint!
}

class DailyTableCell: UITableViewCell{
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var imageLabelView: ImgLabelView!
    @IBOutlet weak var leadingSpace: NSLayoutConstraint!
}

struct DailyEntryObject {
    var label: String!
    var img: UIImage!
    var bg: UIImage!
    var badge: Int?
    
    init(label: String, img: UIImage, bg: UIImage){
        self.label = label
        self.img = img
        self.bg = bg
    }
}

struct DailyListItem {
    var icon: UIImage
    var millisecond: Int64!
    var date: String!
    var title: String!
    var avatarUrl: String!
    var data: Data!
    var pageNumber: Int!
    
    init(millisecond: Int64, date: String, title: String, data: Data, icon: UIImage, avatarUrl: String, pageNumber: Int){
        self.millisecond = millisecond
        self.date = date
        self.title = title
        self.data = data
        self.icon = icon
        self.avatarUrl = avatarUrl
        self.pageNumber = pageNumber
    }
}

class DailyViewController: BaseViewController {
    let TableViewTopHeight: CGFloat = 18
    let TableViewTopLow: CGFloat = 72
    let HoursImageSize: CGFloat = 36

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var greetLabel: UILabel!
    @IBOutlet weak var hoursView: ImgLabelView!
    @IBOutlet weak var tableTopConstraint: NSLayoutConstraint!
    
    var tabBar: TabBarController?
    var direction: Direction?
    
    fileprivate var collectionCellObjects = [DailyEntryObject]()
    fileprivate var todoItemList = [TodoItemV11]()
    fileprivate var requestItemList = [Request]()
    
    fileprivate var dailyListItems = [DailyListItem]()
    fileprivate var todoParams = Dictionary<String, Any>()
    fileprivate var requestParams = Dictionary<String, Any>()
    fileprivate var reportItParams = Dictionary<String, Any>()
    
    
    
    
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .red
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        return refreshControl
    }()

    override func viewDidLoad() {
        tabBar = tabBarController as! TabBarController
        if(tabBar == nil){
            return
        }
        remoteManager = tabBar!.remoteManager
        
        //without remoteManager, app can not be continued login
        if(remoteManager == nil){
            return
        }
        
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = true
        
        if(refreshTokenNotificationName != nil){
            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
        }else{
            
            if(AssociateHelper.get()?.employeeId == nil){
                fetchAssociateInfoNotificationName = Notification.Name(rawValue: fetchAssocateInfoNotificationKey)
                NotificationCenter.default.addObserver(self, selector: #selector(fetchAssociateInfoCompletion(notification:)), name: fetchAssociateInfoNotificationName, object: nil)
            }else{
                fetchData()
            }
        }
        
        initCollectionCellObject()
        initViews()
        
        scrollView.addSubview(refresher)
        
    }
    
    @objc func pullToRefresh(){
        print("############ pullToRefresh")
        let deadLine = DispatchTime.now() + .milliseconds(500)
        DispatchQueue.main.asyncAfter(deadline: deadLine){
            self.refresher.endRefreshing()
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if(getIsPortrait()){
            pageControl?.numberOfPages = 2
//            setPageControl()
        }else {
            pageControl?.numberOfPages = 1
        }
    }

    fileprivate func initCollectionCellObject(){
        collectionCellObjects.append(DailyEntryObject(label: NSLocalizedString("key_open_todos", comment: ""), img: UIImage(named: "To-Dos.png")!, bg: UIImage(named: "To-Dos Filter No Icon.png")!))
        collectionCellObjects.append(DailyEntryObject(label: NSLocalizedString("key_open_requests", comment: ""), img: UIImage(named: "Request.png")!, bg: UIImage(named: "Requests Filter No Icon.png")!))
        collectionCellObjects.append(DailyEntryObject(label: NSLocalizedString("key_open_report_its", comment: ""), img: UIImage(named: "Report-It.png")!, bg: UIImage(named: "Report-Its Filter No Icon.png")!))
        collectionCellObjects.append(DailyEntryObject(label: NSLocalizedString("key_open_complaints", comment: ""), img: UIImage(named: "Complaints.png")!, bg: UIImage(named: "Complaints Filter No Icon.png")!))
    }

    fileprivate func initViews(){
        var layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout

        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        
//        collectionCellObjects[0].badge = 100
        
        hoursView.imgHeight.constant = HoursImageSize
        hoursView.imgWidth.constant = HoursImageSize
        hoursView.img.image = UIImage(named: "Hours.png")
        hoursView.isHidden = true
        
        tableView.isHidden = true
    }
    
    func setPageControl(){
        //For just dragging
        let indexPaths = self.collectionView.indexPathsForVisibleItems
        var index = indexPaths[0].row
        if(direction == Direction.Left){
            for indexPath in indexPaths {
                if(index > indexPath.row){
                    index = indexPath.row
                }
            }
        }else{
            for indexPath in indexPaths {
                if(index < indexPath.row){
                    index = indexPath.row
                }
            }
        }
        
        if(index >= 0 && index < 3){
            pageControl.currentPage = 0
        }else if (index >= 3 && index < 6){
            pageControl.currentPage = 1
        }
    }
    
    fileprivate func updateHoursView(dailyTimeCardStats: [DailyTimeCardStat]){
        if(dailyTimeCardStats.count > 0 && dailyTimeCardStats[0].Status == DailyTimeCardStatus.Finalized.rawValue){
            tableTopConstraint.constant = TableViewTopHeight
            hoursView.isHidden = true
        }else{
            tableTopConstraint.constant = TableViewTopLow
            hoursView.isHidden = false
            hoursView.label1.text = NSLocalizedString("key_confirm_hours", comment: "")
            let dateString = dailyTimeCardStats[0].Date
            if(dateString != nil){
                let formatedDateString = dateString!.formatedDate(dateString: dateString, fromFormat: Constants.DATE_UTC_SS, toFormat: Constants.DATE_DAILY, timeZone: nil)
                hoursView.label2.text = NSLocalizedString("key_due", comment: "") + ": \(formatedDateString ?? "")"
            }
        }
    }
    
    @objc func refreshTokenCompletion(notification: NSNotification){
        fetchData()
    }
    
    @objc func fetchAssociateInfoCompletion(notification: NSNotification){
        fetchData()
    }
    
    fileprivate func fetchData(){
        postDailyTimeCardStats()
        let endDate = Date()
        let startDate = endDate.back180DayDate
        let start = startDate.formateDateUTC(date: startDate, withFormat: Constants.DATE_UTC_SSSZ)
        let end = endDate.formateDateUTC(date: endDate, withFormat: Constants.DATE_UTC_SSSZ)
//        https://prod-web.sbminsite.com/api/v1.1/Services/TodoItem?startDate=2018-03-03T23%3A01%3A47.319Z&endDate=2019-04-03T23%3A01%3A47.319Z&skip=10&take=15&isComplete=false&sortByColumn=DueDate&ascending=true
        
//        let start = "2018-03-03T23:01:47.319Z"
//        let end = "2019-04-03T23:01:47.319Z"
//        let parameters = [
//            "startDate": start,
//            "endDate": end,
//            "isComplete": "false",
//            "sortByColumn": "DueDate",
//            "ascending": "true",
//            "skip": 10,
//            "take": 2
//            ] as [String : Any]
        
        todoParams["startDate"] = start
        todoParams["endDate"] = end
        todoParams["isComplete"] = "false"
        todoParams["sortByColumn"] = "DueDate"
        todoParams["ascending"] = "false"
        todoParams["skip"] = 0
//        parameters["take"] = 2
        
//        fetchTodos(parameters: todoParams, pageNumber: 1)
        
//        requestParams["startDate"] = start
        requestParams["endDate"] = end
        requestParams["status"] = 1
        requestParams["sortByColumn"] = "CreateDate"
        requestParams["ascending"] = "false"
        requestParams["skip"] = 0
//        requestParams["take"] = 100
        fetchRequests(parameters: requestParams, pageNumber: 0)
    }
    
    fileprivate func postDailyTimeCardStats(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetchDailyTimeCardStats")
        }
        let date = Date()
        let dates = [date.formateDateUTC(date: date, withFormat: Constants.DATE_UTC_SS)]
        remoteManager!.postArrayWithDate(remoteManager: remoteManager!, ary: dates, apiRouter: APIRouter.dailyTimeCardStats, completion: postDailyTimeCardStatsCompletion)
    }
    
    fileprivate func postDailyTimeCardStatsCompletion(_ data: Data?, _ error: Error?, _ responseCode: Int?, date: Date?){
        if(responseCode == Constants.ResponseFailed401){
            handleError401()
            return
        }
        if(data == nil && responseCode == Constants.OffLineError){
            //it should not get here, only if the very first time launch the app with no connectivity and no stored data
            showSnackBar(messageText: NSLocalizedString("key_device_off_line", comment: ""),
                         messageTextColor: UIColor.init(hexString: Constants.snackbarMsgColor),
                         actionTitle: NSLocalizedString("key_dismiss", comment: ""),
                         actionTitleColor: UIColor.init(hexString: Constants.snackbarActionColor),
                         snackBarBackgroundColor: UIColor.init(hexString: Constants.snackbarBgColor))
            return
        }
        let jsonDecoder = JSONDecoder()
        guard let dailyTimeCardStats = try? jsonDecoder.decode([DailyTimeCardStat].self, from: data!) else{
            //No data to be shown
            return
        }
        
        updateHoursView(dailyTimeCardStats: dailyTimeCardStats)
    }
    
    fileprivate func fetchTodos(parameters: [String : Any], pageNumber: Int){
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetchTodos")
        }
        
        remoteManager!.fetchDataWithDate(remoteManager: remoteManager!, parameters: parameters, pageNumber: pageNumber, apiRouter: APIRouter.todoItemV11, completion: fetchTodosCompletion)
    }

    fileprivate func fetchTodosCompletion(_ data: Data?, _ error: Error?, _ responseCode: Int?, date: Date?){
        if(responseCode == Constants.ResponseFailed401 || (data == nil && responseCode == Constants.OffLineError)){
            handleError(data, error, responseCode)
            return
        }
        
//        do {
//            let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]
//            //let items = json["Items"] as? [[String: Any]] ?? []
//            print(json)
//        } catch let error as NSError {
//            print(error)
//        }

        let jsonDecoder = JSONDecoder()
        guard let todoItemObject = try? jsonDecoder.decode(TodoItemV11.self, from: data!) else {
            //No data
            return
        }
        todoItemList.append(todoItemObject)
        guard let items = todoItemObject.Items else{
            //No todoItem
            return
        }
        //IsComplete = 1;
        //CreateDate = "2018-10-18T18:17:44.6549626";
        //DueDate = "2018-10-18T23:59:59" == yyy-MM-dd'T'HH:mm:ss,  2019-03-31T06:59:55.875  == yyyy-MM-dd'T'HH:mm:ss.SSS
        let formatters = [Constants.DATE_UTC_SS, Constants.DATE_UTC_SSS, Constants.DATE_UTC_SSZ, Constants.DATE_UTC_SSSZ]
        let title = NSLocalizedString("key_todo", comment: "")
        let icon = UIImage(named: "To-Dos.png")!
        let pageNumber = todoItemObject.PageNumber
        let jsonEncoder = JSONEncoder()
        
//        lock(obj: dailyListItems){
        lock(object: dailyListItems as [AnyObject]){
        for item in items {
            let date = item.DueDate?.convertToDate(dateString: item.DueDate, fromFormatters: formatters, timeZone: "UTC")
            let dateString = date?.formateDate(date: date!, format: Constants.DATE_DAILY, timeZone: nil) ?? ""
            let millisec = date?.toMillis()!
            var avatarUrl = ""
            if(item.UserAttachment != nil && item.UserAttachment!.count > 0){
                for userAttachment in item.UserAttachment! {
                    if(userAttachment.UserAttachmentType == UserAttachmentTypeEnum.ProfilePicture.rawValue){
                        avatarUrl = userAttachment.UniqueId ?? ""
                        avatarUrl = avatarUrl == "" ? "" : remoteManager!.baseCDNUrl + avatarUrl
                    }
                }
            }
            
            guard let data = try? jsonEncoder.encode(item) else{
                continue
            }
            dailyListItems.append(DailyListItem(millisecond: millisec!, date: dateString, title: title, data: data, icon: icon, avatarUrl: avatarUrl, pageNumber: pageNumber ?? 0))
        }
        //because of reportIt, request, complaint and etc, it still needs sort even if request sorted items
        dailyListItems.sort{
            $0.millisecond > $1.millisecond
        }
        }
        tableView.isHidden = false
        tableView.reloadData()
        
        if(todoItemObject.HasNextPage ?? false ){
            todoParams["skip"] = (todoItemObject.PageNumber ?? 0) * (todoItemObject.PageSize ?? 0)
            fetchTodos(parameters: todoParams, pageNumber: todoItemObject.PageNumber! + 1)
        }
        
        if(todoItemObject.PageNumber == 1){
            collectionCellObjects[0].badge = todoItemObject.TotalItemCount
            collectionView.reloadData()
        }
    }
    
    fileprivate func fetchRequests(parameters: [String : Any], pageNumber: Int){
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetchTodos")
        }
        
        remoteManager!.fetchDataWithDate(remoteManager: remoteManager!, parameters: parameters, pageNumber: pageNumber, apiRouter: APIRouter.request, completion: fetchRequestsCompletion)
    }
    
    fileprivate func fetchRequestsCompletion(_ data: Data?, _ error: Error?, _ responseCode: Int?, date: Date?){
        if(responseCode == Constants.ResponseFailed401 || (data == nil && responseCode == Constants.OffLineError)){
            handleError(data, error, responseCode)
            return
        }
        
        do{
            guard let jsonAry = try JSONSerialization.jsonObject(with: data!, options: []) as? [[String: Any]] else {
                //No data
                return
            }
//            requestItemList = jsonAry.map { Request(json: $0) }
//            let list = jsonAry.map { Request(json: $0) }
//            print("\(list[0].CreateDate) **  \(list.count)")
//            print(jsonAry[0])
            
            //CreateDate = "2018-10-18T18:17:44.6549626";
            let formatters = [Constants.DATE_UTC_SS, Constants.DATE_UTC_SSS, Constants.DATE_UTC_SSZ, Constants.DATE_UTC_SSSZ]
            let title = NSLocalizedString("key_request", comment: "")
            let icon = UIImage(named: "Request.png")!
            let pageNumber = 0
            let jsonEncoder = JSONEncoder()
            
            lock(object: dailyListItems as [AnyObject]){
                for json in jsonAry {
                    let item = Request(json: json)
                    print(item)
                    let date = item.CreateDate?.convertToDate(dateString: item.CreateDate, fromFormatters: formatters, timeZone: "UTC")
                    let dateString = date?.formateDate(date: date!, format: Constants.DATE_DAILY, timeZone: nil) ?? ""
                    let millisec = date?.toMillis()!
                    var avatarUrl = ""
                    
                    if(item.RequestedBy != nil && item.RequestedBy?.ProfileImage != nil){
                        avatarUrl = item.RequestedBy?.ProfileImage?.Value ?? ""
                        avatarUrl = avatarUrl == "" ? "" : remoteManager!.baseCDNUrl + avatarUrl
                    }
                    guard let data = try? jsonEncoder.encode(item) else{
                        continue
                    }
                    
                    dailyListItems.append(DailyListItem(millisecond: millisec!, date: dateString, title: title, data: data, icon: icon, avatarUrl: avatarUrl, pageNumber: pageNumber ?? 0))

                }
            }

            tableView.isHidden = false
            tableView.reloadData()
            
            collectionCellObjects[1].badge = jsonAry.count
            collectionView.reloadData()
            
        }catch let error as NSError {
            print(error)
        }
       

//        if(todoItemObject.HasNextPage ?? false ){
//            todoParams["skip"] = (todoItemObject.PageNumber ?? 0) * (todoItemObject.PageSize ?? 0)
//            fetchTodos(parameters: todoParams, pageNumber: todoItemObject.PageNumber! + 1)
//        }
//
//        if(todoItemObject.PageNumber == 1){
//            collectionCellObjects[0].badge = todoItemObject.TotalItemCount
//            collectionView.reloadData()
//        }
        
    }
 
    fileprivate func handleError(_ data: Data?, _ error: Error?, _ responseCode: Int?){
        if(responseCode == Constants.ResponseFailed401){
            handleError401()
            return
        }
        if(data == nil && responseCode == Constants.OffLineError){
            //it should not get here, only if the very first time launch the app with no connectivity and no stored data
            showSnackBar(messageText: NSLocalizedString("key_device_off_line", comment: ""),
                         messageTextColor: UIColor.init(hexString: Constants.snackbarMsgColor),
                         actionTitle: NSLocalizedString("key_dismiss", comment: ""),
                         actionTitleColor: UIColor.init(hexString: Constants.snackbarActionColor),
                         snackBarBackgroundColor: UIColor.init(hexString: Constants.snackbarBgColor))
            return
        }
    }
    
    func lock(object: [AnyObject], blk:() -> ()) {
        objc_sync_enter(object)
        blk()
        objc_sync_exit(object)
    }
    
    func synchronized(object: [AnyObject]!, _ closure: () throws -> ()) rethrows { objc_sync_enter(object);
        defer {
            objc_sync_exit(object)
            
        };
        try closure()
        
    }

    fileprivate func handleError401(){
        refreshTokenNotificationName = Notification.Name(rawValue: refreshTokenNotificationKey)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
        refreshToken()
    }
    
//    func getJString(from object: Any) -> String? {
//        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
//            return nil
//        }
//        return String(data: data, encoding: String.Encoding.utf8)
//    }

    fileprivate func goTodoItemPopupViewController(dailyListItem: DailyListItem){
         print("####### goTodoItemPopupViewController")
        let storyboard =  UIStoryboard(name: "Daily", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "DailyItemPopupViewController") as! DailyItemPopupViewController
        viewController.remoteManager = remoteManager
        viewController.dailyItem = dailyListItem
        viewController.todoItem = todoItemList[dailyListItem.pageNumber - 1]
        present(viewController, animated: true, completion: nil)
        
    }
}

extension DailyViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionCellObjects.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DailyCollectionCell", for: indexPath) as! DailyCollectionCell
        
        cell.cellLabel.text = collectionCellObjects[indexPath.item].label
        cell.cellImage.image = collectionCellObjects[indexPath.item].img
        cell.cellBg.image = collectionCellObjects[indexPath.item].bg
        
        if(collectionCellObjects[indexPath.item].badge == nil){
            cell.cellBage.isHidden = true
        }else{
            cell.cellBage.isHidden = false
//            cell.cellBage.text = "8888   "
            cell.cellBage.text = String(collectionCellObjects[indexPath.item].badge!) + "    "
            cell.cellBage.sizeToFit()
            cell.cellBage.padding = UIEdgeInsets(top: 0, left: 1, bottom: 0, right: 0)
            cell.cellBage.layer.masksToBounds = true
            cell.cellBage.layer.cornerRadius = cell.cellBadgeHeight.constant/2
        }
        
        return cell
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        direction = nil
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(direction == nil){
            direction = scrollView.panGestureRecognizer.translation(in: scrollView.superview).x > 0 ? Direction.Left : Direction.Right
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.setPageControl()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.setPageControl()
    }

}

extension DailyViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dailyListItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DailyTableCell", for: indexPath) as! DailyTableCell
        
        cell.img.image = dailyListItems[indexPath.row].icon
        cell.imageLabelView.img.image = UIImage(named: "Profile Default Pic.png")
        cell.imageLabelView.label1.text = dailyListItems[indexPath.row].title
        cell.imageLabelView.label2.text = dailyListItems[indexPath.row].date
        
        setImage(imageView: cell.imageLabelView.img, imgUrl: dailyListItems[indexPath.row].avatarUrl, isCircle: true)
        
//        if(indexPath.row % 2 == 0){
//            cell.imageLabelView.imgHeight.constant = 0
//            cell.imageLabelView.imgWidth.constant = 0
//            cell.leadingSpace.constant = 0
//        }else {
//            cell.imageLabelView.imgHeight.constant = 40
//            cell.imageLabelView.imgWidth.constant = 40
//            cell.leadingSpace.constant = 16
//        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("####### didSelectRowAt")
        
        goTodoItemPopupViewController(dailyListItem: dailyListItems[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
        
//        if(tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType == .none){
//         goTodoItemPopupViewController(dailyListItem: dailyListItems[indexPath.row])
//        }else{
//            tableView.deselectRow(at: indexPath, animated: true)
//        }
//        print(tableView.cellForRow(at: indexPath as IndexPath)?.backgroundColor)
//        print(CIColor(color: tableView.cellForRow(at: indexPath as IndexPath)!.backgroundColor!))
//        print("################")
//        print(tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType.rawValue)
//        print(UITableViewCell.AccessoryType.none.rawValue)
//        print(UITableViewCell.AccessoryType.disclosureIndicator.rawValue)
//        print(UITableViewCell.AccessoryType.detailDisclosureButton.rawValue)
//        print(UITableViewCell.AccessoryType.checkmark.rawValue)
//        print(UITableViewCell.AccessoryType.detailButton.rawValue)
        
        
    }
    
}
