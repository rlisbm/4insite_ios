//
//  DailyItemPopupViewController.swift
//  Manage
//
//  Created by Rong Li on 4/4/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class DailyItemPopupViewController: BaseViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var headerView: DailyPopupHeaderView!
    @IBOutlet weak var employeeView: ImgLabelView!
    @IBOutlet weak var threeBtnsView: ThreeBtnsView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var building: UILabel!
    @IBOutlet weak var site: UIButton!
    @IBOutlet weak var dueDate: UILabel!

    var dailyItem: DailyListItem?
    var todoItem: TodoItemV11?
    var item: Item?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(dailyItem == nil || todoItem == nil){ //without dailyIte, it won't work. todoItem is for offline handling only
            return
        }
        
//        let jsonDecoder = JSONDecoder()
        do{
            item = try JSONDecoder().decode(Item.self, from: dailyItem!.data)
        }catch {
            print("ERROR")
            return // without valid data, it can not continue
        }
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.touchAction (_:)))
        view.addGestureRecognizer(gesture)

        initViews()
    }
  
    fileprivate func initViews(){
        headerView.contentView.twoRoundedCorner(corners: [.topLeft, .topRight], radius: 4)
        contentView.twoRoundedCorner(corners: [.bottomLeft, .bottomRight], radius: 4)
        
        headerView.title.text = NSLocalizedString("key_todo", comment: "")
        headerView.onCloseBtnClicked = {
            self.dismiss(animated: true)
        }
        
        threeBtnsView.btn1.setTitle(NSLocalizedString("key_edit_up_case", comment: ""), for: .normal)
        threeBtnsView.btn2.setTitle(NSLocalizedString("key_complete_up_case", comment: ""), for: .normal)
        threeBtnsView.btn3.setTitle(NSLocalizedString("key_delete_up_case", comment: ""), for: .normal)
        
        threeBtnsView.btn1.setTitleColor(UIColor.init(hexString: Constants.textBlack), for: .normal)
        threeBtnsView.btn2.setTitleColor(UIColor.init(hexString: Constants.btnTextRed), for: .normal)
        threeBtnsView.btn3.setTitleColor(UIColor.init(hexString: Constants.btnTextRed), for: .normal)
        
        threeBtnsView.onBtn1Clicked = {
            self.onEditBtnClicked()
        }
        
        threeBtnsView.onBtn2Clicked = {
            self.onCompleteBtnClicked()
        }
        
        threeBtnsView.onBtn3Clicked = {
            self.onDeleteBtnClicked()
        }
        
        
        employeeView.label1.text = item?.OrgUserName
        employeeView.label2.text = NSLocalizedString("key_assigned_to_up_case", comment: "")
        textView.text = item?.Comment
        building.text = item?.BuildingName
        let siteName = SelectedSiteHelper.get()?.selectedClientName ?? "" + ", " + SelectedSiteHelper.get()!.selectedSiteName
        site.setTitle(siteName, for: .normal)
        dueDate.text = dueDate.text ?? (NSLocalizedString("key_due", comment: "") + ": ")
        
        setImage(imageView: employeeView.img, imgUrl: dailyItem!.avatarUrl, isCircle: true)
        
    }

    //Touch outside to dismiss the ViewController
    @objc func touchAction(_ sender: UITapGestureRecognizer){
        dismiss(animated: true)
    }
    
    fileprivate func goTodoItemEidtPopupViewController(dailyItem: DailyListItem){
        dismiss(animated: true, completion: {
            guard let viewController = UIStoryboard(name: "Daily", bundle: nil).instantiateViewController(withIdentifier: "DailyItemEditPopupViewController") as? DailyItemEditPopupViewController else{
                fatalError()
            }
            viewController.remoteManager = self.remoteManager
            viewController.dailyItem = self.dailyItem
            viewController.todoItem = self.todoItem
            viewController.item = self.item
            
            self.topMostController().present(viewController, animated: true, completion: nil)
        })
        
    }
    fileprivate func onEditBtnClicked(){
        goTodoItemEidtPopupViewController(dailyItem: dailyItem!)
    }
    
    fileprivate func onCompleteBtnClicked(){
        item?.IsComplete = true
        let message = String(format: NSLocalizedString("key_todo_finish_msg", comment: ""), NSLocalizedString("key_complete", comment: ""))
        showAlert(title: NSLocalizedString("key_complete", comment: ""), message: message, positiveBtn: NSLocalizedString("key_yes_up_case", comment: ""), nagetiveBtn: NSLocalizedString("key_no_up_case", comment: ""),  viewController: self, completion: discardCompleteAlertCompletion)
    }
    
    func discardCompleteAlertCompletion(isPositiveAnswer: Bool){
        if(isPositiveAnswer){
            updateTodoItem()
        }
    }
    
    fileprivate func onDeleteBtnClicked(){
        let message = String(format: NSLocalizedString("key_todo_finish_msg", comment: ""), NSLocalizedString("key_delete", comment: ""))
        showAlert(title: NSLocalizedString("key_delete", comment: ""), message: message, positiveBtn: NSLocalizedString("key_yes_up_case", comment: ""), nagetiveBtn: NSLocalizedString("key_no_up_case", comment: ""),  viewController: self, completion: discardDeleteAlertCompletion)
    }
    
    func discardDeleteAlertCompletion(isPositiveAnswer: Bool){
        if(isPositiveAnswer){
            deleteTodoItem()
        }
    }
    
    fileprivate func deleteTodoItem(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on updateTodoItem")
        }
        remoteManager!.deleteItem(remoteManager: remoteManager!, item: item, itemObject: todoItem, apiRouter: APIRouter.putTodoItem, completion: putCompletion)
    }
    
    fileprivate func updateTodoItem(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on updateTodoItem")
        }
        remoteManager!.putItem(remoteManager: remoteManager!, item: item, itemObject: todoItem, apiRouter: APIRouter.putTodoItem, completion: putCompletion)
        
    }
    
    func putCompletion (_ data: Data?, _ error: Error?, _ responseCode: Int){
        if(responseCode == Constants.ResponseOK){
            //Successfully updated
        }else{
            //Failed
        }
        
        dismiss(animated: true)
        
//        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
}
