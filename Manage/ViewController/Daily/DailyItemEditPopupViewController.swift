//
//  DailyItemEditPopupViewController.swift
//  Manage
//
//  Created by Rong Li on 4/5/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class DailyItemEditPopupViewController: BaseViewController {
    let scrollViewConstrain: CGFloat = 96

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var headerView: DailyPopupHeaderView!
    @IBOutlet weak var employeeView: ImgLabelView!
    @IBOutlet weak var twoBtnsView: TwoBtnsView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var building: UILabel!
    @IBOutlet weak var site: UIButton!
    @IBOutlet weak var dueDate: LableTextFieldView!
    
    
    @IBOutlet weak var scrollViewConstrainTop: NSLayoutConstraint!
    @IBOutlet weak var scrollViewConstrainBottom: NSLayoutConstraint!
    
    var dailyItem: DailyListItem?
    var todoItem: TodoItemV11?
    var item: Item?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dueDate.textField.delegate = self

        initViews()
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if(getIsPortrait()){
            scrollViewConstrainTop.constant = scrollViewConstrain
            scrollViewConstrainBottom.constant = scrollViewConstrain
        }else{
            scrollViewConstrainTop.constant = 0
            scrollViewConstrainBottom.constant = 0
        }
    }
    

    fileprivate func initViews(){
        headerView.contentView.twoRoundedCorner(corners: [.topLeft, .topRight], radius: 4)
        contentView.twoRoundedCorner(corners: [.bottomLeft, .bottomRight], radius: 4)

        headerView.title.text = NSLocalizedString("key_todo", comment: "")
        headerView.onCloseBtnClicked = {
            self.dismiss(animated: true)
        }

        twoBtnsView.btn1.setTitle(NSLocalizedString("key_save_up_case", comment: ""), for: .normal)
        twoBtnsView.btn2.setTitle(NSLocalizedString("key_back_up_case", comment: ""), for: .normal)

        twoBtnsView.btn1.setTitleColor(UIColor.init(hexString: Constants.textBlack), for: .normal)
        twoBtnsView.btn2.setTitleColor(UIColor.init(hexString: Constants.btnTextWhite), for: .normal)

        twoBtnsView.onBtn1Clicked = {
            self.onSaveBtnClicked()
        }

        twoBtnsView.onBtn2Clicked = {
            self.onBackBtnClicked()
        }

        employeeView.label1.text = item?.OrgUserName
        employeeView.label2.text = NSLocalizedString("key_assigned_to_up_case", comment: "")
        setImage(imageView: employeeView.img, imgUrl: dailyItem!.avatarUrl, isCircle: true)
        textView.text = item?.Comment
        building.text = item?.BuildingName
        let siteName = SelectedSiteHelper.get()?.selectedClientName ?? "" + ", " + SelectedSiteHelper.get()!.selectedSiteName
        site.setTitle(siteName, for: .normal)

        dueDate.textField.setPadding(left: 8, right: 0)
        dueDate.textField.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor

        initCalendarBtn(dateTextField: dueDate.textField)
        calendarBtn.addTarget(self, action: #selector(self.popupDatePickerBtnClicked), for: .touchUpInside)
    }
    
    fileprivate func onSelectedDate(_ date: Date){
        selectedDate = date
        dueDate.textField.text = selectedDate!.formattedDateShort
    }
    
    @objc func popupDatePickerBtnClicked(sender:UITapGestureRecognizer) {
        print("####### popupDatePickerBtnClicked")
        let storyboard =  UIStoryboard(name: "Popup", bundle: nil)
        let viewControler = storyboard.instantiateViewController(withIdentifier: "DatePickerPopupViewController") as! DatePickerPopupViewController
        viewControler.tag = DatePickerType.Date.rawValue
        viewControler.onSelectedDate = onSelectedDate
        present(viewControler, animated: true, completion: nil)
        
    }

    //DueDate: "2019-04-29T06:59:59"
    fileprivate func onSaveBtnClicked(){
        let str = selectedDate?.formateDateUTC(date: selectedDate!, withFormat: Constants.DATE_UTC_SS)
        print(selectedDate)
        print(str)
        item?.DueDate = selectedDate?.formateDateUTC(date: selectedDate!, withFormat: Constants.DATE_UTC_SS)
        print(item)
        
        updateTodoItem()
        
    }
    
    fileprivate func onBackBtnClicked(){
        dismiss(animated: true, completion: {
            guard let viewController = UIStoryboard(name: "Daily", bundle: nil).instantiateViewController(withIdentifier: "DailyItemPopupViewController") as? DailyItemPopupViewController else{
                fatalError()
            }
            viewController.remoteManager = self.remoteManager
            viewController.dailyItem = self.dailyItem
            viewController.todoItem = self.todoItem
            
            self.topMostController().present(viewController, animated: true, completion: nil)
        })
    }
    
    fileprivate func updateTodoItem(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on updateTodoItem")
        }
        remoteManager!.putItem(remoteManager: remoteManager!, item: item, itemObject: todoItem, apiRouter: APIRouter.putTodoItem, completion: putCompletion)
        
    }
    
    func putCompletion (_ data: Data?, _ error: Error?, _ responseCode: Int){
        
        if(responseCode == Constants.ResponseOK){
            //Successfully updated
        }else{
            //Failed
        }
       
        dismiss(animated: true)
//        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
}

extension DailyItemEditPopupViewController: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }
}
