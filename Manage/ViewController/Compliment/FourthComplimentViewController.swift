//
//  FourthComplimentViewController.swift
//  Manage
//
//  Created by Rong Li on 1/11/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit
class ComplimentReviewCell: UICollectionViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var content: UILabel!
}

class FourthComplimentViewController: BaseViewController {
    let currentPageIndex = 3
    var compliment: Compliment?
    
    let minSpacing:CGFloat = 5
    let marging:CGFloat = 10
    let cellHeight:CGFloat = 40
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: FourDotsView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var descritionView: DescriptionView!
    @IBOutlet weak var employeeNameView: DescriptionView!
    
    var isViewDidLoad: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initViews()
       
        isViewDidLoad = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateViews()
    }
    
    fileprivate func initViews(){
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_compliments", comment: ""))
        
        let dots = [dotsView.dot1, dotsView.dot2, dotsView.dot3, dotsView.dot4]
        initDotsView(dots: dots as! [UIButton], selectedIndex: currentPageIndex)
        
        //configure collectionview layout
        configureCollectionViewLayour()
        
        descritionView.content.isEditable = false
        
        employeeNameView.title.text = "Employee Names:"
        employeeNameView.content.isEditable = false
        
        collectionView.isScrollEnabled = false

    }
    
    fileprivate func configureCollectionViewLayour(){
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //.horizontal
        layout.minimumLineSpacing = minSpacing
        layout.minimumInteritemSpacing = minSpacing
        collectionView.setCollectionViewLayout(layout, animated: true)
    }
    
    func updateViews(){
        descritionView.content.text = compliment?.Description
        var names: String = ""
        for employee in selectedEmployees{
            names = "\(names)\(employee.FirstName ?? "") \(employee.LastName ?? ""), "
        }
  
//        employeeNameView.content.text = String(names.prefix(names.count - 2))
        employeeNameView.content.text = names
        collectionView.reloadData()
    }
    
    fileprivate func submitCompliment(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on submitCompliment")
        }
        remoteManager!.submitItem(remoteManager: remoteManager!, item: compliment, apiRouter: APIRouter.compliment, completion: submitCompletion)
    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        compliment?.AccountableEmployees.append(67513)
        compliment?.AccountableEmployees.append(67596)
        compliment?.BuildingId = 15688
        compliment?.ComplimentTypeId = 4
        compliment?.CustomerName = "rli testing"
        compliment?.Description = "rli testing"
        compliment?.FeedbackDate = "2019-01-18T18:18:18"
        compliment?.FloorId = 35632
        submitCompliment()
    }
    
}
extension FourthComplimentViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ComplimentReviewCell", for: indexPath) as! ComplimentReviewCell
        cell.title.textColor = UIColor.init(hexString: Constants.textWhite)
        cell.content.textColor = UIColor.init(hexString: Constants.textDarkWhite)
        
        switch indexPath.row {
        case 0:
            cell.title.text = NSLocalizedString("key_date", comment: "")
            cell.content.text = selectedDate?.formattedDateShort
            break
        case 1:
            cell.title.text = NSLocalizedString("key_compliment_type", comment: "")
            cell.content.text = compliment?.ComplimentTypeName
            break
        case 2:
            cell.title.text = NSLocalizedString("key_building", comment: "")
            cell.content.text = compliment?.BuildingName
            break
        case 3:
            cell.title.text = NSLocalizedString("key_floor", comment: "")
            cell.content.text = compliment?.FloorName
            break
        case 4:
            cell.title.text = NSLocalizedString("key_customer", comment: "")
            cell.content.text = compliment?.CustomerName
            break
        case 5:
            cell.title.text = ""
            cell.content.text = ""
            break
        case 6:
            cell.title.text = NSLocalizedString("key_customer_contact", comment: "")
            cell.content.text = compliment?.CustomerPhoneNumber
            break
        case 7:
            cell.title.text = ""
            cell.content.text = ""
            break
            
        default:
            break
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: marging, left: marging, bottom: marging, right: marging)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  marging * 2 + minSpacing
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2, height: cellHeight)
    }
    
}





