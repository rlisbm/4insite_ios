//
//  ThirdComplimentViewController.swift
//  Manage
//
//  Created by Rong Li on 1/11/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class ThirdComplimentViewController: AddLabelKeyboardBaseViewController {
//    let editText = NSLocalizedString("key_edit", comment: "")
    let addEmployeeText = NSLocalizedString("key_add_employees", comment: "")

    
    let currentPageIndex = 2
    var compliment: Compliment?
    
    var isPortrait: Bool = true
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: FourDotsView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var assignToView: AssignToView!
    @IBOutlet weak var nameContainerView: UIView!
    
    @IBOutlet weak var scrollContentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nameLabelViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nameContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nameContainerViewTop: NSLayoutConstraint!
    
    override func viewDidLoad() {
        self.assignTo = assignToView
        self.nameContainer = nameContainerView
        
        self.scrollContentHeight = scrollContentViewHeight
        self.nameLabelHeight = nameLabelViewHeight
        self.nameContainerHeight = nameContainerViewHeight
        self.nameContainerTop = nameContainerViewTop
        
        yOriginalPos = Int(nameContainerViewTop.constant + nameContainerViewHeight.constant)
        scrollContentViewOriginalHeight = scrollContentViewHeight.constant
        nameLabelViewOriginalHeight = nameLabelViewHeight.constant
        nameContainerViewOriginalHeight = nameContainerViewHeight.constant
        
        super.viewDidLoad()

        initViews()
        
        isPortrait = getIsPortrait()
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        updateNameLabelView()
    }
    
//    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        super.viewWillTransition(to: size, with: coordinator)
//        updateNameLabelViewAfterRotate()
//    }
//    func updateNameLabelViewAfterRotate(){
//        if(isPortrait != getIsPortrait()){
//            isPortrait = !isPortrait
//            perform(#selector(updateNameLabelView), with: nil, afterDelay: 0.5)
//        }
//    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        updateNameLabelView()
    }
    
//    func getIsPortrait() -> Bool{
//        return UIDevice.current.orientation.isLandscape == true ? false : true
//    }

    fileprivate func initViews(){
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_compliments", comment: ""))
        
        let dots = [dotsView.dot1, dotsView.dot2, dotsView.dot3, dotsView.dot4]
        initDotsView(dots: dots as! [UIButton], selectedIndex: currentPageIndex)
        
        textField.setPadding(left: 8, right: 8)
        textField.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        
        if(selectedEmployees.count == 0){
            updateEditBtn(title: addEmployeeText)
        }
        
        assignToView.onEditBtnClicked = {
            self.onEditBtnClicked()
        }
        assignToView.onToggleBtnClicked = {
            (toggleBtn) in
            self.onToggleBtnClicked(toggleBtn: toggleBtn)
        }
        
    }
    
//    fileprivate func updateNameContainerView(isShowAll: Bool){
//        for label in nameContainerView.subviews {
//            label.removeFromSuperview()
//        }
//        nameContainerViewHeight.constant = nameContainerViewOriginalHeight
//        addNames(nameContainerView: nameContainerView, isShowAll: isShowAll)
//    }
    
//    @objc override func updateNameLabelView(){
//        var title: String = ""
//
//        if let attributedTitle = assignToView.toggleBtn.attributedTitle(for: .normal) {
//            let mutableAttributedTitle = NSMutableAttributedString(attributedString: attributedTitle)
//
//            if (assignToView.toggleBtn.tag == 1){
//                title = NSLocalizedString("key_see_less", comment: "")
//                updateNameContainerView(isShowAll: true)
//                nameLabelViewHeight.constant = nameLabelViewHeight.constant + CGFloat(yPos - yOriginalPos)
//                scrollContentViewHeight.constant = scrollContentViewHeight.constant + CGFloat(yPos - yOriginalPos)
//            }else{
//                title = NSLocalizedString("key_see_all", comment: "")
//                updateNameContainerView(isShowAll: false)
//                nameLabelViewHeight.constant = nameLabelViewOriginalHeight
//                scrollContentViewHeight.constant = scrollContentViewOriginalHeight
//            }
//
//            mutableAttributedTitle.replaceCharacters(in: NSMakeRange(0, mutableAttributedTitle.length), with: title)
//            assignToView.toggleBtn.setAttributedTitle(mutableAttributedTitle, for: .normal)
//        }
//    }
    
//    fileprivate func updateEditBtn(title: String){
//        if let attributedTitle = assignToView.editBtn.attributedTitle(for: .normal) {
//            let mutableAttributedTitle = NSMutableAttributedString(attributedString: attributedTitle)
//            mutableAttributedTitle.replaceCharacters(in: NSMakeRange(0, mutableAttributedTitle.length), with: title)
//            assignToView.editBtn.setAttributedTitle(mutableAttributedTitle, for: .normal)
//            
//            if(title == editText){
//                assignToView.toggleBtn.isHidden = false
//            }else{
//                assignToView.toggleBtn.isHidden = true
//            }
//        }
//    }

    fileprivate func onEditBtnClicked() {
        self.popupSelectEmployeeViewController(featureName: FeatureName.Complaint)
    }
    
    fileprivate func onToggleBtnClicked(toggleBtn: UIButton) {
        updateNameLabelView()
    }
    
    fileprivate func onConfirmBtnClicked(_ selectedTeam: [TeamMemberV2]) {
        selectedEmployees = selectedTeam
        updateNameContainerView(isShowAll: false)
        updateEditBtn(title: editText)
    }
    
    fileprivate func popupSelectEmployeeViewController(featureName: FeatureName){
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SelectEmployeeViewController") as! SelectEmployeeViewController
        viewController.remoteManager = remoteManager
        viewController.featureName = featureName
        viewController.selectedEmployees = selectedEmployees
        viewController.onConfirmBtnClicked = onConfirmBtnClicked
        
        viewController.modalTransitionStyle = .crossDissolve
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: true, completion: nil)
    }
    
    func isReadyMoveNext() -> Bool {
        if(selectedEmployees.count == 0){
            return false
        }
        let pageController = self.parent as! ComplimentPageViewController
        pageController.setSelectedEmployee(selectedEmployees: selectedEmployees)
        compliment?.CustomerPhoneNumber = textField.text
        compliment?.AccountableEmployees.removeAll()
        for employee in selectedEmployees{
            compliment?.AccountableEmployees.append(employee.EmployeeId!)
        }
        return true
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        if(isReadyMoveNext()){
            let pageController = self.parent as! ComplimentPageViewController
            pageController.onNextBtnClicked(currentIndex: currentPageIndex)
        }
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        let pageController = self.parent as! ComplimentPageViewController
        pageController.onBackBtnClicked(currentIndex: currentPageIndex)
    }
}

