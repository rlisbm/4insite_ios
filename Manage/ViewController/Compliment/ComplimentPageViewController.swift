//
//  ComplimentsPageViewController.swift
//  Manage
//
//  Created by Rong Li on 1/4/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class ComplimentPageViewController: BasePageViewController {
    
    var VC1 : FirstComplimentViewController?
    var VC2 : SecondComplimentViewController?
    var VC3 : ThirdComplimentViewController?
    var VC4 : FourthComplimentViewController?
    
    var compliment: Compliment = Compliment()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        self.dataSource = self
        
        VC1 = storyboard?.instantiateViewController(withIdentifier: "FirstComplimentViewController") as? FirstComplimentViewController
        VC2 = storyboard?.instantiateViewController(withIdentifier: "SecondComplimentViewController") as? SecondComplimentViewController
        VC3 = storyboard?.instantiateViewController(withIdentifier: "ThirdComplimentViewController") as? ThirdComplimentViewController
        VC4 = storyboard?.instantiateViewController(withIdentifier: "FourthComplimentViewController") as? FourthComplimentViewController
        
        
        VCs.append(VC1!)
        VCs.append(VC2!)
        VCs.append(VC3!)
        VCs.append(VC4!)
        
        VC1?.remoteManager = remoteManager
        VC2?.remoteManager = remoteManager
        VC3?.remoteManager = remoteManager
        VC4?.remoteManager = remoteManager
        
        VC1?.compliment = compliment
        VC2?.compliment = compliment
        VC3?.compliment = compliment
        VC4?.compliment = compliment

        setViewControllers([VCs[0]], direction: .forward, animated: true, completion: nil)

    }
    
    func setSelectedEmployee(selectedEmployees: [TeamMemberV2]){
        VC4?.selectedEmployees = selectedEmployees
    }

    func setSelectedDate(selectedDate: Date){
        VC4?.selectedDate = selectedDate
    }
    
    func isReadyMoveNext(currentIndex: Int) -> Bool {
//        switch currentIndex {
//        case 0:
//            return VC1?.isReadyMoveNext() ?? false
//        case 1:
//            return VC2?.isReadyMoveNext() ?? false
//        case 2:
//            return VC3?.isReadyMoveNext() ?? false
//        default:
//            return false
//        }
        
        return true
    }

    //MARK: UIPageViewControllerDataSource, UIPageViewControllerDelegate
    override func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = VCs.index(of: viewController) ?? 0
        
        if(!isReadyMoveNext(currentIndex: currentIndex) || currentIndex >= VCs.count - 1){
            return nil
        }
        
        return VCs[currentIndex + 1]
    }
    
    override func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = VCs.index(of: viewController) ?? 0
        if(currentIndex <= 0){
            return nil
        }
        
        return VCs[currentIndex - 1]
    }
}
