//
//  FirstComplimentViewController.swift
//  Manage
//
//  Created by Rong Li on 1/11/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class FirstComplimentViewController: BuildingBaseViewController {
    let currentPageIndex = 0
    var compliment: Compliment?
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: FourDotsView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var buildingDropdown: DropDown!
    @IBOutlet weak var floorDropdown: DropDown!
    
    override func viewDidLoad() {
        self.buildingDropDown = buildingDropdown
        self.floorDropDown = floorDropdown
        
        super.viewDidLoad()
        
        initViews()

    }
    
    fileprivate func initViews(){
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_compliments", comment: ""))
        
        let dots = [dotsView.dot1, dotsView.dot2, dotsView.dot3, dotsView.dot4]
        initDotsView(dots: dots as! [UIButton], selectedIndex: currentPageIndex)

        textField.setPadding(left: 8, right: 8)
        textField.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
    }
    
    func isReadyMoveNext() -> Bool{
        if(textField.text?.isEmpty ?? true
            || buildingDropdown.text == buildingText
            || floorDropdown.text == floorText){
            return false
        }
        compliment?.CustomerName = textField.text
        compliment?.BuildingId = selectedBuilding?.Key
        compliment?.BuildingName = selectedBuilding?.Value
        compliment?.FloorId = selectedFloor?.Key
        compliment?.FloorName = selectedFloor?.Value
        
        return true
    }
    
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        if(isReadyMoveNext()){
            let pageController = self.parent as! ComplimentPageViewController
            pageController.onNextBtnClicked(currentIndex: currentPageIndex)
        }
    }
    
}

extension FirstComplimentViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
