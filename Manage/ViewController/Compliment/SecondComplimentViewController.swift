//
//  SecondComplimentViewController.swift
//  Manage
//
//  Created by Rong Li on 1/11/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class SecondComplimentViewController: BaseViewController{
    let currentPageIndex = 1
    var compliment: Compliment?
    
    let typeText = NSLocalizedString("key_type", comment: "")
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: FourDotsView!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var dropDown: DropDown!
    @IBOutlet weak var textView: UITextView!
    
    var isDropDownOpen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(refreshTokenNotificationName != nil){
            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
        }else{
            initViews()
            
            if(AssociateHelper.get()?.employeeId == nil){
                fetchAssociateInfoNotificationName = Notification.Name(rawValue: fetchAssocateInfoNotificationKey)
                NotificationCenter.default.addObserver(self, selector: #selector(fetchAssociateInfoCompletion(notification:)), name: fetchAssociateInfoNotificationName, object: nil)
            }else{
                fetchComplimentType()
            }
        }

    }
    
    fileprivate func initViews(){
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_compliments", comment: ""))
        
        let dots = [dotsView.dot1, dotsView.dot2, dotsView.dot3, dotsView.dot4]
        initDotsView(dots: dots as! [UIButton], selectedIndex: currentPageIndex)
        
        dateTextField.delegate = self
        dateTextField.setPadding(left: 8, right: 0)
        dateTextField.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        
        initCalendarBtn(dateTextField: dateTextField)
        calendarBtn.addTarget(self, action: #selector(self.popupDatePickerBtnClicked), for: .touchUpInside)
        
        dropDown.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        dropDown.setDropDown(rowBackgroundColor: Constants.dropDownRowBG, selectedRowColor: Constants.dropDownRowBG, rowTextColor: Constants.dropDownTextColor, rowHeight: 50);
        
        textView.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        textView.text = NSLocalizedString("key_compliment_comments", comment: "")
        textView.textColor = UIColor.init(hexString: Constants.textDarkWhite)
        
        dropDown.didSelect{(selectedText , index , id) in
            print("Selected String: \(selectedText) \n index: \(index) \n Id: \(id)")
            self.isDropDownOpen = false
            self.compliment?.ComplimentTypeId = id
            self.compliment?.ComplimentTypeName = selectedText
        }
        
        let dropDownGesture = UITapGestureRecognizer(target: self, action:  #selector (self.dropDownAction(sender:)))
        dropDown.addGestureRecognizer(dropDownGesture)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if(isDropDownOpen){
            isDropDownOpen = !isDropDownOpen
            dropDown.touchAction()
        }
    }
    
    fileprivate func updateDropDown(nameKeyValue: NameKeyValueMappable, text: String, dropDown: DropDown){
        dropDown.text = text
        let keyValues = nameKeyValue.Options
        for keyValue in keyValues {
            dropDown.optionIds?.append(keyValue.Key)
            dropDown.optionArray.append(keyValue.Value!)
        }
    }

    fileprivate func fetchComplimentType(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetcheTypes")
        }
        remoteManager!.fetchComplimentTypes(remoteManager: remoteManager!, apiRouter: APIRouter.complimentTypes, completion: fetchComplimentTypesCompletion)
    }
    fileprivate func fetchComplimentTypesCompletion(_ complimentTypes: NameKeyValueMappable?, _ error: Error?, _ responseCode: Int){
        if(complimentTypes != nil && complimentTypes?.Options != nil && (complimentTypes?.Options.count)! > 0){
            updateDropDown(nameKeyValue: complimentTypes!, text: typeText, dropDown: dropDown)
        }
       
    }
    
    fileprivate func onSelectedDate(_ date: Date){
        selectedDate = date
        dateTextField.text = selectedDate!.formattedDateShort
    }
    
    func isReadyMoveNext() -> Bool{
        if((selectedDate == nil || dateTextField.text?.isEmpty ?? true)
            || dropDown.text == typeText
            || (textView.text?.isEmpty ?? true || (textView.text == NSLocalizedString("key_compliment_comments", comment: "")))){
            return false
        }
        
        let pageController = self.parent as! ComplimentPageViewController
        pageController.setSelectedDate(selectedDate: selectedDate!)
        
        compliment?.FeedbackDate = selectedDate?.formateDateUTC(date: selectedDate!, withFormat: Constants.DATE_UTC_SS)
        compliment?.Description = textView.text
        
        
        return true
    }
    
    @objc func dropDownAction(sender: UITapGestureRecognizer){
        isDropDownOpen = !isDropDownOpen
        dropDown.touchAction()
    }
    
    @objc func popupDatePickerBtnClicked(sender:UITapGestureRecognizer) {
        let storyboard =  UIStoryboard(name: "Popup", bundle: nil)
        let viewControler = storyboard.instantiateViewController(withIdentifier: "DatePickerPopupViewController") as! DatePickerPopupViewController
        viewControler.tag = DatePickerType.Date.rawValue
        viewControler.onSelectedDate = onSelectedDate
        present(viewControler, animated: true, completion: nil)
    }
    
    @objc func refreshTokenCompletion(notification: NSNotification){
        if(self.isTokenExpirationChanged){
            fetchComplimentType()
        }else{
            initViews()
        }
    }
    
    @objc func fetchAssociateInfoCompletion(notification: NSNotification){
        fetchComplimentType()
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        let pageController = self.parent as! ComplimentPageViewController
        pageController.onBackBtnClicked(currentIndex: currentPageIndex)
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        if(isReadyMoveNext()){
            let pageController = self.parent as! ComplimentPageViewController
            pageController.onNextBtnClicked(currentIndex: currentPageIndex)
        }
    }
}

extension SecondComplimentViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.init(hexString: Constants.textDarkWhite) {
            textView.text = nil
            textView.textColor = UIColor.init(hexString: Constants.textWhite)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

extension SecondComplimentViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == dateTextField {
            return false;
        }
        return true
    }
}

