//
//  SplashViewController.swift
//  Manage
//
//  Created by Rong Li on 11/13/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class SplashViewController: BaseViewController {
    
//    var remoteManager: RemoteManager?

    override func viewDidLoad() {
//        clearKeychainData()
//        clearDefaults()
        
        super.viewDidLoad()
        
        if(refreshTokenNotificationName != nil){
            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
        }
//        if(AssociateHelper.get()?.employeeId == nil){
//            fetchAssociateInfoNotificationName = Notification.Name(rawValue: fetchAssocateInfoNotificationKey)
//        }
//        if(fetchAssociateInfoNotificationName != nil){
//            NotificationCenter.default.addObserver(self, selector: #selector(fetchAssociateInfoCompletion(notification:)), name: fetchAssociateInfoNotificationName, object: nil)
//        }
        perform(#selector(self.launchMainApp), with: nil, afterDelay: 1)
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        if(refreshTokenNotificationName != nil || fetchAssociateInfoNotificationName != nil ){
//            NotificationCenter.default.removeObserver(self)
//        }
//    }
    
    fileprivate func clearKeychainData(){
        let isRemoved: Bool = KeychainWrapper.standard.removeAllKeys()
    }
    
    fileprivate func clearDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    
    @objc func refreshTokenCompletion(notification: NSNotification){
        print("### ### SplashViewController refreshTokenCompletion")
        perform(#selector(self.launchMainApp), with: nil, afterDelay: 1)
    }
    
    @objc func fetchAssociateInfoCompletion(notification: NSNotification){
        print("### ### SplashViewController fetchAssociateInfoCompletion")
        continueLaunchMainApp()
    }
    
//    @objc func launchMainApp(){
//        print("### ### launchManinApp")
//        let token: String = KeychainWrapper.standard.string(forKey: "access_token") ?? ""
////        let token: String? = SBMTokenHelper.get()?.access_token
//        if(token == ""){
//            goLoginViewController()
//        }else{
//            let selectedSite = SelectedSiteInfoHelper.get()
//            if(selectedSite?.Id != nil && selectedSite?.Latitude != 0.0 && selectedSite?.Longitude != 0.0){
//                goLandingViewController()
//            }else{
//                goSitePickViewController()
//            }
//        }
//    }
    
    @objc func launchMainApp(){
        print("### ### launchManinApp")
        let token: String = KeychainWrapper.standard.string(forKey: "access_token") ?? ""
        // let token: String? = SBMTokenHelper.get()?.access_token
        if(token == ""){
            goLoginViewController()
        }else{
            if(AssociateHelper.get()?.employeeId == nil){
                fetchAssociateInfoNotificationName = Notification.Name(rawValue: fetchAssocateInfoNotificationKey)
                NotificationCenter.default.addObserver(self, selector: #selector(fetchAssociateInfoCompletion(notification:)), name: fetchAssociateInfoNotificationName, object: nil)
                fetchAssociateInfo()
            }else{
                continueLaunchMainApp()
            }
        }
    }
    
    func continueLaunchMainApp(){
        let selectedSite = SelectedSiteInfoHelper.get()
        if(selectedSite?.Id != nil && selectedSite?.Latitude != 0.0 && selectedSite?.Longitude != 0.0){
//            goLandingViewController()
            goTabBarController(isCompletion: false, isSuccess: false, tabIndex: 0)
        }else{
            goSitePickViewController()
        }
    }
//    //moved to base class
//    func goLandingViewController(){
//        print("### ### goToLandingViewController")
//        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
//        let landingVC = storyboard.instantiateViewController(withIdentifier: "LandingViewController") as! LandingViewController
//        landingVC.remoteManager = remoteManager
//        present(landingVC, animated: true, completion: nil)
//    }
    
    override func goLoginViewController(){
        print("### ### goToLoginViewController")
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        loginVC.remoteManager = remoteManager
        present(loginVC, animated: true, completion: nil)
    }
    
    func goSitePickViewController(){
        print("### ### goToSitePickViewController")
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let sitePickerVC = storyboard.instantiateViewController(withIdentifier: "SitePickerViewController") as! SitePickerViewController
        sitePickerVC.remoteManager = remoteManager
        present(sitePickerVC, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
