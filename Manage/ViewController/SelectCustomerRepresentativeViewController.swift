//
//  SelectCustomerRepresentativeViewController.swift
//  Manage
//
//  Created by Rong Li on 1/21/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class CustomerRepresentativeCell: UITableViewCell{
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var position: UILabel!
}

class SelectCustomerRepresentativeViewController: BaseViewController {

    var onConfirmBtnClicked: ((_ selectedOnes: [CustomerRepresentative]) -> ())?
    
    var isSingleSelection: Bool = true
    var featureName: FeatureName?
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var multiSelectionView: MultiSelectionView!
    @IBOutlet weak var tableView: UITableView!
    
    var isSearching = false
    
//    var list: [CustomerRepresentativeMappable] = []
//    var searchedList: [CustomerRepresentativeMappable] = []
//    var selectedList: [CustomerRepresentativeMappable] = []
    
    var list: [CustomerRepresentative] = []
    var searchedList: [CustomerRepresentative] = []
    var selectedList: [CustomerRepresentative] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(refreshTokenNotificationName != nil || fetchAssociateInfoNotificationName != nil){
            if(refreshTokenNotificationName != nil){
                NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
            }else{
                NotificationCenter.default.addObserver(self, selector: #selector(fetchAssociateInfoCompletion(notification:)), name: fetchAssociateInfoNotificationName, object: nil)
            }
        }else{
            fetchCustomerRepresentatives()
        }

        initViews()
    }
    
    @objc func refreshTokenCompletion(notification: NSNotification){
        if(fetchAssociateInfoNotificationName == nil){
            fetchCustomerRepresentatives()
        }
    }
    
    @objc func fetchAssociateInfoCompletion(notification: NSNotification){
        fetchCustomerRepresentatives()
    }
    
    fileprivate func initViews(){
        
        hideSearchBar()
        
        headerView.screenName.text = NSLocalizedString("key_select_joint_auditor", comment: "")
        
        headerView.onCloseBtnClicked = {
            if self.featureName == FeatureName.Complaint {
                self.dismiss(animated: true)
            }else{
                self.goLandingViewController()
            }
        }
    }
    fileprivate func enableSearchCancelBtn() {
        if let cancelButton : UIButton = searchBar.value(forKey: "_cancelButton") as? UIButton{
            cancelButton.isEnabled = true
        }
    }
    fileprivate func hideSearchBar(){
        searchBar.isHidden = true
    }
    
    fileprivate func showSearchBar(){
        searchBar.isHidden = false
        enableSearchCancelBtn()
        searchBar.becomeFirstResponder()
    }
    
    
    fileprivate func fetchCustomerRepresentatives(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetchCustomerRepresentatives")
        }
        remoteManager!.fetchCustomerRepresentatives(remoteManager: remoteManager!, apiRouter: APIRouter.customerRepresentative, completion: fetchCustomerRepresentativesCompletion)
    }
    
    fileprivate func fetchCustomerRepresentativesCompletion(_ data: Data?, _ error: Error?, _ responseCode: Int){
        if(responseCode == Constants.ResponseFailed401){
            refreshTokenNotificationName = Notification.Name(rawValue: refreshTokenNotificationKey)
            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
            refreshToken()
            return
        }

        do{
            guard let jsonArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [[String : Any]] else {
                //No CustomerRepresentative data to be shown
                return
            }
            for i in 0..<jsonArray.count {
                list.append(CustomerRepresentative(json: jsonArray[i]))
            }
            tableView.reloadData()
        }catch{
            print("********** error \(error)")
        }
    }
    
    
//    fileprivate func fetchCustomerRepresentatives(){
//        if(remoteManager == nil){
//            fatalError("Missing dependencies on fetchCustomerRepresentatives")
//        }
//        remoteManager!.fetchCustomerRepresentatives(remoteManager: remoteManager!, apiRouter: APIRouter.customerRepresentative, completion: fetchCustomerRepresentativesCompletion)
//    }
//
//    fileprivate func fetchCustomerRepresentativesCompletion(_ customerRepresentativeList: [CustomerRepresentativeMappable]?, _ error: Error?, _ responseCode: Int){
//        if(error != nil){
//            print("*** *** fetchCustomerRepresentativesCompletion: responseCode: \(responseCode) error: \(error)")
//        }else{
//            print("*** *** fetchCustomerRepresentativesCompletion: responseCode: \(responseCode) ")
//        }
//        //if success, fetch from server, otherwise fetch from database
//        //empty data could happen on the very first time failed fetch from server.
//        if(customerRepresentativeList != nil && customerRepresentativeList!.count > 0){
//            if(selectedList.count > 0){
//                for item in selectedList {
//                    selectedList.append(item)
//                }
//            }
//            list = customerRepresentativeList!
//            tableView.reloadData()
//        }
//    }
    
    
    
    
    fileprivate func goNewAuditorViewController(){
        let storyboard =  UIStoryboard(name: "Popup", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "NewAuditorViewController") as! NewAuditorViewController
//        viewController.remoteManager = remoteManager
//        viewController.featureName = featureName
//        viewController.selectedEmployees = selectedEmployees
//        viewController.onConfirmBtnClicked = onConfirmBtnClicked
        
        viewController.modalTransitionStyle = .crossDissolve
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: true, completion: nil)
    }
    @IBAction func searchBtnClicked(_ sender: Any) {
        showSearchBar()
    }
    
    
    @IBAction func NewAuditorBtnClicked(_ sender: Any) {
        goNewAuditorViewController()
    }
    

}

extension SelectCustomerRepresentativeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(isSearching){
            return searchedList.count
        }else{
            return list.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerRepresentativeCell") as! CustomerRepresentativeCell
        if(isSearching){
            cell.name.text = searchedList[indexPath.row].Name
            cell.position.text = searchedList[indexPath.row].JobDescription
        }else{
            cell.name.text = list[indexPath.row].Name
            cell.position.text = list[indexPath.row].JobDescription
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    }
}
extension SelectCustomerRepresentativeViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchedList = list.filter({$0.Name!.lowercased().prefix(searchText.count) == searchText.lowercased()})
        isSearching = true
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
        searchBar.text = ""
        tableView.reloadData()
        
        searchBarSearchButtonClicked(searchBar)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
        searchBar.text = ""
        tableView.reloadData()
        
        searchBar.endEditing(true)
        hideSearchBar()
    }
}
