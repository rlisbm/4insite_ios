//
//  LandingViewController.swift
//  Manage
//
//  Created by Rong Li on 11/5/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON

class LandingViewController: BaseViewController {

//    var remoteManager: RemoteManager?
    
    var isCompletion: Bool?
    var isSuccess: Bool?
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var sideMenuBtn: UIButton!
    
    var locationManager: LocationManager {
        return AppDelegate.locationManager
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        //without remoteManager, app can not be continued login
        if(remoteManager == nil){
            return
        }
        
        if(refreshTokenNotificationName != nil){
            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
        }else{
            //initMKMap()
            initGMSMap()
            initViews()
        }
    }
//    deinit {
//        if(refreshTokenNotificationName != nil){
//            NotificationCenter.default.removeObserver(self)
//        }
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("*** *** \(SelectedSiteInfoHelper.get()?.Name)!  \(SelectedSiteInfoHelper.get()?.Latitude)! \(SelectedSiteInfoHelper.get()?.Longitude)!")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(isCompletion ?? false){
            let storyboard =  UIStoryboard(name: "Popup", bundle: nil)
            let viewControler = storyboard.instantiateViewController(withIdentifier: "CompletionViewController") as! CompletionViewController
            viewControler.isSuccess = isSuccess
            present(viewControler, animated: true, completion: nil)
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(refreshTokenNotificationName != nil || fetchAssociateInfoNotificationName != nil ){
            NotificationCenter.default.removeObserver(self)
        }
    }
    
    @objc func refreshTokenCompletion(notification: NSNotification){
        print("### ### LandingViewController refreshTokenCompletion")
        initGMSMap()
        initViews()
    }
    
    func initMKMap(){
//        //setup location map
//        locationManager.requestAuthorization()
//        let span = MKCoordinateSpanMake(0.01, 0.01)
//        let coordinate = CLLocationCoordinate2DMake((SelectedSiteHelper.get()?.Latitude)!, (SelectedSiteHelper.get()?.Longitude)!)
//        let region = MKCoordinateRegionMake(coordinate, span)
//        mapView.setRegion(region, animated: true)
//        let anotation = MKPointAnnotation()
//        anotation.coordinate = coordinate
//        anotation.title = SelectedSiteHelper.get()?.Name
//        mapView.addAnnotation(anotation)
    }
    
    func initGMSMap(){
        //setup location map
        locationManager.requestAuthorization()
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "landing_map_style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
        print("*** *** \(SelectedSiteInfoHelper.get()?.Name)!  \(SelectedSiteInfoHelper.get()?.Latitude)! \(SelectedSiteInfoHelper.get()?.Longitude)!")
        print("### ### \(AssociateHelper.get()?.profilePictureUniqueId))")
        //b2d17668-93df-8148-57ff-1d6514ad5095.png
        let camera = GMSCameraPosition.camera(withLatitude: (SelectedSiteInfoHelper.get()?.Latitude)!,
                                              longitude: (SelectedSiteInfoHelper.get()?.Longitude)!,
                                              zoom: 15)
        mapView.camera = camera
        showMarker(position: camera.target)
        
    }
    
    func showMarker(position: CLLocationCoordinate2D){
        let marker = GMSMarker()
        marker.position = position
        marker.title = SelectedSiteInfoHelper.get()?.Name
        marker.map = mapView
    }
    
    func  initViews(){
        
//        button.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
//        button.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
//        button.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
    }
    
    func goToQuickEntryViewController(){
        print("### ### goToQuickEntryViewController")
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let quickEntryVC = storyboard.instantiateViewController(withIdentifier: "QuickEntryViewController") as! QuickEntryViewController
        quickEntryVC.remoteManager = remoteManager
        present(quickEntryVC, animated: true, completion: nil)
        
    }
    
    @IBAction func slideMenuClicked(_ sender: UIButton) {
        print("### ### slideMenuClicked")
        if(sender.tag == 0){
            sender.isEnabled = false
            
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let sideMenuVC = storyboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
            sideMenuVC.remoteManager = remoteManager
//            sideMenuVC.sideMenuBtn = sender
            self.view.addSubview(sideMenuVC.view)
            self.addChildViewController(sideMenuVC)
            sideMenuVC.view.layoutIfNeeded()
            
            sideMenuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                sideMenuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
                sender.isEnabled = true
            }, completion:nil)
            
            return
        }
    }
    
    @IBAction func quickEntryClicked(_ sender: Any) {
        goToQuickEntryViewController()
    }
    
}
