//
//  SelectDepartmentViewControlle.swift
//  Manage
//
//  Created by Rong Li on 12/21/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit
import RealmSwift

class SelectDepartmentViewController: BaseViewController {
    var isSingleSelection: Bool = true
    var featureName: FeatureName?

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var multiSelectionView: MultiSelectionView!
    @IBOutlet weak var departmentTable: UITableView!
    @IBOutlet weak var departmentTableTopConstraint: NSLayoutConstraint!
    
    
    var isSearching = false
    var departments: [Department] = []
    var searchedDepartments: [Department] = []
    var selectedDepartments: [Department] = []
    
    var departmentName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        if(refreshTokenNotificationName != nil){
//            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
//        }else{
//            initViews()
//            if(AssociateHelper.get()?.employeeId == nil){
//                fetchAssociateInfoNotificationName = Notification.Name(rawValue: fetchAssocateInfoNotificationKey)
//                NotificationCenter.default.addObserver(self, selector: #selector(fetchAssociateInfoCompletion(notification:)), name: fetchAssociateInfoNotificationName, object: nil)
//            }else{
//                fetchDepartments()
//            }
//        }
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        if(refreshTokenNotificationName != nil || fetchAssociateInfoNotificationName != nil ){
//            NotificationCenter.default.removeObserver(self)
//        }
//    }
//    
//    fileprivate func isSingleEmployeeSelection() -> Bool{
//        if(featureName == FeatureName.SendMessage){
//            return false
//        }
//        return true
//    }
//    
//    fileprivate func removeMultiSelectionView(){
//        multiSelectionView?.removeFromSuperview()
//        departmentTableTopConstraint.constant = 0
//    }
//    
//    func initViews(){
//        headerView.screenName.text = NSLocalizedString("key_send_message_to", comment: "")
//        
//        isSingleSelection = isSingleEmployeeSelection()
//        if(isSingleSelection){
//            removeMultiSelectionView()
//        }
//        
//        searchBar.delegate = self
//        hideSearchBar()
//        
//        headerView.onCloseBtnClicked = {
//            self.goLandingViewController()
//        }
//        
//        multiSelectionView?.onSelectBtnClicked = {
//            self.selectedDepartments.removeAll()
//            if(self.multiSelectionView.button.tag == 0){
//                self.multiSelectionView.button.tag = 1
//                for department in self.departments {
//                    self.selectedDepartments.append(department)
//                }
//            }else{
//                self.multiSelectionView.button.tag = 0
//            }
//            
//            self.departmentTable.reloadData()
//            
//        }
//    }
//    
//    fileprivate func fetchSiteJobs(){
//        print("*** *** fetchDepartments")
//        if(remoteManager == nil){
//        fatalError("Missing dependencies on fetchTeamMembers")
//        }
//        remoteManager!.fetchDepartments(remoteManager: remoteManager!, apiRouter: APIRouter.jobsForSites, completion: fetchDepartmentsCompletion)
//    }
//    
//    func fetchDepartments(){
//        print("*** *** fetchDepartments")
//        if(remoteManager == nil){
//            fatalError("Missing dependencies on fetchTeamMembers")
//        }
//        remoteManager!.fetchDepartments(remoteManager: remoteManager!, apiRouter: APIRouter.department, completion: fetchDepartmentsCompletion)
//    }
//
//    fileprivate func fetchDepartmentsCompletion(_ departmentList: [Department]?, _ error: Error?, _ responseCode: Int){
//        print("*** *** fetchDepartmentsCompletion")
//        if(error != nil){
//            print("*** *** fetchDepartmentsCompletion: responseCode: \(responseCode) error: \(error)")
//        }else{
//            print("*** *** fetchDepartmentsCompletion: responseCode: \(responseCode) ")
//        }
//
//        if(departmentList != nil){
//            print("### ### size : \(departmentList!.count)")
//        }else{
//            print("### ### size : departmentList == nil")
//        }
//
//        //if success, fetch from server, otherwise fetch from database
//        //empty data could happen on the very first time failed fetch from server.
//        if(departmentList != nil && departmentList!.count > 0){
//            if(selectedDepartments.count > 0){
//                for department in selectedDepartments {
//                    selectedDepartments.append(department)
//                }
//                selectedEmployees.removeAll()
//            }
//            departments = departmentList!
//            departmentTable.reloadData()
//        }
//    }
//    
//    fileprivate func goSendMessageViewController(){
//        print("### ### goSendMessageViewController")
//        let storyboard =  UIStoryboard(name: "SendMessage", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "SendMessageViewController") as! SendMessageViewController
//        viewController.remoteManager = remoteManager
//        viewController.selectedDepartmentList = selectedDepartments
//        present(viewController, animated: true, completion: nil)
//    }
//    
//    @objc func refreshTokenCompletion(notification: NSNotification){
//        print("### ### SelectEmployeeViewController refreshTokenCompletion")
//        initViews()
//    }
//    
//    @objc func fetchAssociateInfoCompletion(notification: NSNotification){
//        print("### ### SelectEmployeeViewController fetchAssociateInfoCompletion")
//        fetchDepartments()
//    }
//    
//    @IBAction func searchBtnClicked(_ sender: Any) {
//        showSearchBar()
//    }
//    
//    @IBAction func confirmBtnClicked(_ sender: Any) {
//        if(featureName == nil){
//            return
//        }
//        if(selectedDepartments.count == 0){
//            showAlert(NSLocalizedString("key_infomation", comment: ""), NSLocalizedString("key_select_a_department", comment: ""), NSLocalizedString("key_dismiss", comment: ""), viewController: self)
//            return
//        }
//        
//        switch featureName! {
//        case FeatureName.SendMessage:
//            goSendMessageViewController()
//            break
//        default:
//            break
//        }
//    }
//    
//   
//    fileprivate func enableSearchCancelBtn() {
//        if let cancelButton : UIButton = searchBar.value(forKey: "_cancelButton") as? UIButton{
//            cancelButton.isEnabled = true
//        }
//    }
//    fileprivate func showSearchBar(){
//        headerView.isHidden = true
//        searchBtn.isHidden = true
//        
//        searchBar.isHidden = false
//        enableSearchCancelBtn()
//        
//        searchBar.becomeFirstResponder()
//    }
//    fileprivate func hideSearchBar(){
//        headerView.isHidden = false
//        searchBtn.isHidden = false
//        
//        searchBar.isHidden = true
//    }
//    
//    func didSingleSelectTableView(tableView: UITableView, indexPath: IndexPath, list: [Department]){
//        if let selectedCell  = tableView.cellForRow(at: indexPath as IndexPath) {
//            if(selectedDepartments.count == 0){
//                selectedDepartments.append(list[indexPath.row])
//                selectedCell.accessoryType = .checkmark
//            }else{
//                //if pre-selection is visible, update it
//                let index = list.index(of: selectedDepartments[0])
//                
////                //This block of code is for team list (var team = List<TeamMember>())
////                //Because for some reason Array.index(of: ) does not work Array trasfromed from List
////                let selected = selectedDepartents[0]
////                if(index == nil){
////                    for member in list {
////                        if(member.isSameObject(as: selected)){
////                            index = list.index(of: member)
////                            break
////                        }
////                    }
////                }
//                
//                if(index != nil){
//                    let preIndexPath = IndexPath(row: index!, section: 0)
//                    let preSelectedCell = tableView.cellForRow(at: preIndexPath as IndexPath)
//                    if(preSelectedCell != nil){
//                        preSelectedCell!.accessoryType = .none
//                    }
//                }
//                //update currently selected gui
//                if(selectedCell.accessoryType == .checkmark){
//                    selectedDepartments.removeAll()
//                    selectedCell.accessoryType = .none
//                }else{
//                    selectedCell.accessoryType = .checkmark
//                    selectedDepartments[0] = list[indexPath.row]
//                }
//            }
//        }
//    }
//    func didMultiSelectTableView(tableView: UITableView, indexPath: IndexPath, list: [Department]){
//        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
//            if cell.accessoryType == .checkmark{
//        
//                var index = selectedDepartments.index(of: list[indexPath.row])
//                
//                if(index == nil){
//                    let selected = list[indexPath.row]
//                    for department in selectedDepartments {
//                        if(department.departmentId == selected.departmentId){
//                            index = selectedDepartments.index(of:department)
//                            break
//                        }
//                    }
//                }
//                
//                selectedDepartments.remove(at: index!)
//                
//                cell.accessoryType = .none
//            }
//            else{
//                selectedDepartments.append(list[indexPath.row])
//                cell.accessoryType = .checkmark
//            }
//        }
//    }
//}
//
//extension SelectDepartmentViewController: UITableViewDelegate, UITableViewDataSource {
//    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if(isSearching){
//            return searchedDepartments.count
//        }else{
//            return departments.count
//        }
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//     
//        let cell = tableView.dequeueReusableCell(withIdentifier: "DepartmentCell")!
//        if(isSearching){
//            departmentName = searchedDepartments[indexPath.row].name
//        }else{
//            departmentName = departments[indexPath.row].name
//        }
//
//        cell.textLabel?.text = departmentName
//
//        var isSelected = false
//        if(selectedDepartments.count > 0){
//            for department in selectedDepartments {
//                if(department.name == departmentName) {
//                    cell.accessoryType = .checkmark
//                    isSelected = true
//                    break
//                }
//            }
//        }
//        if(!isSelected){
//            cell.accessoryType = .none
//        }
//
//        return cell
//    }
//    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: true)
//        if(isSingleSelection){
//            if(isSearching){
//                didSingleSelectTableView(tableView: tableView, indexPath: indexPath, list: searchedDepartments)
//            }else{
//                didSingleSelectTableView(tableView: tableView, indexPath: indexPath, list: departments)
//            }
//        }else{
//            if(isSearching){
//                didMultiSelectTableView(tableView: tableView, indexPath: indexPath, list: searchedDepartments)
//
//            }else{
//                didMultiSelectTableView(tableView: tableView, indexPath: indexPath, list: departments)
//            }
//        }
//    }
//    
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        if(isSingleSelection){
//            tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .none
//        }
//    }
//    
//}
//
//extension SelectDepartmentViewController: UISearchBarDelegate {
//    
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        searchedDepartments = departments.filter({$0.name!.lowercased().prefix(searchText.count) == searchText.lowercased()})
//        isSearching = true
//        departmentTable.reloadData()
//    }
//    
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//                isSearching = false
//                searchBar.text = ""
//                departmentTable.reloadData()
//        
//        searchBarSearchButtonClicked(searchBar)
//    }
//    
//    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        isSearching = false
//        searchBar.text = ""
//        departmentTable.reloadData()
//
//        searchBar.endEditing(true)
//        hideSearchBar()
//    }
}

