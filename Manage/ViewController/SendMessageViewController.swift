////
////  SendMessageViewController.swift
////  Manage
////
////  Created by Rong Li on 12/26/18.
////  Copyright © 2018 Insite. All rights reserved.
////
//
//import UIKit
//
//class SendMessageViewController: BaseViewController, UITextViewDelegate {
//    
//    
////    @IBOutlet weak var scrollContentHeight: NSLayoutConstraint!
////    @IBOutlet weak var nameContainerHeight: NSLayoutConstraint!
////    @IBOutlet weak var postDateViewTop: NSLayoutConstraint!
////    @IBOutlet weak var postDateMsgTop: NSLayoutConstraint!
////    @IBOutlet weak var pickDateTimeViewHeight: NSLayoutConstraint!
////
////
////    @IBOutlet weak var headerView: HeaderView!
////    @IBOutlet weak var assignToView: AssignToView!
////    @IBOutlet weak var nameContainerView: UIView!
////    @IBOutlet weak var switchView: SwitchView!
//////    @IBOutlet weak var postDateMsgView: UIView!
////    @IBOutlet weak var pickDateTimeView: PickDateTimeView!
//    
//    
//    @IBOutlet weak var scrollContentViewHeight: NSLayoutConstraint!
//    @IBOutlet weak var nameContainerViewHeight: NSLayoutConstraint!
//    @IBOutlet weak var contentViewTop: NSLayoutConstraint!
//    @IBOutlet weak var postDateMsgTop: NSLayoutConstraint!
//    @IBOutlet weak var pickDateTimeViewHeight: NSLayoutConstraint!
//    @IBOutlet weak var switchViewTop: NSLayoutConstraint!
//    
//    
//    @IBOutlet weak var headerView: HeaderView!
//    @IBOutlet weak var assignToView: AssignToView!
//    @IBOutlet weak var nameContainerView: UIView!
//    @IBOutlet weak var switchView: SwitchView!
//    @IBOutlet weak var pickDateTimeView: PickDateTimeView!
//    @IBOutlet weak var descriptionView: DescriptionView!
//    @IBOutlet weak var submitMsgBtn: UIButton!
//    @IBOutlet weak var postDateLabel: UILabel!
//    @IBOutlet weak var contentView: UIView!
//    @IBOutlet weak var mobileBtn: UIButton!
//    @IBOutlet weak var highPriorityBtn: UIButton!
//    
//    let nameContainerViewOriginalHeight: CGFloat = 90
//    var pickDateTimeViewOriginalHeight: CGFloat = 60
//    
////    var isKeyboardOpen: Bool = false
//    
//    let labelHeight = 26
//    let margin:  CGFloat = 10
//    var width: CGFloat = 0
//    var yPos: Int = 0
//    var xPos: Int = 0
//    var yOriginalPos: Int = 0
//    // Screen width.
//    public var screenWidth: CGFloat {
//        return UIScreen.main.bounds.width
//    }
//    
//    // Screen height.
//    public var screenHeight: CGFloat {
//        return UIScreen.main.bounds.height
//    }
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        descriptionView.title.text = NSLocalizedString("key_message", comment: "")
//        descriptionView.content.delegate = self
//        descriptionView.content.textColor = UIColor.init(hexString: Constants.textDarkWhite)
//        
//        //Listen for keyboard events
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)
//        
//        scrollContentViewHeight.constant = scrollContentViewHeight.constant - 50
//        initViews()
//    }
//    
//    deinit {
//        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidShow, object: nil)
//        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidHide, object: nil)
//        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidChangeFrame, object: nil)
//    }
//    
//    override func viewWillAppear(_ animated: Bool) {
//        print("############# viewWillAppear")
//    }
//    
//    override func viewDidAppear(_ animated: Bool) {
//        print("############# viewDidAppear")
//    }
//    
//    override func viewWillDisappear(_ animated: Bool) {
//        print("############# viewWillDisappear")
////        if(refreshTokenNotificationName != nil || fetchAssociateInfoNotificationName != nil ){
////            NotificationCenter.default.removeObserver(self)
////        }
//    }
//    
//    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
//        print("############# didRotate")
//        updateNameContainerView()
//    }
//    
//    fileprivate func initViews(){
//        print("############# initViews")
//        pickDateTimeViewOriginalHeight = pickDateTimeViewHeight.constant
//        
//        headerView.onCloseBtnClicked = {
//            self.discardWarningAlert()
//        }
//        
//        assignToView.onEditBtnClicked = {
//            self.onEditBtnClicked()
//        }
//        
//        assignToView.onToggleBtnClicked = {
//            (toggleBtn) in
//            self.onToggleBtnClicked(toggleBtn: toggleBtn)
//        }
//        
//        addNames()
//    
//        if(!switchView.switchBtn.isOn){
//            removePickDateTimeView()
//        }
//      
//        switchView.onSwitchClicked = {
//            (isOn) in
//            if(isOn){
//                self.addPickDateTimeView()
//            }else{
//                self.removePickDateTimeView()
//            }
//        }
//        
//        pickDateTimeView.onPopupDatePickerBtnClicked = {
//            (dateTimeBtn) in
//            self.popupDatePickerBtnClicked(sender: dateTimeBtn)
//        }
//        
//        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTouched))
//        contentView.addGestureRecognizer(tapRecognizer)
//        
//    }
//    
//    fileprivate func removePickDateTimeView(){
//        pickDateTimeView.isHidden = true
//        
//        postDateMsgTop.constant = 0
//        scrollContentViewHeight.constant = scrollContentViewHeight.constant - pickDateTimeViewHeight.constant
////        submitMsgBtn.setTitle(NSLocalizedString("key_send_message_up_case", comment: ""), for: .normal)
//    }
//    
//    fileprivate func addPickDateTimeView(){
//        pickDateTimeView.isHidden = false
//        
//        postDateMsgTop.constant = pickDateTimeViewOriginalHeight
//        scrollContentViewHeight.constant = scrollContentViewHeight.constant + pickDateTimeViewHeight.constant
////        submitMsgBtn.setTitle(NSLocalizedString("key_schedul_message_up_case", comment: ""), for: .normal)
//    }
//    
//    fileprivate func addNames(){
//        yOriginalPos = Int(contentViewTop.constant)
//        width = 0
//        xPos = 0
//        yPos = 0
//        
//        for i in 0..<selectedDepartmentList!.count {
//            let name = selectedDepartmentList?[i].name
//            
//            let label = UILabel()
//            if(i % 2 == 0){
//                label.backgroundColor = .red
//            }else {
//                label.backgroundColor = .yellow
//            }
//            label.text = name
//            label.textAlignment = .left
//            label.frame = CGRect( x:Int(xPos), y:yPos, width:250, height: labelHeight)
//            
//            label.sizeToFit()
//            
//            width += label.frame.size.width + margin
//            
//            if(width > nameContainerView.frame.width){
//                print(i)
//                width = 0
//                xPos = 0
//                yPos += labelHeight
//                
//                label.frame = CGRect( x:Int(xPos), y:yPos, width:250, height: labelHeight)
//                label.sizeToFit()
//                width += label.frame.size.width + margin
//            }
//            
//            xPos = Int(width + margin * 2)
//            nameContainerViewHeight.constant = CGFloat(yPos + labelHeight)
//            nameContainerView.addSubview(label)
//        }
//        print("########## \(nameContainerViewHeight.constant)")
//        print("########## \(yPos)")
//        print("########## \(yOriginalPos)")
////        yPos = yPos + 30 > yOriginalPos ? yPos + 30 : yOriginalPos
//        
//        yPos = yPos + 30 > Int(nameContainerViewHeight.constant) ? yPos + 30 : yOriginalPos
//        print("########## \(yPos)")
//        print("########## \(yOriginalPos)")
//        
////        if(yPos + 30 > Int(nameContainerViewHeight.constant)){
////            yPos = yPos + 30
////        }else{
////            yPos = yPos + 30 > yOriginalPos ? yPos + 30 : yOriginalPos
////        }
//    }
//    
//    private func updateNameContainerView(){
//        for label in nameContainerView.subviews {
//            label.removeFromSuperview()
//        }
//        nameContainerViewHeight.constant = nameContainerViewOriginalHeight
//        addNames()
//        contentViewTop.constant = CGFloat(yPos)
//    }
//    
//    fileprivate func onToggleBtnClicked(toggleBtn: UIButton) {
//        var title: String = ""
//        
//        if let attributedTitle = toggleBtn.attributedTitle(for: .normal) {
//            let mutableAttributedTitle = NSMutableAttributedString(attributedString: attributedTitle)
//            
//            print("########## 111 \(contentViewTop.constant)")
//            print("########## 111 \(nameContainerViewHeight.constant)")
//            
//            if (toggleBtn.tag == 1){
//                title = NSLocalizedString("key_see_less", comment: "")
//                contentViewTop.constant = CGFloat(yPos)
//                scrollContentViewHeight.constant = scrollContentViewHeight.constant + CGFloat((yPos - yOriginalPos))
//                
////                if(contentViewTop.constant < nameContainerViewHeight.constant){
////                contentViewTop.constant = CGFloat(yPos)
////                scrollContentViewHeight.constant = scrollContentViewHeight.constant + CGFloat((yPos - yOriginalPos))
////                }
//            
//            }else{
//                title = NSLocalizedString("key_see_all", comment: "")
//                contentViewTop.constant = CGFloat(yOriginalPos)
//                scrollContentViewHeight.constant = scrollContentViewHeight.constant - CGFloat((yPos - yOriginalPos))
//                
////                if(contentViewTop.constant > nameContainerViewHeight.constant){
////                contentViewTop.constant = CGFloat(yOriginalPos)
////                scrollContentViewHeight.constant = scrollContentViewHeight.constant - CGFloat((yPos - yOriginalPos))
////                }
//                
//            }
//            
//            mutableAttributedTitle.replaceCharacters(in: NSMakeRange(0, mutableAttributedTitle.length), with: title)
//            toggleBtn.setAttributedTitle(mutableAttributedTitle, for: .normal)
//        }
//
//    }
//    
//    fileprivate func onEditBtnClicked() {
//
//    }
//    
//    //web    DueDate: "2018-12-14T07:59:59.999Z"
//    //mobile    DueDate: "2018-12-14T07:59:59"
//    fileprivate func onSelectedDateText(_ formattedText: String, _ tag: Int){
//        switch tag {
//        case 2:
//            pickDateTimeView.timeTextField.text = formattedText
//            break
//        default:
//            pickDateTimeView.dateTextField.text = formattedText
//            break
//        }
//    }
//    
//    fileprivate func setCheckBtn(_ sender: UIButton){
//        if(sender.tag == 0){
//            sender.tag = 1
//            sender.setImage(UIImage(named: "check-box-selected.png"), for: .normal)
//        }else{
//            sender.tag = 0
//            sender.setImage(UIImage(named: "check-box-unselected.png"), for: .normal)
//        }
//    }
//    
//    func popupDatePickerBtnClicked(sender: UIButton) {
//        print("############# popupDatePickerBtnClicked")
//        let storyboard =  UIStoryboard(name: "Popup", bundle: nil)
//        let viewControler = storyboard.instantiateViewController(withIdentifier: "DatePickerPopupViewController") as! DatePickerPopupViewController
//        viewControler.tag = sender.tag
//        viewControler.dateStyle = .short
//        viewControler.onSelectedDateText = onSelectedDateText
//        present(viewControler, animated: true, completion: nil)
//    }
//    
//    fileprivate func createTrainingMessage() -> TrainingMessage? {
//        let text = descriptionView.content.text
//        if (text?.isEmpty ?? true) {
//            showInfoAlert(message: NSLocalizedString("key_provide_input", comment: ""))
//            return nil
//        }
//        
//        let associate = AssociateHelper.get()!
//        
////        let trainingMessageRequestItem = TrainingMessageRequestItem(trainingMessageId: 0,
////                                                                    title: "",
////                                                                    text: text,
////                                                                    senderEmail: associate.email,
////                                                                    senderFullName: associate.fullName,
////                                                                    senderJobTitle: associate.jobTitle,
////                                                                    senderProfileImageUrl: associate.profilePictureUniqueId,
////                                                                    senderType: "Manager",
////                                                                    isMobile: mobileBtn.tag == 0 ? false : true,
////                                                                    isCustomer: false,
////                                                                    highPriority: highPriorityBtn.tag == 0 ? false : true,
////                                                                    de)
//        
//        return nil
//    }
//    
//    fileprivate func sendTrainingMessage(trainingMessage: TrainingMessage){
//        
//    }
//    
//    @objc func viewTouched(sender: UITapGestureRecognizer) {
//        print("************* viewTouched")
//        hideKeyboard(hideKeyboardType:HideKeyboardType.ScreenTounch)
//    }
//    
//    
//    @IBAction func mobileBtnClicked(_ sender: UIButton) {
//        setCheckBtn(sender)
//        
//    }
//    
//    @IBAction func highPriorityBtnClicked(_ sender: UIButton) {
//        setCheckBtn(sender)
//    }
//    
//    @IBAction func submitMsgBtnClicked(_ sender: Any) {
////        sendTrainingMessage(createMessage())
//    }
//    
//    
//    //Handle Keyboard
//    func hideKeyboard(hideKeyboardType:HideKeyboardType){
//        switch hideKeyboardType {
//        case HideKeyboardType.KeyboradReturn:
//            descriptionView.content.resignFirstResponder()
//            break
//        case HideKeyboardType.ScreenTounch:
//            self.view.endEditing(true)
//            break
//        default:
//            break
//        }
//    }
//    
//    @objc func keyboardWillChange(notification: Notification){
//        guard let keyboardRect = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
//            return
//        }
//        
//        if (notification.name == Notification.Name.UIKeyboardWillShow || notification.name == Notification.Name.UIKeyboardWillChangeFrame
//            || notification.name == Notification.Name.UIKeyboardDidShow || notification.name == Notification.Name.UIKeyboardDidChangeFrame) {
//            
//            view.frame.origin.y = -keyboardRect.height
//            isKeyboardOpen = true
//        }else{
//            view.frame.origin.y = 0
//            isKeyboardOpen = false
//        }
//    }
//    
//    func textViewDidBeginEditing(_ textView: UITextView) {
//        if textView.textColor == UIColor.init(hexString: Constants.textDarkWhite) {
//            textView.text = nil
//            textView.textColor = UIColor.init(hexString: Constants.textWhite)
//        }
//    }
//    
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        if text == "\n" {
//            textView.resignFirstResponder()
//            return false
//        }
//        return true
//    }
//    
//    //Touch screen outside textfield to dismiss the keyboard
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        print("************* hahaha")
//        hideKeyboard(hideKeyboardType:HideKeyboardType.ScreenTounch)
//    }
//
//}
