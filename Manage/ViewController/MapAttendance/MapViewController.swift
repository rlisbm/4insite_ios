//
//  MapViewController.swift
//  Manage
//
//  Created by Rong Li on 3/14/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewController: BaseViewController {
   
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var clockInLabel: UILabel!
    @IBOutlet weak var noLocationLabel: UILabel!
    @IBOutlet weak var updatedLabel: UILabel!
    
    
    var teamRoster: TeamRosterV2?
    var updatedDate: Date?
    var noLocationCounter: Int?
    
    var tabBar: TabBarController?
    
    var locationManager: LocationManager {
        return AppDelegate.locationManager
    }
    
    lazy var locationObserver: LocationManager.LocationObserver = Observer { [weak self] in
        
        if(self != nil){
            self!.mapView.camera = GMSCameraPosition(target: $0.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        }
    }
    
    override func viewDidLoad() {
        tabBar = tabBarController as! TabBarController
        if(tabBar == nil){
            return
        }
        remoteManager = tabBar!.remoteManager
        
        //without remoteManager, app can not be continued login
        if(remoteManager == nil){
            return
        }
        
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        if(refreshTokenNotificationName != nil || fetchAssociateInfoNotificationName != nil){
            if(refreshTokenNotificationName != nil){
                NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
            }else{
                NotificationCenter.default.addObserver(self, selector: #selector(fetchAssociateInfoCompletion(notification:)), name: fetchAssociateInfoNotificationName, object: nil)
            }
        }else{
        
            fetchTeamRoster()
        }

        
        initMap()
        initViews()
        
    }
    
    @objc func refreshTokenCompletion(notification: NSNotification){
        if(fetchAssociateInfoNotificationName == nil){
            fetchTeamRoster()
        }
    }
    
    @objc func fetchAssociateInfoCompletion(notification: NSNotification){
        fetchTeamRoster()
    }
    
    fileprivate func initMap(){
        //setup location map
        locationManager.requestAuthorization()
        mapView.isMyLocationEnabled = true
        locationManager.didUpdateLocations += locationObserver
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "landing_map_style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
    
    fileprivate func initViews(){
    }
    
    fileprivate func getNoLocationCounter() -> Int{
        guard let teamMembers = teamRoster?.TeamMembers else{
            //No data to be shown
            return 0
        }
        noLocationCounter = 0
        for teamMember in teamMembers {
            if(teamMember.LatestPunch?.Status == Constants.clockIn
                && teamMember.LatestPunch?.Coordinates?.Latitude == 0
                && teamMember.LatestPunch?.Coordinates?.Longitude == 0){
                noLocationCounter! += 1
            }
        }
        
        return noLocationCounter!
    }
    
    fileprivate func updateViews(){
        clockInLabel.text = clockInLabel.text! + " " + String(teamRoster!.ClockedInCountForSite!)
        noLocationCounter = getNoLocationCounter();
        noLocationLabel.text = noLocationLabel.text! + " " + String(noLocationCounter!)
        if(updatedDate == nil){
            updatedDate = Date()
        }
        updatedLabel.text = updatedLabel.text! + " " + updatedDate!.formattedDateShort
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EmployeeListViewControllerSegue"  {
            if let viewController = segue.destination as? EmployeeListViewController{
                viewController.remoteManager = remoteManager
                viewController.teamRoster = teamRoster
                viewController.tabBar = tabBar
            }
        }
    }
    
    fileprivate func fetchTeamRoster(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetchTeamRoster")
        }
        remoteManager!.fetchDataWithDate(remoteManager: remoteManager!, apiRouter: APIRouter.teamRosterV2, completion: fetchTeamRosterCompletion)
    }
    
    fileprivate func fetchTeamRosterCompletion(_ data: Data?, _ error: Error?, _ responseCode: Int?, date: Date?){
        if(responseCode == Constants.ResponseFailed401){
            refreshTokenNotificationName = Notification.Name(rawValue: refreshTokenNotificationKey)
            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
            refreshToken()
            return
        }
        
        let jsonDecoder = JSONDecoder()
        guard let teamRoster = try? jsonDecoder.decode(TeamRosterV2.self, from: data!) else{
            //No data to be shown
            return
        }
//        do {
//            if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] {
//                print(json)
//            }
//        } catch {
//            print("Error deserializing JSON: \(error)")
//        }
     
        self.teamRoster = teamRoster
        updatedDate = date
        updateViews()
    }

}
