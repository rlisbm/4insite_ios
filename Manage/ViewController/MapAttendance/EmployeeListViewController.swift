//
//  EmployeeListViewController.swift
//  Manage
//
//  Created by Rong Li on 3/20/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class EmployeeListCell: UITableViewCell{
    @IBOutlet weak var employeeItemiew: ImgLabelView!
}

class EmployeeListViewController: BaseViewController {
    let filters = ["Clocked In", "No Location", "Clocked Out"]
    let filterIds = [1, 2, 3]
    var filterId: Int = 1
    
    var teamRoster: TeamRosterV2?
    var team: [TeamMemberV2] = [TeamMemberV2]()
    var avatarUrl: String = ""
    
    let originalClock = UIImage(named: "Clock Icon.png")
    var tintedClock: UIImage?
    
    let originalAlpha = UIImage(named: "AZSort.png")
    var tintedAlpha: UIImage?
    
    var tabBar: TabBarController?
    
    @IBOutlet weak var clockedInLabel: UILabel!
    @IBOutlet weak var filterDropDown: PaddingDropDown!
    @IBOutlet weak var clockBtn: UIButton!
    @IBOutlet weak var alphaBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        //without remoteManager, app can not be continued login
        if(remoteManager == nil || teamRoster?.TeamMembers == nil){
            return
        }
        
        super.viewDidLoad()
        
        team = (teamRoster?.TeamMembers)!
        initViews()
        
    }
    
    fileprivate func initViews(){
        clockedInLabel.text = clockedInLabel.text! + " " + String(teamRoster!.ClockedInCountForSite!)
        initDropDown()
        updateBtns()
    }
    
    fileprivate func initDropDown(){
        filterDropDown.setDropDown(rowBackgroundColor: Constants.dropDownRowBG, selectedRowColor: Constants.dropDownRowBG, rowTextColor: Constants.dropDownTextColor, rowHeight: Constants.drowDownRowHeight);
        
        filterDropDown.optionArray = filters
        filterDropDown.optionIds = filterIds
        
        filterDropDown.text = filters[0]
        
        filterDropDown.didSelect{ (selectedText , index , id) in
            self.filterId = id
            self.updateTableView(id: id)
        }
    }
    fileprivate func updateTableView(id: Int){
        team.removeAll()
        if(id == filterIds[0]){
            for member in (teamRoster?.TeamMembers)! {
                if(member.LatestPunch?.Status == Constants.clockIn){
                    team.append(member)
                }
            }
        }else if(id == filterIds[1]){
            for member in (teamRoster?.TeamMembers)! {
                if(member.LatestPunch?.Coordinates?.Latitude == 0 && member.LatestPunch?.Coordinates?.Longitude == 0){
                    team.append(member)
                }
            }
        }else if(id == filterIds[2]){
            for member in (teamRoster?.TeamMembers)! {
                if(member.LatestPunch?.Status == Constants.clockOut){
                    team.append(member)
                }
            }
        }
        
        if(clockBtn.tag == 1){
            team = team.sorted(by: { $0.LatestPunch!.PunchDate! < $1.LatestPunch!.PunchDate!})
        }else{
            team = team.sorted(by: { $0.FirstName! < $1.FirstName!})
        }
        tableView.reloadData()
    }
    
    fileprivate func updateBtns(){
        if(clockBtn.tag == 1){
            tintedClock = originalClock!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            clockBtn.setImage(tintedClock, for: .normal)
            clockBtn.tintColor = UIColor.init(hexString: Constants.btnTextBlue)
            clockBtn.backgroundColor = UIColor.init(hexString: Constants.btnDarkGrayBG)
            
            alphaBtn.setImage(originalAlpha, for: .normal)
            alphaBtn.backgroundColor = .clear
    
        }else{
            clockBtn.setImage(originalClock, for: .normal)
            clockBtn.backgroundColor = .clear
            
            tintedAlpha = originalAlpha!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            alphaBtn.setImage(tintedAlpha, for: .normal)
            alphaBtn.tintColor = UIColor.init(hexString: Constants.btnTextBlue)
            alphaBtn.backgroundColor = UIColor.init(hexString: Constants.btnDarkGrayBG)
        }
        
        updateTableView(id: filterId)
    }
    
    @IBAction func MapViewBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clockBtnClicked(_ sender: UIButton) {
        clockBtn.tag = 1
        updateBtns()
    }
    
    @IBAction func alphaBtnClicked(_ sender: UIButton) {
        clockBtn.tag = 0
        updateBtns()
    }
    
    //MARK: -- Navigation
    fileprivate func goEmployeePopupViewController(teamMember : TeamMemberV2){
        let storyboard =  UIStoryboard(name: "Popup", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "EmployeePopupViewController") as! EmployeePopupViewController
        viewController.remoteManager = remoteManager
        viewController.selectedEmployee = teamMember
        viewController.tabBar = tabBar
        present(viewController, animated: true, completion: nil)
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "EmployeePopupViewControllerSegue"  {
//            if let viewController = segue.destination as? EmployeePopupViewController{
//                viewController.remoteManager = remoteManager
//                viewController.selectedEmployee = selectedEmployee
//                viewController.tabBar = tabBar
//                if(tabBar == nil){
//                    print("################### tabBar == nil 2" )
//                }
//            }
//        }
//    }
    
}

extension EmployeeListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return team.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmployeeListCell", for: indexPath) as! EmployeeListCell
        
        cell.employeeItemiew.label1.text = team[indexPath.row].FirstName! + " " + team[indexPath.row].LastName!
        cell.employeeItemiew.label2.text = team[indexPath.row].Job?.Value
        cell.employeeItemiew.label3.isHidden = false
        let dateString = team[indexPath.row].LatestPunch?.PunchDate //    "2018-10-08T17:01:00"
        
        cell.employeeItemiew.label3.text = dateString?.formatedDate(dateString: dateString!, fromFormat: Constants.DATE_UTC_SS, toFormat: Constants.DATE_FULL, timeZone: "UTC")
        
        if(team[indexPath.row].ProfileImage != nil && team[indexPath.row].ProfileImage!.Value != nil){
            avatarUrl = team[indexPath.row].ProfileImage!.Value!
        }
        avatarUrl = remoteManager!.baseCDNUrl + avatarUrl
        setImage(imageView: cell.employeeItemiew.img, imgUrl: avatarUrl, isCircle: true)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.goEmployeePopupViewController(teamMember : team[indexPath.row])
        
//        selectedEmployee = team[indexPath.row]
//        performSegue(withIdentifier: "EmployeePopupViewControllerSegue", sender: nil)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
