//
//  FirstComplaintViewController.swift
//  Manage
//
//  Created by Rong Li on 1/4/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class FirstComplaintViewController: KeyboardBaseViewController {
    static let currentPageIndex = 0
    
    var complaint: Complaint?

    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: FiveDotsView!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(refreshTokenNotificationName != nil){
            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
        }else{
            initViews()
            
            if(AssociateHelper.get()?.employeeId == nil){
                fetchAssociateInfoNotificationName = Notification.Name(rawValue: fetchAssocateInfoNotificationKey)
                NotificationCenter.default.addObserver(self, selector: #selector(fetchAssociateInfoCompletion(notification:)), name: fetchAssociateInfoNotificationName, object: nil)
            }
        }
    }
    
    @objc func refreshTokenCompletion(notification: NSNotification){
        initViews()
    }
    
    @objc func fetchAssociateInfoCompletion(notification: NSNotification){
        //it's ready to continue, no action is needed
    }
    
    fileprivate func initViews(){
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_complaints", comment: ""))
        
        let dots = [dotsView.dot1, dotsView.dot2, dotsView.dot3, dotsView.dot4, dotsView.dot5]
        initDotsView(dots: dots as! [UIButton], selectedIndex: FirstComplaintViewController.currentPageIndex)
        dotsView.onDotClicked = {
            (dotBtn) in
            self.onDotBtnClicked(dotBtn: dotBtn)
        }
        
        textView.delegate = self
        textView.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        textView.text = NSLocalizedString("key_description", comment: "")
        textView.textColor = UIColor.init(hexString: Constants.textDarkWhite)
        
        phoneTextField.delegate = self
        phoneTextField.setPadding(left: 8, right: 8)
        phoneTextField.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        dateTextField.setPadding(left: 8, right: 8)
        dateTextField.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        
        //calendarBtn: var calendarBtn  = UIButton(type: .custom)
        calendarBtn.frame = CGRect(x:0, y:0, width:30, height:30)
        calendarBtn.addTarget(self, action: #selector(self.popupDatePickerBtnClicked), for: .touchUpInside)
        calendarBtn.setImage(UIImage(named: "Calendar-icon.png"), for: .normal)
        calendarBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
        dateTextField.rightViewMode = .always
        dateTextField.rightView = calendarBtn
        
        dateTextField.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidHide, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidChangeFrame, object: nil)
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        let pageController = self.parent as! ComplaintPageViewController
//        pageController.setRotation(isRotated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    func isReadyMoveNext() -> Bool{
        if(!isViewLoaded){
            return false
        }
        if((selectedDate == nil || dateTextField.text?.isEmpty ?? true)
            || (textView.text?.isEmpty ?? true || (textView.text == NSLocalizedString("key_description", comment: "")))){
            return false
        }
//        if(complaint == nil){
//            let pageController = self.parent as! ComplaintPageViewController
//            pageController.setComplaint()
//        }
        
        let pageController = self.parent as! ComplaintPageViewController
        pageController.setSelectedDate(selectedDate: selectedDate!)
        
        complaint?.FeedbackDate = selectedDate?.formateDateUTC(date: selectedDate!, withFormat: Constants.DATE_UTC_SS)
        complaint?.Description = textView.text
        complaint?.customerPhoneNumber = phoneTextField.text
        
        return true
    }
    
    fileprivate func onDotBtnClicked(dotBtn: UIButton){
        let pageController = self.parent as! ComplaintPageViewController
        pageController.isMoveTo(currentPageIndex: FirstComplaintViewController.currentPageIndex,
                                dotsView: dotsView, dotBtn: dotBtn)
    }
    
//    fileprivate func onDotBtnClicked1(dotBtn: UIButton){
//        let pageController = self.parent as! ComplaintPageViewController
//        if(dotBtn == dotsView.dot1){
//            //do nothing
//        }else if (dotBtn == dotsView.dot2){
//            if(!pageController.isMoveTo(currentPageIndex: FirstComplaintViewController.currentPageIndex, destinationPageIndex: SecondComplaintViewController.currentPageIndex)){
//                //TODO: can not move to the destination page
//            }
//        }else if (dotBtn == dotsView.dot3){
//            if(!pageController.isMoveTo(currentPageIndex: FirstComplaintViewController.currentPageIndex, destinationPageIndex: ThirdComplaintViewController.currentPageIndex)){
//                //TODO: can not move to the destination page
//            }
//        }else if (dotBtn == dotsView.dot4){
//            if(!pageController.isMoveTo(currentPageIndex: FirstComplaintViewController.currentPageIndex, destinationPageIndex: FourthComplaintViewController.currentPageIndex)){
//                //TODO: can not move to the destination page
//            }
//        }else if (dotBtn == dotsView.dot5){
//            if(!pageController.isMoveTo(currentPageIndex: FirstComplaintViewController.currentPageIndex, destinationPageIndex: FifthComplaintViewController.currentPageIndex)){
//                //TODO: can not move to the destination page
//            }
//        }
//    }
    
    //web    DueDate: "2018-12-14T07:59:59.999Z"
    //mobile    DueDate: "2018-12-14T07:59:59"
    fileprivate func onSelectedDate(_ date: Date){
        selectedDate = date
        dateTextField.text = selectedDate!.formattedDateShort   
    }
    
    @objc func popupDatePickerBtnClicked(sender:UITapGestureRecognizer) {
        let storyboard =  UIStoryboard(name: "Popup", bundle: nil)
        let viewControler = storyboard.instantiateViewController(withIdentifier: "DatePickerPopupViewController") as! DatePickerPopupViewController
        viewControler.tag = DatePickerType.Date.rawValue
        viewControler.onSelectedDate = onSelectedDate
        present(viewControler, animated: true, completion: nil)
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        if(isReadyMoveNext()){
            let pageController = self.parent as! ComplaintPageViewController
            pageController.onNextBtnClicked(currentIndex: FirstComplaintViewController.currentPageIndex)
            return
        }
        showAlert(NSLocalizedString("key_infomation", comment: ""), NSLocalizedString("key_provide_input", comment: ""), NSLocalizedString("key_dismiss", comment: ""), viewController: self)
    }
    
    //Touch screen outside textfield to dismiss the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //UITextFieldDelegate Methods: disable keyboard
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if self.dateTextField == textField {
            return false; //do not show keyboard nor cursor
        }
        return true
    }
}
