//
//  ForthComplaintViewController.swift
//  Manage
//
//  Created by Rong Li on 1/4/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class FourthComplaintViewController: AddLabelKeyboardBaseViewController {
//    let editText = NSLocalizedString("key_edit", comment: "")
    let addEmployeeText = NSLocalizedString("key_add_employees", comment: "")
    let textViewText = NSLocalizedString("key_enter_root_cause", comment: "")

    static let currentPageIndex = 3
    
    var complaint: Complaint?
    var isClosed: Bool?
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: FiveDotsView!
    @IBOutlet weak var nameContainerView: UIView!
    @IBOutlet weak var assignToView: AssignToView!
    @IBOutlet weak var toggleView: UIView!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var dropDown: DropDown!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var yesBtn: UIButton!
    @IBOutlet weak var noBtn: UIButton!
    
    @IBOutlet weak var scrollContentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nameLabelViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nameContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nameContainerViewTop: NSLayoutConstraint!
    
    @IBOutlet weak var bottomViewTop: NSLayoutConstraint!
    
    var isPortrait: Bool = true
    var isDropDownOpen: Bool = false
    
    override func viewDidLoad() {
        self.assignTo = assignToView
        self.nameContainer = nameContainerView
        
        self.scrollContentHeight = scrollContentViewHeight
        self.nameLabelHeight = nameLabelViewHeight
        self.nameContainerHeight = nameContainerViewHeight
        self.nameContainerTop = nameContainerViewTop
        
        yOriginalPos = Int(nameContainerViewTop.constant + nameContainerViewHeight.constant)
        scrollContentViewOriginalHeight = scrollContentViewHeight.constant
        nameLabelViewOriginalHeight = nameLabelViewHeight.constant
        nameContainerViewOriginalHeight = nameContainerViewHeight.constant
        
        super.viewDidLoad()
        
        if(refreshTokenNotificationName != nil){
            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
        }else{
            initViews()
            
            if(AssociateHelper.get()?.employeeId == nil){
                fetchAssociateInfoNotificationName = Notification.Name(rawValue: fetchAssocateInfoNotificationKey)
                NotificationCenter.default.addObserver(self, selector: #selector(fetchAssociateInfoCompletion(notification:)), name: fetchAssociateInfoNotificationName, object: nil)
            }else{
                fetchPreventableStatus()
            }
        }
        
        //Listen for keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)
        
        isPortrait = getIsPortrait()
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidHide, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidChangeFrame, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        updateNameLabelView()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if(isDropDownOpen){
            isDropDownOpen = false
            dropDown.touchAction()
        }
    }
    
//    func updateNameLabelViewAfterRotate(){
//        perform(#selector(updateNameLabelView), with: nil, afterDelay: 1)
//    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        updateNameLabelView()
    }
    
//    func getIsPortrait() -> Bool{
//        return UIDevice.current.orientation.isLandscape == true ? false : true
//    }
    
    @objc func refreshTokenCompletion(notification: NSNotification){
        if(self.isTokenExpirationChanged){
            fetchPreventableStatus()
        }else{
            initViews()
        }
    }
    
    @objc func fetchAssociateInfoCompletion(notification: NSNotification){
        fetchPreventableStatus()
    }
    
    fileprivate func fetchPreventableStatus(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetcheTypes")
        }
        if let selectedProgramId = SelectedSiteHelper.get()?.selectedProgramId {
            remoteManager!.fetchPreventableStatus(remoteManager: remoteManager!, apiRouter: APIRouter.preventableStatuses, completion: fetchPreventableStatusCompletion)
        }else {
            //TODO: It should not get into here. if it did, go back to site picker to redo it ???
            // show popup and goSitePicker???
        }
    }
    
    fileprivate func fetchPreventableStatusCompletion(_ preventableStatus: NameKeyValueMappable?, _ error: Error?, _ responseCode: Int){
        if(preventableStatus != nil && preventableStatus?.Options != nil && (preventableStatus?.Options.count)! > 0){
            updateDropDown(nameKeyValue: preventableStatus!, dropDown: dropDown)
        }
    }
    
    fileprivate func initViews(){
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_complaints", comment: ""))
        
        let dots = [dotsView.dot1, dotsView.dot2, dotsView.dot3, dotsView.dot4, dotsView.dot5]
        initDotsView(dots: dots as! [UIButton], selectedIndex: FourthComplaintViewController.currentPageIndex)
        dotsView.onDotClicked = {
            (dotBtn) in
            self.onDotBtnClicked(dotBtn: dotBtn)
        }
        
        if(selectedEmployees.count == 0){
            updateEditBtn(title: addEmployeeText)
        }
        assignToView.onEditBtnClicked = {
            self.onEditBtnClicked()
        }
        assignToView.onToggleBtnClicked = {
            (toggleBtn) in
            self.onToggleBtnClicked(toggleBtn: toggleBtn)
        }
        
        textView.delegate = self
        textView.text = textViewText
        textView.textColor = UIColor.init(hexString: Constants.textDarkWhite)
        textView.layer.borderColor = UIColor.init(hexString: Constants.boarderColor).cgColor
        
        dropDown.layer.borderColor = UIColor.init(hexString: Constants.boarderColor).cgColor
        
        dateTextField.delegate = self
        dateTextField.setPadding(left: 8, right: 0)
        dateTextField.layer.borderColor = UIColor.init(hexString: Constants.boarderColor).cgColor
        //calendarBtn: var calendarBtn  = UIButton(type: .custom)
        calendarBtn.frame = CGRect(x:0, y:0, width:30, height:30)
        calendarBtn.addTarget(self, action: #selector(self.popupDatePickerBtnClicked), for: .touchUpInside)
        calendarBtn.setImage(UIImage(named: "Calendar-icon.png"), for: .normal)
        calendarBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
        dateTextField.rightViewMode = .always
        dateTextField.rightView = calendarBtn
 
        dropDown.setDropDown(rowBackgroundColor: Constants.dropDownRowBG, selectedRowColor: Constants.dropDownRowBG, rowTextColor: Constants.dropDownTextColor, rowHeight: 50);
        
        dropDown.didSelect{(selectedText , index , id) in
            print("Selected String: \(selectedText) \n index: \(index) \n Id: \(id)")
            self.isDropDownOpen = false
            self.complaint?.PreventableStatusId = id
            self.complaint?.PreventableStatusName = selectedText
        }
        
        let dropDownGesture = UITapGestureRecognizer(target: self, action:  #selector (self.dropDownAction(sender:)))
        dropDown.addGestureRecognizer(dropDownGesture)
        
    }
    
    @objc func dropDownAction(sender: UITapGestureRecognizer){
        isDropDownOpen = !isDropDownOpen
        dropDown.touchAction()
    }
    
    fileprivate func updateDropDown(nameKeyValue: NameKeyValueMappable, dropDown: DropDown){
        let keyValues = nameKeyValue.Options
        for keyValue in keyValues {
            dropDown.optionIds?.append(keyValue.Key)
            dropDown.optionArray.append(keyValue.Value!)
        }
    }
//    fileprivate func updateEditBtn(title: String){
//        if let attributedTitle = assignToView.editBtn.attributedTitle(for: .normal) {
//            let mutableAttributedTitle = NSMutableAttributedString(attributedString: attributedTitle)
//            mutableAttributedTitle.replaceCharacters(in: NSMakeRange(0, mutableAttributedTitle.length), with: title)
//            assignToView.editBtn.setAttributedTitle(mutableAttributedTitle, for: .normal)
//            
//            if(title == editText){
//                assignToView.toggleBtn.isHidden = false
//            }else{
//                assignToView.toggleBtn.isHidden = true
//            }
//        }
//    }
    
//    @objc override func updateNameLabelView(){
//        if(!isViewLoaded){
//            return
//        }
//        var title: String = ""
//        if let attributedTitle = assignToView.toggleBtn!.attributedTitle(for: .normal) {
//            let mutableAttributedTitle = NSMutableAttributedString(attributedString: attributedTitle)
//
//            if (assignToView.toggleBtn.tag == 1){
//                title = NSLocalizedString("key_see_less", comment: "")
//                updateNameContainerView(isShowAll: true)
//                nameLabelViewHeight.constant = nameLabelViewHeight.constant + CGFloat(yPos - yOriginalPos)
//                scrollContentViewHeight.constant = scrollContentViewHeight.constant + CGFloat(yPos - yOriginalPos)
//            }else{
//                title = NSLocalizedString("key_see_all", comment: "")
//                updateNameContainerView(isShowAll: false)
//                nameLabelViewHeight.constant = nameLabelViewOriginalHeight
//                scrollContentViewHeight.constant = scrollContentViewOriginalHeight
//            }
//
//            mutableAttributedTitle.replaceCharacters(in: NSMakeRange(0, mutableAttributedTitle.length), with: title)
//            assignToView.toggleBtn.setAttributedTitle(mutableAttributedTitle, for: .normal)
//        }
//    }
    
//    private func updateNameContainerView(isShowAll: Bool){
//        for label in nameContainerView.subviews {
//            label.removeFromSuperview()
//        }
//        nameContainerViewHeight.constant = nameContainerViewOriginalHeight
//        addNames(nameContainerView: nameContainerView, isShowAll: isShowAll)
//    }
    
    var pageController: ComplaintPageViewController?
    fileprivate func onDotBtnClicked(dotBtn: UIButton){
        if(pageController == nil){
        pageController = self.parent as! ComplaintPageViewController
        }
        pageController!.isMoveTo(currentPageIndex: FourthComplaintViewController.currentPageIndex,
                                dotsView: dotsView, dotBtn: dotBtn)
    }
    
    func isReadyMoveNext() -> Bool{
        if(!isViewLoaded){
            return false
        }
        
        isClosed = (yesBtn.tag == 1 ? true : false)
        if(pageController == nil){
        pageController = self.parent as! ComplaintPageViewController
        }
        pageController!.setIsClosed(isClosed: isClosed ?? true)

        if(isClosed ?? true){
            if (selectedEmployees.count == 0
                || complaint?.PreventableStatusName == nil
                || dateTextField.text?.isEmpty ?? false
                || (textView.text?.isEmpty ?? false || (textView.text == NSLocalizedString("key_enter_root_cause", comment: "")))){
                return false
            }
            complaint?.CompletedDate = selectedDate?.formateDateUTC(date: selectedDate!, withFormat: Constants.DATE_UTC_SS)
            complaint?.Action = textView.text
            
            complaint?.AccountableEmployees.removeAll()
            for employee in selectedEmployees {
                complaint?.AccountableEmployees.append(AccountableEmployee(employeeId: employee.EmployeeId!, isExempt: false))
            }
        }
        
        return true
    }
    
    fileprivate func onSelectedDate(_ date: Date){
        selectedDate = date
        dateTextField.text = selectedDate!.formattedDateShort
    }
    
    @objc func popupDatePickerBtnClicked(sender:UITapGestureRecognizer) {
        let storyboard =  UIStoryboard(name: "Popup", bundle: nil)
        let viewControler = storyboard.instantiateViewController(withIdentifier: "DatePickerPopupViewController") as! DatePickerPopupViewController
        viewControler.tag = DatePickerType.Date.rawValue
        viewControler.onSelectedDate = onSelectedDate
        present(viewControler, animated: true, completion: nil)
    }
    
    fileprivate func onToggleBtnClicked(toggleBtn: UIButton) {
        updateNameLabelView()
    }
    
    fileprivate func updateYesNoBtn(sender: UIButton, btn: UIButton, isHiddenToggleView: Bool){
        toggleView.isHidden = isHiddenToggleView
        
        sender.tag = 1
        sender.setTitleColor(UIColor.init(hexString: Constants.btnTextBlue), for: .normal)
        sender.backgroundColor = UIColor.init(hexString: Constants.btnDarkGrayBG)
        
        btn.tag = 0
        btn.setTitleColor(UIColor.init(hexString: Constants.btnTextWhite), for: .normal)
        btn.backgroundColor = .clear
    }
    
    fileprivate func onConfirmBtnClicked(_ selectedTeam: [TeamMemberV2]) {
        selectedEmployees = selectedTeam
        updateNameContainerView(isShowAll: false)
        updateEditBtn(title: editText)
    }
    
    fileprivate func onEditBtnClicked() {
        self.popupSelectEmployeeViewController(featureName: FeatureName.Complaint)
    }
    
    fileprivate func popupSelectEmployeeViewController(featureName: FeatureName){
        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SelectEmployeeViewController") as! SelectEmployeeViewController
        viewController.remoteManager = remoteManager
        viewController.featureName = featureName
        viewController.selectedEmployees = selectedEmployees
        viewController.onConfirmBtnClicked = onConfirmBtnClicked
        
        viewController.modalTransitionStyle = .crossDissolve
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: true, completion: nil)
    }
    @IBAction func yesBtnClicked(_ sender: UIButton) {
        updateYesNoBtn(sender: sender, btn: noBtn, isHiddenToggleView: false)
    }
    
    @IBAction func noBtnClicked(_ sender: UIButton) {
        updateYesNoBtn(sender: sender, btn: yesBtn, isHiddenToggleView: true)
    }

    @IBAction func backBtnClicked(_ sender: Any) {
        if(pageController == nil){
            pageController = self.parent as! ComplaintPageViewController
        }
        pageController!.onBackBtnClicked(currentIndex: FourthComplaintViewController.currentPageIndex)
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        if(isReadyMoveNext()){
            let pageController = self.parent as! ComplaintPageViewController
            pageController.onNextBtnClicked(currentIndex: FourthComplaintViewController.currentPageIndex)
            return
        }
        showAlert(NSLocalizedString("key_infomation", comment: ""), NSLocalizedString("key_provide_input", comment: ""), NSLocalizedString("key_dismiss", comment: ""), viewController: self)
    }

    //Touch screen outside textfield to dismiss the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.view.endEditing(true)
        hideKeyboard(hideKeyboardType:HideKeyboardType.ScreenTounch, textView: textView)
    }
    
    //UITextFieldDelegate Methods: disable keyboard
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if self.dateTextField == textField {
            return false; //do not show keyboard nor cursor
        }
        return true
    }
}
