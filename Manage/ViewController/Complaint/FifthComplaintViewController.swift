//
//  FifthtComplaintViewController.swift
//  Manage
//
//  Created by Rong Li on 1/4/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class TwoLableCell: UICollectionViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var content: UILabel!
}


class FifthComplaintViewController: BaseViewController {
    
    static let currentPageIndex = 4
    
    let minSpacing:CGFloat = 5
    let marging:CGFloat = 10
    let cellHeight:CGFloat = 40
    
    var complaint: Complaint?
    var isClosed: Bool?
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: FiveDotsView!
    @IBOutlet weak var descriptionView: DescriptionView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateViews()
    }

    
    fileprivate func initViews(){
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_complaints", comment: ""))
        
        let dots = [dotsView.dot1, dotsView.dot2, dotsView.dot3, dotsView.dot4, dotsView.dot5]
        initDotsView(dots: dots as! [UIButton], selectedIndex: FifthComplaintViewController.currentPageIndex)
        dotsView.onDotClicked = {
            (dotBtn) in
            self.onDotBtnClicked(dotBtn: dotBtn)
        }
        
        descriptionView.content.text = complaint?.Description
        
        //configure collectionview layout
        configureCollectionViewLayour()
        
        scrollView.setContentOffset(.zero, animated: false)
        
    }
    
    fileprivate func updateViews(){
        descriptionView.content.text = complaint?.Description
        collectionView.reloadData()
    }
    
    
//    func reloadViewContent(){
//        if(!isViewLoaded){
//            return
//        }
//        descriptionView.content.text = complaint?.Description
//        collectionView.reloadData()
//    }

    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
//        let pageController = self.parent as! ComplaintPageViewController
//        pageController.setRotation(isRotated: true)
        
        configureCollectionViewLayour()
    }

    fileprivate func configureCollectionViewLayour(){
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //.horizontal
        layout.minimumLineSpacing = minSpacing
        layout.minimumInteritemSpacing = minSpacing
        collectionView.setCollectionViewLayout(layout, animated: true)
    }
    
    fileprivate func submitComplaint(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on submitAReportIt")
        }
        
        complaint?.BuildingId
        
        remoteManager!.submitItem(remoteManager: remoteManager!, item: complaint, apiRouter: APIRouter.complaint, completion: submitCompletion)
    }
    
    fileprivate func onDotBtnClicked(dotBtn: UIButton){
        let pageController = self.parent as! ComplaintPageViewController
        pageController.isMoveTo(currentPageIndex: FifthComplaintViewController.currentPageIndex,
                                dotsView: dotsView, dotBtn: dotBtn)
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        let pageController = self.parent as! ComplaintPageViewController
        pageController.onBackBtnClicked(currentIndex: FifthComplaintViewController.currentPageIndex)
    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        if(!(isClosed ?? true)){
            complaint?.AccountableEmployees.removeAll()
            complaint?.CompletedDate = ""
            complaint?.Action = ""
            complaint?.PreventableStatusId = nil
        }
        
        complaint?.Action = "rli test"
        complaint?.BuildingId = 15688
        complaint?.ClassificationId = 2
        complaint?.ComplaintTypeId = 2
        complaint?.CompletedDate = "2019-01-11T23:55:54"
        complaint?.CustomerName = "rli"
        complaint?.Description = "rli test"
        complaint?.FeedbackDate = "2019-01-11T23:55:54"
        complaint?.FloorId = 35632
        complaint?.IsRepeatComplaint = false
        complaint?.Note = "rli test"
        complaint?.PreventableStatusId = 2
        
        submitComplaint()
    }
}


extension FifthComplaintViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 14
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TwoLableCell", for: indexPath) as! TwoLableCell
        cell.title.textColor = UIColor.init(hexString: Constants.textWhite)
        cell.content.textColor = UIColor.init(hexString: Constants.textDarkWhite)
        cell.title.text = "Title \(indexPath.row)"
        cell.content.text = "Content \(indexPath.row)"
        
        switch indexPath.row {
        case 0:
            cell.title.text = NSLocalizedString("key_date", comment: "")
            cell.content.text = selectedDate?.formattedDateShort
            break
        case 1:
            cell.title.text = NSLocalizedString("key_phone", comment: "")
            cell.content.text = complaint?.customerPhoneNumber
            break
        case 2:
            cell.title.text = NSLocalizedString("key_building", comment: "")
            cell.content.text = complaint?.BuildingName
            break
        case 3:
            cell.title.text = NSLocalizedString("key_floor", comment: "")
            cell.content.text = complaint?.FloorName
            break
        case 4:
            cell.title.text = NSLocalizedString("key_customer", comment: "")
            cell.content.text = complaint?.CustomerName
            break
        case 5:
            cell.title.text = ""
            cell.content.text = ""
            break
        case 6:
            cell.title.text = NSLocalizedString("key_classification", comment: "")
            cell.content.text = complaint?.ClassificationName
            break
        case 7:
            cell.title.text = NSLocalizedString("key_type", comment: "")
            cell.content.text = complaint?.ComplaintTypeName
            break
        case 8:
            cell.title.text = NSLocalizedString("key_repeat", comment: "")
            cell.content.text = complaint?.IsRepeatComplaint == true ? NSLocalizedString("key_yes", comment: "") : NSLocalizedString("key_no", comment: "")
            break
        case 9:
            cell.title.text = ""
            cell.content.text = ""
            break
        case 10:
            cell.title.text = NSLocalizedString("key_notes", comment: "")
            cell.content.text = complaint?.Note
            break
        case 11:
            cell.title.text = ""
            cell.content.text = ""
            break
        case 12:
            cell.title.text = NSLocalizedString("key_complaint_closed", comment: "")
            cell.content.text = (isClosed ?? true) ? NSLocalizedString("key_yes", comment: "") : NSLocalizedString("key_no", comment: "")
            break
        case 13:
            cell.title.text = ""
            cell.content.text = ""
            break
            
        default:
            break
        }
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: marging, left: marging, bottom: marging, right: marging)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  marging * 2 + minSpacing
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2, height: cellHeight)
    }
    
}
