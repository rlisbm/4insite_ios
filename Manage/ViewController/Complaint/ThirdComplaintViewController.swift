//
//  ThirdComplaintViewController.swift
//  Manage
//
//  Created by Rong Li on 1/4/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

//class ThirdComplaintViewController: KeyboardBaseViewController, UITextFieldDelegate, UITextViewDelegate {
class ThirdComplaintViewController: KeyboardBaseViewController {
    let typeText = NSLocalizedString("key_type", comment: "")
    let classificationText = NSLocalizedString("key_classification", comment: "")
    let repeatText = NSLocalizedString("key_repeat", comment: "")
    let textViewText = NSLocalizedString("key_enter_cause", comment: "")
    
    static let currentPageIndex = 2
    
    var complaint: Complaint?
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: FiveDotsView!
    @IBOutlet weak var typeDropDown: DropDown!
    @IBOutlet weak var classificationDropDown: DropDown!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var repeatDropDown: DropDown!
    
    var isTypeOpen: Bool = false
    var isClassificationOpen: Bool = false
    var isRepeatOpen: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(refreshTokenNotificationName != nil){
            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
        }else{
            initViews()

            if(AssociateHelper.get()?.employeeId == nil){
                fetchAssociateInfoNotificationName = Notification.Name(rawValue: fetchAssocateInfoNotificationKey)
                NotificationCenter.default.addObserver(self, selector: #selector(fetchAssociateInfoCompletion(notification:)), name: fetchAssociateInfoNotificationName, object: nil)
            }else{
                fetchComplaintTypes()
            }
        }
        
        //Listen for keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidHide, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidChangeFrame, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if(isTypeOpen){
            isTypeOpen = false
            typeDropDown.touchAction()
        }else if (isClassificationOpen){
            isClassificationOpen = false
            classificationDropDown.touchAction()
        }
    }
    
    fileprivate func initViews(){
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_complaints", comment: ""))

        let dots = [dotsView.dot1, dotsView.dot2, dotsView.dot3, dotsView.dot4, dotsView.dot5]
        initDotsView(dots: dots as! [UIButton], selectedIndex: ThirdComplaintViewController.currentPageIndex)
        dotsView.onDotClicked = {
            (dotBtn) in
            self.onDotBtnClicked(dotBtn: dotBtn)
        }

        textView.delegate = self
        textView.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        textView.text = textViewText
        textView.textColor = UIColor.init(hexString: Constants.textDarkWhite)

        typeDropDown.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        typeDropDown.setDropDown(rowBackgroundColor: Constants.dropDownRowBG, selectedRowColor: Constants.dropDownRowBG, rowTextColor: Constants.dropDownTextColor, rowHeight: 50);
        typeDropDown.isEnabled = false
        
        classificationDropDown.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        classificationDropDown.setDropDown(rowBackgroundColor: Constants.dropDownRowBG, selectedRowColor: Constants.dropDownRowBG, rowTextColor: Constants.dropDownTextColor, rowHeight: 50);
        classificationDropDown.isEnabled = false
        
        repeatDropDown.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        repeatDropDown.setDropDown(rowBackgroundColor: Constants.dropDownRowBG, selectedRowColor: Constants.dropDownRowBG, rowTextColor: Constants.dropDownTextColor, rowHeight: 50);

        setRepeatDropDown()
        
        typeDropDown.didSelect{(selectedText , index , id) in
            self.isTypeOpen = false
            self.complaint?.ComplaintTypeId = id
            self.complaint?.ComplaintTypeName = selectedText
        }
        
        classificationDropDown.didSelect{(selectedText , index , id) in
            self.isClassificationOpen = false
            self.complaint?.ClassificationId = id
            self.complaint?.ClassificationName = selectedText
        }
        
        repeatDropDown.didSelect{(selectedText , index , id) in
            self.isRepeatOpen = false
            self.complaint?.IsRepeatComplaint = (id == 0) ? false : true
        }
        
        let typeGesture = UITapGestureRecognizer(target: self, action:  #selector (self.typeDropDownAction(sender:)))
        typeDropDown.addGestureRecognizer(typeGesture)
        
        let classificationGesture = UITapGestureRecognizer(target: self, action:  #selector (self.classificationDropDownAction(sender:)))
        classificationDropDown.addGestureRecognizer(classificationGesture)
        
        let repeatGesture = UITapGestureRecognizer(target: self, action:  #selector (self.repeatDropDownAction(sender:)))
        repeatDropDown.addGestureRecognizer(repeatGesture)
    }
    
    @objc func typeDropDownAction(sender: UITapGestureRecognizer){
        if(isClassificationOpen){
            isClassificationOpen = !isClassificationOpen
            classificationDropDown.touchAction()
            return
        }
        
        if(isRepeatOpen){
            isRepeatOpen = !isRepeatOpen
            repeatDropDown.touchAction()
            return
        }
        
        isTypeOpen = !isTypeOpen
        typeDropDown.touchAction()
    }
    
    @objc func classificationDropDownAction(sender: UITapGestureRecognizer){
        if(isTypeOpen){
            isTypeOpen = !isTypeOpen
            typeDropDown.touchAction()
            return
        }
        
        if(isRepeatOpen){
            isRepeatOpen = !isRepeatOpen
            repeatDropDown.touchAction()
            return
        }
        
        isClassificationOpen = !isClassificationOpen
        classificationDropDown.touchAction()
    }
    
    @objc func repeatDropDownAction(sender: UITapGestureRecognizer){
        if(isTypeOpen){
            isTypeOpen = !isTypeOpen
            typeDropDown.touchAction()
            return
        }
        if(isClassificationOpen){
            isClassificationOpen = !isClassificationOpen
            classificationDropDown.touchAction()
            return
        }
        
        isRepeatOpen = !isRepeatOpen
        repeatDropDown.touchAction()
    }
    
    fileprivate func setRepeatDropDown(){
        repeatDropDown.text = repeatText
        repeatDropDown.optionIds?.append(1)
        repeatDropDown.optionArray.append(NSLocalizedString("key_yes", comment: ""))
        repeatDropDown.optionIds?.append(0)
        repeatDropDown.optionArray.append(NSLocalizedString("key_no", comment: ""))
    }
    fileprivate func updateDropDown(nameKeyValue: NameKeyValueMappable, text: String, dropDown: DropDown){
        dropDown.isEnabled = true
        dropDown.text = text
        let keyValues = nameKeyValue.Options
        for keyValue in keyValues {
            dropDown.optionIds?.append(keyValue.Key)
            dropDown.optionArray.append(keyValue.Value!)
        }
    }

    fileprivate func onDotBtnClicked(dotBtn: UIButton){
        let pageController = self.parent as! ComplaintPageViewController
        pageController.isMoveTo(currentPageIndex: ThirdComplaintViewController.currentPageIndex,
                                dotsView: dotsView, dotBtn: dotBtn)
    }
    
    func isReadyMoveNext() -> Bool{
        if(!isViewLoaded){
            return false
        }
        if (complaint?.ComplaintTypeName == nil || complaint?.ClassificationName == nil || complaint?.IsRepeatComplaint == nil
            || (textView.text?.isEmpty ?? false || textView.text == textViewText)){
            return false
        }
        complaint?.Note = textView.text
        return true
    }

    fileprivate func fetchComplaintTypes(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetcheTypes")
        }
        if let selectedProgramId = SelectedSiteHelper.get()?.selectedProgramId {
            remoteManager!.fetchComplaintTypes(remoteManager: remoteManager!, selectedProgramId: selectedProgramId, apiRouter: APIRouter.complimentTypes, completion: fetchComplaintTypesCompletion)
        }else {
            //TODO: It should not get into here. if it did, go back to site picker to redo it ???
            // show popup and goSitePicker???
        }
    }

    fileprivate func fetchComplaintTypesCompletion(_ complaintTypes: NameKeyValueMappable?, _ error: Error?, _ responseCode: Int){
        if(complaintTypes != nil && complaintTypes?.Options != nil && (complaintTypes?.Options.count)! > 0){
            updateDropDown(nameKeyValue: complaintTypes!, text: typeText, dropDown: typeDropDown)
        }
        fetchClassifications()
    }

    fileprivate func fetchClassifications(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetcheTypes")
        }
        if let selectedProgramId = SelectedSiteHelper.get()?.selectedProgramId {
            remoteManager!.fetchClassifications(remoteManager: remoteManager!, apiRouter: APIRouter.classifications, completion: fetchClassificationsCompletion)
        }else {
            //TODO: It should not get into here. if it did, go back to site picker to redo it ???
            // show popup and goSitePicker???
        }
    }

    fileprivate func fetchClassificationsCompletion(_ classifications: NameKeyValueMappable?, _ error: Error?, _ responseCode: Int){
        if(classifications != nil && classifications?.Options != nil && (classifications?.Options.count)! > 0){
            updateDropDown(nameKeyValue: classifications!, text: classificationText, dropDown: classificationDropDown)
        }
    }

    @objc func refreshTokenCompletion(notification: NSNotification){
        if(self.isTokenExpirationChanged){
            fetchComplaintTypes()
        }else{
            initViews()
        }
    }

    @objc func fetchAssociateInfoCompletion(notification: NSNotification){
        fetchComplaintTypes()
    }

    @IBAction func backBtnClicked(_ sender: Any) {
        let pageController = self.parent as! ComplaintPageViewController
        pageController.onBackBtnClicked(currentIndex: ThirdComplaintViewController.currentPageIndex)
    }

    @IBAction func nextBtnClicked(_ sender: Any) {
        if(isReadyMoveNext()){
            let pageController = self.parent as! ComplaintPageViewController
            pageController.onNextBtnClicked(currentIndex: ThirdComplaintViewController.currentPageIndex)
            return
        }
        showAlert(NSLocalizedString("key_infomation", comment: ""), NSLocalizedString("key_provide_input", comment: ""), NSLocalizedString("key_dismiss", comment: ""), viewController: self)
    }
    
//    //UITextFieldDelegate Methods
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        self.view.endEditing(true)
//        return true
//    }
    
    //Touch screen outside textfield to dismiss the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.view.endEditing(true)
        hideKeyboard(hideKeyboardType:HideKeyboardType.ScreenTounch, textView: textView)
    }
}
