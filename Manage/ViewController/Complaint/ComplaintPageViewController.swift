//
//  ComplaintPageViewController.swift
//  Manage
//
//  Created by Rong Li on 1/4/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class ComplaintPageViewController: BasePageViewController {

    var VC1 : FirstComplaintViewController?
    var VC2 : SecondComplaintViewController?
    var VC3 : ThirdComplaintViewController?
    var VC4 : FourthComplaintViewController?
    var VC5 : FifthComplaintViewController?
    
    var complaint: Complaint = Complaint()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        self.dataSource = self
        
        VC1 = storyboard?.instantiateViewController(withIdentifier: "FirstComplaintViewController") as? FirstComplaintViewController
        VC2 = storyboard?.instantiateViewController(withIdentifier: "SecondComplaintViewController") as? SecondComplaintViewController
        VC3 = storyboard?.instantiateViewController(withIdentifier: "ThirdComplaintViewController") as? ThirdComplaintViewController
        VC4 = storyboard?.instantiateViewController(withIdentifier: "FourthComplaintViewController") as? FourthComplaintViewController
        VC5 = storyboard?.instantiateViewController(withIdentifier: "FifthComplaintViewController") as? FifthComplaintViewController
        
        VCs.append(VC1!)
        VCs.append(VC2!)
        VCs.append(VC3!)
        VCs.append(VC4!)
        VCs.append(VC5!)
        
        VC1?.remoteManager = remoteManager
        VC2?.remoteManager = remoteManager
        VC3?.remoteManager = remoteManager
        VC4?.remoteManager = remoteManager
        VC5?.remoteManager = remoteManager
        
        VC1?.complaint = complaint
        VC2?.complaint = complaint
        VC3?.complaint = complaint
        VC4?.complaint = complaint
        VC5?.complaint = complaint
        
        setViewControllers([VCs[0]], direction: .forward, animated: true, completion: nil)
    }
//    func setComplaint(){
//        VC1?.complaint = complaint
//    }
    func setSelectedDate(selectedDate: Date){
        VC5?.selectedDate = selectedDate
    }
    
    func setIsClosed(isClosed: Bool){
        VC5?.isClosed = isClosed
    }

    func isReadyMoveNext(currentIndex: Int) -> Bool {
        switch currentIndex {
        case 0:
            return VC1?.isReadyMoveNext() ?? false
        case 1:
            return VC2?.isReadyMoveNext() ?? false
        case 2:
            return VC3?.isReadyMoveNext() ?? false
        case 3:
            return VC4?.isReadyMoveNext() ?? false
        default:
            return false
        }
        
        return true
    }
    
    fileprivate func moveToDestination(destinationPageIndex: Int, direction: UIPageViewControllerNavigationDirection){
        setViewControllers([VCs[destinationPageIndex]], direction: direction, animated: true, completion: nil)
    }
    
    func isMoveTo(currentPageIndex: Int, dotsView: FiveDotsView, dotBtn: UIButton){
        if(dotBtn == dotsView.dot1){
            if(currentPageIndex != FirstComplaintViewController.currentPageIndex){
                moveToDestination(destinationPageIndex: FirstComplaintViewController.currentPageIndex, direction: .reverse)
            }
        }else if (dotBtn == dotsView.dot2){
            if(currentPageIndex < SecondComplaintViewController.currentPageIndex){
                if(isMoveTo(currentPageIndex: SecondComplaintViewController.currentPageIndex, destinationPageIndex: SecondComplaintViewController.currentPageIndex)){
                    moveToDestination(destinationPageIndex: SecondComplaintViewController.currentPageIndex, direction: .forward)
                }
            }else{
                moveToDestination(destinationPageIndex: SecondComplaintViewController.currentPageIndex, direction: .reverse)
            }
        }else if (dotBtn == dotsView.dot3){
            if(currentPageIndex < ThirdComplaintViewController.currentPageIndex){
                if(isMoveTo(currentPageIndex: ThirdComplaintViewController.currentPageIndex, destinationPageIndex: ThirdComplaintViewController.currentPageIndex)){
                    moveToDestination(destinationPageIndex: ThirdComplaintViewController.currentPageIndex, direction: .forward)
                }
            }else{
                moveToDestination(destinationPageIndex: ThirdComplaintViewController.currentPageIndex, direction: .reverse)
            }
        }else if (dotBtn == dotsView.dot4){
            if(currentPageIndex < FourthComplaintViewController.currentPageIndex){
                if(isMoveTo(currentPageIndex: FourthComplaintViewController.currentPageIndex, destinationPageIndex: FourthComplaintViewController.currentPageIndex)){
                    moveToDestination(destinationPageIndex: FourthComplaintViewController.currentPageIndex, direction: .forward)
                }
            }else{
                moveToDestination(destinationPageIndex: FourthComplaintViewController.currentPageIndex, direction: .reverse)
            }
        }else if (dotBtn == dotsView.dot5){
            if(currentPageIndex < FifthComplaintViewController.currentPageIndex){
                if(isMoveTo(currentPageIndex: FifthComplaintViewController.currentPageIndex, destinationPageIndex: FifthComplaintViewController.currentPageIndex)){
                    moveToDestination(destinationPageIndex: FifthComplaintViewController.currentPageIndex, direction: .forward)
                }
            }else{
                moveToDestination(destinationPageIndex: FifthComplaintViewController.currentPageIndex, direction: .reverse)
            }
        }
    }
    
    func isMoveTo(currentPageIndex: Int, destinationPageIndex: Int) -> Bool{
        for currentIndex in currentPageIndex...destinationPageIndex {
            switch currentIndex {
            case 0:
                if( !(VC1?.isReadyMoveNext() ?? false) ){
                    return false
                }
                break
            case 1:
                if( !(VC2?.isReadyMoveNext() ?? false) ){
                    return false
                }
                break
            case 2:
                if( !(VC3?.isReadyMoveNext() ?? false) ){
                    return false
                }
                break
            case 3:
                if( !(VC4?.isReadyMoveNext() ?? false) ){
                    return false
                }
                break
            default:
                break
            }
        }
        
        return true
    }

    //MARK: UIPageViewControllerDataSource, UIPageViewControllerDelegate
    override func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = VCs.index(of: viewController) ?? 0
        
        if(!isReadyMoveNext(currentIndex: currentIndex) || currentIndex >= VCs.count - 1){
            return nil
        }
        
        return VCs[currentIndex + 1]
    }
    
    override func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = VCs.index(of: viewController) ?? 0
        if(currentIndex <= 0){
            return nil
        }

        return VCs[currentIndex - 1]
    }
}
