//
//  SecondComplaintViewController.swift
//  Manage
//
//  Created by Rong Li on 1/4/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit
import RealmSwift

class SecondComplaintViewController: BaseViewController, UITextFieldDelegate {
//class SecondComplaintViewController: BuildingBaseViewController, UITextFieldDelegate {

    static let currentPageIndex = 1
    
    var complaint: Complaint?
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var dotsView: FiveDotsView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var buildingDropdown: DropDown!
    @IBOutlet weak var floorDropdown: DropDown!
    
    var names = [String]()
    var ids = [Int]()

    var buildingList = List<KeyValueMappable>()
    var floorDic: Dictionary = [Int:[Floor]]()
    var floorIdDic: Dictionary = [Int:[Int]]()
    var floorNameDic: Dictionary = [Int:[String]]()

    var selectedBuilding: KeyValueMappable?
    var selectedFloor: KeyValueMappable?

    var isBuildingOpen: Bool = false
    var isFloorOpen: Bool = false
    var fetchIndex = 0;
    
    override func viewDidLoad() {
//        self.buildingDropDown = buildingDropdown
//        self.floorDropDown = floorDropdown
        
        super.viewDidLoad()
        
        if(refreshTokenNotificationName != nil){
            NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
        }else{
            initViews()
            
            if(AssociateHelper.get()?.employeeId == nil){
                fetchAssociateInfoNotificationName = Notification.Name(rawValue: fetchAssocateInfoNotificationKey)
                NotificationCenter.default.addObserver(self, selector: #selector(fetchAssociateInfoCompletion(notification:)), name: fetchAssociateInfoNotificationName, object: nil)
            }else{
                fetchBuildings()
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
    }
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if(isBuildingOpen){
            isBuildingOpen = false
            buildingDropdown.touchAction()
        }else if (isFloorOpen){
            isFloorOpen = false
            floorDropdown.touchAction()
        }
    }
    
    fileprivate func initViews(){
        initHeaderView(headerView: headerView, screenName: NSLocalizedString("key_complaints", comment: ""))
        
        let dots = [dotsView.dot1, dotsView.dot2, dotsView.dot3, dotsView.dot4, dotsView.dot5]
        initDotsView(dots: dots as! [UIButton], selectedIndex: SecondComplaintViewController.currentPageIndex)
        dotsView.onDotClicked = {
            (dotBtn) in
            self.onDotBtnClicked(dotBtn: dotBtn)
        }
        
        textField.delegate = self
        textField.setPadding(left: 8, right: 8)
        textField.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        
        buildingDropdown.setDropDown(rowBackgroundColor: Constants.dropDownRowBG, selectedRowColor: Constants.dropDownRowBG, rowTextColor: Constants.dropDownTextColor, rowHeight: 50);
        buildingDropdown.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
        floorDropdown.setDropDown(rowBackgroundColor: Constants.dropDownRowBG, selectedRowColor: Constants.dropDownRowBG, rowTextColor: Constants.dropDownTextColor, rowHeight: 50);
        floorDropdown.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor

        buildingDropdown.didSelect{(selectedText , index , id) in
            print("Selected String: \(selectedText) \n index: \(index) \n Id: \(id)")
            self.selectedBuilding = KeyValueMappable(Key: id, Value: selectedText)
            self.isBuildingOpen = false
            self.updateFloorDropDown(selectedBuildingId: id, isResetTitle: true)
            self.complaint?.BuildingId = id
            self.complaint?.BuildingName = selectedText
        }

        floorDropdown.didSelect{(selectedText , index , id) in
            print("Selected String: \(selectedText) \n index: \(index) \n Id: \(id)")
            self.selectedFloor = KeyValueMappable(Key: id, Value: selectedText)
            self.isFloorOpen = false
            self.complaint?.FloorId = id
            self.complaint?.FloorName = selectedText
        }

        let buildingGesture = UITapGestureRecognizer(target: self, action:  #selector (self.buildingDropDownAction(sender:)))
        buildingDropdown.addGestureRecognizer(buildingGesture)
        let floorGesture = UITapGestureRecognizer(target: self, action:  #selector (self.floorDropDownAction(sender:)))
        floorDropdown.addGestureRecognizer(floorGesture)
    }
    @objc func buildingDropDownAction(sender: UITapGestureRecognizer){
        if(isFloorOpen){
            isFloorOpen = !isFloorOpen
            floorDropdown.touchAction()
            return
        }

        isBuildingOpen = !isBuildingOpen
        buildingDropdown.touchAction()
    }

    @objc func floorDropDownAction(sender: UITapGestureRecognizer){
        if(isBuildingOpen){
            isBuildingOpen = !isBuildingOpen
            buildingDropdown.touchAction()
            return
        }

        isFloorOpen = !isFloorOpen
        floorDropdown.touchAction()
    }

    func updateBuildingDropDown(_ buildings: NameKeyValueMappable){
        buildingDropdown.text = buildingText
        buildingDropdown.optionArray = names
        buildingDropdown.optionIds = ids
    }

    func updateFloorDropDown(selectedBuildingId: Int, isResetTitle: Bool){
        floorDropdown.text = floorText
        floorDropdown.selectedIndex = nil
        if(floorNameDic[selectedBuildingId] != nil && floorNameDic[selectedBuildingId]!.count > 0){
            floorDropdown.optionArray = floorNameDic[selectedBuildingId]!
            floorDropdown.optionIds = floorIdDic[selectedBuildingId]
        }else{
            floorDropdown.optionArray = []
            floorDropdown.optionIds = []
        }

        if(!isResetTitle){
            //TODO
        }

    }
    fileprivate func onDotBtnClicked(dotBtn: UIButton){
        let pageController = self.parent as! ComplaintPageViewController
        pageController.isMoveTo(currentPageIndex: SecondComplaintViewController.currentPageIndex,
                                dotsView: dotsView, dotBtn: dotBtn)
    }
    func isReadyMoveNext() -> Bool{
        if(!isViewLoaded){
            return false
        }
        if(textField.text?.isEmpty ?? true
            || complaint?.BuildingName == nil
            || complaint?.FloorName == nil){
            return false
        }
        complaint?.CustomerName = textField.text
        return true
    }
    
    func fetchBuildings(){
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetche Buildings")
        }

        remoteManager!.fetchBuildings(remoteManager: remoteManager!, completion: fetchBuildingsCompletion)

    }
    
    fileprivate func fetchBuildingsCompletion(_ buildings: NameKeyValueMappable?, _ error: Error?, _ responseCode: Int){
        if(buildings != nil && buildings?.Options != nil && (buildings?.Options.count)! > 0){
            buildingList = (buildings?.Options)!
            for keyValue in buildingList {
                ids.append(keyValue.Key)
                names.append(keyValue.Value!)
            }
            updateBuildingDropDown(buildings!)

            fetchIndex = 0;
            fetchBuildingFloors(buildingId: buildingList[0].Key)
        }else{
            //TODO: Handle Error here
            if(responseCode == 401){
                //TODO: it could be token expired. This is not likely to happen, because it's validated in viewDidLoad().
                //only if expiration duration has been changed on server side
                refreshTokenNotificationName = Notification.Name(rawValue: refreshTokenNotificationKey)
                NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenCompletion(notification:)), name: refreshTokenNotificationName, object: nil)
                refreshToken()
            }
        }
    }
    
    fileprivate func fetchBuildingFloors(buildingId: Int) {
        if(remoteManager == nil){
            fatalError("Missing dependencies on fetche Floors")
        }
        remoteManager!.fetchBuildingFloors(remoteManager: remoteManager!, buildingId: buildingId, completion: fetchBuildingFloorsCompletion)
    }
    
    func fetchBuildingFloorsCompletion(_ floors: NameKeyValueMappable?, _ error: Error?, _ responseCode: Int){
        if(floors != nil && floors?.Options != nil && (floors?.Options.count)! > 0){
            ids.removeAll()
            names.removeAll()
            
            for keyValue in (floors?.Options)! {
                ids.append(keyValue.Key)
                names.append(keyValue.Value!)
            }
            floorIdDic[buildingDropdown.optionIds![fetchIndex]] = ids
            floorNameDic[buildingDropdown.optionIds![fetchIndex]] = names
            
            fetchIndex += 1
            if(fetchIndex < buildingDropdown.optionIds!.count){
                fetchBuildingFloors(buildingId: buildingDropdown.optionIds![fetchIndex])
            }else{
                floorDropdown.text = floorText
            }
        }else{
            
            fetchIndex += 1
            if(fetchIndex < buildingDropdown.optionIds!.count){
                fetchBuildingFloors(buildingId: buildingDropdown.optionIds![fetchIndex])
            }else{
                floorDropdown.text = floorText
            }
        }
    }
    
    @objc func refreshTokenCompletion(notification: NSNotification){
        if(self.isTokenExpirationChanged){
            fetchBuildings()
        }else{
            initViews()
        }
    }
    
    @objc func fetchAssociateInfoCompletion(notification: NSNotification){
        fetchBuildings()
    }

    @IBAction func backBtnClicked(_ sender: Any) {
        let pageController = self.parent as! ComplaintPageViewController
        pageController.onBackBtnClicked(currentIndex: SecondComplaintViewController.currentPageIndex)
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        if(isReadyMoveNext()){
            let pageController = self.parent as! ComplaintPageViewController
            pageController.onNextBtnClicked(currentIndex: SecondComplaintViewController.currentPageIndex)
            return
        }
        showAlert(NSLocalizedString("key_infomation", comment: ""), NSLocalizedString("key_provide_input", comment: ""), NSLocalizedString("key_dismiss", comment: ""), viewController: self)
    }
    
    //UITextFieldDelegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    //Touch screen outside textfield to dismiss the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
