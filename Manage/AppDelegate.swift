//
//  AppDelegate.swift
//  Manage
//
//  Created by Rong Li on 10/24/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit
import GoogleMaps
import RealmSwift

//google maps key
let googleApiKey = "AIzaSyCZyHHVt6iiM_tTLSohVyqsWlpKW7B3378"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    /// Global location manager
    static var locationManager: LocationManager = {
        return $0
    }(LocationManager())
    
    static let locationAuthorizationType: LocationManager.AuthorizationType = .whenInUse

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //instantiate google maps servics with the API key using GMSServices class method provideAPIKey()
        GMSServices.provideAPIKey(googleApiKey)
        
//        //with storyboards
//        if let viewController = window?.rootViewController as? LoginViewController {
//            let remoteManager = RemoteManager.shared()
//            viewController.remoteManager = remoteManager
//        }
        
        //with storyboards
        if let viewController = window?.rootViewController as? SplashViewController {
            let remoteManager = RemoteManager.shared()
            viewController.remoteManager = remoteManager
        }
        
//        //splash screen
//        self.extendSplashScreenPresentation()
        
        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
        
//        var config = Realm.Configuration(
//            // Set the new schema version. This must be greater than the previously used
//            // version (if you've never set a schema version before, the version is 0).
//            schemaVersion: 0,
//            
//            // Set the block which will be called automatically when opening a Realm with
//            // a schema version lower than the one set above
//            migrationBlock: { migration, oldSchemaVersion in
//                // We haven’t migrated anything yet, so oldSchemaVersion == 0
//                if (oldSchemaVersion < 1) {
//                    // Nothing to do!
//                    // Realm will automatically detect new properties and removed properties
//                    // And will update the schema on disk automatically
//                }
//        })
//        do{
//            let _ = try Realm(configuration: config)
//            
//            print("Database Path : \(config.fileURL!)")
//        }catch{
//            print(error.localizedDescription)
//        }

        return true
    }
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//        if let viewController = window?.rootViewController as? SplashViewController {
//            doUpdate()
//        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

//    // MARK: splash screen
//    private func extendSplashScreenPresentation(){
//        // Get a refernce to LaunchScreen.storyboard
//        let launchStoryBoard = UIStoryboard.init(name: "LaunchScreen", bundle: nil)
//        // Get the splash screen controller
//        let splashController = launchStoryBoard.instantiateViewController(withIdentifier: "SplashViewController")
//        // Assign it to rootViewController
//        self.window?.rootViewController = splashController
//        self.window?.makeKeyAndVisible()
//        // Setup a timer to remove it after n seconds
//        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(dismissSplashController), userInfo: nil, repeats: false)
//    }
//
//    @objc private func dismissSplashController() {
//
//        // Get a refernce to Main.storyboard
//        let mainStoryBoard = UIStoryboard.init(name: "Main", bundle: nil)
//        // Get initial viewController
//        // let initController = mainStoryBoard.instantiateViewController(withIdentifier: "PickSiteViewController")
//        let initController = mainStoryBoard.instantiateViewController(withIdentifier: "LoginViewController");
//        // Assign it to rootViewController
//        self.window?.rootViewController = initController
//        self.window?.makeKeyAndVisible()
//
//    }

}

