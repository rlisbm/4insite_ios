//
//  RemoteManager.swift
//  Manage
//
//  Created by Rong Li on 10/24/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import UIKit

class RemoteManager: Service {
    
    struct BaseUrl {
        static let prodBaseUrl = "https://r2.sbminsite.com/";
        static let devBaseUrl = "https://dev-web.sbminsite.com/";
        static let uatBaseUrl = "https://uat-web.sbminsite.com/";
        static let qaBaseUrl = "https://qa-web.sbminsite.com/";
        
        static let prodBaseCDNUrl = "https://prod-web-media-cdn.sbminsite.com/image/"
        static let devBaseCDNUrl = "https://dev-web-media-cdn.sbminsite.com/image/"
        static let uatBaseCDNUrl = "https://uat-web-media-cdn.sbminsite.com/image/"
        static let qaBaseCDNUrl = "https://qa-web-media-cdn.sbminsite.com/image/"
    }
    
    // MARK: - Properties
    static let sharedUrlManager: RemoteManager = {
//        var remoteManager = RemoteManager(baseUrl:BaseUrl.prodBaseUrl, baseCDNUrl:BaseUrl.prodBaseCDNUrl)
        
        var remoteManager = RemoteManager(baseUrl:BaseUrl.devBaseUrl, baseCDNUrl:BaseUrl.devBaseCDNUrl)
        
        if(UIApplication.shared.getEnv == "Manager-Prod-Debug"){
            remoteManager = RemoteManager(baseUrl:BaseUrl.prodBaseUrl,
                                          baseCDNUrl:BaseUrl.prodBaseCDNUrl)
        }else if(UIApplication.shared.getEnv == "Manager-Dev-Debug"){
            remoteManager = RemoteManager(baseUrl:BaseUrl.devBaseUrl, baseCDNUrl:BaseUrl.devBaseCDNUrl)
        }else if(UIApplication.shared.getEnv == "Manager-Uat-Debug"){
            remoteManager = RemoteManager(baseUrl:BaseUrl.uatBaseUrl, baseCDNUrl:BaseUrl.uatBaseCDNUrl)
        }else if(UIApplication.shared.getEnv == "Manager-Qa-Debug"){
            remoteManager = RemoteManager(baseUrl:BaseUrl.qaBaseUrl, baseCDNUrl:BaseUrl.qaBaseCDNUrl)
        }else if(UIApplication.shared.getEnv == "Manager-Prod-Release"){
            remoteManager = RemoteManager(baseUrl:BaseUrl.prodBaseUrl, baseCDNUrl:BaseUrl.prodBaseCDNUrl)
        }
        
        return remoteManager
    }()
    
    let baseUrl: String
    let baseCDNUrl: String
    
    // Initialization
    private init(baseUrl: String, baseCDNUrl: String) {
        self.baseUrl = baseUrl
        self.baseCDNUrl = baseCDNUrl
        
        //        super.init(baseUrl: baseUrl, baseCDNUrl: baseCDNUrl)
    }
    
    // MARK: - Accessors
    class func shared() -> RemoteManager {
        return sharedUrlManager
    }
}


// MARK: - extension
extension UIApplication {
    var getEnv: String{
        let dictionary = ProcessInfo.processInfo.environment
        
        if(dictionary["Manager-Debug"] != nil){
            return dictionary["Manager-Debug"]!
        }else if(dictionary["Manager-Release"] != nil) {
            return dictionary["Manager-Release"]!
        }
        return ""

//        #if DEBUG
//        return dictionary["Manager-Debug"]!
//        #else
//        return dictionary["Manager-Release"]!
//        #endif

    }
}
