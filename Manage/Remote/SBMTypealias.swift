//
//  SBMTypealias.swift
//  Manage
//
//  Created by Rong Li on 10/24/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import AlamofireImage
import RealmSwift
import Realm

struct SBMTypealias {
    typealias LoginResult = (_ sbmToken: SBMToken?, _ error: Error?, _ responseCode: Int) -> ()
    
    typealias AssociateInfoResult = (_ associateInfo: AssociateInfo?, _ error: Error?, _ responseCode: Int) -> ()
    
    typealias ResponseString = (_ :String?, _ error: Error?, _ responseCode: Int) -> ()
    typealias ResponseAny = (_ :Any?, _ error: Error?, _ responseCode: Int) -> ()
    
    //for fetching response JSON Data for all
    typealias ResponseJSON = (_ :Data?, _ error: Error?, _ responseCode: Int) -> ()
    typealias ResponseJSONWithDate = (_ :Data?, _ error: Error?, _ responseCode: Int?, _ date: Date?) -> ()
    //end for
    
    
    typealias ResponseResult<T: Object> = (_ :T?, _ error: Error?, _ responseCode: Int) -> ()
    typealias ResponseResultList<T: Object> = (_ :List<T>?, _ error: Error?, _ responseCode: Int) -> ()
    typealias ResponseResults<T: Object> = (_ :[T]?, _ error: Error?, _ responseCode: Int) -> ()
    
    //for submittime item
    typealias ResponseDatum<T> = (_ :[T]?, _ error: Error?, _ responseCode: Int) -> ()
    typealias ResponseData<T> = (_ :T?, _ error: Error?, _ responseCode: Int) -> ()
    //end for
    
    
//    typealias ResponseResult<T: Object> = (_ :T?, _ error: Error?, _ responseCode: Int, _ isRefreshToken: Bool) -> ()
//    typealias ResponseResults<T: Object> = (_ :List<T>?, _ error: Error?, _ responseCode: Int, _ isRefreshToken: Bool) -> ()
    
//    typealias SubmitResponse = (_ :String?, _ error: Error?, _ responseCode: Int) -> ()
    typealias AvatarResult = (_ image: Image?, _ error: Error?, _ responseCode: Int) -> ()
//    typealias AvatarResult<T> = (_ :T?, _ image: Image?, _ error: Error?, _ responseCode: Int) -> ()
    
    
    
    typealias WarningCompletion = (_ isPositiveAnswer: Bool) -> ()
    
}

