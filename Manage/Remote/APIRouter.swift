//
//  APIRouter.swift
//  Manage
//
//  Created by Rong Li on 10/24/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation

enum APIRouter {
    case login
    case associate
    case clients
    case sites
    case programs
    case clientLocation
    case teamRoster
    case buildings
    case floors
    case todoItem
    case employeeAudit
    case conduct
    case attendance
    case department
    case jobsForSites
    case reportIt
    case compliantTypes
    case classifications
    case preventableStatuses
    case complaint
    case complimentTypes
    case compliment
    case trainingMessage
    case customerRepresentative
    case scoreCard
    case scoreCardUpdate
    case scoreCardDetail
    case awsCredentials
    case siteReportSetting
    case teamRosterV2
    case dailyTimeCardStats
    case todoItemV11
    case putTodoItem
    case request
    
    var path: String {
        switch self {
        case .login:
            return "oauth/token"
        case .associate:
            return "api/mobile/associates/current"
        case .clients:
            return "api/Services/User/Clients"
        case .sites:
            return "api/Services/Site/Client/{clientId}"
        case .programs:
            return "api/Services/Site/Programs/{siteId}"
        case .clientLocation:
            return "api/Services/Client/{clientId}"
        case .teamRoster:
            return "/api/v1.1/Services/TeamRoster"
        case .buildings:
            return "api/Services/LookupList/Buildings"
        case .floors:
            return "api/services/lookuplist/floors/building/{buildingId}"
        case .todoItem:
            return "api/Services/TodoItem"
        case .employeeAudit:
            return "api/Services/EmployeeAudit"
        case .conduct:
            return "api/Services/Conduct"
        case .attendance:
            return "api/Services/Issue"
        case .department:
            return "/api/Services/Department"
        case .jobsForSites:
            return "/api/Services/Job/JobsForSites"
        case .reportIt:
            return "api/services/ownership"
        case .compliantTypes:
            return "api/services/LookupList/ComplaintTypes/Program/{programId}"
        case .classifications:
            return "api/services/LookupList/Classifications"
        case .preventableStatuses:
            return "api/services/LookupList/PreventableStatuses"
        case .complaint:
            return "api/v1.1/Services/Complaints"
        case .complimentTypes:
            return "api/services/LookupList/ComplimentTypes"
        case .compliment:
            return "api/services/compliments"
        case .trainingMessage:
            return "api/services/TrainingMessage"
        case .customerRepresentative:
            return "api/services/CustomerRepresentative"
        case .scoreCard:
            return "api/v1.1/Services/ScoreCard"
        case .scoreCardUpdate:
            return "api/v1.1/Services/ScoreCard/{sorecardId}"
        case .scoreCardDetail:
            return "api/services/ScoreCardDetail"
        case .siteReportSetting:
            return "api/services/SiteReportSetting/{siteId}"
        case .awsCredentials:
            return "api/Services/Security/Aws/Credentials"
        case .teamRosterV2:
            return "api/v1.1/Services/TeamRoster/v2"
        case .dailyTimeCardStats:
            return "api/v1.1/Services/attendance/DailyTimeCardStats"
        case .todoItemV11:
            return "api/v1.1/Services/TodoItem"
        case .putTodoItem:
            return "api/Services/TodoItem/{todoItemId}"
        case .request:
            return "api/v1.1/Services/Request"
        }
    }
    
    func url(_ baseUrl: String) -> String {
        return "\(baseUrl)\(path)"
    }
    
    func url(_ baseUrl: String, _ id: Int, _ placeholder: String) -> String{
        return "\(baseUrl)\(path)".replacingOccurrences(of: placeholder, with: String(id))
    }
}
