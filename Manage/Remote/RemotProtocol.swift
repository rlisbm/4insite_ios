//
//  ManagerProtocol.swift
//  Manage
//
//  Created by Rong Li on 11/27/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation

protocol RemoteProtocol: class{
    func login(remoteManager: RemoteManager, userName: String, password: String, completion: @escaping SBMTypealias.LoginResult)
    func fetchAssociateInfo(remoteManager: RemoteManager, token: String, completion: @escaping SBMTypealias.AssociateInfoResult)
    
//    func fetchClients(remoteManager: RemoteManager, completion: @escaping SBMTypealias.ResponseResult<ClientMappable>)
//    func fetchSites(remoteManager: RemoteManager, clientId: Int, completion: @escaping SBMTypealias.ResponseResultList<SiteMappable>)
//    func fetchPrograms(remoteManager: RemoteManager, siteId: Int, completion: @escaping SBMTypealias.ResponseResultList<KeyValueMappable>)
//    func fetchClientLocation(remoteManager: RemoteManager, selectedClientId: Int, completion: @escaping SBMTypealias.ResponseResult<ClientLocation>)
    
    func fetchAvatar(remoteManager: RemoteManager, completion: @escaping SBMTypealias.AvatarResult)
    
//    func fetchBuildings(remoteManager: RemoteManager, clientId: Int, siteId: Int, completion: @escaping SBMTypealias.ResponseResult<NameKeyValue>)
    
    func fetchBuildings(remoteManager: RemoteManager, completion: @escaping SBMTypealias.ResponseResult<NameKeyValueMappable>)
    func fetchBuildingFloors(remoteManager: RemoteManager, buildingId: Int, completion: @escaping SBMTypealias.ResponseResult<NameKeyValueMappable>)
}
