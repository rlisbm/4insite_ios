//
//  UserInfo.swift
//  Manage
//
//  Created by Rong Li on 10/24/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper

public class UserInfo: NSObject, Mappable {
    
    public var userId: Int?
    public var userName: String?
    
    override init() {
        super.init()
    }
    
    convenience required public init?(map: Map) {
        self.init()
    }
    public func mapping(map: Map) {
        userId <- map["UserId"]
        userName <- map["UserName"]
    }
}
