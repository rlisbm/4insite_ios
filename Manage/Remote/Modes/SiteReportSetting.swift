//
//  SiteReportSetting.swift
//  Manage
//
//  Created by Rong Li on 2/8/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

open class SiteReportSetting {
    var SiteReportSettingId: Int?
    var SiteId: Int?
    var ScoringProfileId: Int?
    var ScoringProfileMinimumScore: Int?
    var ScoringProfileMaximumScore: Int?
    var ScoringProfileGoal: Int?
    var ComplaintControlLimit: Int?
    var SurveyControlLimit: Int?
    
    required public init(){}
    init(data: Data){
        do {
            if let body = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                SiteReportSettingId = body["SiteReportSettingId"] as? Int
                SiteId = body["SiteId"] as? Int
                ScoringProfileId = body["ScoringProfileId"] as? Int
                ScoringProfileMinimumScore = body["ScoringProfileMinimumScore"] as? Int
                ScoringProfileMaximumScore = body["ScoringProfileMaximumScore"] as? Int
                ScoringProfileGoal = body["ScoringProfileGoal"] as? Int
                ComplaintControlLimit = body["ComplaintControlLimit"] as? Int
                SurveyControlLimit = body["SurveyControlLimit"] as? Int

            }
        } catch {
            print("Error deserializing JSON: \(error)")

        }
        
    }
}
