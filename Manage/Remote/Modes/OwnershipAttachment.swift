//
//  OwnershipAttachment.swift
//  Manage
//
//  Created by Rong Li on 1/3/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

open class OwnershipAttachmentRequestItem: Codable {
    
//    let OwnershipAttachmentId: Int?
//    let AttachmentId: Int?
//    let UniqueId: String?
//    let Name: String?
//    let Description: String?
//    let OwnershipAttachmentTypeId: Int?
//    let OwnershipAttachmentType: String?
//    let AttachmentType: String?
//
//    init(ownershipAttachmentId: Int?,
//         attachmentId: Int?,
//         clientName: String?,
//         uniqueId: String?,
//         name: String?,
//         _description: String?,
//         ownershipAttachmentTypeId: Int?,
//         ownershipAttachmentType: String?,
//         attachmentType: String?){
//        self.OwnershipAttachmentId = ownershipAttachmentId
//        self.AttachmentId = attachmentId
//        self.UniqueId = uniqueId
//        self.Name = name
//        self.Description = _description
//        self.OwnershipAttachmentTypeId = ownershipAttachmentTypeId
//        self.OwnershipAttachmentType = ownershipAttachmentType
//        self.AttachmentType = attachmentType
//    }
    
    
    var OwnershipAttachmentId: Int?
    var AttachmentId: Int?
    var UniqueId: String?
    var Name: String?
    var Description: String?
    var OwnershipAttachmentTypeId: Int?
    var OwnershipAttachmentType: String?
    var AttachmentType: String?
    
    required public init(){}
}
