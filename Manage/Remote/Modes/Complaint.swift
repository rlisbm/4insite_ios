//
//  Complaint.swift
//  Manage
//
//  Created by Rong Li on 1/10/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation
import RealmSwift

open class Complaint: Codable {
    var Action: String?
    var BuildingId: Int?
    var BuildingName: String?
    var ClassificationId: Int?
    var ClassificationName: String?
    var ComplaintTypeId: Int?
    var ComplaintTypeName: String?
    var CompletedDate: String?
    var CustomerName: String?
    var Description: String?
    var FeedbackDate: String?
    var FloorId: Int?
    var FloorName: String?
    var IsRepeatComplaint: Bool?
    var Note: String?
    var PreventableStatusId: Int?
    var PreventableStatusName: String?
    var customerPhoneNumber: String?
    var AccountableEmployees: [AccountableEmployee] = [AccountableEmployee]()
    
    required public init(){}
}
