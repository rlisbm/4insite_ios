//
//  KeyValueValue.swift
//  Manage
//
//  Created by Rong Li on 1/15/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation
import ObjectMapper

class KeyValueValue: Mappable {
    var Key: Int = 0
    var Value1: String?
    var Value2: String?
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        //JobForSite.LegalDescriptions
        Key <- map["LegalDescriptionId"]
        Value1 <- map["Name"]
        Value2 <- map["Description"]
    }
}

open class LegalDescription: Codable {
    
    var LegalDescriptionId: Int?
    var Name: String?
    var Description: String?
    
    required public init(){}
    
    init(Key: Int?, Value1: String?, Value2: String?){
        LegalDescriptionId = Key
        Name = Value1
        Description = Value2
    }
    
}
