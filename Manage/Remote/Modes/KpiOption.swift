//
//  KpiOption.swift
//  Manage
//
//  Created by Rong Li on 12/13/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

@objcMembers class KpiOption: Object, Mappable {
    
    dynamic var kpiOptionId: Int = 0
    dynamic var name: String?
    var kpiSubOptions: List<KpiSubOption> = List<KpiSubOption>()
    
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        kpiOptionId <- map["KpiOptionId"]
        name <- map["Name"]
        
        var subOptions: [KpiSubOption] = []
        subOptions <- map["KpiSubOptions"]
        
        for subOption in subOptions{
            kpiSubOptions.append(subOption)
        }
    }
}

