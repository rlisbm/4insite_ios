//
//  ReportedByAttachments.swift
//  Manage
//
//  Created by Rong Li on 1/3/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

open class ReportedByAttachmentRequestItem: Codable {
    
    let UserAttachmentId: Int?
    let UniqueId: String?
    let Name: String?
    let Descrption: String?
    let UserAttachmentType: String?
    let AttachmentType: String?
    
    init(userAttachmentId: Int?,
         uniqueId: String?,
         name: String?,
         _descrption: String?,
         userAttachmentType: String?,
         attachmentType: String?){
        
        self.UserAttachmentId = userAttachmentId
        self.UniqueId = uniqueId
        self.Name = name
        self.Descrption = _descrption
        self.UserAttachmentType = userAttachmentType
        self.AttachmentType = attachmentType
    }
}

