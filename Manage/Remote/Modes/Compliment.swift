//
//  Compliment.swift
//  Manage
//
//  Created by Rong Li on 1/14/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

open class Compliment: Codable {
    var AccountableEmployees: [Int] = [Int]()
    var BuildingId: Int?
    var BuildingName: String?
    var ComplimentTypeId: Int?
    var ComplimentTypeName: String?
    var CustomerName: String?
    var CustomerPhoneNumber: String?
    var Description: String?
    var FeedbackDate: String?
    var FloorId: Int?
    var FloorName: String?
    
    required public init(){}
}
