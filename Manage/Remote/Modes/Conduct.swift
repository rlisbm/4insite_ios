//
//  Conduct.swift
//  Manage
//
//  Created by Rong Li on 12/18/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift
import Realm

@objcMembers class Conduct: Object, Mappable {
    
    dynamic var conductPrimaryKey: String?
    
    dynamic var conductId: Int = 0
    dynamic var orgUserId: Int = 0
//    dynamic var createDate: String?
    dynamic var dateOfOccurrence: String?
    var isExempt: Bool?
    dynamic var reason: String?
    var conductTypes = List<ConductType>()
    var conductAttachments = List<ConductAttachment>()
    
    override static func primaryKey() -> String? {
        return "conductPrimaryKey"
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        conductId <- map["ConductId"]
        orgUserId <- map["OrgUserId"]
//        createDate <- map["CreateDate"]
        dateOfOccurrence <- map["DateOfOccurrence"]
        isExempt <- map["IsExempt"]
        reason <- map["Reason"]
        
        var types: [ConductType] = []
        types <- map["ConductTypes"]
        for type in types {
            conductTypes.append(type)
        }
        
        var attachments: [ConductAttachment] = []
        attachments <- map["ConductAttachments"]
        for attachment in attachments {
            conductAttachments.append(attachment)
        }
    }
}



class ConductRequestItem: Codable{
    let ConductId: Int?
    let OrgUserId: Int?
//    let CreateDate: String?
    let DateOfOccurrence: String?
    let IsExempt: Bool?
    let Reason: String?
    var ConductTypes = [ConductType]()
    var ConductAttachments = [ConductAttachmentRequestItem]()
    
    init(conductId: Int?,
         orgUserId: Int?,
         dateOfOccurrence: String?,
         isExempt: Bool?,
         reason: String?,
         conductTypes: List<ConductType>,
         conductAttachments: List<ConductAttachment>){
        
        self.ConductId = conductId
        self.OrgUserId = orgUserId
        self.DateOfOccurrence = dateOfOccurrence
        self.IsExempt = isExempt
        self.Reason = reason
        
        for type in conductTypes {
            self.ConductTypes.append(ConductType(conductTypeId: type.ConductTypeId, name: type.Name, isSelected: type.IsSelected))
        }
       
        for attachment in conductAttachments {
            self.ConductAttachments.append(ConductAttachmentRequestItem(conductAttachmentId: attachment.conductAttachmentId,
                                                                        uniqueId: attachment.uniqueId,
                                                                        name: attachment.name,
                                                                        _description: attachment._description,
                                                                        conductAttachmentTypeId: attachment.conductAttachmentTypeId,
                                                                        conductAttachmentType: attachment.conductAttachmentType,
                                                                        attachmentType: attachment.attachmentType))
        }
    }
 
}
