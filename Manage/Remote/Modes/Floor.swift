//
//  Floor.swift
//  Manage
//
//  Created by Rong Li on 12/6/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

@objcMembers class Floor: Object, Mappable {
    dynamic var id: Int = 0
    dynamic var name: String?
    dynamic var parentId: Int = 0
    dynamic var areaCount: Int = 0
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["Id"]
        name <- map["Name"]
        parentId <- map["ParentId"]
        areaCount <- map["AreaCount"]
    }
}
