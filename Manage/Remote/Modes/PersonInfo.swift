//
//  PersonInfo.swift
//  Manage
//
//  Created by Rong Li on 10/24/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper

public class PersonInfo: NSObject, Mappable {
    
    public var personId: Int?
    public var firstName: String?
    public var lastName: String?
    public var fullName: String?
    public var email: String?
    public var profilePictureUniqueId: String?
    
    override init() {
        super.init()
    }
    
    convenience required public init?(map: Map) {
        self.init()
    }
    public func mapping(map: Map) {
        personId <- map["PersonId"]
        firstName <- map["FirstName"]
        lastName <- map["LastName"]
        fullName <- map["FullName"]
        email <- map["Email"]
        profilePictureUniqueId <- map["ProfilePictureUniqueId"]
    }
}



