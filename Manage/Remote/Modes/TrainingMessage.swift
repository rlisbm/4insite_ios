//
//  TrainingMessage.swift
//  Manage
//
//  Created by Rong Li on 1/2/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

open class TrainingMessage: Codable {
    
//    var TrainingMessageId: Int = 0
    var Title: String = ""
    var Text: String?
    var SenderEmail: String?
    var SenderFullName: String?
//    var SenderId: Int = 0
    var SenderJobTitle: String?
    var SenderProfileImageUrl: String?
    var SenderType: String?
    var IsMobile: Bool = false
    var IsCustomer: Bool = false
    var IsTrainingAdmin: Bool = false
    var HighPriority: Bool = false
    var Department: DepartmentRequestItem?
    var Jobs = [Job]()
    var Sites = [IdName]()
    var SourceLanguageId: Int = 0
    var CreateDate: String?
    
    required public init(){}
}



//@objcMembers public class TrainingMessage: Object, Codable {
//
//    dynamic var TrainingMessageId: Int = 0
//    dynamic var Title: String?
//    dynamic var Text: String?
//    dynamic var SenderEmail: String?
//    dynamic var SenderFullName: String?
//    dynamic var SenderId: Int = 0
//    dynamic var SenderJobTitle: String?
//    dynamic var SenderProfileImageUrl: String?
//    dynamic var SenderType: String?
//    dynamic var IsMobile: Bool?
//    dynamic var IsCustomer: Bool?
//    dynamic var HighPriority: Bool?
//    dynamic var Department: DepartmentRequestItem?
//    dynamic var Jobs = [KeyValueRequestItem]()
//    dynamic var Sites = [KeyValueRequestItem]()
//
//    convenience init(trainingMessageId: Int?,
//                     title: String?,
//                     text: String?,
//                     senderEmail: String?,
//                     senderFullName: String?,
//                     senderJobTitle: String?,
//                     senderProfileImageUrl: String?,
//                     senderType: String?,
//                     isMobile: Bool?,
//                     isCustomer: Bool?,
//                     highPriority: Bool?,
//                     department: Department?,
//                     jobs: [KeyValueRequestItem]?,
//                     sites: [KeyValueRequestItem]?
//                     ){
//        self.init()
//        self.TrainingMessageId = trainingMessageId ?? 0
//        self.Title = title
//        self.Text = text
//        self.SenderEmail = senderEmail
//        self.SenderFullName = senderFullName
//        self.SenderJobTitle = senderJobTitle
//        self.SenderProfileImageUrl = senderProfileImageUrl
//        self.SenderType = senderType
//        self.IsMobile = isMobile
//        self.IsCustomer = isCustomer
//        self.HighPriority = highPriority
//        self.Department = Department!
//        self.Jobs = jobs!
//        self.Sites = sites!
//    }
//}
//
