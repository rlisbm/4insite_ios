//
//  CustomerRepresentative.swift
//  Manage
//
//  Created by Rong Li on 1/3/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

//@objcMembers class CustomerRepresentativeMappable: Object, Mappable{
//
//    dynamic var CustomerRepresentativeId: Int?
//    dynamic var Name: String?
//    dynamic var Email: String?
//    dynamic var JobDescription: String?
//
//    convenience required init?(map: Map) {
//        self.init()
//    }
//
//    func mapping(map: Map) {
//        CustomerRepresentativeId <- map["CustomerRepresentativeId"]
//        Name <- map["Name"]
//        Email <- map["Email"]
//        JobDescription <- map["JobDescription"]
//    }
//}

//open class CustomerRepresentativeItem: Codable{
//    
//    var CustomerRepresentativeId: Int?
//    var Name: String?
//    var Email: String?
//    var JobDescription: String?
//    
//    required public init() {
//    }
//    
//    init(json: [String: Any]) {
//        CustomerRepresentativeId = json["CustomerRepresentativeId"] as? Int
//        Name = json["Name"] as? String
//        Email = json["Email"] as? String
//        JobDescription = json["JobDescription"] as? String
//    }
//}


open class CustomerRepresentativeRequestItem: Codable {
    
    let CustomerRepresentativeId: Int?
    let ClientId: Int?
    let ClientName: String?
    let Name: String?
    let Email: String?
    let JobDescrption: String?
    let SourceLanguageId: Int?
    
    init(customerRepresentativeId: Int?,
         clientId: Int?,
         clientName: String?,
         name: String?,
         email: String?,
         jobDescrption: String?,
         sourceLanguageId: Int?){
        self.CustomerRepresentativeId = customerRepresentativeId
        self.ClientId = clientId
        self.ClientName = clientName
        self.Name = name
        self.Email = email
        self.JobDescrption = jobDescrption
        self.SourceLanguageId = sourceLanguageId
    }
}


open class CustomerRepresentative: Codable {
    
    var CustomerRepresentativeId: Int?
    var ClientId: Int?
    var ClientName: String?
    var Name: String?
    var Email: String?
    var JobDescription: String?
    var SourceLanguageId: Int?
    
    required public init(){}
    
    init(data: Data){
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                CustomerRepresentativeId = json["CustomerRepresentativeId"] as? Int
                ClientId = json["ClientId"] as? Int
                ClientName = json["ClientName"] as? String
                Name = json["Name"] as? String
                Email = json["Email"] as? String
                JobDescription = json["JobDescription"] as? String
                SourceLanguageId = json["SourceLanguageId"] as? Int
            }
        } catch {
            print("Error deserializing JSON: \(error)")
        }
    }
    
    init(json: [String: Any]){
        CustomerRepresentativeId = json["CustomerRepresentativeId"] as? Int
        ClientId = json["ClientId"] as? Int
        ClientName = json["ClientName"] as? String
        Name = json["Name"] as? String
        Email = json["Email"] as? String
        JobDescription = json["JobDescription"] as? String
        SourceLanguageId = json["SourceLanguageId"] as? Int
    }
}


