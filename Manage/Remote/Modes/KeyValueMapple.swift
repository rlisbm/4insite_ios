//
//  Options.swift
//  Manage
//
//  Created by Rong Li on 10/24/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

@objcMembers public class KeyValueMappable: Object, Mappable {

    dynamic var Key:Int = 0
    dynamic var Value: String?

    convenience init(Key: Int?, Value: String?){
        self.init()
        self.Key = Key!
        self.Value = Value
    }
    
    convenience required public init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        Key <- map["Key"]
        Value <- map["Value"]
        
        
        Key <- map["Id"]
        Value <- map["Name"]
        
        
        Key <- map["ConductTypeId"]
        //Value <- map["Name"]
        
        
        Key <- map["ClassificationId"]
        //Value <- map["Name"]
        
        Key <- map["DutyId"]
        Value <- map["Description"]
        
        Key <- map["SampleTaskId"]
//        Value <- map["Name"]
    }
}

open class KeyValueRequestItem: Codable {
    
    var Key:Int?
    var Value: String?
    
    required public init(){}
    
    init(Key: Int?, Value: String?){
        self.Key = Key!
        self.Value = Value
    }
}






open class ClassificationRequestItem: Codable {
    
    var ClassificationId: Int?
    var Name: String?
    
    required public init(){}
    
    init(Key: Int?, Value: String?){
        self.ClassificationId = Key
        self.Name = Value
    }
}

open class JobDuty: Codable {
    
    var DutyId: Int?
    var Description: String?
    
    required public init(){}
    
    init(Key: Int?, Value: String?){
        DutyId = Key
        Description = Value
    }
}

open class SampleTask: Codable {
    
    var SampleTaskId: Int?
    var Name: String?
    
    required public init(){}
    
    init(Key: Int?, Value: String?){
        SampleTaskId = Key
        Name = Value
    }
}

open class Job: Codable {
    
    var JobId: Int?
    var Name: String?
    
    required public init(){}
    
    init(Key: Int?, Value: String?){
        JobId = Key
        Name = Value
    }
}


