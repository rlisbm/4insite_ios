//
//  Issue.swift
//  Manage
//
//  Created by Rong Li on 12/20/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

@objcMembers class IssueMappable: Object, Mappable {
    dynamic var issuePrimaryKey: String?
    
    dynamic var comment: String?
    dynamic var dateOfOccurrence: String?
    dynamic var isExempt: Bool = true
    dynamic var issueId:Int = 0
    dynamic var issueType: String?
    var issueTypeId: Int?
    dynamic var orgUserId:Int = 0
    var reasonId:Int?
    dynamic var reasonName:String?
    
    dynamic var createDate: String?
    var sourceLanguageId: Int?
    
    override static func primaryKey() -> String? {
        return "issuePrimaryKey"
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        comment <- map["Comment"]
        dateOfOccurrence <- map["DateOfOccurrence"]
        isExempt <- map["IsExempt"]
        issueId <- map["IssueId"]
        issueType <- map["IssueType"]
        issueTypeId <- map["IssueTypeId"]
        orgUserId <- map["OrgUserId"]
        reasonId <- map["ReasonId"]
        reasonName <- map["ReasonName"]
        
        createDate <- map["CreateDate"]
        sourceLanguageId <- map["SourceLanguageId"]
    }
}

open class Issue: Codable{
    var issuePrimaryKey: String?
    
    var comment: String?
    var dateOfOccurrence: String?
    var isExempt: Bool?
    var issueId:Int?
    var issueType: String?
    var issueTypeId: Int?
    var orgUserId:Int?
    var reasonId:Int?
    var reasonName:String?
    
    var createDate: String?
    var sourceLanguageId: Int?
    
    
    required public init(){}
    
    init(data: Data) {
        do {
            if let body = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                comment = body["Comment"] as? String
                dateOfOccurrence = body["DateOfOccurrence"] as? String
                isExempt = body["IsExempt"] as? Bool
                issueId = body["IssueId"] as? Int
                issueType = body["IssueType"] as? String
                issueTypeId = body["IssueTypeId"] as? Int
                orgUserId = body["OrgUserId"] as? Int
                reasonId = body["ReasonId"] as? Int
                reasonName = body["ReasonName"] as? String
                
                createDate = body["CreateDate"] as? String
                sourceLanguageId = body["SourceLanguageId"] as? Int
            }
        }catch {
            print("Error deserializing JSON: \(error)")
            
        }
    }
}



