//
//  ProfessionalismEmployeeAudit.swift
//  Manage
//
//  Created by Rong Li on 12/13/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift


@objcMembers class ProfessionalismEmployeeAudit: Object, Mappable {
    dynamic var employeeId: Int = 0
    dynamic var commet: String?
    var isEvenBaseAudit: Bool?
    var checkBoxSelection: List<Int> = List<Int>()
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        employeeId <- map["EmployeeId"]
        commet <- map["Commet"]
        isEvenBaseAudit <- map["IsEvenBaseAudit"]
        
        var selections: [Int] = []
        selections <- map["CheckBoxSelection"]
        for selection in selections{
            checkBoxSelection.append(selection)
        }
    }
}

