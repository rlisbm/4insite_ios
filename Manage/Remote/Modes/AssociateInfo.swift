//
//  AssociateInfo.swift
//  Manage
//
//  Created by Rong Li on 10/24/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper

public class AssociateInfo: NSObject, Mappable{
    public var userInfo: UserInfo?
    public var personInfo: PersonInfo?
    public var employeeInfo: EmployeeInfo?
    public var contextInfo: ContextInfo?
    
    override init() {
        super.init()
    }
    
    convenience required public init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        userInfo <- map["UserInfo"]
        personInfo <- map["PersonInfo"]
        employeeInfo <- map["EmployeeInfo"]
        contextInfo <- map["ContextInfo"]
    }
    
}


