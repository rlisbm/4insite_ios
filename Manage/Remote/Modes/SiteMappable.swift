//
//  Site.swift
//  Manage
//
//  Created by Rong Li on 11/8/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

@objcMembers class SiteMappable: Object, Mappable, Codable {
    
//    dynamic var primaryKey: String?
    
    dynamic var Id: Int = 0
    dynamic var Name: String?
    dynamic var Longitude: Double = 0.0
    dynamic var Latitude: Double = 0.0
    dynamic var ParentId: Int = 0
    dynamic var ChildCount: Int = 0
    dynamic var ProgramCount: Int = 0
    
    
//    override static func primaryKey() -> String? {
//        return "primaryKey"
//    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        Id <- map["Id"]
        Name <- map["Name"]
        Longitude <- map["Longitude"]
        Latitude <- map["Latitude"]
        ParentId <- map["ParentId"]
        ChildCount <- map["ChildCount"]
        ProgramCount <- map["ProgramCount"]
        
    }
}

