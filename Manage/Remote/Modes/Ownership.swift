//
//  Ownership.swift
//  Manage
//
//  Created by Rong Li on 1/3/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation
import RealmSwift

open class OwnershipRequestItem: Codable {
//    let BuildingId: Int?
//    let BuildingName: String?
//    var Classifications: [ClassificationRequestItem] = [ClassificationRequestItem]()
//    let CorrectiveActionDescription: String?
//    let CreateDate: String?
//    var CustomerRepresentatives: [CustomerRepresentativeRequestItem] = [CustomerRepresentativeRequestItem]()
//    let Description: String?
//    let EmployeeId: Int?
//    let FloorId: Int?
//    let FloorName: String?
//    let IsCritical: Bool?
//    let IsExempt: Bool?
//    var OwnershipAttachments: [OwnershipAttachmentRequestItem] = [OwnershipAttachmentRequestItem]()
//    let OwnershipId: Int?
//    let RejectionTypeId: Int?
//    let RejectionTypeName: String?
//    let ReportTypeId: Int?
//    let ReportTypeName: String?
//    let Status: String?
//
//    init(BuildingId: Int?,
//         BuildingName: String?,
//         Classifications: [ClassificationRequestItem]?,
//         CorrectiveActionDescription: String?,
//         CreateDate: String?,
//         CustomerRepresentatives: [CustomerRepresentativeRequestItem]?,
//         Description: String?,
//         EmployeeId: Int?,
//         FloorId: Int?,
//         FloorName: String?,
//         IsCritical: Bool?,
//         IsExempt: Bool?,
//         OwnershipAttachments: [OwnershipAttachmentRequestItem]?,
//         OwnershipId: Int?,
//         RejectionTypeId: Int?,
//         RejectionTypeName: String?,
//         ReportTypeId: Int?,
//         ReportTypeName: String?,
//         Status: String?
//         ){
//
//        self.BuildingId = BuildingId
//        self.BuildingName = BuildingName
//        self.Classifications = Classifications!
//        self.CorrectiveActionDescription = CorrectiveActionDescription
//        self.CreateDate = CreateDate
//        self.CustomerRepresentatives = CustomerRepresentatives!
//        self.Description = Description
//        self.EmployeeId = EmployeeId
//        self.FloorId = FloorId
//        self.FloorName = FloorName
//        self.IsCritical = IsCritical
//        self.IsExempt = IsExempt
//        self.OwnershipAttachments = OwnershipAttachments!
//        self.OwnershipId = OwnershipId
//        self.RejectionTypeId = RejectionTypeId
//        self.RejectionTypeName = RejectionTypeName
//        self.ReportTypeId = ReportTypeId
//        self.ReportTypeName = ReportTypeName
//        self.Status = Status
//    }
    
    
    var BuildingId: Int?
    var BuildingName: String?
    var Classifications: [ClassificationRequestItem] = [ClassificationRequestItem]()
    var CorrectiveActionDescription: String?
    var CreateDate: String?
    var CustomerRepresentatives: [CustomerRepresentativeRequestItem] = [CustomerRepresentativeRequestItem]()
    var Description: String?
    var EmployeeId: Int?
    var FloorId: Int?
    var FloorName: String?
    var IsCritical: Bool?
    var IsExempt: Bool?
    var OwnershipAttachments: [OwnershipAttachmentRequestItem] = [OwnershipAttachmentRequestItem]()
    var OwnershipId: Int?
    var RejectionTypeId: Int?
    var RejectionTypeName: String?
    var ReportTypeId: Int?
    var ReportTypeName: String?
    var Status: String?
    
    required public init(){}
}
