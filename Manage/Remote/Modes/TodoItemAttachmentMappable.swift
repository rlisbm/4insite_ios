//
//  TodoItemAttachment.swift
//  Manage
//
//  Created by Rong Li on 12/12/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

//@objcMembers class TodoItemAttachment: Object{
//    
//    dynamic var attachmentType: String?
//    dynamic var todoItemAttachmentType: Int = 0
//    dynamic var todoItemAttachmentTypeId: Int = 0
//    dynamic var uniqueId: String?
//    
//}


@objcMembers class TodoItemAttachmentMappable: Object, Mappable {

    dynamic var attachmentType: String?
    dynamic var todoItemAttachmentType: Int = 0
    dynamic var todoItemAttachmentTypeId: Int = 0
    dynamic var uniqueId: String?

    convenience required init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        attachmentType <- map["AttachmentType"]
        todoItemAttachmentType <- map["TodoItemAttachmentType"]
        todoItemAttachmentTypeId <- map["TodoItemAttachmentTypeId"]
        uniqueId <- map["UniqueId"]
    }
}


