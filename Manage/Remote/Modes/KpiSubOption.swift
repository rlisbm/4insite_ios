//
//  KpiSubOption.swift
//  Manage
//
//  Created by Rong Li on 12/13/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

@objcMembers class KpiSubOption: Object, Mappable {
    
    dynamic var kpiSubOptionId: Int = 0
    dynamic var name: String?
    dynamic var kpiName: String?
    dynamic var kpiOption: KeyValueMappable?
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        kpiSubOptionId <- map["KpiSubOptionId"]
        name <- map["Name"]
        kpiName <- map["KpiName"]
        kpiOption <- map["KpiOption"]
    }
}

