//
//  PersitionInfo.swift
//  Manage
//
//  Created by Rong Li on 10/24/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper

public class PositionInfo: NSObject, Mappable {
    
    public var jobId: Int?
    public var title: String?
    
    override init() {
        super.init()
    }
    
    convenience required public init?(map: Map) {
        self.init()
    }
    public func mapping(map: Map) {
        jobId <- map["JobId"]
        title <- map["Title"]
    }
}

