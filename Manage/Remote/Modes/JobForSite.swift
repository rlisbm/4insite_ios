//
//  JobForSite.swift
//  Manage
//
//  Created by Rong Li on 1/15/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation
import ObjectMapper

class JobForSite: Mappable {
    var JobId: Int = 0
    var Name: String?
    var Abbreviation: String?
    var Summary: String?
    var Purpose: String?
    var Layer: String?
    var ReportsTo: String?
    var Department: String?
    var ExemptionTypeId: Int = 0
    var PreparedBy: String?
    var ApprovedBy: String?
    var ApprovedDate: String?
    var ExternalJobCode: String?
    var JobStep: String?
    var EssentialDutiesSummaryLabel: String?
    var EssentialDutiesAndResponsibilitiesLabel: String?
    var ExampleOfTasksLabel: String?
    var DutySummary: String?
    var SampleTaskSummary: String?
    var LegalDescriptionSummary: String?
    var WorkEnvironment: String?
    var UpdateDate: String?
    var UpdatedBy: String?
    var JobDuties: [KeyValueMappable] = [KeyValueMappable]()
    var SampleTasks: [KeyValueMappable] = [KeyValueMappable]()
    var LegalDescriptions: [KeyValueValue] = [KeyValueValue]()

    convenience required init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        JobId <- map["JobId"]
        Name <- map["Name"]
        Abbreviation <- map["Abbreviation"]
        Summary <- map["Summary"]
        Purpose <- map["Purpose"]
        Layer <- map["Layer"]
        ReportsTo <- map["ReportsTo"]
        Department <- map["Department"]
        ExemptionTypeId <- map["ExemptionTypeId"]
        PreparedBy <- map["PreparedBy"]
        ApprovedBy <- map["ApprovedBy"]
        ApprovedDate <- map["ApprovedDate"]
        ExternalJobCode <- map["ExternalJobCode"]
        JobStep <- map["JobStep"]
        EssentialDutiesSummaryLabel <- map["EssentialDutiesSummaryLabel"]
        EssentialDutiesAndResponsibilitiesLabel <- map["EssentialDutiesAndResponsibilitiesLabel"]
        ExampleOfTasksLabel <- map["ExampleOfTasksLabel"]
        DutySummary <- map["DutySummary"]
        SampleTaskSummary <- map["SampleTaskSummary"]
        LegalDescriptionSummary <- map["LegalDescriptionSummary"]
        WorkEnvironment <- map["WorkEnvironment"]
        UpdateDate <- map["UpdateDate"]
        UpdatedBy <- map["UpdatedBy"]
        Abbreviation <- map["Abbreviation"]
        JobDuties <- map["JobDuties"]
        SampleTasks <- map["SampleTasks"]
        LegalDescriptions <- map["LegalDescriptions"]
    }

}

//open class JobForSite: Codable {
//    var JobId: Int = 0
//    var Name: String?
//    var Abbreviation: String?
//    var Summary: String?
//    var Purpose: String?
//    var Layer: String?
//    var ReportsTo: String?
//    var Department: String?
//    var ExemptionTypeId: Int = 0
//    var PreparedBy: String?
//    var ApprovedBy: String?
//    var ApprovedDate: String?
//    var ExternalJobCode: String?
//    var JobStep: String?
//    var EssentialDutiesSummaryLabel: String?
//    var EssentialDutiesAndResponsibilitiesLabel: String?
//    var ExampleOfTasksLabel: String?
//    var DutySummary: String?
//    var SampleTaskSummary: String?
//    var LegalDescriptionSummary: String?
//    var WorkEnvironment: String?
//    var UpdateDate: String?
//    var UpdatedBy: String?
//    var JobDuties: [JobDuty] = [JobDuty]()
//    var SampleTasks: [SampleTask] = [SampleTask]()
//    var LegalDescriptions: [LegalDescription] = [LegalDescription]()
//
//    required public init(){}
//
//}
