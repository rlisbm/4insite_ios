//
//  SBMToken.swift
//  Manage
//
//  Created by Rong Li on 10/24/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct SBMToken: Codable{
    
    public let access_token: String
    public let token_type: String
    public var expires_in: Int64
    
    public init?(json: [String: Any]){
        self.access_token = (json["assess_token"] as? String)!
        self.token_type = (json["token_type"] as? String)!
        self.expires_in = (json["expires_in"] as? Int64)!
    }
}


