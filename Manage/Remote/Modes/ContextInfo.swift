//
//  ContextInfo.swift
//  Manage
//
//  Created by Rong Li on 10/24/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper

public class ContextInfo: NSObject, Mappable {
    
    public var clientId: Int?
    public var siteId: Int?
    
    override init() {
        super.init()
    }
    
    convenience required public init?(map: Map) {
        self.init()
    }
    public func mapping(map: Map) {
        clientId <- map["ClientId"]
        siteId <- map["SiteId"]
    }
}

