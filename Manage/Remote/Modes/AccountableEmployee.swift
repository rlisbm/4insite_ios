//
//  AccountableEmployee.swift
//  Manage
//
//  Created by Rong Li on 1/10/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation
import RealmSwift

open class AccountableEmployee: Codable {
    var EmployeeId: Int?
    var IsExempt: Bool = false
    
    required public init(employeeId: Int, isExempt: Bool){
        EmployeeId = employeeId
        IsExempt = isExempt
    }
}

