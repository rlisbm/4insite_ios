//
//  EmployeeInfo.swift
//  Manage
//
//  Created by Rong Li on 10/24/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper

public class EmployeeInfo: NSObject, Mappable {
    
    public var employeeId: Int?
    public var timeCenterId: Int?
    public var positionInfo: [PositionInfo]?
    
    override init() {
        super.init()
    }
    
    convenience required public init?(map: Map) {
        self.init()
    }
    public func mapping(map: Map) {
        employeeId <- map["EmployeeId"]
        timeCenterId <- map["TimeCard"]
        positionInfo <- map["Positions"]
    }
}


