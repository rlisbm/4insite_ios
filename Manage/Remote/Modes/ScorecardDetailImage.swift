//
//  ScorecardDetailImage.swift
//  Manage
//
//  Created by Rong Li on 1/25/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

open class ScorecardDetailImage: Codable {
    var ImageId: Int?
    var PublicId: String?
    var ScorecardDetailId: Int?
    var Comment: String?
    var SourceLanguageId: Int?
    
    required public init(){}
    
    init(json: [String: Any]) {
        ImageId = json["ImageId"] as? Int
        PublicId = json["PublicId"] as? String
        ScorecardDetailId = json["ScorecardDetailId"] as? Int
        Comment = json["Comment"] as? String
        SourceLanguageId = json["SourceLanguageId"] as? Int
    }
}
