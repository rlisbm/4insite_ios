//
//  TodoItemId.swift
//  Manage
//
//  Created by Rong Li on 12/13/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

@objcMembers class TodoItemIdMappable: Object, Mappable {
    dynamic var todoItemId: Int = 0
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        todoItemId <- map["TodoItemId"]
    }
}

