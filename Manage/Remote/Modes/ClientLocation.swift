//
//  ClientLocation.swift
//  Manage
//
//  Created by Rong Li on 11/12/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

@objcMembers class ClientLocation: Object, Mappable {
    
    dynamic var primaryKey: String?
    
    var parentId: Int?
    var childCount: Int?
    var latitude: Double?
    var longitude: Double?
    
    override static func primaryKey() -> String? {
        return "primaryKey"
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        parentId <- map["ParentId"]
        childCount <- map["ChildCount"]
        latitude <- map["Latitude"]
        longitude <- map["Longitude"]
    }
}



