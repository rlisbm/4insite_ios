//
//  KeyBool.swift
//  Manage
//
//  Created by Rong Li on 1/24/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

open class KeyBool: Codable{
    var FloorId: Int?
    var InProgress: Bool?
    
    required public init(){}
}
