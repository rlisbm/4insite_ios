//
//  NameOptions.swift
//  Manage
//
//  Created by Rong Li on 10/24/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift
import Realm

@objcMembers class NameKeyValueMappable: Object, Mappable {
    
//    dynamic var nameKeyValuePrimaryKey: String?
    
    
    dynamic var Name: String?
//    dynamic var Options = RLMArray<KeyValue> (objectClassName: KeyValue.className())
    var Options = List<KeyValueMappable>()
    
//    override static func primaryKey() -> String? {
//        return "nameKeyValuePrimaryKey"
//    }
    
    convenience required public init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        var options: [KeyValueMappable] = []
        Name <- map["Name"]
        options <- (map["Options"])
        
        for option in options{
            Options.append(option)
        }
    }
}





