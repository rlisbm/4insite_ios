//
//  EmployeeAuditItem.swift
//  Manage
//
//  Created by Rong Li on 12/14/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

@objcMembers class EmployeeAuditItem: Object, Mappable {

    dynamic var employeeAuditItemPrimaryKey: String?

    var checkBoxSelections: List<Int> = List<Int>()
    dynamic var comment: String?
    dynamic var employeeId: Int = 0
    dynamic var isEventBasedAudit: Bool = false

    override static func primaryKey() -> String? {
        return "employeeAuditItemPrimaryKey"
    }

    convenience required init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        comment <- map["Comment"]
        employeeId <- map["EmployeeId"]
        isEventBasedAudit <- map["IsEventBasedAudit"]

        var selections: [Int] = []
        selections <- map["CheckBoxSelections"]
        for selection in selections {
            checkBoxSelections.append(selection)
        }
    }
}

