//
//  CheckBoxOption.swift
//  Manage
//
//  Created by Rong Li on 12/14/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

@objcMembers class CheckBoxOption: Object, Mappable {
    
    dynamic var kpiSubOptionId: Int = 0
    dynamic var name: String?
    dynamic var kpiOption: KeyValueMappable?
    dynamic var kpiName: String?
    
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        kpiSubOptionId <- map["KpiSubOptionId"]
        name <- map["Name"]
        kpiOption <- map["KpiOption"]
        kpiName <- map["KpiName"]
    }
}

