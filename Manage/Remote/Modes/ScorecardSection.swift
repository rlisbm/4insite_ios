//
//  ScorecardSection.swift
//  Manage
//
//  Created by Rong Li on 1/24/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

open class ScorecardSection: Codable {
    var AreaClassificationId: Int?
    var AreaClassificationName: String?
    var AreaId: Int?
    var AreaName: String?
    var ScorecardDetails: [ScorecardDetail]?
    var ScorecardSectionId: Int?
    
    required public init(){}
    
    init(json: [String: Any]) {
        AreaClassificationId = json["AreaClassificationId"] as? Int
        AreaClassificationName = json["AreaClassificationName"] as? String
        AreaId = json["AreaId"] as? Int
        AreaName = json["AreaName"] as? String
        
        if let scorecardDetails = json["ScorecardDetails"] as? [[String: Any]] {
            ScorecardDetails = scorecardDetails.map { ScorecardDetail(json: $0) }
        }

        
        ScorecardSectionId = json["ScorecardSectionId"] as? Int
    }
}
