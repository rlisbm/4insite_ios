//
//  ScorecardDetail.swift
//  Manage
//
//  Created by Rong Li on 1/25/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

open class ScorecardDetail: Codable {
    var AuditInspectionItemName: String?
    var Comment: String?
    var Score: Double?
    var ScorecardDetailId: Int?
    
    var ScorecardDetailImages: [ScorecardDetailImage]?
    
    var ScorecardSectionId: Int?
    var SourceLanguageId: Int?
    var TodoItems: [TodoItem]?
    
    required public init(){}
    
    init(json: [String: Any]) {
        AuditInspectionItemName = json["AuditInspectionItemName"] as? String
        Comment = json["Comment"] as? String
        Score = json["Score"] as? Double
        ScorecardDetailId = json["ScorecardDetailId"] as? Int
        
        if let scorecardDetailImages = json["ScorecardDetailImages"] as? [[String: Any]] {
            ScorecardDetailImages = scorecardDetailImages.map { ScorecardDetailImage(json: $0) }
        }
        
        ScorecardSectionId = json["ScorecardSectionId"] as? Int
        SourceLanguageId = json["SourceLanguageId"] as? Int
        
        if let todoItems = json["TodoItems"] as? [[String: Any]] {
            TodoItems = todoItems.map { TodoItem(json: $0) }
        }
        
    }
}


open class ResponseScorecardDetail: Codable {
    var Body: [ScorecardDetail]?
    var Message: String?
    var Status: Int?


    required public init(){}

//    init(json: [[String: Any]]){
//        Body = json["Body"] as? ScorecardDetail
//
//    }
}
