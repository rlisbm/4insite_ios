//
//  Department.swift
//  Manage
//
//  Created by Rong Li on 12/21/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

@objcMembers class Department: Object, Mappable {
    
    dynamic var departmentId: Int = 0
    dynamic var name: String?
    dynamic var departmentImageUrl: String?
    
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        departmentId <- map["DepartmentId"]
        name <- map["Name"]
        departmentImageUrl <- map["DepartmentImageUrl"]
    }
}

class DepartmentRequestItem: Codable{
    let DepartmentId: Int?
    let Name: String?
    let DepartmentImageUrl: String?
    
    init(departmentId: Int?,
         name: String?,
         departmentImageUrl: String?){
        DepartmentId =  departmentId
        Name = name
        DepartmentImageUrl = departmentImageUrl
    }
}
