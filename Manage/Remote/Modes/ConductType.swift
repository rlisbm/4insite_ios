//
//  ConductType.swift
//  Manage
//
//  Created by Rong Li on 12/18/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

@objcMembers public class ConductType: Object, Codable {
    
    dynamic var ConductTypeId:Int = 0
    dynamic var Name: String?
    dynamic var IsSelected: Bool = true
    
    convenience init(conductTypeId: Int?, name: String?, isSelected: Bool?){
        self.init()
        self.ConductTypeId = conductTypeId!
        self.Name = name
        self.IsSelected = isSelected ?? true
    }
}

