//
//  Clients.swift
//  Manage
//
//  Created by Rong Li on 10/24/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

@objcMembers class ClientMappable: Object, Mappable {
    
    dynamic var clientsPrimaryKey: String?
    
    dynamic var Clients: NameKeyValueMappable?
    dynamic var Sites: NameKeyValueMappable?
    dynamic var Programs: NameKeyValueMappable?
    dynamic var Languages: NameKeyValueMappable?
    
    override static func primaryKey() -> String? {
        return "clientsPrimaryKey"
    }

    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        Clients <- map["Clients"]
        Sites <- map["Sites"]
        Programs <- map["Programs"]
        Languages <- map["Languages"]
    }
}

