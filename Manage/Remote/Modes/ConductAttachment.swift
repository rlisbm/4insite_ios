//
//  ConductAttachment.swift
//  Manage
//
//  Created by Rong Li on 12/18/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift
import Realm

@objcMembers class ConductAttachment: Object, Mappable {
    dynamic var conductAttachmentId: Int = 0
    dynamic var uniqueId: String?
    dynamic var name: String?
    dynamic var _description: String?
    dynamic var conductAttachmentTypeId: Int = 0
    dynamic var conductAttachmentType: String?
    dynamic var attachmentType: String?
    
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        conductAttachmentId <- map["ConductAttachmentId"]
        uniqueId <- map["UniqueId"]
        name <- map["Name"]
        _description <- map["Description"]
        conductAttachmentTypeId <- map["ConductAttachmentTypeId"]
        conductAttachmentType <- map["ConductAttachmentType"]
        attachmentType <- map["AttachmentType"]
        
    }

}


open class ConductAttachmentRequestItem: Codable {
    let ConductAttachmentId: Int?
    let UniqueId: String?
    let Name: String?
    let Description: String?
    let ConductAttachmentTypeId: Int?
    let ConductAttachmentType: String?
    let AttachmentType: String?
    
//    init(conductAttachment: ConductAttachment) {
//
//        self.conductAttachmentId = conductAttachment.conductAttachmentId
//        self.uniqueId = conductAttachment.uniqueId
//        self.name = conductAttachment.name
//        self._description = conductAttachment._description
//        self.conductAttachmentId = conductAttachment.conductAttachmentId
//        self.conductAttachmentType = conductAttachment.conductAttachmentType
//        self.attachmentType = conductAttachment.attachmentType
//    }
    
    init(conductAttachmentId: Int?,
         uniqueId: String?,
         name: String?,
         _description: String?,
         conductAttachmentTypeId: Int?,
         conductAttachmentType: String?,
         attachmentType: String?) {

        self.ConductAttachmentId = conductAttachmentId
        self.UniqueId = uniqueId
        self.Name = name
        self.Description = _description
        self.ConductAttachmentTypeId = conductAttachmentTypeId
        self.ConductAttachmentType = conductAttachmentType
        self.AttachmentType = attachmentType
    }
    
}

