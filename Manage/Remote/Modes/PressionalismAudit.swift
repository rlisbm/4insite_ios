//
//  PressionalismAudit.swift
//  Manage
//
//  Created by Rong Li on 1/18/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

open class PressionalismAudit: Codable{
    var CheckBoxSelections: [Int] = [Int]()
    var Comment: String?
    var EmployeeId: Int?
    var IsEventBasedAudit: Bool?
    
    required public init(){}
}
