//
//  TodoItem.swift
//  Manage
//
//  Created by Rong Li on 12/12/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

@objcMembers class TodoItemMappable: Object, Mappable {

    dynamic var todoItemPrimaryKey: String?

    dynamic var comment: String?
    dynamic var dueDate: String?
    dynamic var buildingId: Int = 0
    dynamic var buildingName: String?
    dynamic var floorId: Int = 0
    dynamic var floorName: String?

    var isActive: Bool?
    var isComplete: Bool?
    var dueIn24: Bool?

    dynamic var orgUserId: Int = 0  //employeeId
    var todoItemAttachments: List<TodoItemAttachmentMappable> = List<TodoItemAttachmentMappable>()
    dynamic var todoItemId: Int = 0

    override static func primaryKey() -> String? {
        return "todoItemPrimaryKey"
    }

    convenience required init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        comment <- map["Comment"]
        dueDate <- map["DueDate"]
        buildingId <- map["BuildingId"]
        floorId <- map["FloorId"]
        isActive <- map["IsActive"]
        isComplete <- map["IsComplete"]
        orgUserId <- map["OrgUserId"]
        todoItemId <- map["TodoItemId"]

        var attachments: [TodoItemAttachmentMappable] = []
        attachments <- map["TodoItemAttachments"]

        for attachment in attachments {
            todoItemAttachments.append(attachment)
        }
    }
}

open class TodoItem: Codable {
    var TodoItemId: Int?
    var ScorecardDetailId: Int?
    var OrgUserId: Int?
    var OrgUserName: String?
    var Comment: String?
    var SourceLanguageId: Int?
    var DueDate: String?
    var IsComplete: Bool?
    var IsNew: Bool?
    var LastSeenDate: String?
    var CreateDate: String?
    var UpdateDate: String?
    var CreateByAttachments: [UserAttachment]?
    
    var FloorId: Int?
    var IsActive: Bool?
    var ScorecadDetailId: Int?
    var TodoItemAttachments: [TodoItemAttachment]?
    
    
    required public init(){}
    
    init(json: [String: Any]) {
        TodoItemId = json["TodoItemId"] as? Int
        ScorecardDetailId = json["ScorecardDetailId"] as? Int
        OrgUserId = json["OrgUserId"] as? Int
        OrgUserName = json["OrgUserName"] as? String
        Comment = json["Comment"] as? String
        SourceLanguageId = json["SourceLanguageId"] as? Int
        DueDate = json["DueDate"] as? String
        IsComplete = json["IsComplete"] as? Bool
        IsNew = json["IsNew"] as? Bool
        LastSeenDate = json["LastSeenDate"] as? String
        CreateDate = json["CreateDate"] as? String
        
        if let createByAttachments = json["CreateByAttachments"] as? [[String: Any]] {
            CreateByAttachments = createByAttachments.map { UserAttachment(json: $0) }
        }
        
        FloorId = json["FloorId"] as? Int
        IsActive = json["IsActive"] as? Bool
        ScorecadDetailId = json["ScorecadDetailId"] as? Int
        
        if let todoItemAttachments = json["TodoItemAttachments"] as? [[String: Any]] {
            TodoItemAttachments = todoItemAttachments.map { TodoItemAttachment(json: $0) }
        }
    }
}
