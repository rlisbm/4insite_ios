//
//  Coordinate.swift
//  Manage
//
//  Created by Rong Li on 3/19/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation
import RealmSwift

open class Coordinate: Codable {
    var Longitude: Double?
    var Latitude: Double?
   
    public required init()  {
        
    }
    
    init(json: [String: Any]) {
        Longitude = json["Longitude"] as? Double
        Latitude = json["Latitude"] as? Double
    }
}


