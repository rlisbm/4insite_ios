//
//  Punch.swift
//  Manage
//
//  Created by Rong Li on 3/19/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation
import RealmSwift

open class Punch: Codable {
    var Coordinates: Coordinate?
    var PunchDate: String?
    var Status: String?
    var StreetAddress: String?
    
    public required init()  {
        
    }
    
    init(json: [String: Any]) {
        Coordinates = json["Coordinates"] as? Coordinate
        PunchDate = json["PunchDate"] as? String
        Status = json["Status"] as? String
        StreetAddress = json["StreetAddress"] as? String
    }
}

