//
//  DailyTimeCardStat.swift
//  Manage
//
//  Created by Rong Li on 4/1/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation
import RealmSwift

open class DailyTimeCardStat: Codable {
    var Date: String?
    var TotalTime: String?
    var Status: String?
    
    public required init()  {
        
    }
}


