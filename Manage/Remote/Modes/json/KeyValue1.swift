//
//  KeyValue.swift
//  Manage
//
//  Created by Rong Li on 3/19/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation
import RealmSwift

open class KeyValue1: Codable {
    
    var Key:Int?
    var Value: String?
    
    var Id: Int?
    var Name: String?
    
    required public init(){}
    
    init(Key: Int?, Value: String?){
        self.Key = Key
        self.Value = Value
    }
    
    init(json: [String: Any]) {
        Key = json["Key"] as? Int
        Value = json["Value"] as? String
    }
    
}


open class IdName: Codable {
    
    var Id: Int?
    var Name: String?
    
    required public init(){}
    
    init(Key: Int?, Value: String?){
        Id = Key
        Name = Value
    }
}

