//
//  ClientIdName.swift
//  Manage
//
//  Created by Rong Li on 4/8/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

open class ClientIdName: Codable {
    var ClientId: Int?
    var Name: String?
    
    public required init()  {
        
    }
    
    public init?(json: [String: Any]){
        self.ClientId = (json["ClientId"] as? Int)
        self.Name = (json["Name"] as? String)
    }
}


