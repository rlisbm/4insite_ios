//
//  Site.swift
//  Manage
//
//  Created by Rong Li on 3/26/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

import RealmSwift

open class Site: Codable {
    
    var Id: Int?
    var Name: String?
    var Longitude: Double?
    var Latitude: Double?
    var ParentId: Int?
    var ChildCount: Int?
    var ProgramCount: Int?
    
    required public init(){}
    
}





