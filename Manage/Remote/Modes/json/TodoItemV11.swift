//
//  TodoItemV1.swift
//  Manage
//
//  Created by Rong Li on 4/2/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

open class TodoItemV11: Codable {
    
    var FirstItemOnPage: Int?
    var HasPreviousPage: Bool?
    var HasNextPage: Bool?
    var IsFirstPage: Bool?
    var IsLastPage: Bool?
    var LastItemOnPage: Int?
    var PageCount: Int?
    var PageNumber: Int?
    var PageSize: Int?
    var TotalItemCount: Int?
    var Items: [Item]?
    
    required public init(){}
    
}
