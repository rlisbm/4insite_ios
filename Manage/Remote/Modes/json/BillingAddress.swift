//
//  File.swift
//  Manage
//
//  Created by Rong Li on 4/8/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

open class BillingAddress1: Codable {
    
    var Street1: String?
    var Street2: String?
    var City: String?
    var Region: String?
    var PostalCode: String?
    var Country: String?
    
    public required init(){}
    
    
    public init?(json: [String: Any]){
        self.Street1 = json["Street1"] as? String
        self.Street2 = json["Street2"] as? String
        self.City = json["City"] as? String
        self.Region = json["Region"] as? String
        self.PostalCode = json["PostalCode"] as? String
        self.Country = json["Country"] as? String
    }
}


