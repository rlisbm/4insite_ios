//
//  TeamMember.swift
//  Manage
//
//  Created by Rong Li on 11/27/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift
import AlamofireImage

open class TeamMemberV2: Codable {
    var UserId: Int?
    var EmployeeId: Int?
    var HireDate: String?
    var FirstName: String?
    var LastName: String?
    var _Type: KeyValue1?
    var Job: KeyValue1?
    var ProfileImage: KeyValue1?
    var Status: KeyValue1?    //for TeamRoster, not for TeamRosterV2 which uses LatestPunch
    var LatestPunch: Punch?
    
    public required init()  {
        
    }
    
    init(json: [String: Any]) {
        UserId = json["UserId"] as? Int
        EmployeeId = json["EmployeeId"] as? Int
        HireDate = json["HireDate"] as? String
        FirstName = json["FirstName"] as? String
        LastName = json["LastName"] as? String
        _Type = json["Type"] as? KeyValue1
        Job = json["Job"] as? KeyValue1
        ProfileImage = json["ProfileImage"] as? KeyValue1
        Status = json["Status"] as? KeyValue1
        LatestPunch = json["LatestPunch"] as? Punch
    }
}


//@objcMembers class TeamMember: Object, Mappable {
//    dynamic var userId: Int = 0
//    dynamic var employeeId: Int = 0
//    dynamic var hireDate: String?
//    dynamic var firstName: String?
//    dynamic var lastName: String?
//    dynamic var type: KeyValue?
//    dynamic var job: KeyValue?
//    dynamic var profileImage: KeyValue?
//    dynamic var status: KeyValue?
//    
//    convenience required init?(map: Map) {
//        self.init()
//    }
//    
//    func mapping(map: Map) {
//        userId <- map["UserId"]
//        employeeId <- map["EmployeeId"]
//        hireDate <- map["HireDate"]
//        firstName <- map["FirstName"]
//        lastName <- map["LastName"]
//        type <- map["Type"]
//        job <- map["Job"]
//        profileImage <- map["ProfileImage"]
//        status <- map["Status"]
//    }
//}

