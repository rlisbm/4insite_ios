//
//  Owership.swift
//  Manage
//
//  Created by Rong Li on 4/10/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

open class Ownership: Codable {
    var ClientId: Int?
    var Name: String?
    
    public required init()  {
        
    }
    
    public init?(json: [String: Any]){
        self.ClientId = (json["ClientId"] as? Int)
        self.Name = (json["Name"] as? String)
    }
}


