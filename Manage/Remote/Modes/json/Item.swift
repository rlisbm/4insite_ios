//
//  Item.swift
//  Manage
//
//  Created by Rong Li on 4/2/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

open class Item: Codable {
    
    var TodoItemId: Int?
    var OrgUserId: Int?
    var OrgUserName: String?
    var UserAttachment: [UserAttachment]?
    var Comment: String?
    var SourceLanguageId: Int?
    var DueDate: String?
    var IsComplete: Bool?
    var FloorId: Int?
    var FloorName: String?
    var BuildingId: Int?
    var BuildingName: String?
    var ScorecardDetailId: Int?
    var ScorecardId: Int?
    var CreateByName: String?
    var CreateByAttachments: [UserAttachment]?
    var CreateDate: String?
    var UpdateDate: String?
    var IsOrgUserActive: Bool?
    var IsNew: Bool?
    var LastSeenDate: String?
    var EmployeeId: Int?
    var CreateById: Int?
    var CreateByEmployeeId: Int?
    var TodoItemAttachments: [TodoItemAttachment]?
    
    
    required public init(){}
    
}
