//
//  TodoItemAttachment.swift
//  Manage
//
//  Created by Rong Li on 4/2/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

open class TodoItemAttachment: Codable {
    var TodoItemAttachmentId: Int?
    var AttachmentId: Int?
    var UniqueId: String?
    var Name: String?
    var Description: String?
    var TodoItemAttachmentTypeId: Int?
    var TodoItemAttachmentType: Int?
    var AttachmentType: String?
    
    required public init(){}
    
    init(json: [String: Any]) {
        TodoItemAttachmentId = json["odoItemAttachmentId"] as? Int
        AttachmentId = json["AttachmentId"] as? Int
        UniqueId = json["UniqueId"] as? String
        Name = json["Name"] as? String
        Description = json["Description"] as? String
        TodoItemAttachmentTypeId = json["TodoItemAttachmentTypeId"] as? Int
        TodoItemAttachmentType = json["TodoItemAttachmentType"] as? Int
        AttachmentType = json["AttachmentType"] as? String
    }
    
    
    //    var UniqueId: String?
    //    var TodoItemAttachmentTypeId: Int?
    //    var TodoItemAttachmentType: Int?
    //    var AttachmentType: String?
    //
    //    required public init(){}
    
    //    init(json: [String: Any]) {
    //        UniqueId = json["UniqueId"] as? String
    //        TodoItemAttachmentTypeId = json["TodoItemAttachmentTypeId"] as? Int
    //        TodoItemAttachmentType = json["TodoItemAttachmentType"] as? Int
    //        AttachmentType = json["AttachmentType"] as? String
    //    }
}
