//
//  SiteLocation.swift
//  Manage
//
//  Created by Rong Li on 4/8/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

open class SiteLocation: Codable {
 
    var SiteId: Int?
    var Name: String?
    var Longitude: Double?
    var Latitude: Double?
    var BillingAddress: BillingAddress1?
    var TotalBuilding: Int?
    var TotalSquareFeet: Double?
    var EmployeeCount: Int?
    var RouteCount: Int?
    var Client: ClientIdName?
    
    required public init(){}
    
    public init(json: [String: Any]){
        self.SiteId = (json["SiteId"] as? Int)!
        self.Name = (json["Name"] as? String)!
        self.Longitude = (json["Longitude"] as? Double)!
        self.Latitude = (json["Latitude"] as? Double)!
        
        let billingAddress = json["BillingAddress"] as? [String: Any]
        self.BillingAddress = BillingAddress1(json: billingAddress ?? [:])
        
        self.TotalBuilding = json["TotalBuilding"] as? Int
        self.TotalSquareFeet = json["TotalSquareFeet"] as? Double
        self.EmployeeCount = json["EmployeeCount"] as? Int
        self.RouteCount = json["RouteCount"] as? Int
        self.Client = json["Client"] as? ClientIdName
        
        let client = json["Client"] as? [String: Any]
        Client = ClientIdName(json: client ?? [:])
    }
    
}
