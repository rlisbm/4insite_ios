//
//  TeamRoster.swift
//  Manage
//
//  Created by Rong Li on 11/27/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift
import AlamofireImage

open class TeamRosterV2: Codable {
    
    var RosterCount: Int?
    var ClockedInCountForSite: Int?
    var TeamMembers: [TeamMemberV2]?
    
   required public init(){}
    
}

//@objcMembers class TeamRoster: Object, Mappable {
//    
//    dynamic var teamRosterPrimaryKey: String?
//    
//    dynamic var rosterCount: Int = 0
//    dynamic var clockedInCountForSite: Int = 0
//    dynamic var teamMembers: List<TeamMember> = List<TeamMember>()
//    
//    override static func primaryKey() -> String? {
//        return "teamRosterPrimaryKey"
//    }
//    
//    convenience required init?(map: Map) {
//        self.init()
//    }
//    
//    func mapping(map: Map) {
//        rosterCount <- map["RosterCount"]
//        clockedInCountForSite  <- map["ClockedInCountForSite"]
//       
//        var members: [TeamMember] = []
//        members <- map["TeamMembers"]
//        
//        for member in members {
//            teamMembers.append(member)
//        }
//    }
//}
//
