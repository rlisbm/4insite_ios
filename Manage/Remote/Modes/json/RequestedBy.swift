//
//  RequestedBy.swift
//  Manage
//
//  Created by Rong Li on 4/8/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

open class RequestedBy1: Codable {
    
    var EmployeeId: Int?
    var FullName: String?
    var IsActive: Bool?
    var Job: KeyValue1?
    var ProfileImage: KeyValue1?
    var _Type: KeyValue1?
    var Manager: KeyValue1?
    var Location: SiteLocation?
    var DeviceTypes: IdTextText?
    
    required public init(){}
    
    init(json: [String: Any]){
        self.EmployeeId = (json["EmployeeId"] as? Int)
        self.FullName = (json["FullName"] as? String)
        self.IsActive = (json["IsActive"] as? Bool)!
        
        let profileImage = json["ProfileImage"] as? [String: Any]
        ProfileImage = KeyValue1(json: profileImage ?? [:])
        
        let _type = json["Type"] as? [String: Any]
        _Type = KeyValue1(json: _type ?? [:])
        
        let manager = json["Manager"] as? [String: Any]
        Manager = KeyValue1(json: manager ?? [:])
        
        let location = json["Location"] as? [String: Any]
        Location = SiteLocation(json: location ?? [:])
        
        let deviceTypes = json["DeviceTypes"] as? [String: Any]
        DeviceTypes = IdTextText(json: deviceTypes ?? [:])
    }
    
}
