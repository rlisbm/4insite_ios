//
//  Client.swift
//  Manage
//
//  Created by Rong Li on 3/26/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation
import RealmSwift

open class Client: Codable {
    
    var Clients: NameKeyValue?
    var Sites: NameKeyValue?
    var Programs: NameKeyValue?
    var Languages: NameKeyValue?
    
    required public init(){}

}

