//
//  AuditorAttachment.swift
//  Manage
//
//  Created by Rong Li on 1/24/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

open class UserAttachment: Codable {
    var AttachmentType: String?
    var Description: String?
    var Name: String?
    var UniqueId: String?
    var UserAttachmentId: Int?
    var UserAttachmentType: String?
    
    required public init(){}
    
    init(json: [String: Any]) {
        AttachmentType = json["AttachmentType"] as? String
        Description = json["Description"] as? String
        Name = json["Name"] as? String
        UniqueId = json["UniqueId"] as? String
        UserAttachmentId = json["UserAttachmentId"] as? Int
        UserAttachmentType = json["UserAttachmentType"] as? String
    }
}
