//
//  ProcessedBy.swift
//  Manage
//
//  Created by Rong Li on 4/8/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

open class ProcessedBy: Codable {
    
    var UserId: Int?
    var FullName: String?
    var IsActive: Bool?
    var ProfileImage: KeyValue1?
    
    required public init(){}
    
    public init?(json: [String: Any]){
        self.UserId = (json["UserId"] as? Int)
        self.FullName = (json["FullName"] as? String)
        self.IsActive = (json["IsActive"] as? Bool)
        
        let profileImage = json["ProfileImage"] as? [String: Any]
        ProfileImage = KeyValue1(json: profileImage ?? [:])
    }
    
}
