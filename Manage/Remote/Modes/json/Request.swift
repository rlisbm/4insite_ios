//
//  Request.swift
//  Manage
//
//  Created by Rong Li on 4/8/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

open class Request: Codable {
    
    var RequestId: Int?
    var IsCritical: Bool?
    var IsActive: Bool?
    var SiteId: Int?
    var Status: KeyValue1?
    var RequestType: KeyValue1?
    var RequestedBy: RequestedBy1?
    var Description: String?
    var OriginalDescription: String?
    var SourceLanguageId: Int?
    var ProcessedBy: ProcessedBy?
    var ProcessedDate: String?
    var ProcessorComment: String?
    var OriginalProcessorComment: String?
    var CreateDate: String?
    var RequestedByProfileImages: [UserAttachment]?
    var ProcessedByProfileImages: [UserAttachment]?

    
    required public init(){}
    
    init(json: [String: Any]) {
        RequestId = json["RequestId"] as? Int
        IsCritical = json["IsCritical"] as? Bool
        IsActive = json["IsActive"] as? Bool
        SiteId = json["SiteId"] as? Int
        let status = json["Status"] as? [String: Any]
        Status = KeyValue1(json: status ?? [:])
        let requestType = json["RequestType"] as? [String: Any]
        RequestType = KeyValue1(json: requestType ?? [:])
        
        let requestedBy = json["RequestedBy"] as? [String: Any]
        RequestedBy = RequestedBy1(json: requestedBy ?? [:])
        
        
        Description = json["Description"] as? String
        OriginalDescription = json["OriginalDescription"] as? String
        SourceLanguageId = json["SourceLanguageId"] as? Int
        
        
        ProcessedBy = json["ProcessedBy"] as? ProcessedBy
        
        
        ProcessedDate = json["ProcessedDate"] as? String
        ProcessorComment = json["ProcessorComment"] as? String
        OriginalProcessorComment = json["OriginalProcessorComment"] as? String
        CreateDate = json["CreateDate"] as? String
        
        if let requestedByProfileImages = json["RequestedByProfileImages"] as? [[String: Any]] {
            RequestedByProfileImages = requestedByProfileImages.map{ UserAttachment(json: $0) }
        }
        
        if let processedByProfileImages = json["ProcessedByProfileImages"] as? [[String: Any]] {
            ProcessedByProfileImages = processedByProfileImages.map{ UserAttachment(json: $0) }
        }
        
    }
}
