//
//  Scorecard.swift
//  Manage
//
//  Created by Rong Li on 1/24/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation

//open class Scorecard: Codable{
open class Scorecard {
    var AuditDate: String?
//    var AuditType: KeyValueRequestItem = KeyValueRequestItem()
    var AuditType: [String: Any]?
    var AuditorAttachments: [UserAttachment] = [UserAttachment]()
//    var AuditorAttachments: [UserAttachment]?
    var AuditorId: Int?
    var AuditorName: String?
    var Average: Double?
    var BuildingId: Int?
    var BuildingName: String?
    var Comment: String?
//    var CustomerRepresentatives: [CustomerRepresentativeItem] = [CustomerRepresentativeItem]()
//    var CustomerRepresentatives: [CustomerRepresentativeItem]?
    var CustomerRepresentatives: [CustomerRepresentative]?
    var FloorId: Int?
    var FloorName: String?
    var InProgress: Bool?
    var IsJointAudit: Bool?
    var MaximumScore: Int?
    var MinimumScore: Int?
    var ParentScorecardId: Int?
    var ProfileId: Int?
    var ProgramId: Int?
    var ProgramName: String?
    var ScorecardId: Int?
//    var ScorecardSections: [ScorecardSection] = [ScorecardSection]()
    var ScorecardSections: [ScorecardSection]?
    var SiteId: Int?
    var SiteName: String?
    var SourceLanguageId: Int?
    var UpdateDate: String?
    
    required public init(){}
    
    init(data: Data){
        do {
            if let body = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                AuditDate = body["AuditDate"] as? String
                AuditType = body["AuditType"] as? [String: Any]

                if let auditorAttachments = body["AuditorAttachments"] as? [[String: Any]] {
                    AuditorAttachments = auditorAttachments.map { UserAttachment(json: $0) }
                }
         
                AuditorId = body["AuditorId"] as? Int
                AuditorName = body["AuditorName"] as? String
                Average = body["Average"] as? Double
                BuildingId = body["BuildingId"] as? Int
                BuildingName = body["BuildingName"] as? String
                Comment = body["Comment"] as? String

                if let customerRepresentatives = body["CustomerRepresentatives"] as? [[String: Any]] {
//                    CustomerRepresentatives = customerRepresentatives.map { CustomerRepresentativeItem(json: $0) }
                    CustomerRepresentatives = customerRepresentatives.map { CustomerRepresentative(json: $0) }
                }
                
                FloorId = body["FloorId"] as? Int
                FloorName = body["FloorName"] as? String
                InProgress = body["InProgress"] as? Bool
                IsJointAudit = body["IsJointAudit"] as? Bool
                MaximumScore = body["MaximumScore"] as? Int
                MinimumScore = body["MinimumScore"] as? Int
                ParentScorecardId = body["ParentScorecardId"] as? Int
                ProfileId = body["ProfileId"] as? Int
                ProgramId = body["ProgramId"] as? Int
                ProgramName = body["ProgramName"] as? String
                ScorecardId = body["ScorecardId"] as? Int
                
                if let scorecardSections = body["ScorecardSections"] as? [[String: Any]] {
                    ScorecardSections = scorecardSections.map { ScorecardSection(json: $0) }
                }
                
                SiteId = body["SiteId"] as? Int
                SiteName = body["SiteName"] as? String
                SourceLanguageId = body["SourceLanguageId"] as? Int
                UpdateDate = body["UpdateDate"] as? String
            }
        } catch {
            print("Error deserializing JSON: \(error)")

        }

    }
}
