//
//  EmployeeAudit.swift
//  Manage
//
//  Created by Rong Li on 12/13/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

@objcMembers class EmployeeAudit: Object, Mappable {

    dynamic var employeeAuditId: Int = 0
    dynamic var employeeId: Int = 0
    var isPass: Bool?
    var isEventBasedAudit: Bool?
    dynamic var comment: String?
    var kpiOptions: List<KpiOption> = List<KpiOption>()
    var checkBoxOptions: List<CheckBoxOption> = List<CheckBoxOption>()
    var checkBoxSelections: List<Int> = List<Int>()
    dynamic var createBy: String?
    dynamic var createDate: String?
    dynamic var sourceLanguageId: Int = 1
    dynamic var updateBy: String?
    dynamic var updateDate: String?
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        employeeAuditId <- map["EmployeeAuditId"]
        employeeId <- map["EmployeeId"]
        isPass <- map["IsPass"]
        isEventBasedAudit <- map["IsEventBasedAudit"]
        comment <- map["Comment"]
//        kpiOptions <- map["KpiOptions"]
//        checkBoxOptions <- map["CheckBoxOptions"]
//        checkBoxSelections <- map["CheckBoxSelections"]
        createBy <- map["CreateBy"]
        createDate <- map["CreateDate"]
        sourceLanguageId <- map["SourceLanguageId"]
        updateBy <- map["UpdateBy"]
        updateDate <- map["UpdateDate"]
        
        
        var options: [KpiOption] = []
        options <- map["KpiOptions"]
        for option in options {
            kpiOptions.append(option)
        }
        
        var checkOptions: [CheckBoxOption] = []
        checkOptions <- map["CheckBoxOptions"]
        for checkOption in checkOptions {
            checkBoxOptions.append(checkOption)
        }
        
        var selections: [Int] = []
        selections <- map["CheckBoxOptions"]
        for selection in selections {
            checkBoxSelections.append(selection)
        }
    }
}

