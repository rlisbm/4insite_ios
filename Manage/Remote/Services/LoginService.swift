//
//  LoginService.swift
//  Manage
//
//  Created by Rong Li on 10/24/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireObjectMapper
import SwiftKeychainWrapper

class LoginService: BaseService{
    
    let remoteManager: RemoteManager
    
    init(remoteManager: RemoteManager){
        self.remoteManager = remoteManager
    }
    
    func login(remoteManager: RemoteManager, userName: String, password: String, completion: @escaping SBMTypealias.LoginResult){
        print("### ### login")
        let url = getUrl(at: .login, baseUrl: remoteManager.baseUrl)
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters = [
            "username": userName, //userName
            "password": password, //password
            "grant_type": "password"
            
        ]
        print(url)
        Alamofire.request(url, method:.post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers)
            .responseData { response in
                var statusCode = response.response?.statusCode
                print("### ### login: \(statusCode)")
                switch response.result{
                case .success(let value):
//                    print("### ### am i here \(response.response?.description)")
                    if(response.response?.statusCode == Constants.ResponseOK){
                        KeychainWrapper.standard.removeAllKeys()
                        if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                            var sbmToken = utf8Text.parse(to: SBMToken.self)
                            
                            //save token
                            let token = sbmToken!.token_type + " " + sbmToken!.access_token
//                            print("### ### *** new \(token)")
//                            let old = KeychainWrapper.standard.string(forKey: "access_token")
//                            print("### ### *** old \(old)")
//                            if(old == token){
//                                print("### ### *** old == new")
//                            }else{
//                                print("### ### *** old != new")
//                            }
                            
                            let saveToken: Bool = KeychainWrapper.standard.set(token, forKey:"access_token")
                            //save expiration in seconds
                            let expirationTime = Date().toSeconds() + sbmToken!.expires_in
//                            let expirationTime = Date().toSeconds() + 60 * 1
                            let saveExpirationTime: Bool = KeychainWrapper.standard.set(String(expirationTime), forKey:"expires_in")
                            
                            
                            //save userName and password
                            let saveUserName: Bool = KeychainWrapper.standard.set(userName, forKey:"user_name")
                            let savePassword: Bool = KeychainWrapper.standard.set(password, forKey:"password")
                            
    //                        let loginCredentials = LoginCredentials(userName: userName, password: password)
    //                        LoginCredentialsHelper.save(loginCredentials)
    //
    //                        if(SBMTokenHelper.get() == nil){
    //                            let expirationTime = Date().toSeconds() + 30
    //                            sbmToken?.expires_in = expirationTime
    //                        }else{
    //                        let expirationTime = Date().toSeconds() + sbmToken!.expires_in
    //                        sbmToken?.expires_in = expirationTime
    //                        }
    //                        SBMTokenHelper.save(sbmToken)
                            
                            completion(sbmToken, nil, statusCode ?? Constants.UnKnowFailureCode)
                        }
                    }else{
                        completion(nil, nil, statusCode ?? Constants.UnKnowFailureCode)
                    }
                    break
                case .failure(let error):
//                    if let error = response.result.error as? AFError{
//                        statusCode = error._code
//                        switch error {
//                        case .invalidURL(let url):
//                            print("### Invalid URL: \(url) - \(error.localizedDescription)")
//                        case .parameterEncodingFailed(let reason):
//                            print("### Parameter encoding failed: \(error.localizedDescription)")
//                            print("### Failure Reason: \(reason)")
//                        case .multipartEncodingFailed(let reason):
//                            print("### Multipart encoding failed: \(error.localizedDescription)")
//                            print("### Failure Reason: \(reason)")
//                        case .responseValidationFailed(let reason):
//                            print("### Response validation failed: \(error.localizedDescription)")
//                            print("### Failure Reason: \(reason)")
//
//                            switch reason {
//                            case .dataFileNil, .dataFileReadFailed:
//                                print("### Downloaded file could not be read")
//                            case .missingContentType(let acceptableContentTypes):
//                                print("### Content Type Missing: \(acceptableContentTypes)")
//                            case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
//                                print("### Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)")
//                            case .unacceptableStatusCode(let code):
//                                print("### Response status code was unacceptable: \(code)")
//                                statusCode = code
//                            }
//                        case .responseSerializationFailed(let reason):
//                            print("### Response serialization failed: \(error.localizedDescription)")
//                            print("### Failure Reason: \(reason)")
//                        }
//
//                    }else if let error = response.result.error as? URLError {
//                        statusCode = error.code.rawValue
//                        print("### URLError occurred: \(error)  \(error.code.rawValue)")
//                    } else {
//                        statusCode = Constants.UnKnowFailureCode
//                        print("### Unknown error: \(response.result.error)")
//                    }
                    
                    
                    statusCode = self.getFailureStatusCode(response: response)
       
//                    print(statusCode!)
                    
                    completion(nil, error, statusCode!)
                }
        }
    }
    
    func fetchAssociateInfo(remoteManager: RemoteManager, token: String, completion: @escaping SBMTypealias.AssociateInfoResult) {
        print("### ### fetchAssociateInfo")
        let url = getUrl(at: .associate, baseUrl: remoteManager.baseUrl)
        let headers = [
            "Authorization": token,
            "Content-Type": "application/json"
        ]
        Alamofire.request(url, method:.get, headers: headers).validate().responseObject { (response: DataResponse<AssociateInfo>) in
            let statusCode = (response.response?.statusCode)!
            print("### ### fetchAssociateInfo: \(statusCode)")
            switch(response.result){
            case .success(let value):
                //save associate info
                let associate = Associate(employeeId: value.employeeInfo?.employeeId,
                                          clientId: value.contextInfo?.clientId,
                                          siteId: value.contextInfo?.siteId,
                                          firstName: value.personInfo?.firstName ?? "",
                                          lastName: value.personInfo?.lastName ?? "",
                                          fullName: value.personInfo?.fullName ?? "",
                                          email: value.personInfo?.email ?? "",
                                          profilePictureUniqueId: value.personInfo?.profilePictureUniqueId ?? "",
                                          jobTitle: value.employeeInfo?.positionInfo?[0].title ?? "")
                AssociateHelper.save(associate)
                print("########## count \(value.employeeInfo?.positionInfo!.count)")
                print("########## title \(value.employeeInfo?.positionInfo![0].title)")
                
                completion(value, nil, statusCode)
                break
            case .failure(let error):
                print(error)
                completion(nil, error, statusCode)
                break
            }
        }
    }
}
