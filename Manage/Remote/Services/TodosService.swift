//
//  TodosService.swift
//  Manage
//
//  Created by Rong Li on 12/6/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireObjectMapper
import SwiftKeychainWrapper
import RealmSwift
import Realm

class TodosService: LookupBaseService {
    
    let remoteManager: RemoteManager
    
    init(remoteManager: RemoteManager){
        self.remoteManager = remoteManager
        super.init(lookupRemoteManager: remoteManager)
        
    }
    
    fileprivate func saveItem(todoItem: TodoItemMappable){
        todoItem.todoItemPrimaryKey = String(Date().toMillis())
        realmDB.insertObject(todoItem)
    }
    
    func submitTodoItem(todoItem: TodoItemMappable, completion: @escaping SBMTypealias.ResponseResult<TodoItemIdMappable>){
        
        let JSONString = todoItem.toJSONString(prettyPrint: true)
        print(JSONString?.convertToDictionary())
        
        let parameters = JSONString?.convertToDictionary()
        
        let url = getUrl(at: .todoItem, baseUrl: remoteManager.baseUrl)
        let headers = getHeaders()
        Alamofire.request(url, method:.post, parameters: parameters, encoding:  JSONEncoding.default, headers: headers)
            .responseString { response in
                print("### ### Success: \(response.result.isSuccess)")
                print("### ### Response String: \(String(describing: response.result.value))")
                
                
        }
        
        Alamofire.request(url, method:.post, parameters: parameters, encoding:  JSONEncoding.default, headers: headers)
            .validate().responseObject { (response: DataResponse<TodoItemIdMappable>) in
                
                switch(response.result){
                case .success(let value):
                    let statusCode = (response.response?.statusCode)!
                    if(statusCode != Constants.ResponseOK){
                        self.saveItem(todoItem: todoItem)
                    }
                    completion(value, nil, statusCode)
                    break
                case .failure(let error):
                    self.saveItem(todoItem: todoItem)
                    completion(nil, error, -1)
                    break
                    
                }
        }
        
        
        
    }
    
//    func fetchBuildings(clientId: Int, siteId: Int,  completion: @escaping SBMTypealias.ResponseResult<NameKeyValue>){
//        let token = getToken()
//        if(token == ""){
//            return
//        }
//        let url = getUrl(at: .buildings, baseUrl: remoteManager.baseUrl)
//
//        let headers = getHeaders()
//
////        Alamofire.request(url, method:.get, headers: headers)
////            .responseString { response in
////                print("### ### Success: \(response.result.isSuccess)")
////                print("### ### Response String: \(String(describing: response.result.value))")
////            }
//
//        Alamofire.request(url, method:.get, headers: headers).validate().responseObject { (response: DataResponse<NameKeyValue>) in
//
//            switch(response.result){
//            case .success(let value):
//                let statusCode = (response.response?.statusCode)!
//                if(statusCode == self.resultOK){
//                    print("################ 111 \(value.Name)  \(value.Options.count)")
//                    print("################ 111  \(value.Options[0].Value)  \(value.Options[1].Value)")
//                    let fetchedBuidings = value
//                    completion(fetchedBuidings, nil, statusCode)
//                }
//                break
//            case .failure(let error):
//                print(error)
//                break
//            }
//
//
////            let primaryKey = "\(String(self.getEmployeeId()))\(siteId)"
////            let storedPrograms = self.realmDB.getObject(type: ProgramsObject.self, primaryKey: primaryKey)
////            switch(response.result){
////            case .success(let value):
////                if(statusCode == self.resultOK){
////                    let fetchedPrograms = ProgramsObject();
////                    fetchedPrograms.programsPrimaryKey = primaryKey
////                    for program in value {
////                        fetchedPrograms.programs.append(program)
////                    }
////                    if(storedPrograms == nil){
////                        self.realmDB.insertObject(fetchedPrograms)
////                    }else{
////                        self.realmDB.updateObject(fetchedPrograms)
////                    }
////                    completion(fetchedPrograms.programs, nil, statusCode)
////                }else{
////                    completion(storedPrograms!.programs, nil, statusCode)
////                }
////                break
////            case .failure(let error):
////                print(error)
////                completion(storedPrograms!.programs, error, statusCode)
////                break
////            }
//        }
//    }
//
//    func fetchFloors(clientId: Int, siteId: Int, parentId: Int, filter: String, orderBy: String, skip: Int, completion: @escaping SBMTypealias.ResponseResults<Floor>){
//        let token = getToken()
//        if(token == ""){
//            return
//        }
//        let url = getUrl(at: .floors, baseUrl: remoteManager.baseUrl)
//
//        let headers = getHeaders()
//
//        Alamofire.request(url, method:.get, headers: headers).validate().responseArray { (response: DataResponse<[Floor]>) in
//
//            switch(response.result){
//            case .success(let value):
//                let statusCode = (response.response?.statusCode)!
//                if(statusCode == self.resultOK){
//
//                    let fetchedFloors = FloorsObject()
//                    fetchedFloors.floorsPrimaryKey = "primaryKey xxxxxx"
//                    for floor in value {
//                        fetchedFloors.floors.append(floor)
//                    }
//
//                    completion(fetchedFloors.floors, nil, statusCode)
//
//                }
//                break
//            case .failure(let error):
//                print(error)
//                break
//            }
//        }
//    }
    
}

//extension String {
//    func convertToDictionary() -> [String: Any]? {
//        if let data = self.data(using: .utf8) {
//            do {
//                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
//            } catch {
//                print(error.localizedDescription)
//            }
//        }
//        return nil
//    }
//}
