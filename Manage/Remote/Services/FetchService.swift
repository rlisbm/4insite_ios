//
//  FetchService.swift
//  Manage
//
//  Created by Rong Li on 12/21/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireObjectMapper
import SwiftKeychainWrapper
import RealmSwift
import Realm

//class FetchService: BaseService {
class FetchService: LookupBaseService {

    let remoteManager: RemoteManager
    
    init(remoteManager: RemoteManager){
        self.remoteManager = remoteManager
        super.init(lookupRemoteManager: remoteManager)
    }
    
//    func fetchDepartments(completion: @escaping SBMTypealias.ResponseString){
//        let url = getUrl(at: .department, baseUrl: remoteManager.baseUrl)
//        let headers = getHeaders()
//
//        Alamofire.request(url, method:.get, headers: headers)
//            .responseString { response in
//            print("### ### Success: \(response.result.isSuccess)")
//            print("### ### Response String: \(String(describing: response.result.value))")
//                switch(response.result){
//                case .success(let value):
//                    let statusCode = (response.response?.statusCode)!
//                    completion(value, nil, statusCode)
//                    break
//                case .failure(let error):
//                    print(error)
//                    break
//                }
//        }
//    }
    
    func fetchDepartments(apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseDatum<Department>){

        let url = getUrl(at: apiRouter, baseUrl: remoteManager.baseUrl)
        let headers = getHeaders()

//        Alamofire.request(url, method:.get, headers: headers)
//            .responseString { response in
//                print("### ### Success: \(response.result.isSuccess)")
//                print("### ### Response String: \(String(describing: response.result.value))")
//        }

        Alamofire.request(url, method:.get, encoding:  JSONEncoding.default, headers: headers).responseArray { (response: DataResponse<[Department]>) in
            

            let primaryKey = "primarykey"  //TODO: need to have real primaryKey here
            let storedPrograms = self.realmDB.getObject(type: ProgramsObject.self, primaryKey: primaryKey)

            switch(response.result){
            case .success(let value):
                print("### ### Success: \(response.result.isSuccess)")
                print("### ### Response String: \(String(describing: response.result.value))")
                let statusCode = (response.response?.statusCode)!
                if(statusCode == Constants.ResponseOK){
                    let fetchedDepartments = DepartmentsObject() //TODO: this object is for store in db purpose

                    for department in value {
                        fetchedDepartments.departments.append(department)
                    }

                    print("### ### Response String: \(String(describing: response.result.value?.toJSON()))")
                    completion(value, nil, statusCode)

                }
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    func fetchSiteJobs(apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseDatum<JobForSite>){
        let url = getUrl(at: apiRouter, baseUrl: remoteManager.baseUrl)
        let headers = getHeaders()
        let siteId = SelectedSiteHelper.get()?.selectedSiteId
        let parameters = [
            "SiteIds": [siteId]
            ]
        
//        Alamofire.request(url, method:.post, parameters: parameters, encoding:  JSONEncoding.default, headers: headers)
//            .responseJSON { response in
//                print("### ### Success: \(response.result.isSuccess)  ")
//                print("### ### Response String: \(String(describing: response.result.value))")
//        }
        
        Alamofire.request(url, method:.post, parameters: parameters, encoding:  JSONEncoding.default, headers: headers)
            .responseArray { (response: DataResponse<[JobForSite]>) in
            switch(response.result){
            case .success(let value):
                print("### ### Success: \(response.response?.statusCode)")
                let statusCode = (response.response?.statusCode)!
                if(statusCode == Constants.ResponseOK){
//                    let fetchedDepartments = DepartmentsObject() //TODO: this object is for store in db purpose
//                    for department in value {
//                        fetchedDepartments.departments.append(department)
//                    }
                    
//                    print("### ### Response String: \(String(describing: response.result.value?.toJSON()))")
                    print("### ### \(value.count)")
                    completion(value, nil, statusCode)
                    
                }
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    func fetchComplaintTypes(programId: Int, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseResult<NameKeyValueMappable>){
        let url = getUrl(at: .compliantTypes, baseUrl: remoteManager.baseUrl, id: programId, placeholder: "{programId}")
        fetchNameKeyValue(url: url, completion: completion)
    }

    func fetchNameKeyValue(apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseResult<NameKeyValueMappable>){
        let url = getUrl(at: apiRouter, baseUrl: remoteManager.baseUrl)
        fetchNameKeyValue(url: url, completion: completion)
    }
    
    fileprivate func fetchNameKeyValue(url: String, completion: @escaping SBMTypealias.ResponseResult<NameKeyValueMappable>){
        let headers = getHeaders()
        
                Alamofire.request(url, method:.get, headers: headers)
                    .responseString { response in
                        print("### ### Success: \(response.result.isSuccess)")
                        print("### ### Response String: \(String(describing: response.result.value))")
                }
        Alamofire.request(url, method:.get, headers: headers).validate().responseObject { (response: DataResponse<NameKeyValueMappable>) in
            
            switch(response.result){
            case .success(let value):
                let statusCode = (response.response?.statusCode)!
                if(statusCode == Constants.ResponseOK){
                    let statusCode = (response.response?.statusCode)!
                    if(statusCode == Constants.ResponseOK){
                        let fetchedComplaintTypes = value

                        completion(fetchedComplaintTypes, nil, statusCode)
                    }
                }
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    
    fileprivate func fetchFromDB() -> Scorecard{
        var scorecard: Scorecard = Scorecard()
        return scorecard
    }
    func fetchScorecards(apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseData<Scorecard>){
        let url = getUrl(at: apiRouter, baseUrl: remoteManager.baseUrl)
        let headers = getHeaders()
        
        print("### ### url: \(String(describing: url))")
        
        let parameters = [
            "FloorId": 28998,
            "InProgress": true
            ] as [String : Any]
        
//        let primaryKey = String(siteId)
//        let storedObject = self.realmDB.getObject(type: SiteReportSettingObject.self, primaryKey: primaryKey)
        
        Alamofire.request(url, method:.post, parameters: parameters, encoding:  JSONEncoding.default, headers: headers)
            .responseJSON { response in
//                do {
//                    if let body = try JSONSerialization.jsonObject(with: response.data!) as? [String: Any] {
//                        let scorecard = Scorecard()
//                        scorecard.AuditType = body["AuditType"] as? [String: Any]
//                        scorecard.AuditorAttachments = body["AuditorAttachments"] as! [UserAttachment]
//                    }
//                } catch {
//                    print("Error deserializing JSON: \(error)")
//                }
//                let scorecard = Scorecard(data: response.data!)
//                print(scorecard.AuditDate)
                
                let storedScorecard:SiteReportSetting?
                switch(response.result){
                case .success:
                    let statusCode = (response.response?.statusCode)!
                    if(statusCode == Constants.ResponseOK){
                        let fetchedScorecard = Scorecard(data: response.data!)
                        completion(fetchedScorecard, nil, statusCode)
                    }else{
//                        completion(storedScorecard?, nil, statusCode)
                    }
                    break
                case .failure(let error):
                    print(error)
//                    completion(storedScorecard?, nil, Constants.ConnectivityFailureCode)
                    break
                }
        }
    }
    func fetchSiteReportSetting(siteId: Int, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseData<SiteReportSetting>){
        let url = getUrl(at: apiRouter, baseUrl: remoteManager.baseUrl, id: siteId, placeholder: "{siteId}")
        let headers = getHeaders()
        
        let primaryKey = String(siteId)
        let storedObject = self.realmDB.getObject(type: SiteReportSettingObject.self, primaryKey: primaryKey)
        Alamofire.request(url, method:.get, headers: headers)
            .responseJSON{ response in
                switch(response.result){
                case .success:
                    let statusCode = (response.response?.statusCode)!
                    if(statusCode == Constants.ResponseOK){
                        let object = SiteReportSettingObject()
                        object.siteReportSettingObjectPrimaryKey = primaryKey
                        object.data = response.data
                        if(storedObject == nil){
                            self.realmDB.insertObject(object)
                        }else{
                            self.realmDB.updateObject(object)
                        }
                        let fetchedItem = SiteReportSetting(data: response.data!)
                        completion(fetchedItem, nil, statusCode)
                    }else{
                        if (storedObject != nil){
                            let storedItem =  SiteReportSetting(data: (storedObject?.data)!)
                            completion(storedItem, nil, statusCode)
                        }else{
                            //TODO: error handling
                        }
                    }
                    break
                case .failure(let error):
                    print(error)
                    if(storedObject != nil){
                        let storedItem =  SiteReportSetting(data: (storedObject?.data)!)
                        completion(storedItem, nil, Constants.ConnectivityFailureCode)
                    }else{
                        //TODO: error handling
                    }
                    break
                }
        }
    }
    
    ///// *** JSON *** /////
    func fetchCustomerRepresentatives(apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseJSON){
        let url = getUrl(at: apiRouter, baseUrl: remoteManager.baseUrl)
        let headers = getHeaders()
        
        let primaryKey = String(SelectedSiteHelper.get()!.selectedSiteId!)+String(SelectedSiteHelper.get()!.selectedProgramId!)
        let storedObject = self.realmDB.getObject(type: CustomerRepresentativeObject.self, primaryKey: primaryKey)

        Alamofire.request(url, method:.get, headers: headers)
            .responseJSON { response in
//                print("### ### Success: \(response.result.isSuccess)  ")
//                print("### ### Response String: \(String(describing: response.result.value))")
                switch(response.result){
                case .success:
                    let statusCode = (response.response?.statusCode)!
                    if(statusCode == Constants.ResponseOK){
                        let object = CustomerRepresentativeObject()
                        object.customerRepresentativeObjectPrimaryKey = primaryKey
                        object.data = response.data
                        if(storedObject == nil){
                            self.realmDB.insertObject(object)
                        }else{
                            self.realmDB.updateObject(object)
                        }
                        completion(response.data, nil, statusCode)
                    }else{
                        if (storedObject != nil){
                            completion(storedObject?.data, nil, statusCode)
                        }else{
                            //TODO: error handling
                        }
                    }
                    break
                case .failure(let error):
                    print(error)
                    if(storedObject != nil){
                        completion(storedObject?.data, nil, Constants.ConnectivityFailureCode)
                    }else{
                        //TODO: error handling
                    }
                    break
                }
        }
    }
    
//    fileprivate func getPrimaryKey(apiRouter: APIRouter, id: Int?) -> String?{
//        switch apiRouter {
//        case APIRouter.clients:
//            return "clients"+String(self.getEmployeeId())
//        case APIRouter.sites:
//            return "sites"+String(id!)
//        case APIRouter.programs:
//            return "programs"+String(id!)
//        case APIRouter.teamRosterV2:
//            return "teamRoster"+String(SelectedSiteHelper.get()!.selectedSiteId!)+String(SelectedSiteHelper.get()!.selectedProgramId!)
//        case APIRouter.dailyTimeCardStats:
//            return "dailyTimeCardStats"+String(SelectedSiteHelper.get()!.selectedSiteId!)+String(SelectedSiteHelper.get()!.selectedProgramId!)
//        default:
//            return nil
//        }
//    }
//    
//    fileprivate func getJSONFromDB(objectType: Object.Type, primaryKey: String) -> Object?{
//        return realmDB.getObject(type: objectType, primaryKey: primaryKey)
//    }
    
    
    func fetchDataWithDate(apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseJSONWithDate){
        if(apiRouter == APIRouter.todoItemV11 || apiRouter == APIRouter.todoItem){
            fetchDataWithParams(id: nil, apiRouter: apiRouter, completion: completion)
            return
        }
        
        fetchDataWithDate(id: nil, apiRouter: apiRouter, completion: completion)
    }
    
    func fetchDataWithDate(parameters: [String: Any], apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseJSONWithDate){
        fetchDataWithDate(id: nil, parameters: parameters, pageNumber: nil, apiRouter: apiRouter, completion: completion)
    }
    
    func fetchDataWithDate(parameters: [String: Any], pageNumber: Int, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseJSONWithDate){
        fetchDataWithDate(id: nil, parameters: parameters, pageNumber: pageNumber, apiRouter: apiRouter, completion: completion)
    }
    
    func fetchDataWithDate(id: Int?, parameters: [String: Any], pageNumber: Int?, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseJSONWithDate){
        var url: String
        var headers: [String: String]
        
        switch apiRouter {
        case APIRouter.todoItemV11: //https://prod-web.sbminsite.com/api/v1.1/Services/TodoItem?startDate=2019-03-02T22%3A50%3A38.532Z&endDate=2019-04-02T22%3A50%3A38.532Z&isComplete=false&sortByColumn=DueDate&ascending=true
            url = getUrl(at: apiRouter, baseUrl: remoteManager.baseUrl)
            headers = getHeaders()
            break
        default:
            url = getUrl(at: apiRouter, baseUrl: remoteManager.baseUrl)
            headers = getHeaders()
            break
        }
        let primaryKey = getPrimaryKey(apiRouter: apiRouter, id: id, pageNumber: pageNumber)
        let storedObject = getJSONFromDB(objectType: JSONData.self, primaryKey: primaryKey!) as? JSONData
        print("######### 1")
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers)
            .responseJSON { response in
//                print("### ### Success: \(response.result.isSuccess)  ")
//                print("### ### Response String: \(String(describing: response.result.value))")
//                print("### ### request url: \(String(describing: response.request?.url))")
                
                print("######### 2 url: \(response.request?.url)")
                let heads = response.request?.allHTTPHeaderFields
                print(heads)
                print("######### 3")
                
                
                self.handleResponse(response: response, primaryKey: primaryKey!, storedObject: storedObject, apiRouter: apiRouter, completion: completion)
                
        }
    }
    
    func fetchDataWithParams(id: Int?, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseJSONWithDate){
        var url: String
        var headers: [String: String]
        var parameters: [String: String]?
        
        switch apiRouter {
    
        case APIRouter.todoItem:
            //https://r2.sbminsite.com/api/services/ToDoItem?$orderby=DueDate%20desc&$filter=DueDate%20ge%20DateTime%272019-04-01%27%20and%20DueDate%20le%20DateTime%272019-04-30%27
            let date = Date().back180DayDate
            let startDate = date.formateDateUTC(date: date, withFormat: Constants.DATE_UTC_SSS)
            let filter = "CreateDate gt DateTime'" + startDate + "' and IsComplete eq false"
            parameters = [
                "orderBy": "UpdateDate desc",
                "filter": filter
            ]
            url = getUrl(at: apiRouter, baseUrl: remoteManager.baseUrl)
            headers = getHeaders()
            break
        case APIRouter.todoItemV11:
            //https://prod-web.sbminsite.com/api/v1.1/Services/TodoItem?startDate=2019-03-02T22%3A50%3A38.532Z&endDate=2019-04-02T22%3A50%3A38.532Z&isComplete=false&sortByColumn=DueDate&ascending=true
            let endDate = Date()
            let startDate = endDate.back180DayDate
            let start = startDate.formateDateUTC(date: startDate, withFormat: Constants.DATE_UTC_SSSZ)
            let end = endDate.formateDateUTC(date: endDate, withFormat: Constants.DATE_UTC_SSSZ)
            parameters = [
                "startDate": start,
                "endDate": end
            ]
            url = getUrl(at: apiRouter, baseUrl: remoteManager.baseUrl)
            headers = getHeaders()
            break
        default:
            url = getUrl(at: apiRouter, baseUrl: remoteManager.baseUrl)
            headers = getHeaders()
            break
        }
        
        let primaryKey = getPrimaryKey(apiRouter: apiRouter, id: id)
        let storedObject = getJSONFromDB(objectType: JSONData.self, primaryKey: primaryKey!) as? JSONData
        print("### ### url : \(url)")
        
//        let parameters: Parameters = [
//            "q": ["member_id": 2],
//            "apiKey": "2ABdhQTy1GAWiwfvsKfJyeZVfrHeloQI"
//        ]
//        Alamofire.request("https://api.mlab.com/api/1/databases/mysignal/collections/Ctemp", method: .get, parameters: parameters, encoding: URLEncoding.default)
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers)
//        Alamofire.request(url, method:.get, headers: headers)
            .responseJSON { response in
//                print("### ### Success: \(response.result.isSuccess)  ")
//                print("### ### Response String: \(String(describing: response.result.value))")
//                print("### ### request url: \(String(describing: response.request?.url))")
                
//                print("#########")
//                let heads = response.request?.allHTTPHeaderFields
//                print(heads)
//                print("#########")
                
                self.handleResponse(response: response, primaryKey: primaryKey!, storedObject: storedObject, apiRouter: apiRouter, completion: completion)
                
        }
    }
    
    func fetchDataWithDate(id: Int?, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseJSONWithDate){
        var url: String
        var headers: [String: String]
        
        switch apiRouter {
        case APIRouter.clients:
            url = getUrl(at: .clients, baseUrl: remoteManager.baseUrl)
            headers = getSimpleHeaders(token: getToken() ?? "")
            break
        case APIRouter.sites:
            url = getUrl(at: .sites, baseUrl: remoteManager.baseUrl, id: id!, placeholder: "{clientId}")
            headers = getSimpleHeaders(token: getToken() ?? "")
            break
        case APIRouter.programs:
            url = getUrl(at: .programs, baseUrl: remoteManager.baseUrl, id: id!, placeholder: "{siteId}")
            headers = getSimpleHeaders(token: getToken() ?? "")
            break
        default:
            url = getUrl(at: apiRouter, baseUrl: remoteManager.baseUrl)
            headers = getHeaders()
        }
        
        let primaryKey = getPrimaryKey(apiRouter: apiRouter, id: id)
        let storedObject = getJSONFromDB(objectType: JSONData.self, primaryKey: primaryKey!) as? JSONData
        print("### ### url : \(url)")
        Alamofire.request(url, method:.get, headers: headers)
            .responseJSON { response in
//                print("### ### Success: \(response.result.isSuccess)  ")
//                print("### ### Response String: \(String(describing: response.result.value))")
//                guard let object = response.result.value else {
//                    print("Ah, no data!!!")
//                    return
//                }
//                let json = JSON(object)
//                if let jArray = json.array {
//                    print(jArray)
//                }
//
//                print("#########")
//                let heads = response.request?.allHTTPHeaderFields
//                print(heads)
//                print("#########")
                
                self.handleResponse(response: response, primaryKey: primaryKey!, storedObject: storedObject, apiRouter: apiRouter, completion: completion)
        }
    }
    
    func handleResponse(response: DataResponse<Any>, primaryKey: String, storedObject: JSONData?, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseJSONWithDate ){
        var statusCode = response.response?.statusCode ?? Constants.UnKnowFailureCode
        switch(response.result){
        case .success:
            if(statusCode == Constants.ResponseOK){
                let object = JSONData()
                object.jsonDataPrimaryKey = primaryKey
                object.data = response.data
                object.date = Date()
                if(storedObject == nil){
                    self.realmDB.insertObject(object)
                }else{
                    self.realmDB.updateObject(object)
                }
                completion(response.data, nil, statusCode, Date())
            }else{
                completion(storedObject?.data, nil, statusCode, storedObject?.date)
                
            }
            break
        case .failure(let error):
            print(error)
            statusCode = self.getFailureStatusCode(response: response)
            print("### ### statusCode: \(statusCode)")
            completion(storedObject?.data, nil, statusCode, storedObject?.date)
            break
        }
    }
}

