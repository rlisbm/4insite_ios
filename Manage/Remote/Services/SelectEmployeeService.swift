////
////  SelectEmployeeService.swift
////  Manage
////
////  Created by Rong Li on 11/27/18.
////  Copyright © 2018 Insite. All rights reserved.
////
//
//import Foundation
//import Alamofire
//import SwiftyJSON
//import AlamofireObjectMapper
//import SwiftKeychainWrapper
//import RealmSwift
//import Realm
//
//class SelectEmployeeService: BaseService{
//    let PAGE_SIZE = 50
//    var pageNumber = 0
//    
//    let remoteManager: RemoteManager
//    
//    init(remoteManager: RemoteManager){
//        self.remoteManager = remoteManager
//    }
//    
////    @GET("/api/v1.1/Services/TeamRoster")
////    Call<TeamRoster> getTeamRoster(
////    @Query("skip") int skip,
////    @Query("take") int take,
////    @Query("sortByColumn") String sortColumn,
////    @Query("orderBy") String orderBy);
////     PAGE_SIZE = 50
//    func fetchTeamMembers(completion: @escaping SBMTypealias.ResponseResult<TeamRoster>){
//        let url = getUrl(at: .teamRoster, baseUrl: remoteManager.baseUrl)
//        let headers = getHeaders()
////        let headers = [
////            "Authorization": token,
////            "Content-Type": "application/json"
////        ]
//        let params = [
//            "skip": String(pageNumber * PAGE_SIZE),
//            "take": String(PAGE_SIZE),
//            "sortByColumn": "FirstName",
//            "orderBy": "ASC"
//        ]
//
//        let selectedSite = SelectedSiteHelper.get()!
//        let primaryKey = String(self.getEmployeeId()) + String(selectedSite.selectedClientId!) + String(selectedSite.selectedSiteId!) + String(selectedSite.selectedProgramId!) + "TeamRoster"
//        let storedTeamRoster = self.realmDB.getObject(type: TeamRoster.self, primaryKey: primaryKey)
//  
//        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.queryString, headers: headers).validate().responseObject { (response: DataResponse<TeamRoster>) in
////            let statusCode = response.response?.statusCode
//            switch(response.result){
//            case .success(let value):
//                let statusCode = response.response?.statusCode
//                print("### ### teamRoster: \(value.rosterCount) \(value.clockedInCountForSite) \(value.teamMembers.count) \(value.teamMembers[0].profileImage?.Value)")
//                
//                if(statusCode == 200){
//                    value.teamRosterPrimaryKey = primaryKey
//                    if (storedTeamRoster == nil) {
//                        self.realmDB.insertObject(value)
//                    }else if (value != storedTeamRoster){
//                        self.realmDB.updateObject(value)
//                    }
//                    completion(value, nil, statusCode!)
//                }else{
//                    completion(storedTeamRoster, nil, statusCode!)
//                }
//            
//                break
//            case .failure(let error):
//                print(error)
//                completion(storedTeamRoster, error, -1)
//                break
//            }
//        }
//    }
//}
