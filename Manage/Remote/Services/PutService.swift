//
//  PutService.swift
//  Manage
//
//  Created by Rong Li on 4/5/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireObjectMapper
import SwiftKeychainWrapper
import RealmSwift
import Realm

class PutService: BaseService {
    
    let remoteManager: RemoteManager
    
    fileprivate var url: String?
    fileprivate var headers:[String: String]?
    
    init(remoteManager: RemoteManager){
        self.remoteManager = remoteManager
    }
    
    
    func putItem<T, O>(item: T, itemObject: O, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseJSON) {
        var data: Data?
        let jsonEncoder = JSONEncoder()
        
        switch apiRouter {
        case APIRouter.putTodoItem:
            url = getUrl(at: apiRouter, baseUrl: remoteManager.baseUrl, id: (item as! Item).TodoItemId!, placeholder: "{todoItemId}")
            headers = getHeaders()
            data = try? jsonEncoder.encode(item as! Item)
            break
        default:
            url = getUrl(at: apiRouter, baseUrl: remoteManager.baseUrl)
            headers = getHeaders()
            break
        }
        
        if(data == nil){
            return //can not continue without data
        }
        
        var request = URLRequest(url: try! url!.asURL())
        request.httpMethod = HTTPMethod.put.rawValue
        request.allHTTPHeaderFields = getHeaders()
        request.httpBody = data

        Alamofire.request(request).responseJSON { response in
            var statusCode = response.response?.statusCode ?? Constants.UnKnowFailureCode
            print("### ### Put statusCode: \(statusCode)")
            print("### ### Put Success: \(response.result.isSuccess)  ")
            print("### ### Put Response String: \(String(describing: response.result.value))")

            self.handleResponse(response: response, item: item, itemObject: itemObject, apiRouter: apiRouter, completion: completion)
            
        }
    }
    
    fileprivate func handleResponse<T, O>(response: DataResponse<Any>, item: T, itemObject: O, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseJSON){
        var objectData: Data?
        var itemData: Data?
        var id: Int?
        var pageNumber: Int?
        var primaryKey: String?
        
        let jsonEncoder = JSONEncoder()
        
        switch apiRouter {
        case APIRouter.putTodoItem:
            id = (item as! Item).TodoItemId
            pageNumber = (itemObject as! TodoItemV11).PageNumber
            primaryKey = getPrimaryKey(apiRouter: apiRouter, id: nil, pageNumber: pageNumber)
            let tmpItemObject = itemObject as! TodoItemV11
            for (var tmpItem) in tmpItemObject.Items! {
                if(tmpItem.TodoItemId == id){
                    tmpItem = (item as! Item)
                    break
                }
            }
            
            objectData = try? jsonEncoder.encode(itemObject as! TodoItemV11)
            
            break
        default:
            break
        }
        
        //update data stored in the local database
        print("### ### Put Response String: \(String(describing: response.result.value))")
        let storedObject = getJSONFromDB(objectType: JSONData.self, primaryKey: primaryKey!) as? JSONData
        let object = JSONData()
        object.jsonDataPrimaryKey = primaryKey
        object.data = response.data
        object.date = Date()
        if(storedObject == nil){
            self.realmDB.insertObject(object)
        }else{
            self.realmDB.updateObject(object)
        }
        
        var statusCode = response.response?.statusCode ?? Constants.UnKnowFailureCode
        switch(response.result){
        case .success:
            if(statusCode == Constants.ResponseOK){
                //ResponseOK: back to caller
                completion(response.data, nil, statusCode)
            }else{
                //Trouble: save it to try later
                completion(storedObject?.data, nil, statusCode)
                storeDataToResend(response: response, item: item, apiRouter: apiRouter, statusCode: statusCode)
                
            }
            break
        case .failure(let error):
            //Trouble: save it to try later
            print(error)
            statusCode = self.getFailureStatusCode(response: response)
            print("### ### statusCode: \(statusCode)")
            completion(storedObject?.data, nil, statusCode)
            storeDataToResend(response: response, item: item, apiRouter: apiRouter, statusCode: statusCode)
            break
        }
    }

//    fileprivate func storeDataToResend<T>(response: DataResponse<Any>, item: T, apiRouter: APIRouter, statusCode: Int){
////        let object = RequestObject()
////        object.requestObjectPrimaryKey = String(Date().toMillis())
////        object.requestObject = response.request
//
//
//        let object = RequestObject()
//        object.requestObjectPrimaryKey = String(Date().toMillis())
//        object.data = response.request?.httpBody
//
//        let headers: [String: String] = (response.request?.allHTTPHeaderFields)!
//        object.headers = try! JSONSerialization.data(withJSONObject: headers, options: JSONSerialization.WritingOptions.prettyPrinted)
//        object.url = response.request?.url?.absoluteString
//        object.method = response.request?.httpMethod
//        object.userName = KeychainWrapper.standard.string(forKey: "user_name")!
//        object.password = KeychainWrapper.standard.string(forKey: "password")!
//        object.responseCode = statusCode
//        object.resendingState = Constants.ReSendingNow
//
//        self.realmDB.insertObject(object)
//
//        let dict = try! JSONSerialization.jsonObject(with: object.headers! , options: []) as? [String: String]
//        print(dict)
//    }
}
