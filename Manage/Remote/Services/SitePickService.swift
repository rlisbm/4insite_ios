////
////  SitePickService.swift
////  Manage
////
////  Created by Rong Li on 10/24/18.
////  Copyright © 2018 Insite. All rights reserved.
////
//
//import Foundation
//import Alamofire
//import SwiftyJSON
//import AlamofireObjectMapper
//import SwiftKeychainWrapper
//import RealmSwift
//import Realm
//
//class SitePickService: BaseService{
//    
//    let remoteManager: RemoteManager
//    
//    init(remoteManager: RemoteManager){
//        self.remoteManager = remoteManager
//    }
//    
//    func fetchClients(completion: @escaping SBMTypealias.ResponseResult<ClientMappable>){
//        let token = getToken()
////        if(token == ""){
//////            refreshToken(remoteManager: remoteManager, completion: completion)
////            return
////        }
//        
//        let url = getUrl(at: .clients, baseUrl: remoteManager.baseUrl)
//        let headers = getSimpleHeaders(token: token!)
//        Alamofire.request(url, method:.get, headers: headers).validate().responseObject { (response: DataResponse<ClientMappable>) in
////            let statusCode = (response.response?.statusCode)!
//            
//            let primaryKey = String(self.getEmployeeId())
//            let storedClients = self.realmDB.getObject(type: ClientMappable.self, primaryKey: self.getEmployeeId())
////            let storedClients = self.realmDB.getObject(type: ClientsObject.self, primaryKey: primaryKey)
//            switch(response.result){
//            case .success(let value):
//                let statusCode = (response.response?.statusCode)!
////                self.realmDB.deleteDatabase()
//                //save to database
//                if(statusCode == 200){
//                    value.clientsPrimaryKey = primaryKey
//                    if (storedClients == nil) {
//                        self.realmDB.insertObject(value)
//                    }else if (value != storedClients){
//                        self.realmDB.updateObject(value)
//                    }
//                    completion(value, nil, statusCode)
//                }else{
//                    completion(storedClients, nil, statusCode)
//                }
//
////                self.realmDB.deleteDatabase()
////                switch(response.result){
////                case .success(let value):
////                    if(statusCode == self.resultOK){
////                        let fetchedClients = ClientsObject()
////                        fetchedClients.clientsPrimaryKey = primaryKey
////                        fetchedClients.clients = value
////                        if(storedClients == nil){
////                            self.realmDB.insertObject(fetchedClients)
////                        }else{
////                            self.realmDB.updateObject(fetchedClients)
////                        }
////                        print(value.Clients?.Name)
////                        print(value.Clients?.Options.count)
////                        let list = value.Clients?.Options
////                        for c in list!{
////                            print("\(c.Key)  \(c.Value)")
////                        }
////                        completion(value, nil, statusCode, false)
////                        return
////                    }else{
////                        //if storedClient == nil, Can not continue to proceed. Handle it in UI Level
////                        completion(storedClients?.clients, nil, statusCode, false)
////                    }
////                    break
//                case .failure(let error):
//                    print(error)
//                    completion(storedClients, error, -1)
////                    completion(storedClients, error, statusCode)
////                    completion(storedClients?.clients, error, statusCode, false)
//                    break
//                }
//        }
//    }
//    
//    func fetchSites(clientId: Int,  completion: @escaping SBMTypealias.ResponseResultList<SiteMappable>){
//        let token = getToken()
////        if(token == ""){
//////            refreshToken(remoteManager: remoteManager, completion: completion)
////            return
////        }
//        let url = getUrl(at: .sites, baseUrl: remoteManager.baseUrl, id: clientId, placeholder: "{clientId}")
//        let headers = getSimpleHeaders(token: token!)
//        
//        Alamofire.request(url, method:.get, headers: headers).validate().responseArray { (response: DataResponse<[SiteMappable]>) in
//            
////            let statusCode = (response.response?.statusCode)!
//            let primaryKey = "\(String(self.getEmployeeId()))\(clientId)"
//            let storedSites = self.realmDB.getObject(type: SitesObject.self, primaryKey: primaryKey)
//            switch(response.result){
//            case .success(let value):
//                let statusCode = (response.response?.statusCode)!
//                if(statusCode == Constants.ResponseOK){
//                    let fetchedSites = SitesObject()
//                    fetchedSites.sitesPrimaryKey = primaryKey
//                    for site in value {
//                        fetchedSites.sites.append(site)
//                    }
//                    if(storedSites == nil){
//                        self.realmDB.insertObject(fetchedSites)
//                    }else{
//                        self.realmDB.updateObject(fetchedSites)
//                    }
//                    completion(fetchedSites.sites, nil, statusCode)
//                }else{
//                    completion(storedSites?.sites, nil, statusCode)
//                }
//                break
//            case .failure(let error):
//                print(error)
//                completion(storedSites?.sites, error, -1)
////                completion(storedSites?.sites, error, statusCode)
//                break
//            }
//        }
//    }
//    
//    func fetchPrograms(siteId: Int,  completion: @escaping SBMTypealias.ResponseResultList<KeyValueMappable>){
//        let token = getToken()
////        print("### #### token: \(token)")
////        if(token == ""){
////            //refreshToken(remoteManager: remoteManager, completion: completion)
////            return
////        }
//        let url = getUrl(at: .programs, baseUrl: remoteManager.baseUrl, id: siteId, placeholder: "{siteId}")
//        
//        let headers = getSimpleHeaders(token: token!)
//        Alamofire.request(url, method:.get, headers: headers).validate().responseArray { (response: DataResponse<[KeyValueMappable]>) in
//            
////            let statusCode = (response.response?.statusCode)!
//            let primaryKey = "\(String(self.getEmployeeId()))\(siteId)"
//            let storedPrograms = self.realmDB.getObject(type: ProgramsObject.self, primaryKey: primaryKey)
//            switch(response.result){
//            case .success(let value):
//                let statusCode = (response.response?.statusCode)!
//                if(statusCode == Constants.ResponseOK){
//                    let fetchedPrograms = ProgramsObject();
//                    fetchedPrograms.programsPrimaryKey = primaryKey
//                    for program in value {
//                        fetchedPrograms.programs.append(program)
//                    }
//                    if(storedPrograms == nil){
//                        self.realmDB.insertObject(fetchedPrograms)
//                    }else{
//                        self.realmDB.updateObject(fetchedPrograms)
//                    }
//                    completion(fetchedPrograms.programs, nil, statusCode)
//                }else{
//                    completion(storedPrograms!.programs, nil, statusCode)
//                }
//                break
//            case .failure(let error):
//                print(error)
//                completion(storedPrograms!.programs, error, -1)
////                completion(storedPrograms!.programs, error, statusCode)
//                break
//            }
//        }
//    }
//    
//    //This is not used for now
//    func fetchClientLocation(selectedClientId: Int, completion: @escaping SBMTypealias.ResponseResult<ClientLocation>){
//        let token = getToken()
//        if(token == ""){
//            //refreshToken(remoteManager: remoteManager, completion: completion)
//            return
//        }
//        var url = getUrl(at: .clientLocation, baseUrl: remoteManager.baseUrl, id: selectedClientId, placeholder: "{clientId}")
//        let headers = getSimpleHeaders(token: token!)
//        Alamofire.request(url, method:.get, headers: headers).validate().responseObject { (response: DataResponse<ClientLocation>) in
////            let statusCode = (response.response?.statusCode)!
//            
//            switch(response.result){
//            case .success(let value):
//                let statusCode = (response.response?.statusCode)!
//                var storedClientLocation = self.realmDB.getObject(type: ClientLocation.self, primaryKey: String(selectedClientId))
//                if(statusCode == Constants.ResponseOK ){
//                    value.primaryKey = String(selectedClientId)
//                    if(storedClientLocation == nil){
//                        self.realmDB.insertObject(value)
//                    }else{
//                        self.realmDB.updateObject(value)
//                    }
//                    completion(value, nil, statusCode)
//                    return
//                }else{
//                    //if storedClientLocation == nil, Can not continue to proceed. Handle it in UI Level
//                    completion(storedClientLocation, nil, statusCode)
//                }
//                break
//            case .failure(let error):
//                print(error)
//                completion(nil, error, -1)
////                completion(nil, error, statusCode)
//                break
//            }
//        }
//    }
//    
//}
