//
//  SubmitService.swift
//  Manage
//
//  Created by Rong Li on 12/18/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireObjectMapper
import SwiftKeychainWrapper
import RealmSwift
import Realm

class SubmitService: BaseService {
    
    let remoteManager: RemoteManager
    
    init(remoteManager: RemoteManager){
        self.remoteManager = remoteManager
    }
    
    func submitConduct(conduct: Conduct, completion: @escaping SBMTypealias.ResponseString){
        
        let conductRequestItem = ConductRequestItem (conductId: conduct.conductId,
                                                     orgUserId: conduct.orgUserId,
                                                     dateOfOccurrence: conduct.dateOfOccurrence,
                                                     isExempt: conduct.isExempt,
                                                     reason: conduct.reason,
                                                     conductTypes: conduct.conductTypes,
                                                     conductAttachments: conduct.conductAttachments)
        let jsonEncoder = JSONEncoder()
        var jsonString: String?
        do {
            let jsonData = try jsonEncoder.encode(conductRequestItem)
            jsonString = String(data: jsonData, encoding: .utf8)
//            print("JSON String : " + jsonString!)
        }
        catch {
            print("error")
        }
        
        let parameters = jsonString?.convertToDictionary()
//        print("###################")
        print(parameters)

        let url = getUrl(at: .conduct, baseUrl: remoteManager.baseUrl)
        let headers = getHeaders()
        Alamofire.request(url, method:.post, parameters: parameters, encoding:  JSONEncoding.default, headers: headers)
            .responseString { response in
//                print("### ### Success: \(response.result.isSuccess)  ")
//                print("### ### Response String: \(String(describing: response.result.value))")
                
                switch(response.result){
                    case .success(let value):
                        let statusCode = (response.response?.statusCode)!
//                        print("### ### statusCode: \(statusCode)  ")
                        if(statusCode != Constants.ResponseOK){
//                            self.saveConduct(conduct: conduct)
                        }
                        completion(value, nil, statusCode)
                        break
                    case .failure(let error):
//                        self.saveConduct(conduct: conduct)
                        completion(nil, error, -1)
                    break
                }

        }
    }
    
    func submitAttendance(issue: IssueMappable, completion: @escaping SBMTypealias.ResponseResult<IssueMappable>){
        let jsonString = issue.toJSONString(prettyPrint: true)
        print(jsonString?.convertToDictionary())
        
        let parameters = jsonString?.convertToDictionary()
        
        let url = getUrl(at: .attendance, baseUrl: remoteManager.baseUrl)
        let headers = getHeaders()
        
        Alamofire.request(url, method:.post, parameters: parameters, encoding:  JSONEncoding.default, headers: headers)
            .validate().responseObject { (response: DataResponse<IssueMappable>) in
                
                switch(response.result){
                case .success(let value):
                    let statusCode = (response.response?.statusCode)!
                    if(statusCode != Constants.ResponseOK){
//                        self.saveItem(todoItem: todoItem)
                    }
                    completion(value, nil, statusCode)
                    break
                case .failure(let error):
//                    self.saveItem(todoItem: todoItem)
                    completion(nil, error, -1)
                    break
                    
                }
        }
    }
//    fileprivate func getSubmitUrl(apiRouterPath: APIRouter) -> String{
//        switch (apiRouterPath) {
//        case APIRouter.reportIt:
//            return getUrl(at: .reportIt, baseUrl: remoteManager.baseUrl)
//        case APIRouter.complaint:
//            return getUrl(at: .complaint, baseUrl: remoteManager.baseUrl)
//        default:
//            return ""
//
//        }
//        return ""
//    }
    fileprivate func getJSONString<T>(item: T, apiRouter: APIRouter) -> String {
        let jsonEncoder = JSONEncoder()
        var jsonString: String?
        do {
            switch (apiRouter) {
            case APIRouter.reportIt:
                let jsonData = try jsonEncoder.encode(item as! OwnershipRequestItem)
                jsonString = String(data: jsonData, encoding: .utf8)
                
                do{
                    let object = try JSONDecoder().decode(OwnershipRequestItem.self, from: jsonString!.data(using: .utf8)!)
//                    print("****************")
//                    print(object.Description)
//                    print(object)
//                    print("****************")
                }catch {
                    print("ERROR")
                }
                
                break
            case APIRouter.complaint:
                let jsonData = try jsonEncoder.encode(item as! Complaint)
                jsonString = String(data: jsonData, encoding: .utf8)
                break
            case APIRouter.compliment:
                let jsonData = try jsonEncoder.encode(item as! Compliment)
                jsonString = String(data: jsonData, encoding: .utf8)
                break
            case APIRouter.trainingMessage:
                let jsonData = try jsonEncoder.encode(item as! TrainingMessage)
                jsonString = String(data: jsonData, encoding: .utf8)
                break
            case APIRouter.employeeAudit:
                let jsonData = try jsonEncoder.encode(item as! PressionalismAudit)
                jsonString = String(data: jsonData, encoding: .utf8)
                break
            case APIRouter.todoItem:
                let jsonData = try jsonEncoder.encode(item as! TodoItem)
                jsonString = String(data: jsonData, encoding: .utf8)
                break
            case APIRouter.scoreCardDetail:
                let jsonData = try jsonEncoder.encode(item as! [ScorecardDetail])
                jsonString = String(data: jsonData, encoding: .utf8)
                break
            case APIRouter.attendance:
                let jsonData = try jsonEncoder.encode(item as! Issue)
                jsonString = String(data: jsonData, encoding: .utf8)
                break
            default:
                return ""
            }
            print("JSON String : " + jsonString!)
        }
        catch {
            print("error")
        }
        return jsonString ?? ""
    }
    
    fileprivate func saveSubmitItemObject(url: String, headers: String, parameters: [String: Any], statusCode: Int) {
        let submitItemObject = SubmitItemObject()
        submitItemObject.submitItemObjectPrimaryKey = String(Int(Date().timeIntervalSince1970 * 1000))
        submitItemObject.urlString = url
        submitItemObject.headers = headers
        submitItemObject.userName = KeychainWrapper.standard.string(forKey: "user_name")!
        submitItemObject.password = KeychainWrapper.standard.string(forKey: "password")!
        
        submitItemObject.responseCode = statusCode
        submitItemObject.resendingState = Constants.ReSendingNow
        
        if let jsonData = try?  JSONSerialization.data(
            withJSONObject: parameters,
            options: .prettyPrinted
            ),
            let jsonString = String(data: jsonData,
                                     encoding: String.Encoding.ascii) {
            submitItemObject.params = jsonString
            
            print("JSON string = \n\(jsonString)")
        }
        
        self.realmDB.insertObject(submitItemObject)
    }
    
    func submitItem<T>(item: T, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseString) {
        let jsonString = getJSONString(item: item, apiRouter: apiRouter)
        if(jsonString.isEmpty){
            return
        }
        
        let parameters = jsonString.convertToDictionary()
        print(parameters)

        let url = getUrl(at: apiRouter, baseUrl: remoteManager.baseUrl)
        let headers = getHeaders()
        
        Alamofire.request(url, method:.post, parameters: parameters, encoding:  JSONEncoding.default, headers: headers)
            .responseString { response in
                print("### ### 111 Success: \(response.result.isSuccess)  ")
                print("### ### Success: \(response.result.description)  ")
                print("### ### Response String: \(String(describing: response.result.value))")
                
                let statusCode = response.response == nil ? Constants.ConnectivityFailureCode : response.response!.statusCode
                
                self.saveSubmitItemObject(url: url, headers: jsonString,  parameters: parameters!, statusCode: statusCode)
                
                switch(response.result){
                case .success(let value):
//                    let statusCode = (response.response?.statusCode)!
                    self.saveSubmitItemObject(url: url, headers: jsonString,  parameters: parameters!, statusCode: statusCode)
//                    if(statusCode != Constants.ResponseOK){
//                        self.saveSubmitItemObject(url: url, headers: jsonString,  parameters: parameters!, statusCode: statusCode)
//                    }
                    completion(value, nil, statusCode)
                    break
                case .failure(let error):
//                    self.saveSubmitItemObject(url: url, headers: jsonString, parameters: parameters, statusCode: Constants.ConnectivityFailureCode)
                    completion(nil, error, Constants.ConnectivityFailureCode)
                    break
                }
                
        }
    }
    
    //for scorecardDetails
    func submitItemArray<T>(items: [T], apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseAny) {
        let jsonString = getJSONString(item: items, apiRouter: apiRouter)
        if(jsonString.isEmpty){
            return
        }
        
        let url = getUrl(at: apiRouter, baseUrl: remoteManager.baseUrl)
        let headers = getHeaders()
        
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "PUT"
        request.allHTTPHeaderFields = headers
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        var error: NSError?
        do {
            request.httpBody = jsonString.data(using: .utf8)!
        } catch {
            //TODO: handle error
            print("JSON serialization failed:  \(error)")
        }
        
        Alamofire.request(request as! URLRequestConvertible)
            .responseJSON { response in
                print("### ### Success: \(response.result.isSuccess)  ")
                print("### ### Success: \(response.result.description)  ")
                print("### ### Response String: \(String(describing: response.result.value))")
                
                switch(response.result){
                //                case .success(let value):
                case .success(let value):
                    print(value)
//                    guard let jsonArray = value as? [[String: Any]] else {
//                        return
//                    }
//                    print("********** 1")
//                    print(jsonArray)
//                    print("********** 2 \(jsonArray.count)")
//                    guard let item = jsonArray[0]["Body"] as? [String: Any] else {
//                        print("********** 21")
//                        return
//                    }
//                    print(item)
                    
                    completion(value, error, -1)
                    
                    break
                case .failure(let error):
                    //self.saveConduct(conduct: conduct)
                    completion(nil, error, -1)
                    break
                }
        }
    }
    
//    func submitItemArray<T>(items: [T], apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseString) {
//        let jsonString = getJSONString(item: items, apiRouter: apiRouter)
//        if(jsonString.isEmpty){
//            return
//        }
//
//        let url = getUrl(at: apiRouter, baseUrl: remoteManager.baseUrl)
//        let headers = getHeaders()
//
//        var request = URLRequest(url: URL(string: url)!)
//        request.httpMethod = "PUT"
//        request.allHTTPHeaderFields = headers
//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//
//        var error: NSError?
//        do {
//            request.httpBody = jsonString.data(using: .utf8)!
//        } catch {
//            //TODO: handle error
//            print("JSON serialization failed:  \(error)")
//        }
//
//        Alamofire.request(request as! URLRequestConvertible)
//            .responseJSON { response in
//                                print("### ### Success: \(response.result.isSuccess)  ")
//                                print("### ### Success: \(response.result.description)  ")
//                                print("### ### Response String: \(String(describing: response.result.value))")
//
//                switch(response.result){
////                case .success(let value):
//                    case .success(let JSON):
//                    let statusCode = (response.response?.statusCode)!
//                    //                    print("### ### statusCode: \(statusCode)  ")
//                    if(statusCode != Constants.ResponseOK){
//                        //self.saveConduct(conduct: conduct)
//                    }
//
////                    self.responseDecoder(value: value)
////                    completion(value, nil, statusCode)
//
//
//                    guard let jsonArray = response.result.value! as? [[String: Any]] else {
//                        print("### ### am i here")
//                        return
//                    }
//                    print("### ###")
//                    print(jsonArray)
//                    print("### ###")
//                    print(jsonArray[0])
//
//                    //Now get title value
//                    guard let title = jsonArray[0]["Status"] as? Int else {
//                        print("************ 1")
//                        return
//
//                    }
//                    print(title)
//                    guard let item = jsonArray[0]["Body"] as? Any else {
//                        print("************ 1")
//                        return
//                    }
//                    print("************ 2")
//                    print(item)
//                    var itemString = String(describing: item)
//                    print("************ 3")
//                    itemString = itemString.replacingOccurrences(of: " =", with: ":")
//                    print(itemString)
//                    print("************ 4")
//                    let item1 = itemString.parse(to: ScorecardDetail.self)
//                    print(item1?.AuditInspectionItemName)
//
//
////                    if let data = response.data {
////                        let jsonDecoder = JSONDecoder()
////                        let items = try? jsonDecoder.decode(Array<ResponseScorecardDetail>.self, from: data)
////                        print("************ count: \(items!.count)")
////                        print(items)
////                    }
//                    break
//                case .failure(let error):
//                    //self.saveConduct(conduct: conduct)
//                    completion(nil, error, -1)
//                    break
//                }
//            }
//    }
    
//    func responseDecoder(value: String){
//        do{
//            let data = value.data(using: .utf8)!
//            let JSONData = try JSONDecoder().decode([ResponseScorecardDetail].self, from: data)
//            print("************ \(JSONData)")
//        }catch{
//            print("*** *** error: \(error)")
//        }
//
//
////        if let myJSON = try JSONSerialization.j(with: value, options: []) as? [Any] {
////            print("************ \(myJSON.count)")
//////            //For getting customer_id try like this
//////            if let data = myJSON["data"] as? [[String: Any]] {
//////                for jsonDict in data {
//////                    var try = jsonDict["customer_id"] as? String
//////                }
//////            }
////        }
//    }
}
