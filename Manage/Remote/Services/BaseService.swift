//
//  File.swift
//  Manage
//
//  Created by Rong Li on 10/24/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SwiftKeychainWrapper
import RealmSwift


class BaseService : NSObject {
    let WEB_TOOL_ENGLISH_ID = 1;
    let WEB_TOOL_SPANISH_ID = 2;
    
//    let resultOK = 200
    let realmDB: RealmDB = RealmDB.shared
    var employeeId: String = ""
    
    var completionHandlers = [(SBMTypealias.ResponseResult<Object>) -> Void]()
    
    func getEmployeeId() -> String{
        if(employeeId == ""){
            guard let employeeId = AssociateHelper.get()?.employeeId else{
                fatalError("app can not be continued without employeeId")
            }
            self.employeeId = String(employeeId)
        }
        return employeeId;
    }

    func getUrl(at route: APIRouter, baseUrl: String) -> String{
        return route.url(baseUrl)
    }
    
    func getUrl(at route: APIRouter, baseUrl: String, id: Int, placeholder: String ) -> String{
        return route.url(baseUrl, id, placeholder)
    }
    
    func getImageUrl(baseCNDUrl: String) -> String{
        let url = AssociateHelper.get()?.profilePictureUniqueId
        return baseCNDUrl + url!
    }
    
    
//    //this may not need, it's always true, because tokenvalidated in ViewController
//    func isTokenValid(_ token: String) -> Bool{
//        if(token != ""){
////            let expiration = SBMTokenHelper.get()?.expires_in
////            let now: Int64  = Date().toSeconds()
////            if(expiration! > now){
////                return true
////            }
//            let expirationText = KeychainWrapper.standard.string(forKey: "expires_in") ?? ""
//            if(expirationText != ""){
//                //let expiration = SBMTokenHelper.get()?.expires_in
//                let expiration = Int64(expirationText)
//                let now: Int64  = Date().toSeconds()
//                if(expiration! > now){
//                    return true
//                }
//            }
//        }
//        //TODO: relogin to refresh token
//        print("token == nil")
//        return false
//    }
    
//    func getToken() -> String? {
//        let token: String = KeychainWrapper.standard.string(forKey: "access_token") ?? ""
////        let token: String = (SBMTokenHelper.get()?.token_type)! + " " + (SBMTokenHelper.get()?.access_token)!
//        if(token != "" && isTokenValid(token)){
//            return token
//        }
//        return ""
//    }
    
    func getToken() -> String? {
        let token: String = KeychainWrapper.standard.string(forKey: "access_token") ?? ""
        //let token: String = (SBMTokenHelper.get()?.token_type)! + " " + (SBMTokenHelper.get()?.access_token)!
        return token
    }
    
    func getLanguage() -> String {
        //Device language
        //let locale = NSLocale.current.languageCode
        
        //App language
        if(Locale.preferredLanguages[0].prefix(2) == "en"){
            return String(WEB_TOOL_ENGLISH_ID)
        }else if(Locale.preferredLanguages[0].prefix(2) == "es"){
            return String(WEB_TOOL_SPANISH_ID)
        }
        return String(WEB_TOOL_ENGLISH_ID)
    }
    
    func getSimpleHeaders(token: String) -> [String: String] {
        let headers = [
            "Authorization": token,
            "Content-Type": "application/json"
        ]
        return headers
    }
    
    func getHeaders() -> [String: String]{
        let token = getToken()!
        let selectedSite = SelectedSiteHelper.get()!
        print("#### clientId: \(String(selectedSite.selectedClientId!))  siteId: \(String(selectedSite.selectedSiteId!))  programId: \(String(selectedSite.selectedProgramId!))" )
        let languageId = getLanguage()
        let headers = [
            "Authorization": token,
            "Content-Type": "application/json",
            "clientId": String(selectedSite.selectedClientId!),
            "siteId": String(selectedSite.selectedSiteId!),
            "programId": String(selectedSite.selectedProgramId!),
            "languageId": languageId
        ]
        
        return headers
    }
    
//    func getFailureStatusCode(response: DataResponse<Data>) -> Int{
    func getFailureStatusCode<T>(response: DataResponse<T>) -> Int{
        var statusCode = Constants.UnKnowFailureCode
        if let error = response.result.error as? AFError{
            statusCode = error._code
            switch error {
            case .invalidURL(let url):
                print("### Invalid URL: \(url) - \(error.localizedDescription)")
            case .parameterEncodingFailed(let reason):
                print("### Parameter encoding failed: \(error.localizedDescription)")
                print("### Failure Reason: \(reason)")
            case .multipartEncodingFailed(let reason):
                print("### Multipart encoding failed: \(error.localizedDescription)")
                print("### Failure Reason: \(reason)")
            case .responseValidationFailed(let reason):
                print("### Response validation failed: \(error.localizedDescription)")
                print("### Failure Reason: \(reason)")
                
                switch reason {
                case .dataFileNil, .dataFileReadFailed:
                    print("### Downloaded file could not be read")
                case .missingContentType(let acceptableContentTypes):
                    print("### Content Type Missing: \(acceptableContentTypes)")
                case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                    print("### Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)")
                case .unacceptableStatusCode(let code):
                    print("### Response status code was unacceptable: \(code)")
                    statusCode = code
                }
            case .responseSerializationFailed(let reason):
                print("### Response serialization failed: \(error.localizedDescription)")
                print("### Failure Reason: \(reason)")
            }
            
        }else if let error = response.result.error as? URLError {
            statusCode = error.code.rawValue
            print("### URLError occurred: \(error)  \(error.code.rawValue)")
        } else {
            statusCode = Constants.UnKnowFailureCode
            print("### Unknown error: \(response.result.error)")
        }
        
        return statusCode
    }
    
    func getPrimaryKey(apiRouter: APIRouter, id: Int?) -> String?{
        return getPrimaryKey(apiRouter: apiRouter, id: id, pageNumber: nil)
//        switch apiRouter {
//        case APIRouter.clients:
//            return "clients"+String(self.getEmployeeId())
//        case APIRouter.sites:
//            return "sites"+String(id!)
//        case APIRouter.programs:
//            return "programs"+String(id!)
//        case APIRouter.teamRosterV2:
//            return "teamRoster"+String(SelectedSiteHelper.get()!.selectedSiteId!)+String(SelectedSiteHelper.get()!.selectedProgramId!)
//        case APIRouter.dailyTimeCardStats:
//            return "dailyTimeCardStats"+String(SelectedSiteHelper.get()!.selectedSiteId!)+String(SelectedSiteHelper.get()!.selectedProgramId!)
//        case APIRouter.todoItem:
//            return "todoItem"+String(SelectedSiteHelper.get()!.selectedSiteId!)+String(SelectedSiteHelper.get()!.selectedProgramId!)
//        default:
//            return nil
//        }
    }
    
    func getPrimaryKey(apiRouter: APIRouter, id: Int?, pageNumber: Int?) -> String?{
        switch apiRouter {
        case APIRouter.clients:
            return "clients"+String(self.getEmployeeId())
        case APIRouter.sites:
            return "sites"+String(id!)
        case APIRouter.programs:
            return "programs"+String(id!)
        case APIRouter.teamRosterV2:
            return "teamRoster"+String(SelectedSiteHelper.get()!.selectedSiteId!) //programId removed from site picker screen
//            return "teamRoster"+String(SelectedSiteHelper.get()!.selectedSiteId!)+String(SelectedSiteHelper.get()!.selectedProgramId!)
        case APIRouter.dailyTimeCardStats:
            return "dailyTimeCardStats"+String(SelectedSiteHelper.get()!.selectedSiteId!)
//            return "dailyTimeCardStats"+String(SelectedSiteHelper.get()!.selectedSiteId!)+String(SelectedSiteHelper.get()!.selectedProgramId!)
        case APIRouter.todoItem:
            return "todoItem"+String(SelectedSiteHelper.get()!.selectedSiteId!)
//            return "todoItem"+String(SelectedSiteHelper.get()!.selectedSiteId!)+String(SelectedSiteHelper.get()!.selectedProgramId!)
        case APIRouter.todoItemV11:
            return "todoItemV11"+String(SelectedSiteHelper.get()!.selectedSiteId!)+String(pageNumber!)
        case APIRouter.putTodoItem:
            return "putTodoItem"+String(SelectedSiteHelper.get()!.selectedSiteId!)+String(pageNumber!)
        case APIRouter.request:
            return "request"+String(SelectedSiteHelper.get()!.selectedSiteId!)+String(pageNumber!)
        default:
            return nil
        }
    }

    func getJSONFromDB(objectType: Object.Type, primaryKey: String) -> Object?{
        return realmDB.getObject(type: objectType, primaryKey: primaryKey)
    }
    
    func storeDataToResend<T>(response: DataResponse<Any>, item: T, apiRouter: APIRouter, statusCode: Int){
        
        let object = RequestObject()
        object.requestObjectPrimaryKey = String(Date().toMillis())
        object.data = response.request?.httpBody
        
        let headers: [String: String] = (response.request?.allHTTPHeaderFields)!
        object.headers = try! JSONSerialization.data(withJSONObject: headers, options: JSONSerialization.WritingOptions.prettyPrinted)
        object.url = response.request?.url?.absoluteString
        object.method = response.request?.httpMethod
        object.userName = KeychainWrapper.standard.string(forKey: "user_name")!
        object.password = KeychainWrapper.standard.string(forKey: "password")!
        object.responseCode = statusCode
        object.resendingState = Constants.ReSendingNow
        
        self.realmDB.insertObject(object)
        
        let dict = try! JSONSerialization.jsonObject(with: object.headers! , options: []) as? [String: String]
        print(dict)
    }
    
//    func refreshToken(remoteManager: RemoteManager, completion: @escaping SBMTypealias.ResponseResult<Client>){
//        //completionHandlers.append(completion)
//        refreshToken(remoteManager: remoteManager, completion: completion)
//    }
//    func refreshToken(remoteManager: RemoteManager, completion: @escaping SBMTypealias.ResponseResult<Site>){
//        //completionHandlers.append(completion)
//        refreshToken(remoteManager: remoteManager, completionIndex: completionHandlers.count-1)
//    }
//
//    func refreshToken(remoteManager: RemoteManager, completion: @escaping SBMTypealias.ResponseResult<Client>){
//        print("### ### refreshToken")
//        let url = getUrl(at: .login, baseUrl: remoteManager.baseUrl)
//        let headers = [
//            "Content-Type": "application/x-www-form-urlencoded"
//        ]
//
//        let userName: String = KeychainWrapper.standard.string(forKey: "user_name")!
//        let password: String = KeychainWrapper.standard.string(forKey: "password")!
//        print(url)
//        print("**\(userName)")
//        print("**\(password)")
//
//        let parameters = [
//            "username": userName, //userName
//            "password": password, //password
//            "grant_type": "password"
//
//        ]
//
//        Alamofire.request(url, method:.post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers)
//            .responseData { response in
//                let statusCode = (response.response?.statusCode)!
//                print("*** *** refreshToken: \(statusCode)")
//
//
//                switch response.result{
//                case .success(let value):
//                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
//                        let sbmToken = utf8Text.parse(to: SBMToken.self)
//
//                        //save token
//                        let token = sbmToken!.token_type + " " + sbmToken!.access_token
//                        let saveToken: Bool = KeychainWrapper.standard.set(token, forKey:"access_token")
//                        //save expiration in seconds
//                        let expirationTime = Date().toSeconds() + sbmToken!.expires_in
//                        let saveExpirationTime: Bool = KeychainWrapper.standard.set(String(expirationTime), forKey:"expires_in")
//
//                        print(sbmToken!.token_type)
//                        print(sbmToken!.expires_in)
//
//
//                        completionHandlers[completionIndex](nil, nil, statusCode, true)
//
//                    }
//                    break
//                case .failure(let error):
//                    completionHandlers[completionIndex](nil, nil, statusCode, true)
//                    break
//                }
//        }
//    }
    

//    func refreshToken(remoteManager: RemoteManager, completion: @escaping SBMTypealias.ResponseResult<Client>){
//        print("### ### refreshToken")
//        let url = getUrl(at: .login, baseUrl: remoteManager.baseUrl)
//        let headers = [
//            "Content-Type": "application/x-www-form-urlencoded"
//        ]
//
//        let userName: String = KeychainWrapper.standard.string(forKey: "user_name")!
//        let password: String = KeychainWrapper.standard.string(forKey: "password")!
////        let userName: String = (LoginCredentialsHelper.get()?.userName)!
////        let password: String = (LoginCredentialsHelper.get()?.password)!
//        print(url)
//        print("**\(userName)")
//        print("**\(password)")
//
//        let parameters = [
//            "username": userName, //userName
//            "password": password, //password
//            "grant_type": "password"
//
//        ]
//
//        Alamofire.request(url, method:.post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers)
//            .responseData { response in
//                let statusCode = (response.response?.statusCode)!
//                print("*** *** refreshToken: \(statusCode)")
//
//
//                switch response.result{
//                case .success(let value):
//                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
//                        let sbmToken = utf8Text.parse(to: SBMToken.self)
//
//                        //save token
//                        let token = sbmToken!.token_type + " " + sbmToken!.access_token
//                        let saveToken: Bool = KeychainWrapper.standard.set(token, forKey:"access_token")
//                        //save expiration in seconds
//                        let expirationTime = Date().toSeconds() + sbmToken!.expires_in
//                        let saveExpirationTime: Bool = KeychainWrapper.standard.set(String(expirationTime), forKey:"expires_in")
//
//                        print(sbmToken!.token_type)
//                        print(sbmToken!.expires_in)
//
//                        completion(nil, nil, statusCode)
//                    }
//                    break
//                case .failure(let error):
//                    completion(nil, nil, statusCode)
//                    break
//                }
//        }
//    }

}
