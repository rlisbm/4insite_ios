//
//  CommonService.swift
//  Manage
//
//  Created by Rong Li on 11/16/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import AlamofireImage
import SwiftyJSON

class CommonService: BaseService{
    
    let remoteManager: RemoteManager
    
    init(remoteManager: RemoteManager){
        self.remoteManager = remoteManager
    }
    
    func fetchAvatar(completion: @escaping SBMTypealias.AvatarResult) {
        let avatarUrl = getImageUrl(baseCNDUrl: remoteManager.baseCDNUrl)
        print("### ### avatarUrl: \(avatarUrl)");
        Alamofire.request(avatarUrl).responseImage { response in
            
//            let statusCode = (response.response?.statusCode)!
            switch(response.result){
            case .success(let value):
                let statusCode = (response.response?.statusCode)!
                completion(value, nil, statusCode)
                break
            case .failure(let error):
//                completion(nil, error, statusCode)
                completion(nil, error, -1)
                break
            }
        }
    }
}
