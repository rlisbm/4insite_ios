//
//  Service.swift
//  Manage
//
//  Created by Rong Li on 10/24/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireObjectMapper
import SwiftKeychainWrapper
import RealmSwift

//class Service: LoginProtocol, SitePickProtocol, CommonProtocol{
class Service: RemoteProtocol{
    
    fileprivate var loginService: LoginService?
//    fileprivate var sitePickService: SitePickService?
    fileprivate var commonService: CommonService?
//    fileprivate var selectEmployeeService: SelectEmployeeService?
    fileprivate var todosService: TodosService?
//    fileprivate var professionalismService: ProfessionalismService?
    fileprivate var submitService: SubmitService?
    fileprivate var fetchService: FetchService?
    fileprivate var postService: PostService?
    fileprivate var putService: PutService?
    fileprivate var deleteService: DeleteService?
    
    func login(remoteManager: RemoteManager, userName: String, password: String, completion: @escaping SBMTypealias.LoginResult){
        if(loginService == nil){
            loginService = LoginService(remoteManager:remoteManager)
        }
        loginService!.login(remoteManager: remoteManager, userName: userName, password: password, completion: completion)
    }
    
    func fetchAssociateInfo(remoteManager: RemoteManager, token: String, completion: @escaping SBMTypealias.AssociateInfoResult) {
        if(loginService == nil){
            loginService = LoginService(remoteManager:remoteManager)
        }
        loginService!.fetchAssociateInfo(remoteManager: remoteManager, token: token, completion: completion)
    }
    
//    func fetchClients(remoteManager: RemoteManager, completion: @escaping SBMTypealias.ResponseResult<ClientMappable>) {
//        if(sitePickService == nil){
//            sitePickService = SitePickService(remoteManager: remoteManager)
//        }
//        sitePickService?.fetchClients(completion: completion)
//    }
//
//    func fetchSites(remoteManager: RemoteManager, clientId: Int, completion: @escaping SBMTypealias.ResponseResultList<SiteMappable>) {
//        if(sitePickService == nil){
//            sitePickService = SitePickService(remoteManager: remoteManager)
//        }
//        sitePickService?.fetchSites(clientId: clientId, completion: completion)
//    }
//
//    func fetchPrograms(remoteManager: RemoteManager, siteId: Int, completion: @escaping SBMTypealias.ResponseResultList<KeyValueMappable>) {
//        if(sitePickService == nil){
//            sitePickService = SitePickService(remoteManager: remoteManager)
//        }
//        sitePickService?.fetchPrograms(siteId: siteId, completion: completion)
//    }
//
//    func fetchClientLocation(remoteManager: RemoteManager, selectedClientId: Int, completion: @escaping SBMTypealias.ResponseResult<ClientLocation>) {
//        if(sitePickService == nil){
//            sitePickService = SitePickService(remoteManager: remoteManager)
//        }
//        sitePickService?.fetchClientLocation(selectedClientId: selectedClientId, completion: completion)
//    }
    
    func fetchAvatar(remoteManager: RemoteManager, completion: @escaping SBMTypealias.AvatarResult){
        if(commonService == nil){
            commonService = CommonService(remoteManager: remoteManager)
        }
        commonService?.fetchAvatar(completion: completion)
        
    }
    
//    func fetchTeamMembers(remoteManager: RemoteManager, completion: @escaping SBMTypealias.ResponseResult<TeamRoster>){
//        if(selectEmployeeService == nil){
//            selectEmployeeService = SelectEmployeeService(remoteManager: remoteManager)
//        }
//        selectEmployeeService?.fetchTeamMembers(completion: completion)
//    }
    
 
    
    func fetchBuildings(remoteManager: RemoteManager, completion: @escaping SBMTypealias.ResponseResult<NameKeyValueMappable>) {
        if(todosService == nil){
            todosService = TodosService(remoteManager: remoteManager)
        }
        todosService?.fetchBuildings(completion: completion)
    }
    
    func fetchBuildingFloors(remoteManager: RemoteManager, buildingId: Int, completion: @escaping SBMTypealias.ResponseResult<NameKeyValueMappable>) {
        if(todosService == nil){
            todosService = TodosService(remoteManager: remoteManager)
        }
        todosService?.fetchBuildingFloors(buildingId: buildingId, completion: completion)
    }
    
    func submitTodoItem(remoteManager: RemoteManager, todoItem: TodoItemMappable, completion: @escaping SBMTypealias.ResponseResult<TodoItemIdMappable>){
        if(todosService == nil){
            todosService = TodosService(remoteManager: remoteManager)
        }
        todosService?.submitTodoItem(todoItem: todoItem, completion: completion)
    }
    
//    func submitProfessionalismEmployeeAudit(remoteManager: RemoteManager, employeeAuditItem: EmployeeAuditItem!, completion: @escaping SBMTypealias.ResponseResult<EmployeeAudit>){
//        if(professionalismService == nil){
//            professionalismService = ProfessionalismService(remoteManager: remoteManager)
//        }
//        professionalismService?.submitProfessionalismEmployeeAudit(employeeAuditItem: employeeAuditItem, completion: completion)
//    }

    func submitAttendance(remoteManager: RemoteManager, issue: IssueMappable, completion: @escaping SBMTypealias.ResponseResult<IssueMappable>){
        if(submitService == nil){
            submitService = SubmitService(remoteManager: remoteManager)
        }
        submitService?.submitAttendance(issue: issue, completion: completion)
    }
    
    func submitConduct(remoteManager: RemoteManager, conduct: Conduct, completion: @escaping SBMTypealias.ResponseString){
        if(submitService == nil){
            submitService = SubmitService(remoteManager: remoteManager)
        }
        submitService?.submitConduct(conduct: conduct, completion: completion)
    }
    
    func submitItem<T>(remoteManager: RemoteManager, item: T, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseString) {
        if(submitService == nil){
            submitService = SubmitService(remoteManager: remoteManager)
        }
        submitService?.submitItem(item: item, apiRouter: apiRouter, completion: completion)
    }
    func submitItemArray<T>(remoteManager: RemoteManager, items: [T], apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseAny) {
        if(submitService == nil){
            submitService = SubmitService(remoteManager: remoteManager)
        }
        submitService?.submitItemArray(items: items, apiRouter: apiRouter, completion: completion)
    }
    
//    func fetchDepartments(remoteManager: RemoteManager, completion: @escaping SBMTypealias.ResponseString) {
//        if(fetchService == nil){
//            fetchService = FetchService(remoteManager: remoteManager)
//        }
//        fetchService?.fetchDepartments(completion: completion)
//    }
    
    func fetchSiteJobs(remoteManager: RemoteManager, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseDatum<JobForSite>) {
        if(fetchService == nil){
            fetchService = FetchService(remoteManager: remoteManager)
        }
        fetchService?.fetchSiteJobs(apiRouter: apiRouter, completion: completion)
    }
    
    func fetchDepartments(remoteManager: RemoteManager,  apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseDatum<Department>) {
        if(fetchService == nil){
            fetchService = FetchService(remoteManager: remoteManager)
        }
        fetchService?.fetchDepartments(apiRouter: apiRouter, completion: completion)
    }
    
    func fetchComplaintTypes(remoteManager: RemoteManager, selectedProgramId: Int, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseResult<NameKeyValueMappable>){
        if(fetchService == nil){
            fetchService = FetchService(remoteManager: remoteManager)
        }
        fetchService?.fetchComplaintTypes(programId: selectedProgramId, apiRouter: apiRouter, completion: completion)
        
    }
    
    func fetchClassifications(remoteManager: RemoteManager, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseResult<NameKeyValueMappable>){
        fetchNameKeyValue(remoteManager: remoteManager, apiRouter: apiRouter, completion: completion)
        
    }
    func fetchPreventableStatus(remoteManager: RemoteManager, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseResult<NameKeyValueMappable>){
        fetchNameKeyValue(remoteManager: remoteManager, apiRouter: apiRouter, completion: completion)
    }
    
    func fetchComplimentTypes(remoteManager: RemoteManager, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseResult<NameKeyValueMappable>){
        fetchNameKeyValue(remoteManager: remoteManager, apiRouter: apiRouter, completion: completion)
    }
    
    fileprivate func fetchNameKeyValue(remoteManager: RemoteManager, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseResult<NameKeyValueMappable>){
        if(fetchService == nil){
            fetchService = FetchService(remoteManager: remoteManager)
        }
        fetchService?.fetchNameKeyValue(apiRouter: apiRouter, completion: completion)
    }
    
    
    
    func fetchScorecards(remoteManager: RemoteManager, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseData<Scorecard>){
        if(fetchService == nil){
            fetchService = FetchService(remoteManager: remoteManager)
        }
        fetchService?.fetchScorecards(apiRouter: apiRouter, completion: completion)
    }
    func fetchSiteReportSetting(remoteManager: RemoteManager, selectedSiteId: Int, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseData<SiteReportSetting>){
        if(fetchService == nil){
            fetchService = FetchService(remoteManager: remoteManager)
        }
        fetchService?.fetchSiteReportSetting(siteId: selectedSiteId, apiRouter: apiRouter, completion: completion)
    }
    
    
    ///// Fetch - JSON /////
    func fetchCustomerRepresentatives(remoteManager: RemoteManager, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseJSON){
        
        if(fetchService == nil){
            fetchService = FetchService(remoteManager: remoteManager)
        }
        fetchService?.fetchCustomerRepresentatives(apiRouter: apiRouter, completion: completion)
    }
    
    func fetchDataWithDate(remoteManager: RemoteManager, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseJSONWithDate){
        
        if(fetchService == nil){
            fetchService = FetchService(remoteManager: remoteManager)
        }
        fetchService?.fetchDataWithDate(apiRouter: apiRouter, completion: completion)
    }
    
    func fetchDataWithDate(remoteManager: RemoteManager, id: Int, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseJSONWithDate){
        
        if(fetchService == nil){
            fetchService = FetchService(remoteManager: remoteManager)
        }
        fetchService?.fetchDataWithDate(id: id, apiRouter: apiRouter, completion: completion)
    }
    
    func fetchDataWithDate(remoteManager: RemoteManager, parameters: [String: Any], pageNumber: Int, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseJSONWithDate){
        
        if(fetchService == nil){
            fetchService = FetchService(remoteManager: remoteManager)
        }
        fetchService?.fetchDataWithDate(parameters: parameters, pageNumber: pageNumber, apiRouter: apiRouter, completion: completion)
    }
    ///// DELETE - JSON /////
    func deleteItem<T, O>(remoteManager: RemoteManager, item: T, itemObject: O, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseJSON) {
        if(deleteService == nil){
            deleteService = DeleteService(remoteManager: remoteManager)
        }
        deleteService?.deleteItem(item: item, itemObject: itemObject, apiRouter: apiRouter, completion: completion)
    }
    
    ///// PUT - JSON /////
    func putItem<T, O>(remoteManager: RemoteManager, item: T, itemObject: O, apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseJSON) {
        if(putService == nil){
            putService = PutService(remoteManager: remoteManager)
        }
        putService?.putItem(item: item, itemObject: itemObject, apiRouter: apiRouter, completion: completion)
    }
    
    ///// POST - JSON /////
    func postArrayWithDate(remoteManager: RemoteManager, ary: [String], apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseJSONWithDate){
        if(postService == nil){
            postService = PostService(remoteManager: remoteManager)
        }
        postService?.postDataWithDate(id: nil, ary: ary, apiRouter: apiRouter, completion: completion)

    }
//    func postArrayWithDate<T>(remoteManager: RemoteManager, ary: [T], apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseJSONWithDate){
//        postArrayWithDate(remoteManager: remoteManager, id: nil, ary: ary, apiRouter: apiRouter, completion: completion)
//        if(postService == nil){
//            postService = PostService(remoteManager: remoteManager)
//        }
//        postService?.postDataWithDate(apiRouter: apiRouter, completion: completion)
//    }
    
//    func postArrayWithDate<T>(remoteManager: RemoteManager, id: Int?, ary: [T], apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseJSONWithDate){
//        
//        if(postService == nil){
//            postService = PostService(remoteManager: remoteManager)
//        }
//        postService?.postDataWithDate(id: id, ary: ary, apiRouter: apiRouter, completion: completion)
//    }
    
}

