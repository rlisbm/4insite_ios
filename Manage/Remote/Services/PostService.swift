//
//  PostService.swift
//  Manage
//
//  Created by Rong Li on 4/1/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireObjectMapper
import SwiftKeychainWrapper
import RealmSwift
import Realm

class PostService: BaseService {
    
    let remoteManager: RemoteManager
    
    fileprivate var url: String?
    fileprivate var headers:[String: String]?
    
    init(remoteManager: RemoteManager){
        self.remoteManager = remoteManager
    }
    
    func getJString(from object: Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    func postDataWithDate(id: Int?, ary: [String], apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseJSONWithDate){
        
        switch apiRouter {
        case APIRouter.dailyTimeCardStats:
            url = getUrl(at: apiRouter, baseUrl: remoteManager.baseUrl)
            headers = getHeaders()
            break
        default:
            url = getUrl(at: apiRouter, baseUrl: remoteManager.baseUrl)
            headers = getHeaders()
            break
        }
        
        var request = URLRequest(url: try! url!.asURL())
        request.httpMethod = HTTPMethod.post.rawValue
        
        request.allHTTPHeaderFields = getHeaders()
        guard let data = try? JSONSerialization.data(withJSONObject: ary, options: []) else {
            return
        }
        request.httpBody = data
        
        let primaryKey = getPrimaryKey(apiRouter: apiRouter, id: id)
        let storedObject = getJSONFromDB(objectType: JSONData.self, primaryKey: primaryKey!) as? JSONData
        Alamofire.request(request).responseJSON { response in
            
//            print("### ### Success: \(response.result.isSuccess)  ")
//            print("### ### Response String: \(String(describing: response.result.value))")
            
            self.handleResponse(response: response, primaryKey: primaryKey!, storedObject: storedObject, requestData: ary, apiRouter: apiRouter, completion: completion)
            
        }
    }
    
    func handleResponse(response: DataResponse<Any>, primaryKey: String, storedObject: JSONData?, requestData: [String], apiRouter: APIRouter, completion: @escaping SBMTypealias.ResponseJSONWithDate ){
        var statusCode = response.response?.statusCode ?? Constants.UnKnowFailureCode
        switch(response.result){
        case .success:
            if(statusCode == Constants.ResponseOK){
                let object = JSONData()
                object.jsonDataPrimaryKey = primaryKey
                object.data = response.data
                object.date = Date()
                if(storedObject == nil){
                    self.realmDB.insertObject(object)
                }else{
                    self.realmDB.updateObject(object)
                }
                completion(response.data, nil, statusCode, Date())
            }else{
                completion(storedObject?.data, nil, statusCode, storedObject?.date)
                
            }
            break
        case .failure(let error):
            print(error)
            statusCode = self.getFailureStatusCode(response: response)
            print("### ### statusCode: \(statusCode)")
            completion(storedObject?.data, nil, statusCode, storedObject?.date)
            break
        }
    }
}
