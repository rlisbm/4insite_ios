//
//  LookupBaseService.swift
//  Manage
//
//  Created by Rong Li on 12/10/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireObjectMapper
import SwiftKeychainWrapper
import RealmSwift
import Realm

class LookupBaseService: BaseService {
    
    let lookupRemoteManager: RemoteManager

    init(lookupRemoteManager: RemoteManager){
        self.lookupRemoteManager = lookupRemoteManager
    }
    
    func fetchBuildings(completion: @escaping SBMTypealias.ResponseResult<NameKeyValueMappable>){
        let token = getToken()
        if(token == ""){
            return
        }
        let url = getUrl(at: .buildings, baseUrl: lookupRemoteManager.baseUrl)
        
        let headers = getHeaders()
        
        Alamofire.request(url, method:.get, headers: headers)
            .responseString { response in
                print("### ### buildings")
                print("### ### Success: \(response.result.isSuccess)")
                print("### ### Response String: \(String(describing: response.result.value))")
        }

        
        Alamofire.request(url, method:.get, headers: headers).validate().responseObject { (response: DataResponse<NameKeyValueMappable>) in
            
            switch(response.result){
            case .success(let value):
                let statusCode = (response.response?.statusCode)!
                if(statusCode == Constants.ResponseOK){
                    let fetchedBuidings = value
                    completion(fetchedBuidings, nil, statusCode)
                }
                break
            case .failure(let error):
                print(error)
                break
            }
            
        }
    }
    
    func fetchBuildingFloors(buildingId: Int, completion: @escaping SBMTypealias.ResponseResult<NameKeyValueMappable>){
        let token = getToken()
        if(token == ""){
            return
        }
        let url = getUrl(at: .floors, baseUrl: lookupRemoteManager.baseUrl, id: buildingId, placeholder: "{buildingId}")
        let headers = getHeaders()
        
        Alamofire.request(url, method:.get, headers: headers)
            .responseString { response in
                print("### ### floors")
                print("### ### Success: \(response.result.isSuccess)")
                print("### ### Response String: \(String(describing: response.result.value))")
        }
        
        Alamofire.request(url, method:.get, headers: headers).validate().responseObject { (response: DataResponse<NameKeyValueMappable>) in
            
            switch(response.result){
            case .success(let value):
                let statusCode = (response.response?.statusCode)!
                if(statusCode == Constants.ResponseOK){
                    
                    let statusCode = (response.response?.statusCode)!
                    if(statusCode == Constants.ResponseOK){
                        let fetchedBuidingFloors = value
                        completion(fetchedBuidingFloors, nil, statusCode)
                    }
                    break
                    
                }
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
}
