//
//  ProfessionalismService.swift
//  Manage
//
//  Created by Rong Li on 12/13/18.
//  Copyright © 2018 Insite. All rights reserved.
//

//import Foundation
//import Alamofire
//import SwiftyJSON
//import AlamofireObjectMapper
//import SwiftKeychainWrapper
//import RealmSwift
//import Realm
//
//class EmployeeAuditRequestItem: Codable{
//    var CheckBoxSelections: [Int] = [Int]()
//    let Comment: String
//    let EmployeeId: Int!
//    let IsEventBasedAudit: Bool!
//
//    init(checkBoxSelections: List<Int>,
//         comment: String,
//         employeeId: Int,
//         isEventBasedAudit: Bool){
//        for selection in checkBoxSelections {
//            self.CheckBoxSelections.append(selection)
//        }
//        self.Comment = comment
//        self.EmployeeId = employeeId
//        self.IsEventBasedAudit = isEventBasedAudit
//    }
//}
//
//
//class ProfessionalismService: BaseService {
//
//    let remoteManager: RemoteManager
//
//    init(remoteManager: RemoteManager){
//        self.remoteManager = remoteManager
//
//    }
//    fileprivate func saveItem(employeeAuditItem: EmployeeAuditItem){
//        employeeAuditItem.employeeAuditItemPrimaryKey = String(Date().toMillis())
//        realmDB.insertObject(employeeAuditItem)
//    }
//
//    func submitProfessionalismEmployeeAudit(employeeAuditItem: EmployeeAuditItem, completion: @escaping SBMTypealias.ResponseResult<EmployeeAudit>){
//        let employeeAuditRequestItem = EmployeeAuditRequestItem(checkBoxSelections: employeeAuditItem.checkBoxSelections,
//                                                                comment: employeeAuditItem.comment ?? "",
//                                                        employeeId: employeeAuditItem.employeeId,
//                                                        isEventBasedAudit: employeeAuditItem.isEventBasedAudit)
//
//        let jsonEncoder = JSONEncoder()
//        var jsonString: String?
//        do {
//            let jsonData = try jsonEncoder.encode(employeeAuditRequestItem)
//            jsonString = String(data: jsonData, encoding: .utf8)
//            print("JSON String : " + jsonString!)
//        }
//        catch {
//            print("error")
//        }
//
////        let JSONString = employeeAuditItem.toJSONString(prettyPrint: true)
////        print(JSONString?.convertToDictionary())
////        let parameters = JSONString?.convertToDictionary()
//
//        let parameters = jsonString?.convertToDictionary()
//        let url = getUrl(at: .employeeAudit, baseUrl: remoteManager.baseUrl)
//        print("*** *** url : \(url)")
//        let headers = getHeaders()
//
//        Alamofire.request(url, method:.post, parameters: parameters, encoding:  JSONEncoding.default, headers: headers)
//            .responseString { response in
//                print("### ### Success: \(response.result.isSuccess)")
//                print("### ### Response String: \(String(describing: response.result.value))")
//
//
//        }
//
////        Alamofire.request(url, method:.post, parameters: parameters, encoding:  JSONEncoding.default, headers: headers)
////            .validate().responseObject { (response: DataResponse<EmployeeAudit>) in
////
////                switch(response.result){
////                case .success(let value):
////                    let statusCode = (response.response?.statusCode)!
////                    if(statusCode != self.resultOK){
//////                        self.saveItem(todoItem: todoItem)
////                    }
////                    completion(value, nil, statusCode)
////                    break
////                case .failure(let error):
//////                    self.saveItem(todoItem: todoItem)
////                    completion(nil, error, -1)
////                    break
////
////                }
////        }
//
//    }
//
//}
