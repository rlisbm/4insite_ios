//
//  HideKeyboardType.swift
//  Manage
//
//  Created by Rong Li on 10/24/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation

enum HideKeyboardType: String {
    case KeyboradReturn = "KeyboradReturn"
    case ScreenTounch = "ScreenTounch"
}

enum FeatureName: String {
    case Attendance = "Attendance"
    case Audit = "Audit"
    case RateAudit = "RateAudit"
    case AuditAssignTodo = "AuditAssignTodo"
    case Complaint = "Complaint"
    case Compliment = "Compliment"
    case Conduct = "Conduct"
    case Professionalism = "Professionalism"
    case ReportIt = "ReportIt"
    case Safety = "Safety"
    case SendMessage = "SendMessage"
    case Todos = "Todos"
}

enum DatePickerType: Int {
    case DateAndTime = 0  //show datetime popup, but datetime pass back
    case DateOnly = 1  //show date popup, only date pass back
    case TimeOnly = 2  //show time popup, only time pass back
    case Date = 3  //show date only popup, but datetime pass back
}

enum SenderType: String {
    case manager
    case department
    
    var path: String {
        switch self {
        case .manager:
            return "Manager"
        case .department:
            return "Department"
        }
    }
}

enum UserAttachmentTypeEnum: String{
    case ProfilePicture = "ProfilePicture"
    case SocialPicture = "SocialPicture"
    case BackgroundPicture = "BackgroundPicture"
}

