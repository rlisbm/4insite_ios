//
//  ProfessionalismItem.swift
//  Manage
//
//  Created by Rong Li on 12/13/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation

//let kpiOptionIdAry: [Int] = [1, 2, 4, 3]
let kpiSubOptionIdAry: [Int] = [1, 4, 7, 9]
let titleAry: [String] = [NSLocalizedString("key_appearance_title", comment: ""),
                          NSLocalizedString("key_attitude_title", comment: ""),
                          NSLocalizedString("key_responsiveness_title", comment: ""),
                          NSLocalizedString("key_equipment_closets_title", comment: "")]

let descriptionAry: [String] = [NSLocalizedString("key_appearance_description", comment: ""),
                                NSLocalizedString("key_attitude_description", comment: ""),
                                NSLocalizedString("key_responsiveness_description", comment: ""),
                                NSLocalizedString("key_equipment_closets_description", comment: "")]
let isPassAry: [Bool] = [true, true, true, true]

class ProfessionalismItem {
//    var kpiOptionId: Int
    var kpiSubOptionId: Int
    var title: String
    var description: String
    var isPass: Bool
    
    init(kpiSubOptionId: Int, title: String, description: String, isPass: Bool) {
//        self.kpiOptionId = kpiOptionId
        self.kpiSubOptionId = kpiSubOptionId
        self.title = title
        self.description = description
        self.isPass = isPass
    }
}
