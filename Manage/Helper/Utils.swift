//
//  Utils.swift
//  Manage
//
//  Created by Rong Li on 10/30/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

struct Constants {
    static let ResponseOK: Int = 200
    static let ResponseFailed401: Int = 401
    static let PostResponseSaveSuccess: Int = 2000
    static let PostResponseSaveFailed: Int = 4001
    static let ConnectivityFailureCode: Int = 4003  //this may be replaced by OffLineError
    static let UnKnowFailureCode: Int = -4444
    static let OffLineError: Int = -1009
    
    static let ReSendingNow: Int = 1
    static let ReSendingPause: Int = 0

    static let textBlack = "#212121"
    static let textWhite = "#D4D3E6"
    static let textDarkWhite = "#8785A1"
    
    static let btnDarkGrayBG = "#3D3C4D"
    static let btnLightGrayBG = "#CFCFCF"
    static let btnTextBlue = "#03A9F4"
    static let btnTextWhite = "#A7A7B2"
    static let btnTextRed = "#EF5350"
    
    static let pageSelectedDotBGColor: String = "#7680C8"
    static let pageUnSelectedDotBGColor: String = "#3D3C4D"
    static let pageSelectedDotTextColor: String = "#04040E"
    static let pageUnSelectedDotTextColor: String = "#E0E0E0"
    
//    static let boarderColor: String = "#262626"
    static let boarderColor: String = "#4d4c5b"
    static let dropDownRowBG: String = "#212033"
    static let dropDownTextColor: String = "#A7A7B2"
    static let drowDownRowHeight: CGFloat = 50
    
    static let passGreen: String = "#8BC34A"
    static let failRed: String = "#F44336"
    static let passNilGray: String = "#BDBDBD"
    
    //snackbar color
    static let snackbarMsgColor: String = "#D4D3E6"
    static let snackbarActionColor: String = "#03A9F4"
    static let snackbarBgColor: String = "#212033"
    
    static let DATE_UTC_SSSZ = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    static let DATE_UTC_SS = "yyyy-MM-dd'T'HH:mm:ss"
    static let DATE_UTC_SSS = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    static let DATE_UTC_SSZ = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    static let DATE_LOCAL_SS = "mm.dd/yy HH:mm:ss"
    static let DATE_NO_YEAR = "MMM dd hh:mm a"
    static let DATE_FULL = "HH:mm MM/dd/yy"
    static let TIME_A = "hh:mm a"
    static let DATE_DAILY = "MM/dd/yy hh:mm a"
    
    static let tabMap = 0
    static let tabDaily = 1
    static let tabCreate = 2
    static let tabMore = 3
    
    static let clockIn = "ClockIn"
    static let clockOut = "ClockOut"
    
}

struct Connectivity {
    static let sharedInstance = NetworkReachabilityManager()!
    static var isConnectedToInternet:Bool {
        return self.sharedInstance.isReachable
    }
}

func showAlert(_ title: String, _ message: String, _ dismiss: String, viewController: UIViewController){
    let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
    alertController.addAction(UIAlertAction(title: dismiss, style: .default, handler: nil))
    viewController.present(alertController, animated: true, completion: nil)
}

func showAlert(title: String, message: String, positiveBtn: String, nagetiveBtn: String, viewController: UIViewController, completion: @escaping SBMTypealias.WarningCompletion){
    let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
    alertController.addAction(UIAlertAction(title: nagetiveBtn, style: .default, handler: { (action) in
        completion(false)
    }))
    alertController.addAction(UIAlertAction(title: positiveBtn, style: .default, handler: { (action) in
        completion(true)
    }))
    
    viewController.present(alertController, animated: true, completion: nil)
}


