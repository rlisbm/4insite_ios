//
//  AssociateHelper.swift
//  Manage
//
//  Created by Rong Li on 10/24/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation

////Mark: SBMTokenHelper
//struct SBMTokenHelper {
//    static let key = "sbm_auth_token"
//    
//    static func save(_ value: SBMToken!) {
//        UserDefaults.standard.set(try? PropertyListEncoder().encode(value), forKey: key)
//    }
//    static func get() -> SBMToken! {
//        var userData: SBMToken!
//        if let data = UserDefaults.standard.value(forKey: key) as? Data {
//            userData = try? PropertyListDecoder().decode(SBMToken.self, from: data)
//            return userData!
//        } else {
//            return userData
//        }
//    }
//    static func remove() {
//        UserDefaults.standard.removeObject(forKey: key)
//    }
//}
//
////Mark: LoginCredentials
//struct LoginCredentials: Codable {
//    let userName: String!
//    let password: String!
//}
//struct LoginCredentialsHelper {
//    static let key = "login_credentials"
//    
//    static func save(_ value: LoginCredentials!) {
//        UserDefaults.standard.set(try? PropertyListEncoder().encode(value), forKey: key)
//    }
//    static func get() -> LoginCredentials! {
//        var userData: LoginCredentials!
//        if let data = UserDefaults.standard.value(forKey: key) as? Data {
//            userData = try? PropertyListDecoder().decode(LoginCredentials.self, from: data)
//            return userData!
//        } else {
//            return userData
//        }
//    }
//    static func remove() {
//        UserDefaults.standard.removeObject(forKey: key)
//    }
//}

//Mark: Associate
struct Associate: Codable {
    let employeeId: Int!
    let clientId: Int!
    let siteId: Int!
    let firstName: String
    let lastName: String
    let fullName: String
    let email: String
    let profilePictureUniqueId: String
    let jobTitle: String
}
struct AssociateHelper {
    
    static let key = "associate_info"
    
    static func save(_ value: Associate!) {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(value), forKey: key)
    }
    static func get() -> Associate! {
        var userData: Associate!
        if let data = UserDefaults.standard.value(forKey: key) as? Data {
            userData = try? PropertyListDecoder().decode(Associate.self, from: data)
            return userData!
        } else {
            return userData
        }
    }
    static func remove() {
        UserDefaults.standard.removeObject(forKey: key)
    }
}


struct SitePickerSelectedSite: Codable {
    let selectedClientId: Int!
    let selectedClientName: String
    let selectedSiteId: Int!
    let selectedSiteName: String
    let selectedProgramId: Int!
    let selectedProgramName: String
}

struct SelectedSiteHelper {
    static let key = "selected_site"
    
    static func save(_ value: SitePickerSelectedSite!) {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(value), forKey: key)
    }
    static func get() -> SitePickerSelectedSite! {
        var userData: SitePickerSelectedSite!
        if let data = UserDefaults.standard.value(forKey: key) as? Data {
            userData = try? PropertyListDecoder().decode(SitePickerSelectedSite.self, from: data)
            return userData!
        } else {
            return userData
        }
    }
    static func remove() {
        UserDefaults.standard.removeObject(forKey: key)
    }
}

struct SelectedSiteInfoHelper {
    static let key = "selected_site_info"
    
//    static func save(_ value: SiteMappable!) {
    static func save(_ value: Site!) {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(value), forKey: key)
    }
//    static func get() -> SiteMappable! {
    static func get() -> Site! {
//        var userData: SiteMappable!
        var userData: Site!
        if let data = UserDefaults.standard.value(forKey: key) as? Data {
//            userData = try? PropertyListDecoder().decode(SiteMappable.self, from: data)
            userData = try? PropertyListDecoder().decode(Site.self, from: data)
            return userData!
        } else {
            return userData
        }
    }
    static func remove() {
        UserDefaults.standard.removeObject(forKey: key)
    }
}
