//
//  AbsentReasonView.swift
//  Manage
//
//  Created by Rong Li on 12/19/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class AbsentReasonView: UIView {
    let sickReasonId = 1
    let vacationReasonId = 2
    let otherReasonId = 3

    @IBOutlet var subView: UIView!
    @IBOutlet weak var sickBtn: UIButton!
    @IBOutlet weak var vacationBtn: UIButton!
    @IBOutlet weak var otherBtn: UIButton!
    
    var onAbsentReasonClicked: ((_ reasonName: String, _ reasonId: Int) -> ())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "AbsentReasonView", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        addSubview(subView)
        subView.frame = bounds
    }
    
    @IBAction func sickBtnClicked(_ sender: UIButton) {
        onAbsentReasonClicked?(sender.titleLabel?.text ?? "", sickReasonId)
        if(sender.tag == 0){
            sender.tag = 1
            sender.backgroundColor = UIColor.init(hexString: Constants.btnLightGrayBG)
            sender.setTitleColor(UIColor.init(hexString: Constants.textBlack), for: .normal)
            
            vacationBtn.tag = 0
            vacationBtn.backgroundColor = UIColor.init(hexString: Constants.btnDarkGrayBG)
            vacationBtn.setTitleColor(UIColor.init(hexString: Constants.textWhite), for: .normal)
            otherBtn.tag = 0
            otherBtn.backgroundColor = UIColor.init(hexString: Constants.btnDarkGrayBG)
            otherBtn.setTitleColor(UIColor.init(hexString: Constants.textWhite), for: .normal)
        }
    }
    
    @IBAction func vacationBtnClicked(_ sender: UIButton) {
        onAbsentReasonClicked?(sender.titleLabel?.text ?? "", sickReasonId)
        if(sender.tag == 0){
            sender.tag = 1
            sender.backgroundColor = UIColor.init(hexString: Constants.btnLightGrayBG)
            sender.setTitleColor(UIColor.init(hexString: Constants.textBlack), for: .normal)
            
            sickBtn.tag = 0
            sickBtn.backgroundColor = UIColor.init(hexString: Constants.btnDarkGrayBG)
            sickBtn.setTitleColor(UIColor.init(hexString: Constants.textWhite), for: .normal)
            otherBtn.tag = 0
            otherBtn.backgroundColor = UIColor.init(hexString: Constants.btnDarkGrayBG)
            otherBtn.setTitleColor(UIColor.init(hexString: Constants.textWhite), for: .normal)
        }
    }
    
    @IBAction func otherBtnClicked(_ sender: UIButton) {
        onAbsentReasonClicked?(sender.titleLabel?.text ?? "", sickReasonId)
        if(sender.tag == 0){
            sender.tag = 1
            sender.backgroundColor = UIColor.init(hexString: Constants.btnLightGrayBG)
            sender.setTitleColor(UIColor.init(hexString: Constants.textBlack), for: .normal)
            
            vacationBtn.tag = 0
            vacationBtn.backgroundColor = UIColor.init(hexString: Constants.btnDarkGrayBG)
            vacationBtn.setTitleColor(UIColor.init(hexString: Constants.textWhite), for: .normal)
            sickBtn.tag = 0
            sickBtn.backgroundColor = UIColor.init(hexString: Constants.btnDarkGrayBG)
            sickBtn.setTitleColor(UIColor.init(hexString: Constants.textWhite), for: .normal)
        }
    }
    
}
