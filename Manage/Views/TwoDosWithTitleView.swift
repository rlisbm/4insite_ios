//
//  TwoDosView.swift
//  Manage
//
//  Created by Rong Li on 12/17/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class TwoDosWithTitleView: UIView {

    @IBOutlet var twoDotsView: UIView!
    @IBOutlet weak var dot1: UIButton!
    @IBOutlet weak var dot2: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "TwoDotsWithTitleView", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        addSubview(twoDotsView)
        twoDotsView.frame = bounds
    }
    
}
