//
//  ReviewAssignedTopView.swift
//  Manage
//
//  Created by Rong Li on 12/20/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class ReviewAssignedTopView: UIView {
    
    @IBOutlet var subView: UIView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var fullName: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "ReviewAssignTopView", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        addSubview(subView)
        subView.frame = bounds
    }
}
