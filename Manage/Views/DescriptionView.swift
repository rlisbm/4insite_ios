//
//  DescriptionView.swift
//  Manage
//
//  Created by Rong Li on 12/20/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class DescriptionView: UIView {
    @IBOutlet var subView: DescriptionView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var content: UITextView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "DescriptionView", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        addSubview(subView)
        subView.frame = bounds
        
        content.layer.borderColor = UIColor(hexString: Constants.boarderColor).cgColor
     
    }

}
