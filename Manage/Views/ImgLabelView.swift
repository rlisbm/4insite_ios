//
//  ImgLabelView.swift
//  Manage
//
//  Created by Rong Li on 3/20/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class ImgLabelView: UIView {
    @IBOutlet var imgLabelView: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!    
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var imgWidth: NSLayoutConstraint!
    @IBOutlet weak var imgHeight: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "ImgLabelView", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        addSubview(imgLabelView)
        imgLabelView.frame = bounds
    }

}
