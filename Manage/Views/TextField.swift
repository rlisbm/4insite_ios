//
//  TextField.swift
//  Manage
//
//  Created by Rong Li on 3/21/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

open class TextField: UITextField {
    
    let padding = UIEdgeInsets(top: 20, left: 16, bottom: 0, right: 0)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
}

// for Swift 4.2
//class TextField: UITextField {
//
//    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
//
//    override open func textRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.inset(by: padding)
//    }
//
//    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.inset(by: padding)
//    }
//
//    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.inset(by: padding)
//    }
//}
