//
//  RightImgBtn.swift
//  Manage
//
//  Created by Rong Li on 11/20/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import Foundation
import UIKit

class RightImageBtn: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            imageEdgeInsets = UIEdgeInsets(top: 5, left: (bounds.width - 25), bottom: 5, right: 5)
            //titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: (imageView?.frame.width)!)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: -(imageView?.frame.width)!, bottom: 0, right: (imageView?.frame.width)!)
        }
    }
}

