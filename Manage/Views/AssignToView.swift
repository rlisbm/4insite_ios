//
//  AssignToView.swift
//  Manage
//
//  Created by Rong Li on 12/27/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class AssignToView: UIView {

    @IBOutlet var assignToView: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var toggleBtn: UIButton!
    
    var onEditBtnClicked: (() -> Void)?
    var onToggleBtnClicked: ((_ toggleBtn: UIButton) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
        
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "AssignToView", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        addSubview(assignToView)
        assignToView.frame = bounds
        
        editBtn.underLine()
        toggleBtn.underLine()
        
    }
    
    @IBAction func editBtnClicked(_ sender: Any) {
        onEditBtnClicked?()
    }
    
    @IBAction func toggleBtnClicked(_ sender: UIButton) {
        sender.tag = sender.tag == 0 ? 1 : 0
        onToggleBtnClicked?(sender)
    }
    
    
}
