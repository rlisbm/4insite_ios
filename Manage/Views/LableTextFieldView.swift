//
//  LableTextField.swift
//  Manage
//
//  Created by Rong Li on 4/5/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class LableTextFieldView: UIView {
    
    @IBOutlet var labelTextFieldView: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "LableTextFieldView", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        addSubview(labelTextFieldView)
        labelTextFieldView.frame = bounds
    }

}
