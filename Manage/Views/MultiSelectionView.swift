//
//  MultiSelectionView.swift
//  Manage
//
//  Created by Rong Li on 11/28/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class MultiSelectionView: UIView {

    var onSelectBtnClicked: (() -> Void)?
    
    @IBOutlet var multiSelectionView: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var button: UIButton!

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
        
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "MultiSelectionView", bundle: bundle).instantiate(withOwner: self, options: nil)

        addSubview(multiSelectionView)
        multiSelectionView.frame = bounds
    }
    
    @IBAction func btnClicked(_ sender: Any) {
        onSelectBtnClicked?()
    }


}
