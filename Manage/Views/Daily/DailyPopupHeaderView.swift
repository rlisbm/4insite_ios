//
//  DailyPopupHeaderView.swift
//  Manage
//
//  Created by Rong Li on 4/4/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class DailyPopupHeaderView: UIView {

    var onCloseBtnClicked: (() -> Void)?
    
    @IBOutlet var dailyPopupHeaderView: UIView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var closeBtn: UIButton!
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
        
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "DailyPopupHeaderView", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        addSubview(dailyPopupHeaderView)
        dailyPopupHeaderView.frame = bounds
    }
    
    @IBAction func closeBtnClicked(_ sender: UIButton) {
        onCloseBtnClicked?()
    }
    
    

}
