//
//  TwoBtnsView.swift
//  Manage
//
//  Created by Rong Li on 4/5/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class TwoBtnsView: UIView {

    @IBOutlet var twoBtnsView: UIView!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    
    var onBtn1Clicked: (() -> Void)?
    var onBtn2Clicked: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
        
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "TwoBtnsView", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        addSubview(twoBtnsView)
        twoBtnsView.frame = bounds
    }
    
    @IBAction func btn1Clicked(_ sender: Any) {
        onBtn1Clicked?()
    }
    
    @IBAction func btn2Clicked(_ sender: Any) {
        onBtn2Clicked?()
    }

}
