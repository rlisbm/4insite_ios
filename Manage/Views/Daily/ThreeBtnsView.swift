//
//  ThreeBtns.swift
//  Manage
//
//  Created by Rong Li on 4/4/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class ThreeBtnsView: UIView {
    
    @IBOutlet var threeBtnsView: UIView!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    
    var onBtn1Clicked: (() -> Void)?
    var onBtn2Clicked: (() -> Void)?
    var onBtn3Clicked: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
        
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "ThreeBtnsView", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        addSubview(threeBtnsView)
        threeBtnsView.frame = bounds
    }
    
    @IBAction func btn1Clicked(_ sender: Any) {
        onBtn1Clicked?()
    }
    
    @IBAction func btn2Clicked(_ sender: Any) {
        onBtn2Clicked?()
    }
    
    @IBAction func btn3Clicked(_ sender: Any) {
        onBtn3Clicked?()
    }
}
