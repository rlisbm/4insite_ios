//
//  HeaderView.swift
//  Manage
//
//  Created by Rong Li on 11/26/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class HeaderView: UIView {
    
    var onCloseBtnClicked: (() -> Void)?
    var onSaveBtnClicked: (() -> Void)?

    @IBOutlet var headerView: UIView!
    @IBOutlet weak var screenName: UILabel!
    @IBOutlet weak var saveBtn: UIButton!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
        
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "HeaderView", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        addSubview(headerView)
        headerView.frame = bounds
    }

    @IBAction func closeBtnClicked(_ sender: UIButton) {
        onCloseBtnClicked?()
    }
    
    @IBAction func saveBtnClicked(_ sender: Any) {
        onSaveBtnClicked?()
    }
    
}
