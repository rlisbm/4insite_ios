//
//  CircleRateView.swift
//  Manage
//
//  Created by Rong Li on 2/1/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class CircleProgressView: UIView {
    let rateCircleSize: CGFloat = 65
    let circleGray: String = "#616161"
    
    var maxScore: Double = 5
    
    @IBInspectable public var trackClolor: CGColor = UIColor.green.cgColor
    @IBInspectable public var shapeClolor: UIColor = UIColor.red

    @IBOutlet var circleProgressView: UIView!
    
    let shapeLayer = CAShapeLayer()
    let trackLayer = CAShapeLayer()
    let bgLayer = CAShapeLayer()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "CircleProgressView", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        let center = circleProgressView.center
        
        //bg
        let bgPath = UIBezierPath(arcCenter: center, radius: rateCircleSize + 10, startAngle: 0, endAngle: CGFloat.pi * 2, clockwise: true)
        
        bgLayer.path = bgPath.cgPath
        bgLayer.fillColor = UIColor.init(hexString: Constants.dropDownRowBG).cgColor
        
        circleProgressView.layer.addSublayer(bgLayer)
        
        
        //bottom track layer
        let trackPath = UIBezierPath(arcCenter: center, radius: rateCircleSize, startAngle: 0, endAngle: CGFloat.pi * 2, clockwise: true)
        
        trackLayer.path = trackPath.cgPath
        trackLayer.strokeColor = UIColor.init(hexString: circleGray).cgColor
        trackLayer.lineWidth = 5
        
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.lineCap = kCALineCapRound
//        trackLayer.lineCap = CAShapeLayerLineCap.round //swift 4.2
        
        circleProgressView.layer.addSublayer(trackLayer)
        
        
        //top shape layer
//        let circularPath = UIBezierPath(arcCenter: center, radius: rateCircleSize, startAngle: CGFloat.pi / 2, endAngle: CGFloat.pi * 2.5, clockwise: true)
        
        let circularPath = UIBezierPath(arcCenter: center, radius: rateCircleSize, startAngle: CGFloat.pi / 2, endAngle: CGFloat.pi * 2.5, clockwise: true)
        
        shapeLayer.path = circularPath.cgPath
        
        shapeLayer.strokeColor = UIColor.init(hexString: circleGray).cgColor
        shapeLayer.lineWidth = 5
        
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineCap = kCALineCapRound
//        shapeLayer.lineCap = CAShapeLayerLineCap.round //swift 4.2
        
        shapeLayer.strokeEnd = 1 //total 8
        
        circleProgressView.layer.addSublayer(shapeLayer)
        
        addSubview(circleProgressView)
        circleProgressView.frame = bounds
    }
    
    public func setMaxScore(maxScore: Double){
        self.maxScore = maxScore
    }
    
    public func updateRateCircle(rate: Double){
        trackLayer.strokeEnd = 1
        shapeLayer.strokeEnd = CGFloat(rate/maxScore)
        if(rate > 0){
            trackLayer.strokeColor = UIColor.init(hexString: Constants.failRed).cgColor
            shapeLayer.strokeColor = UIColor.init(hexString: Constants.passGreen).cgColor
        }else{
            trackLayer.strokeColor = UIColor.init(hexString: circleGray).cgColor
            shapeLayer.strokeColor = UIColor.init(hexString: circleGray).cgColor
        }
    }
}
