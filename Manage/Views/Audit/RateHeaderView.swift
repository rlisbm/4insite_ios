//
//  RateHeaderView.swift
//  Manage
//
//  Created by Rong Li on 2/1/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class RateHeaderView: UIView {

    @IBOutlet var rateHeaderView: UIView!
    @IBOutlet var auditInspectionItemName: UILabel!
    @IBOutlet var buildingFloor: UILabel!
    
    var onNextBtnClicked: (() -> Void)?
    var onPreviousBtnClicked: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "RateHeaderView", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        addSubview(rateHeaderView)
        rateHeaderView.frame = bounds
    }
    
    @IBAction func previousBtnClicked(_ sender: Any) {
        onPreviousBtnClicked?()
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        onNextBtnClicked?()
    }
    
}
