//
//  TableHeaderView.swift
//  Manage
//
//  Created by Rong Li on 1/25/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

protocol TableHeaderViewDelegate: class {
    func toggleSection(header: TableHeaderView, section: Int)
}

class TableHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var average: UILabel!
    @IBOutlet weak var arrow: UIImageView!
    
    
//    var item: ProfileViewModelItem? {
//        didSet {
//            guard let item = item else {
//                return
//            }
//
//            titleLabel?.text = item.sectionTitle
//            setCollapsed(collapsed: item.isCollapsed)
//        }
//    }
    
    var section: Int = 0

    weak var delegate: TableHeaderViewDelegate?

    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapHeader)))
    }

    @objc private func didTapHeader() {
        delegate?.toggleSection(header: self, section: section)
    }

    func setCollapsed(collapsed: Bool) {
        arrow?.rotate(collapsed ? 0.0 : .pi)
    }
    
}
extension UIView {
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")

        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = kCAFillModeForwards

        self.layer.add(animation, forKey: nil)
    }
}
