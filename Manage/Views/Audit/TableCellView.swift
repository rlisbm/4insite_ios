//
//  CellViewTableViewCell.swift
//  Manage
//
//  Created by Rong Li on 1/29/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class TableCellView: UITableViewCell {
    
    @IBOutlet weak var auditInspectionItemName: UILabel!
    @IBOutlet weak var areaName: UILabel!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var areaScore: UILabel!
    @IBOutlet weak var photoView: AuditBageView!
    @IBOutlet weak var todoView: AuditBageView!
    @IBOutlet weak var scoreBarView: UIView!
    
    @IBOutlet weak var commentTextViewHeight: NSLayoutConstraint!
    
    var onAuditBtnClicked: (() -> Void)?
    
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func auditBtnClicked(_ sender: Any) {
        onAuditBtnClicked?()
    }
}
