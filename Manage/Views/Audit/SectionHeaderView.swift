//
//  sectionHeaderView.swift
//  Manage
//
//  Created by Rong Li on 2/4/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class SectionHeaderView: UIView {
    @IBOutlet var sectionHeaderView: UIView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var image: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "SectionHeaderView", bundle: bundle).instantiate(withOwner: self, options: nil)
        addSubview(sectionHeaderView)
        sectionHeaderView.frame = bounds
    }

}
