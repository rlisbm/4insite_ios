//
//  AuditBageView.swift
//  Manage
//
//  Created by Rong Li on 1/29/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class AuditBageView: UIView {

    @IBOutlet var auditBageView: UIView!
    @IBOutlet weak var bageIcon: UIImageView!
    @IBOutlet weak var bageLable: UILabel!
    @IBOutlet weak var bageName: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "AuditBageView", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        addSubview(auditBageView)
        auditBageView.frame = bounds
    }

}
