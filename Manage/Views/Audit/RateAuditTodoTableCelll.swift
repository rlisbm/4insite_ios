//
//  RateAuditTodoTableCellTableViewCell.swift
//  Manage
//
//  Created by Rong Li on 2/5/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class RateAuditTodoTableCell: UITableViewCell {
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var badge: UILabel!
    @IBOutlet weak var comment: UITextView!
    @IBOutlet weak var buildingFloor: UILabel!
    @IBOutlet weak var due: UILabel!
    @IBOutlet weak var separator: UIView!
    
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
