//
//  RateView.swift
//  Manage
//
//  Created by Rong Li on 2/1/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class RateView: UIView {
    let notApplicableText = NSLocalizedString("key_n_a", comment: "")
    
    @IBOutlet var rateView: UIView!
    @IBOutlet weak var progress: CircleProgressView!
    @IBOutlet weak var rate: UILabel!
    
    var onPlusClicked: (() -> Void)?
    var onMinusClicked: (() -> Void)?
    var onPassMeetGoalClicked: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "RateView", bundle: bundle).instantiate(withOwner: self, options: nil)
        addSubview(rateView)
        rateView.frame = bounds
    }
    
    @IBAction func passMeetGoalClicked(_ sender: UIButton) {
        onPassMeetGoalClicked?()
    }
    @IBAction func plusClicked(_ sender: UIButton) {
        onPlusClicked?()
        progress.updateRateCircle(rate: rate.text?.toDouble() ?? 0)
    }
    @IBAction func minusClicked(_ sender: UIButton) {
        onMinusClicked?()
        var score: Double = 0
        if(rate.text != notApplicableText){
            score = rate.text?.toDouble() ?? 0
        }
        progress.updateRateCircle(rate: score)
    }
    
    public func setMaxScore(maxScore: Double){
        progress.setMaxScore(maxScore: maxScore)
    }
}

//extension String {
//    func toDouble() -> Double? {
//        return NumberFormatter().number(from: self)?.doubleValue
//    }
//    
//    func toFloat() -> Float? {
//        return NumberFormatter().number(from: self)?.floatValue
//    }
//}
