//
//  ThreeDotsView.swift
//  Manage
//
//  Created by Rong Li on 1/2/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class ThreeDotsView: UIView {

    @IBOutlet var threeDotsView: UIView!
    @IBOutlet weak var dot1: UIButton!
    @IBOutlet weak var dot2: UIButton!
    @IBOutlet weak var dot3: UIButton!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "ThreeDotsView", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        addSubview(threeDotsView)
        threeDotsView.frame = bounds
    }

}
