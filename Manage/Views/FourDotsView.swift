//
//  FourDotsView.swift
//  Manage
//
//  Created by Rong Li on 12/5/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class FourDotsView: UIView {

    @IBOutlet var fourDotsView: UIView!
    @IBOutlet weak var dot1: UIButton!
    @IBOutlet weak var dot2: UIButton!
    @IBOutlet weak var dot3: UIButton!
    @IBOutlet weak var dot4: UIButton!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "FourDotsView", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        addSubview(fourDotsView)
        fourDotsView.frame = bounds
    }
    
}
