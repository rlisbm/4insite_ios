////
////  DropDown.swift
////  Manage
////
////  Created by Rong Li on 11/5/18.
////  Copyright © 2018 Insite. All rights reserved.
////
//
//import Foundation
//import UIKit
//import RealmSwift
//import Realm
//
//open class DropDown : UITextField{
//    
//    var arrow : Arrow!
//    var table : UITableView!
//    var shadow : UIView!
//    
//    public  var selectedIndex: Int?
//
//    var rowTextColor = UIColor.white
//    
//    //MARK: IBInspectable
//    @IBInspectable public var rowHeight: CGFloat = 50
//    @IBInspectable public var rowBackgroundColor: UIColor = UIColor.init(hexString: "#212033")
//    @IBInspectable public var selectedRowColor: UIColor = UIColor.init(hexString: "#212033")
//    @IBInspectable public var hideOptionsWhenSelect = true
//    @IBInspectable  public var isSearchEnable: Bool = false {
//        didSet{
//            addGesture()
//        }
//    }
//    
//    
//    @IBInspectable public var borderColor: UIColor =  UIColor.lightGray {
//        didSet {
//            layer.borderColor = borderColor.cgColor
//        }
//    }
//    @IBInspectable public var listHeight: CGFloat = 150{
//        didSet {
//            
//        }
//    }
//    @IBInspectable public var borderWidth: CGFloat = 0.0 {
//        didSet {
//            layer.borderWidth = borderWidth
//        }
//    }
//    
//    @IBInspectable public var cornerRadius: CGFloat = 5.0 {
//        didSet {
//            layer.cornerRadius = cornerRadius
//        }
//    }
//    
//    //set dropdown backgroud color
//    func setDropDown(rowBackgroundColor: String, selectedRowColor: String, rowTextColor: String, rowHeight: CGFloat) {
//        self.rowBackgroundColor = UIColor.init(hexString: rowBackgroundColor)
//        self.selectedRowColor = UIColor.init(hexString: selectedRowColor)
//        self.rowTextColor = UIColor.init(hexString: rowTextColor)
//        self.rowHeight = rowHeight
//        //set textfield textcolor
//        self.textColor = self.rowTextColor
//    }
//    
//    //Variables
//    fileprivate  var tableheightX: CGFloat = 100
////    fileprivate  var dataArray = [String]()
////    fileprivate  var dataArray = RLMArray<KeyValue> (objectClassName: KeyValue.className())
//    fileprivate  var dataArray = List<KeyValue> ()
////    public var optionArray = [String]() {
//    public var optionArray = List<KeyValue> () {
//        didSet{
//            self.dataArray = self.optionArray
//            print("========= dataArray size: \(self.dataArray.count)")
//            print("========= dataArray size: \(self.dataArray[0].Key) \(self.dataArray[0].Value)")
//        }
//    }
////    var searchText = String() {
////        didSet{
////            if searchText == "" {
////                self.dataArray = self.optionArray
////            }else{
//////                self.dataArray = optionArray.filter {
//////                    return $0.range(of: searchText, options: .caseInsensitive) != nil
//////                }
//////                self.dataArray = optionArray.filter {
//////                    return $0.Value!.range(of: searchText, options: .caseInsensitive) != nil
//////                }
////            }
////            reSizeTable()
////            selectedIndex = nil
////            self.table.reloadData()
////        }
////    }
//    @IBInspectable var arrowSize: CGFloat = 15 {
//        didSet{
//            let center =  arrow.superview!.center
//            arrow.frame = CGRect(x: center.x - arrowSize/2, y: center.y - arrowSize/2, width: arrowSize, height: arrowSize)
//        }
//    }
//    
//    // Init
//    public override init(frame: CGRect) {
//        super.init(frame: frame)
//        setupUI()
//        self.delegate = self
//    }
//    
//    public required init(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)!
//        setupUI()
//        self.delegate = self
//    }
//    
//    
//    //MARK: Closures
//    fileprivate var didSelectCompletion: (KeyValue, Int ) -> () = {selectedText, index  in }
//    fileprivate var TableWillAppearCompletion: () -> () = { }
//    fileprivate var TableDidAppearCompletion: () -> () = { }
//    fileprivate var TableWillDisappearCompletion: () -> () = { }
//    fileprivate var TableDidDisappearCompletion: () -> () = { }
//    
//    func setupUI () {
//        let size = self.frame.height
//        self.setPadding(left: 14, right: 0)
////        self.textColor = rowTextColor
//        let rightView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: size, height: size))
//        self.rightView = rightView
//        self.rightViewMode = .always
//        let arrowContainerView = UIView(frame: rightView.frame)
//        self.rightView?.addSubview(arrowContainerView)
//        let center = arrowContainerView.center
//        arrow = Arrow(origin: CGPoint(x: center.x - arrowSize/2,y: center.y - arrowSize/2),size: arrowSize)
//        arrowContainerView.addSubview(arrow)
//        addGesture()
//    }
//    fileprivate func addGesture (){
//        let gesture =  UITapGestureRecognizer(target: self, action:  #selector(touchAction))
//        if isSearchEnable{
//            self.rightView?.addGestureRecognizer(gesture)
//        }else{
//            self.addGestureRecognizer(gesture)
//        }
//        
//    }
//    
//    public func showList() {
//        print("========= 20 dataArray size: \(self.dataArray.count)")
//        print("========= dataArray size: \(self.dataArray[0].Key) \(self.dataArray[0].Value)")
//        TableWillAppearCompletion()
//        
//        if listHeight > rowHeight * CGFloat( dataArray.count) {
//            self.tableheightX = rowHeight * CGFloat(dataArray.count)
//        }else{
//            self.tableheightX = listHeight
//        }
//        table = UITableView(frame: CGRect(x: self.frame.minX,
//                                          y: self.frame.minY,
//                                          width: self.frame.width,
//                                          height: self.frame.height))
//        shadow = UIView(frame: CGRect(x: self.frame.minX,
//                                      y: self.frame.minY,
//                                      width: self.frame.width,
//                                      height: self.frame.height))
//        shadow.backgroundColor = .clear
//        
//        table.dataSource = self
//        table.delegate = self
//        table.alpha = 0
//        table.separatorStyle = .none
//        table.layer.cornerRadius = 3
//        table.backgroundColor = rowBackgroundColor
//        table.rowHeight = rowHeight
//        
//        self.superview?.insertSubview(shadow, belowSubview: self)
//        self.superview?.insertSubview(table, belowSubview: self)
//        self.isSelected = true
//        UIView.animate(withDuration: 0.9,
//                       delay: 0,
//                       usingSpringWithDamping: 0.4,
//                       initialSpringVelocity: 0.1,
//                       options: .curveEaseInOut,
//                       animations: { () -> Void in
//                            self.table.frame = CGRect(x: self.frame.minX,
//                                                      y: self.frame.maxY+2,
//                                                      width: self.frame.width,
//                                                      height: self.tableheightX)
//                            self.table.alpha = 1
//                            self.shadow.frame = self.table.frame
//                            self.shadow.dropShadow()
//                            self.arrow.position = .up
//                       },
//                       completion: { (finish) -> Void in
//        })
//    }
//    
//    public func hideList() {
//        print("========= 21 dataArray size: \(self.dataArray.count)")
//        print("========= dataArray size: \(self.dataArray[0].Key) \(self.dataArray[0].Value)")
//        TableWillDisappearCompletion()
//        UIView.animate(withDuration: 1.0,
//                       delay: 0.4,
//                       usingSpringWithDamping: 0.9,
//                       initialSpringVelocity: 0.1,
//                       options: .curveEaseInOut,
//                       animations: { () -> Void in
//                        self.table.frame = CGRect(x: self.frame.minX,
//                                                  y: self.frame.minY,
//                                                  width: self.frame.width,
//                                                  height: 0)
//                        self.shadow.alpha = 0
//                        self.shadow.frame = self.table.frame
//                        self.arrow.position = .down
//        },
//                       completion: { (didFinish) -> Void in
//                        
//                        self.shadow.removeFromSuperview()
//                        self.table.removeFromSuperview()
//                        self.isSelected = false
//                        self.TableDidDisappearCompletion()
//        })
//    }
//    
//    @objc public func touchAction() {
//        print("========= 22 dataArray size: \(self.dataArray.count)")
//        print("========= dataArray size: \(self.dataArray[0].Key) \(self.dataArray[0].Value)")
//        isSelected ?  hideList() : showList()
//    }
//    func reSizeTable() {
//        print("========= 5 dataArray size: \(self.dataArray.count)")
//        print("========= dataArray size: \(self.dataArray[0].Key) \(self.dataArray[0].Value)")
//        if listHeight > rowHeight * CGFloat( dataArray.count) {
//            self.tableheightX = rowHeight * CGFloat(dataArray.count)
//        }else{
//            self.tableheightX = listHeight
//        }
//        UIView.animate(withDuration: 0.2,
//                       delay: 0.1,
//                       usingSpringWithDamping: 0.9,
//                       initialSpringVelocity: 0.1,
//                       options: .curveEaseInOut,
//                       animations: { () -> Void in
//                        self.table.frame = CGRect(x: self.frame.minX,
//                                                  y: self.frame.maxY+5,
//                                                  width: self.frame.width,
//                                                  height: self.tableheightX)
//        },
//                       completion: { (didFinish) -> Void in
//                        self.shadow.layer.shadowPath = UIBezierPath(rect: self.table.bounds).cgPath
//        })
//    }
//    
//    //MARK: Actions Methods
//    public func didSelect(completion: @escaping (_ selectedText: KeyValue, _ index: Int) -> ()) {
//        if(self.dataArray.count > 0){
//        print("========= 30 dataArray size: \(self.dataArray[0].Value)")
//        print("========= dataArray size: \(self.dataArray[0].Key)")
//        }
//        didSelectCompletion = completion
//    }
//    
//    public func listWillAppear(completion: @escaping () -> ()) {
//        if(self.dataArray.count > 0){
//        print("========= 31 dataArray size: \(self.dataArray[0].Value)")
//        print("========= dataArray size: \(self.dataArray[0].Key)")
//        }
//        TableWillAppearCompletion = completion
//    }
//    
//    public func listDidAppear(completion: @escaping () -> ()) {
//        if(self.dataArray.count > 0){
//        print("========= 32 dataArray size: \(self.dataArray[0].Value)")
//        print("========= dataArray size: \(self.dataArray[0].Key)")
//        }
//        TableDidAppearCompletion = completion
//    }
//    
//    public func listWillDisappear(completion: @escaping () -> ()) {
//        if(self.dataArray.count > 0){
//        print("========= 33 dataArray size: \(self.dataArray[0].Value)")
//        print("========= dataArray size: \(self.dataArray[0].Key)")
//        }
//        TableWillDisappearCompletion = completion
//    }
//    
//    public func listDidDisappear(completion: @escaping () -> ()) {
//        if(self.dataArray.count > 0){
//        print("========= 34 dataArray size: \(self.dataArray[0].Value)")
//        print("========= dataArray size: \(self.dataArray[0].Key)")
//        }
//        TableDidDisappearCompletion = completion
//    }
//    
//}
//
////MARK: UITextFieldDelegate
//extension DropDown : UITextFieldDelegate {
//    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        superview?.endEditing(true)
//        return false
//    }
//    public func  textFieldDidBeginEditing(_ textField: UITextField) {
//        textField.text = ""
//        //self.selectedIndex = nil
//        print("========= 6 dataArray size: \(self.dataArray.count)")
//        print("========= dataArray size: \(self.dataArray[0].Key)")
//        self.dataArray = self.optionArray
//        touchAction()
//    }
//    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        return isSearchEnable
//    }
//    
//    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
////        if string != "" {
////            self.searchText = self.text! + string
////        }else{
////            let subText = self.text?.dropLast()
////            self.searchText = String(subText!)
////        }
////        if !isSelected {
////            showList()
////        }
//        return true;
//    }
//    
//}
/////MARK: UITableViewDataSource
//extension DropDown: UITableViewDataSource {
//    
//    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
////        print("========= 1 dataArray size: \(self.dataArray[0].Value)")
////        print("========= dataArray size: \(self.dataArray[0].Key)")
//        return dataArray.count
//    }
//    
//    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
////        print("========= 2 dataArray size: \(self.dataArray[0].Value)")
////        print("========= dataArray size: \(self.dataArray[0].Key)")
//        let cellIdentifier = "DropDownCell"
//        
//        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
//        
//        if cell == nil {
//            cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
//        }
////        print("========= 3 dataArray size: \(self.dataArray[0].Value)")
////        print("========= dataArray size: \(self.dataArray[0].Key)")
//        cell!.textLabel!.text = "\(dataArray[indexPath.row].Value!)"
//        cell!.textLabel!.textColor = rowTextColor
//        cell?.backgroundColor = indexPath.row == selectedIndex ? selectedRowColor : rowBackgroundColor
//        cell!.accessoryType = indexPath.row == selectedIndex ? .checkmark : .none
//        cell!.selectionStyle = .none
//        cell?.textLabel?.font = self.font
//        cell?.textLabel?.textAlignment = self.textAlignment
//        return cell!
//    }
//}
//
////MARK: UITableViewDelegate
//extension DropDown: UITableViewDelegate {
//    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print("========= 4 dataArray size: \(self.dataArray[0].Value)")
//        print("========= dataArray size: \(self.dataArray[0].Key)")
//        
//        print("========= optionArray size: \(self.optionArray.count)")
//        print("========= optionArray size: \(self.optionArray[0].Key) \(self.dataArray[0].Value)")
//        selectedIndex = (indexPath as NSIndexPath).row
//        let selectedText = self.dataArray[self.selectedIndex!]
//        tableView.cellForRow(at: indexPath)?.alpha = 0
//        UIView.animate(withDuration: 0.5,
//                       animations: { () -> Void in
//                        tableView.cellForRow(at: indexPath)?.alpha = 1.0
//                        tableView.cellForRow(at: indexPath)?.backgroundColor = self.selectedRowColor
//        } ,
//                       completion: { (didFinish) -> Void in
//                        self.text = "\(selectedText.Value!)"
//                        self.textColor = self.rowTextColor
//                        tableView.reloadData()
//        })
//        if hideOptionsWhenSelect {
//            touchAction()
//            self.endEditing(true)
//        }
//        
//        print("################ 1 \(selectedText.Value)")
//        print("################ 1 \(selectedText.Key)")
//    
////        print(optionArray.index(of: selectedText))
////        let selected = optionArray.index(of: selectedText)
////        didSelectCompletion(selectedText, selected! )
//        
//        
////        if let selected = optionArray.index(where: {$0.Value == selectedText.Value}) {
////            print("################ 2")
////            didSelectCompletion(selectedText, selected )
////        }
//        
//        if let selected = optionArray.index(where: {$0.Value == selectedText.Value}){
//            print("################ 2")
//            didSelectCompletion(selectedText, selected)
//        }
//        
//        for item in optionArray {
//            print("@@@@@@@ \(item.Key)  \(item.Value)")
//        }
//
//        
////        print("################ 3")
////        let kv1 = KeyValue()
////        kv1.Key = 1
////        kv1.Value = "aa"
////        let kv2 = KeyValue()
////        kv2.Key = 2
////        kv2.Value = "bb"
////
////        let ary = [kv1, kv2]
//////        let ary = List<KeyValue>()
//////        ary.append(kv1)
//////        ary.append(kv2)
////        let indexOfKv1 = ary.index{$0 === kv1}
////        print(indexOfKv1)
////
////        if let indexOfPerson1 = ary.index(where: {$0 == kv2}){
////            print("################ 4")
////            print(indexOfPerson1)
////        }
//        
//        
//        //        if let selected = optionArray.index(where: {$0 == selectedText}) {
//        //            if let id = optionIds?[selected] {
//        //                didSelectCompletion(selectedText, selected , id )
//        //            }else{
//        //                didSelectCompletion(selectedText, selected , 0)
//        //            }
//        //
//        //        }
//    }
//    
//    
//}
//
//
//
//
//
//
////MARK: Arrow
//enum Position {
//    case left
//    case down
//    case right
//    case up
//}
//
//class Arrow: UIView {
//    
//    var position: Position = .down {
//        didSet{
//            switch position {
//            case .left:
//                self.transform = CGAffineTransform(rotationAngle: -CGFloat.pi/2)
//                break
//                
//            case .down:
//                self.transform = CGAffineTransform(rotationAngle: CGFloat.pi*2)
//                break
//                
//            case .right:
//                self.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
//                break
//                
//            case .up:
//                self.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
//                break
//            }
//        }
//    }
//    
//    init(origin: CGPoint, size: CGFloat) {
//        super.init(frame: CGRect(x: origin.x, y: origin.y, width: size, height: size))
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
//    override func draw(_ rect: CGRect) {
//        
//        // Get size
//        let size = self.layer.frame.width
//        
//        // Create path
//        let bezierPath = UIBezierPath()
//        
//        // Draw points
//        let qSize = size/4
//        
//        bezierPath.move(to: CGPoint(x: 0, y: qSize))
//        bezierPath.addLine(to: CGPoint(x: size, y: qSize))
//        bezierPath.addLine(to: CGPoint(x: size/2, y: qSize*3))
//        bezierPath.addLine(to: CGPoint(x: 0, y: qSize))
//        bezierPath.close()
//        
//        // Mask to path
//        let shapeLayer = CAShapeLayer()
//        shapeLayer.fillColor = UIColor(hexString: "#726FAB").cgColor
//        shapeLayer.path = bezierPath.cgPath
//        if #available(iOS 12.0, *) {
//            self.layer.addSublayer (shapeLayer)
//        } else {
//            self.layer.mask = shapeLayer
//        }
//    }
//}
//
//extension UIView {
//    func dropShadow(scale: Bool = true) {
//        layer.masksToBounds = false
//        layer.shadowColor = UIColor.black.cgColor
//        layer.shadowOpacity = 0.5
//        layer.shadowOffset = CGSize(width: 1, height: 1)
//        layer.shadowRadius = 2
//        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
//        layer.shouldRasterize = true
//        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
//    }
//}
