//
//  PickDateTimeView.swift
//  Manage
//
//  Created by Rong Li on 12/27/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class PickDateTimeView: UIView {
    
    @IBOutlet var pickDateTimeView: UIView!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var timeTextField: UITextField!
    
    
    var calendarBtn  = UIButton(type: .custom)
    var clockBtn  = UIButton(type: .custom)
    
    var onPopupDatePickerBtnClicked: ((_ dateTimeBtn: UIButton) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
        
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "PickDateTimeView", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        addSubview(pickDateTimeView)
        pickDateTimeView.frame = bounds
        
        dateTextField.layer.masksToBounds = true
        dateTextField.layer.borderColor = UIColor.init(hexString: Constants.boarderColor).cgColor
        dateTextField.layer.borderWidth = 1
        dateTextField.setPadding(left: 8, right: 8)
        
        calendarBtn.frame = CGRect(x:0, y:0, width:30, height:30)
        calendarBtn.tag = 1
        calendarBtn.addTarget(self, action: #selector(self.popupDatePickerBtnClicked), for: .touchUpInside)
        calendarBtn.setImage(UIImage(named: "Calendar-icon.png"), for: .normal)
        calendarBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
        dateTextField.rightViewMode = .always
        dateTextField.rightView = calendarBtn
        
        timeTextField.layer.masksToBounds = true
        timeTextField.layer.borderColor = UIColor.init(hexString: Constants.boarderColor).cgColor
        timeTextField.layer.borderWidth = 1
        timeTextField.setPadding(left: 8, right: 8)
        
        //calendarBtn: var calendarBtn  = UIButton(type: .custom)
        clockBtn.frame = CGRect(x:0, y:0, width:30, height:30)
        clockBtn.tag = 2
        clockBtn.addTarget(self, action: #selector(self.popupTimePickerBtnClicked), for: .touchUpInside)
        clockBtn.setImage(UIImage(named: "Clock Icon"), for: .normal)
        clockBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
        timeTextField.rightViewMode = .always
        timeTextField.rightView = clockBtn
    }
    
    @objc func popupDatePickerBtnClicked(sender: UIButton) {
        onPopupDatePickerBtnClicked?(calendarBtn)
    }
    
    @objc func popupTimePickerBtnClicked(sender: UIButton) {
        onPopupDatePickerBtnClicked?(clockBtn)
    }

}
