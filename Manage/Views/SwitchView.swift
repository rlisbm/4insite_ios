//
//  SwitchView.swift
//  Manage
//
//  Created by Rong Li on 12/27/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class SwitchView: UIView {

    @IBOutlet var switchView: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var switchBtn: UISwitch!
    
    var onSwitchClicked: ((_ isOn: Bool) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
        
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "SwitchView", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        addSubview(switchView)
        switchView.frame = bounds
        
        switchBtn.backgroundColor = UIColor.white.withAlphaComponent(0.7)
        switchBtn.layer.cornerRadius = switchBtn.bounds.height / 2
    }

    
    @IBAction func switchClicked(_ sender: UISwitch) {
        onSwitchClicked?(sender.isOn)
    }
    
}
