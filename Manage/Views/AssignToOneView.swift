//
//  AssignToView.swift
//  Manage
//
//  Created by Rong Li on 12/6/18.
//  Copyright © 2018 Insite. All rights reserved.
//

import UIKit

class AssignToOneView: UIView {
    
    var onEditBtnClicked: (() -> Void)?

    @IBOutlet var assignToView: UIView!
    @IBOutlet weak var assignToLabel: UILabel!
    @IBOutlet weak var fullNameLable: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var seeAllBtn: RightImageBtn!
    
    @IBOutlet weak var fullNameLabelHeight: NSLayoutConstraint!
    
    
    @IBAction func editBtnClicked(_ sender: Any) {
        onEditBtnClicked?()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "AssignToOneView", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        addSubview(assignToView)
        assignToView.frame = bounds
        
        editBtn.underLine()
        seeAllBtn.underLine()
        seeAllBtn.isHidden = true
    }
    
}
