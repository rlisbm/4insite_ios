//
//  FiveDotsView.swift
//  Manage
//
//  Created by Rong Li on 1/4/19.
//  Copyright © 2019 Insite. All rights reserved.
//

import UIKit

class FiveDotsView: UIView {

    @IBOutlet var fiveDotsView: UIView!
    @IBOutlet weak var dot1: UIButton!
    @IBOutlet weak var dot2: UIButton!
    @IBOutlet weak var dot3: UIButton!
    @IBOutlet weak var dot4: UIButton!
    @IBOutlet weak var dot5: UIButton!
    
    var onDotClicked: ((_ dotBtn: UIButton) -> ())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "FiveDotsView", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        addSubview(fiveDotsView)
        fiveDotsView.frame = bounds
    }
    
    @IBAction func dot1Clicked(_ sender: UIButton) {
        onDotClicked?(sender)
    }
    
    @IBAction func dot2Clicked(_ sender: UIButton) {
        onDotClicked?(sender)
    }
    
    @IBAction func dot3Clicked(_ sender: UIButton) {
        onDotClicked?(sender)
    }
    
    @IBAction func dot4Clicked(_ sender: UIButton) {
        onDotClicked?(sender)
    }
    
    @IBAction func dot5Clicked(_ sender: UIButton) {
        onDotClicked?(sender)
    }

}
